//
//  HHSearchController.m
//  HHNew
//
//  Created by Liubin on 2020/4/23.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHSearchController.h"

@interface HHSearchController ()<UISearchBarDelegate>
@property (nonatomic, strong) UIImage * searchWhiteImage;
@property (nonatomic, strong) UIImage * searchGrayImage;

@end

@implementation HHSearchController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = ColorManage.searchControllerBgColor;
    self.searchBar.delegate = self;
    self.searchBar.tintColor = ColorManage.loginTextColor;
//    self.searchBar.barTintColor = ColorManage.background_color;
    self.searchBar.searchTextPositionAdjustment = UIOffsetMake(3, 0);
    [self.searchBar sizeToFit];
//    self.searchBar.layer.masksToBounds = YES;
//    self.searchBar.layer.cornerRadius = 5;

    //        UIOffset offset = {10.0,0};
    //        _searchController.searchBar.searchTextPositionAdjustment = offset;
//            self.searchBar.backgroundImage = self.searchGrayImage;
//            [self.searchBar setSearchFieldBackgroundImage:self.searchWhiteImage forState:UIControlStateNormal];
    
   self.searchBar.backgroundImage = [HHDefaultTools sharedInstance].searchBarGrayImage;
    [self.searchBar setSearchFieldBackgroundImage:[HHDefaultTools sharedInstance].searchBarTextFieldImage forState:UIControlStateNormal];
    self.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;//关闭提示
    self.searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;//关闭自动首字母大写
}


#pragma mark - UISearchBarDelegate
//- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
//    self.searchBar.backgroundImage = [HHDefaultTools sharedInstance].searchBarTextFieldImage;
//}
//
//- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
//    //改变SearchBar 背景颜色
//    self.searchBar.backgroundImage = [HHDefaultTools sharedInstance].searchBarGrayImage;
//}
//
//- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
//    //改变SearchBar 背景颜色
//    self.searchBar.backgroundImage = [HHDefaultTools sharedInstance].searchBarGrayImage;
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
