//
//  HHSearchController.h
//  HHNew
//
//  Created by Liubin on 2020/4/23.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


typedef void(^willDismissSearch)(void);
@interface HHSearchController : UISearchController
@property (nonatomic, copy) willDismissSearch willDismissSearch;
@end

NS_ASSUME_NONNULL_END
