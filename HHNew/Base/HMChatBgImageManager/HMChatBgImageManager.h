//
//  HMChatBgImageManager.h
//  huihe
//
//  Created by ZY on 2019/11/27.
//  Copyright © 2019 BQ. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMChatBgImageManager : NSObject
// 单例
+ (instancetype)sharedInstance;
- (UIImage *)defaultChatBgImage:(NSString *)imageName;
@end

NS_ASSUME_NONNULL_END
