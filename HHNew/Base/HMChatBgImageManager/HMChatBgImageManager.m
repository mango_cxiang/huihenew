//
//  HMChatBgImageManager.m
//  huihe
//
//  Created by ZY on 2019/11/27.
//  Copyright © 2019 BQ. All rights reserved.
//

#import "HMChatBgImageManager.h"

@implementation HMChatBgImageManager
// 单例
+ (instancetype)sharedInstance {
    
    static HMChatBgImageManager * _manager = nil;
    static dispatch_once_t onceT;
    dispatch_once(&onceT, ^{
        _manager = [[HMChatBgImageManager alloc] init];
    });
    return _manager;
}

- (UIImage *)defaultChatBgImage:(NSString *)imageName {
    UIImage *image = [UIImage imageNamed:imageName];
    return image;
}
@end
