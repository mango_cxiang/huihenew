//
//  HMJShowLoding.h
//  TodayOffice
//
//  Created by changle on 2017/1/3.
//  Copyright © 2017年 Six. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
@interface HMJShowLoding : NSObject

// 单例
+ (instancetype)sharedInstance;

- (MBProgressHUD *)showLoading;
- (MBProgressHUD *)showIssueLoading;
- (MBProgressHUD *)showLoadingWithTitle:(NSString *)title;
- (void)showLoadingTextWithTitle:(NSString *)title;
- (void)showText:(id)text;
- (void)showTextThreeSecond:(id)text;
- (void)hiddenLoading;

@end
