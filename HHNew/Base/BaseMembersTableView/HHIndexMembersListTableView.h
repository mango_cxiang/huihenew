//
//  HHIndexMembersListTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HHContactsModel;
NS_ASSUME_NONNULL_BEGIN
typedef void(^clickHHIndexMembersTableViewCell)(NSIndexPath *indexPath, HHContactsModel * _Nonnull model);

@interface HHIndexMembersListTableView : UITableView

@property (copy, nonatomic) clickHHIndexMembersTableViewCell clickHHIndexMembersTableViewCell;

@property (strong, nonatomic) UILabel *footerView;

@property (strong, nonatomic) NSMutableArray *dataListArray;
@end

NS_ASSUME_NONNULL_END
