//
//  HHLimitMoneyTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HHContactsModel;
NS_ASSUME_NONNULL_BEGIN
typedef void(^clickHHMembersListTableViewCell)(NSIndexPath *indexPath, HHContactsModel * _Nonnull model);
typedef void(^deleteHHMembersListTableViewCell)(NSIndexPath *indexPath, HHContactsModel * _Nonnull model);

@interface HHMembersListTableView : UITableView
@property (nonatomic, copy) clickHHMembersListTableViewCell clickHHMembersListTableViewCell;
@property (nonatomic, copy) deleteHHMembersListTableViewCell deleteHHMembersListTableViewCell;

@property (strong, nonatomic) NSMutableArray *dataArry;

@end

NS_ASSUME_NONNULL_END
