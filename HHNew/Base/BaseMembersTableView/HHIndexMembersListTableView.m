//
//  HHIndexMembersListTableView.m
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHIndexMembersListTableView.h"
#import "HMJGroupMembersTableViewCell.h"
#import "HHContactsModel.h"

@interface HHIndexMembersListTableView ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) NSMutableArray *indexPathArray;
@property (strong, nonatomic) NSMutableArray *addArray;
@property (assign, nonatomic) NSInteger addNum;

@end
@implementation HHIndexMembersListTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.delegate = self;
        self.dataSource = self;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
    }
    return self;
}

- (void)setDataListArray:(NSMutableArray *)dataListArray{
    _dataListArray = [DataHelper getContactListDataBy:dataListArray];
    _indexPathArray = [DataHelper getContactListSectionBy:_dataListArray];
    [self createUI:dataListArray.count];
    [self reloadData];
}

- (void)createUI:(NSInteger)count{
    self.footerView.frame = CGRectMake(0, 20, SCREEN_WIDTH,50);
    self.footerView.font = SystemFont(14);
    [self.footerView setTextAlignment:NSTextAlignmentCenter];
    self.footerView.textColor = ColorManage.grayTextColor_Deault;
    self.tableFooterView = self.footerView;
//    self.footerView.text = [NSString stringWithFormat:@"%zi位",count];
}

#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.indexPathArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataListArray[section] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellName = @"HMJGroupMembersTableViewCell";
    HMJGroupMembersTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if(!cell) {
        cell = LoadCell(@"HMJGroupMembersTableViewCell");
    }
    HHContactsModel *model = self.dataListArray[indexPath.section][indexPath.row];
    cell.contacts = model;
    cell.backgroundColor = ColorManage.darkBackgroundColor;
    cell.selectedBackgroundView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] init];
    header.backgroundColor = ColorManage.grayBackgroundColor;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 200, 30)];
    label.text = [_indexPathArray objectAtIndex:section];
    label.textColor = ColorManage.grayTextColor_Deault;
    label.font = [UIFont systemFontOfSize:14];
    [header addSubview:label];
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    HMJGroupMembersTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (self.clickHHIndexMembersTableViewCell) {
        self.clickHHIndexMembersTableViewCell(indexPath,cell.contacts);
    }
//    [self.addBtn setTitle:[NSString stringWithFormat:@"删除(%lu)",self.addNum] forState:UIControlStateNormal];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    [self.addBtn setTitle:[NSString stringWithFormat:@"删除(%lu)",self.addNum] forState:UIControlStateNormal];
}

- (NSMutableArray *)addArray{
    if (!_addArray) {
        _addArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _addArray;
}


- (NSMutableArray *)indexPathArray{
    if (!_indexPathArray) {
        _indexPathArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _indexPathArray;
}


- (UILabel *)footerView {
    if (!_footerView) {
        _footerView = [[UILabel alloc] init];
    }
    return _footerView;
}


@end
