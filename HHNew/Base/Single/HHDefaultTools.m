//
//  HHDefaultTools.m
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHDefaultTools.h"
#import "JJImagePicker.h"

@implementation HHDefaultTools
// 单例
+ (instancetype)sharedInstance {
    
    static HHDefaultTools * _manager = nil;
    static dispatch_once_t onceT;
    dispatch_once(&onceT, ^{
        _manager = [[HHDefaultTools alloc] init];
    });
    return _manager;
}


- (float) heightForString:(NSString *)value andWidth:(float)width{
    //获取当前文本的属性
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:value];
    NSRange range = NSMakeRange(0, attrStr.length);
    // 获取该段attributedString的属性字典
    NSDictionary *dic = [attrStr attributesAtIndex:0 effectiveRange:&range];
    // 计算文本的大小
    CGSize sizeToFit = [value boundingRectWithSize:CGSizeMake(width - 16.0, MAXFLOAT) // 用于计算文本绘制时占据的矩形块
                                               options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading // 文本绘制时的附加选项
                                            attributes:dic        // 文字的属性
                                               context:nil].size; // context上下文。包括一些信息，例如如何调整字间距以及缩放。该对象包含的信息将用于文本绘制。该参数可为nil
    return sizeToFit.height + 16.0;
}

//产生3个随机字母
- (NSString *)shuffledAlphabet {
    NSMutableArray * shuffledAlphabet = [NSMutableArray arrayWithArray:@[@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z"]];
    
    NSString *strTest = [[NSString alloc]init];
    for (int i=0; i<3; i++) {
        int x = arc4random() % 25;
        strTest = [NSString stringWithFormat:@"%@%@",strTest,shuffledAlphabet[x]];
    }
    return strTest;
}

- (UIView *)cellSelectedHighlightBgView{
    UIView * bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
    bgView.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:0.3];
    return bgView;
}


- (void)loadImageView:(UIImageView *)imgView imgUrl:(NSString *)imgUrl{
    if([imgUrl hasPrefix:@"http"]){
        [imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:nil options:SDWebImageRefreshCached];
    }else{
        imgView.image = [UIImage imageNamed:imgUrl];
    }
}

- (NSMutableAttributedString *)updateWithKeywordMode:(NSString *)keywordMode textColor:(UIColor *)textColor tintStr:(NSString *)hintStr tintColor:(UIColor *)tintColor{
    if (keywordMode) {
        NSString *string = keywordMode;
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.f],NSForegroundColorAttributeName:textColor}];
        
        if (hintStr) {
            NSRange range = [string.lowercaseString rangeOfString:hintStr.lowercaseString];
            if (range.location != NSNotFound) {
                [attString setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.f],NSForegroundColorAttributeName:tintColor} range:range];
            }
        }
        return attString;
    }
    return [[NSMutableAttributedString alloc]init];
}

#pragma mark- 得到当前时间戳
- (NSString *)acquireTimestamp
{
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval a=[dat timeIntervalSince1970];
    NSString *timeString = [NSString stringWithFormat:@"%.0f", a];//转为字符型
    return timeString;
}

- (JJImagePicker *)getJJImagePicker{
    JJImagePicker *picker = [JJImagePicker sharedInstance];
    //自定义裁剪图片的ViewController
    picker.customCropViewController = ^TOCropViewController *(UIImage *image) {
        if (picker.type == JJImagePickerTypePhoto) {
            //使用默认
            return nil;
        }
        TOCropViewController  *cropController = [[TOCropViewController alloc] initWithImage:image];
        //选择框可以按比例来手动调节
        cropController.aspectRatioLockEnabled = YES;
    //                     cropController.resetAspectRatioEnabled = NO;
        //设置选择宽比例
        cropController.aspectRatioPreset = TOCropViewControllerAspectRatioPresetOriginal;
        //显示选择框比例的按钮
        cropController.aspectRatioPickerButtonHidden = YES;
        //显示选择按钮
        cropController.rotateButtonsHidden = YES;
        //设置选择框可以手动移动
        cropController.cropView.cropBoxResizeEnabled = YES;
        return cropController;
    };
    picker.albumText = @"照片";
    picker.cancelText = @"取消";
    picker.doneText = @"完成";
    picker.retakeText = @"重拍";
    picker.choosePhotoText = @"选择照片";
    picker.automaticText = @"自动";
    picker.closeText = @"关闭";
    picker.openText = @"打开";
    return picker;
}

- (UIImage *)searchBarGrayImage{
     UIImage *image = [UIImage imageWithColor:ColorManage.nav_color size:CGSizeMake(SCREEN_WIDTH - 14, 35)];
    return image;
}


- (UIImage *)searchBarTextFieldImage{
    UIImage *image = [UIImage imageNamed:@"search_field_bg"];
    return image;
}
@end
