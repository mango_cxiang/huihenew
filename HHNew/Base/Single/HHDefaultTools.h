//
//  HHDefaultTools.h
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <Foundation/Foundation.h>
@class JJImagePicker;
NS_ASSUME_NONNULL_BEGIN

@interface HHDefaultTools : NSObject
// 单例
+ (instancetype)sharedInstance;

//获取String内容高度
- (float) heightForString:(NSString *)value andWidth:(float)width;

//产生随机字母和数字
- (NSString *)shuffledAlphabet;

//设置cell的高亮背景
- (UIView *)cellSelectedHighlightBgView;

//加载图片
- (void)loadImageView:(UIImageView *)imgView imgUrl:(NSString *)imgUrl;

//设置字符串中的部分字符
- (NSMutableAttributedString *)updateWithKeywordMode:(NSString *)keywordMode textColor:(UIColor *)textColor tintStr:(NSString *)hintStr tintColor:(UIColor *)tintColor;

/**
 获取当前时间戳

 @return 时间戳
 */
- (NSString *)acquireTimestamp;


/// 获取JJImagePicker
- (JJImagePicker *)getJJImagePicker;


/// SearchBar背景色图片
- (UIImage *)searchBarGrayImage;


/// SearchBar TextField背景色图片
- (UIImage *)searchBarTextFieldImage;

@end

NS_ASSUME_NONNULL_END
