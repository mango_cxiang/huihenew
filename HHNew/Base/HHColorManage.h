//
//  HHColorManage.h
//  HHNew
//
//  Created by Zxs on 2020/4/21.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HHColorManage : NSObject

//文本颜色
@property(nonatomic,strong)UIColor * text_color;
//背景颜色
@property(nonatomic,strong)UIColor * background_color;
//导航背景颜色
@property(nonatomic,strong)UIColor * nav_color;
//聊天InputToolBar背景颜色
@property(nonatomic,strong)UIColor * inputBar_color;
//聊天时间背景颜色
@property(nonatomic,strong)UIColor * time_color;
//灰色背景
@property(nonatomic,strong)UIColor * grayBackgroundColor;
//黑白
@property(nonatomic,strong)UIColor * whiteBackgroundColor;

//登录文本颜色
@property(nonatomic,strong)UIColor * loginTextColor;
//按钮不可点击时的颜色
@property(nonatomic,strong)UIColor * loginBtnColorDefault;
//按钮可点击时的颜色
@property(nonatomic,strong)UIColor * greenTextColor;
//通讯录联系人总数字体颜色
@property(nonatomic,strong)UIColor * grayTextColor_Deault;
//群详情用户名颜色
@property(nonatomic,strong)UIColor * black_grayTextColor_Deault;
//通讯录索引字体颜色
@property(nonatomic,strong)UIColor * addressHeaderViewTextColor;
//下划线颜色
@property(nonatomic,strong)UIColor * lineViewColor;
//背景色
@property(nonatomic,strong)UIColor * darkBackgroundColor;
@property(nonatomic,strong)UIColor * blackBackgroundColor;

@property(nonatomic,strong)UIColor * grayBlackTextColor;
@property(nonatomic,strong)UIColor * grayBgColor_SecondLevel;
@property(nonatomic,strong)UIColor * searchBar_color;
@property(nonatomic,strong)UIColor * searchControllerBgColor;



+(HHColorManage *)sharedHHColorManage;


@end

NS_ASSUME_NONNULL_END
