//
//  BaseViewController.m
//  HHNew
//
//  Created by 储翔 on 2020/4/20.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)setLeftBarButtonItem:(NSString *)title
{
    UIButton * leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, title.length*17+10, 44)];
    [leftBtn setTitle:title forState:UIControlStateNormal];
    [leftBtn setTitleColor:ColorManage.text_color forState:UIControlStateNormal];
    leftBtn.titleLabel.font = Font(17);
    [leftBtn addTarget:self action:@selector(leftBarButtonItemAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
}

- (void)setRightBarButtonItemWithImageArray:(NSArray *)imageArray BeginTag:(NSInteger)beginTag
{
    NSMutableArray * itemArray = [NSMutableArray new];
    for (NSString * imageName in imageArray) {
        UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [rightBtn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        rightBtn.tag = beginTag;
        [rightBtn addTarget:self action:@selector(rightBarButtonItemAction:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem * rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
        [itemArray addObject:rightItem];
        beginTag+=1;
    }
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithArray:itemArray];
}

- (void)setLeftBarButtonItem:(NSString *)title color:(UIColor *)color
{
    UIButton * leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    [leftBtn setTitle:title forState:UIControlStateNormal];
    leftBtn.titleLabel.font = SystemFont(17);
    [leftBtn setTitleColor:color forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBarButtonItemAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = button;
}

- (void)setRightBarButtonItem:(NSString *)title color:(UIColor *)color
{
    UIButton * rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    [rightBtn setTitle:title forState:UIControlStateNormal];
    rightBtn.titleLabel.font = SystemFont(17);
    [rightBtn setTitleColor:color forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightBarButtonItemAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightButton;
}

- (void)setupRightBarButtonItem:(UIButton *)rightBtn {
    UIBarButtonItem * rightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightBtnItem;
}

- (void)setupLeftBarButtonItem:(UIButton *)leftButton {
    UIBarButtonItem * leftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    self.navigationItem.leftBarButtonItem = leftBtnItem;
}

-(void)leftBarButtonItemAction:(id)sender
{
    
}
-(void)rightBarButtonItemAction:(id)sender
{
    
}

@end
