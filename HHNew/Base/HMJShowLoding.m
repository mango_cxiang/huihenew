//
//  HMJShowLoding.m
//  TodayOffice
//
//  Created by changle on 2017/1/3.
//  Copyright © 2017年 Six. All rights reserved.
//

#import "HMJShowLoding.h"

@implementation HMJShowLoding

+ (instancetype)sharedInstance
{
    static HMJShowLoding * _loading = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _loading = Init(HMJShowLoding);
    });
    return _loading;
}

#pragma mark - show alert
- (MBProgressHUD *)showLoading
{
    return [self showLoadingWithTitle:@"加载中..."];
}

- (MBProgressHUD *)showIssueLoading
{
    return [self showLoadingWithTitle:@"发布中..."];
}

- (MBProgressHUD *)showLoginLoading
{
    return [self showLoadingWithTitle:@"登录中..."];
}

- (MBProgressHUD *)showLoadingWithTitle:(NSString *)title
{

    UIWindow *window = [UIApplication sharedApplication].keyWindow;

    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:window];
    [window addSubview:HUD];
    
    HUD.label.text = title;

    [HUD showAnimated:YES];
    return HUD;
}
- (void)showLoadingTextWithTitle:(NSString *)title
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:window];
        [window addSubview:HUD];
        HUD.label.text = title;
        [HUD showAnimated:YES];
    });
}
- (void)hiddenLoading
{
     dispatch_async(dispatch_get_main_queue(), ^{
         UIWindow *window = [UIApplication sharedApplication].keyWindow;
         for (UIView * vc in [window subviews]) {
             if ([[vc class] isSubclassOfClass:[MBProgressHUD class]]) {
                 [vc removeFromSuperview];
             }
         }
     });
//    UIWindow *window = [UIApplication sharedApplication].keyWindow;
//    for (UIView * vc in [window subviews]) {
//        if ([[vc class] isSubclassOfClass:[MBProgressHUD class]]) {
//            [vc removeFromSuperview];
//        }
//    }
}

- (void)showText:(id)text
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:window];
        [window addSubview:HUD];
        HUD.label.text = text;
        HUD.mode = MBProgressHUDModeText;
        [HUD showAnimated:YES];
        [HUD hideAnimated:YES afterDelay:2.0];

    });
}

- (void)showTextThreeSecond:(id)text
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:window];
        [window addSubview:HUD];
        
        HUD.label.text = text;
        HUD.mode = MBProgressHUDModeText;
        [HUD showAnimated:YES];
        [HUD hideAnimated:YES afterDelay:3.0];

    });
}
@end
