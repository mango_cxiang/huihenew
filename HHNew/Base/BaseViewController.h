//
//  BaseViewController.h
//  HHNew
//
//  Created by 储翔 on 2020/4/20.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController

- (void)setLeftBarButtonItem:(NSString *)title;

- (void)setRightBarButtonItemWithImageArray:(NSArray *)imageArray BeginTag:(NSInteger)beginTag;

- (void)setLeftBarButtonItem:(NSString *)title color:(UIColor *)color;

- (void)setRightBarButtonItem:(NSString *)title color:(UIColor *)color;

- (void)setupRightBarButtonItem:(UIButton *)rightBtn;
- (void)setupLeftBarButtonItem:(UIButton *)leftButton;

@end

NS_ASSUME_NONNULL_END
