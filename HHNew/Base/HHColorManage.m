//
//  HHColorManage.m
//  HHNew
//
//  Created by Zxs on 2020/4/21.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHColorManage.h"

@interface HHColorManage ()

@end

@implementation HHColorManage

+(HHColorManage *)sharedHHColorManage {
    static HHColorManage * sharedHHColorManage = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedHHColorManage = [[self alloc] init];
    });
    return sharedHHColorManage;
}

-(UIColor *)text_color
{
    return [self colorWithLightColor:UIColor.blackColor DarkColor:UIColor.whiteColor];
}
-(UIColor *)background_color
{
    return [self colorWithLightColor:UIColor.whiteColor DarkColor:RGB_Color(34, 35, 36)];
}
-(UIColor *)nav_color
{
    return [self colorWithLightColor:RGB_Color(236, 237, 238) DarkColor:RGB_Color(23, 25, 26)];
}

-(UIColor *)searchBar_color
{
    return [self colorWithLightColor:RGB_Color(236, 237, 238) DarkColor:RGB_Color(43, 45, 46)];
}


-(UIColor *)grayBackgroundColor
{
    return [self colorWithLightColor:RGB_Color(236, 237, 238) DarkColor:RGB_Color(34, 35, 36)];
}

-(UIColor *)darkBackgroundColor
{
    return [self colorWithLightColor:[UIColor whiteColor] DarkColor:RGB_Color(43, 45, 46)];
}

-(UIColor *)searchControllerBgColor
{
    return [self colorWithLightColor:RGB_Color(236, 237, 238) DarkColor:UIColor.blackColor];
}


-(UIColor *)blackBackgroundColor
{
    return [self colorWithLightColor:UIColor.whiteColor DarkColor:UIColor.blackColor];
}

-(UIColor *)whiteBackgroundColor
{
    return [self colorWithLightColor:UIColor.blackColor DarkColor:UIColor.whiteColor];
}


-(UIColor *)loginTextColor
{
    return [self colorWithLightColor:RGB_Color(87, 107, 149) DarkColor:RGB_Color(87, 107, 149)];
}

-(UIColor *)loginBtnColorDefault
{
    return [self colorWithLightColor:RGB_Color(156, 230, 191) DarkColor:RGB_Color(156, 230, 191)];
}

-(UIColor *)greenTextColor
{
    return [self colorWithLightColor:RGB_Color(7, 193, 96) DarkColor:RGB_Color(7, 193, 96)];
}

-(UIColor *)grayTextColor_Deault
{
    return [self colorWithLightColor:RGB_Color(153, 153, 153) DarkColor:RGB_Color(153, 153, 153)];
}

-(UIColor *)grayBlackTextColor
{
    return [self colorWithLightColor:RGB_Color(178, 178, 178) DarkColor:UIColor.blackColor];
}

-(UIColor *)grayBgColor_SecondLevel
{
    return [self colorWithLightColor:RGB_Color(242, 242, 242) DarkColor:RGB_Color(60, 61, 62)];
}


-(UIColor *)black_grayTextColor_Deault
{
    return [self colorWithLightColor:RGB_Color(53, 53, 53) DarkColor:RGB_Color(153, 153, 153)];
}

-(UIColor *)addressHeaderViewTextColor
{
    return [self colorWithLightColor:RGB_Color(242, 242, 242) DarkColor:RGB_Color(23, 25, 26)];
}

-(UIColor *)lineViewColor
{
    return [self colorWithLightColor:RGB_Color(229, 229, 229) DarkColor:RGB_Color(60, 61, 62)];
}

-(UIColor *)inputBar_color
{
    return [self colorWithLightColor:RGB_Color(248, 248, 248) DarkColor:RGB_Color(29, 30, 31)];
}
-(UIColor *)time_color
{
//    return [self colorWithLightColor:RGB_Color(236, 237, 238) DarkColor:RGB_Color(242, 242, 242)];
    return [self colorWithLightColor:RGB_Color(236, 237, 238) DarkColor:RGB_Color(137, 137, 137)];
}





-(UIColor *)colorWithLightColor:(UIColor *)lightColor DarkColor:(UIColor *)darkColor
{
    if (@available(iOS 13.0, *)) {
       return [UIColor colorWithDynamicProvider:^UIColor * _Nonnull(UITraitCollection * _Nonnull traitCollection) {
            if (traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) {
                return darkColor;
            }else{
                return lightColor;
            }
        }];
    }
    return lightColor;
}

@end
