//
//  BaseTableViewCell.h
//  huihe
//
//  Created by 有慧科技 on 17/8/14.
//  Copyright © 2017年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewCell : UITableViewCell
@property (nonatomic,strong) UIView *bottomLine;
-(void)showBottomLine:(BOOL)isShow;
@end
