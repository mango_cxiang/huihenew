//
//  BaseTableViewCell.m
//  huihe
//
//  Created by 有慧科技 on 17/8/14.
//  Copyright © 2017年 Six. All rights reserved.
//

#import "BaseTableViewCell.h"

@implementation BaseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.bottomLine = [[UIView alloc]init];
    self.bottomLine.backgroundColor = ColorManage.lineViewColor;
    self.bottomLine.hidden = YES;
    [self.contentView addSubview:self.bottomLine];
}
- (void)layoutSubviews {
    [super layoutSubviews];
    self.bottomLine.frame = CGRectMake(15, self.contentView.height - 0.5, SCREEN_WIDTH-15, 0.3);
}
-(void)showBottomLine:(BOOL)isShow{
    self.bottomLine.hidden = !isShow;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
