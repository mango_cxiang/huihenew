//
//  HHEnumClass.h
//  HHNew
//
//  Created by 张 on 2020/4/23.
//  Copyright © 2020 储翔. All rights reserved.
//

#ifndef HHEnumClass_h
#define HHEnumClass_h

//会话类型
typedef NS_ENUM(NSInteger, TOSessionType)
{
    TOSessionTypeSingleChat = 1,        //单聊
    TOSessionTypeGroupChat,             //群聊
    TOSessionTypeSystemMessage,         //系统消息
    TOSessionTypeAddFriend = 101,       //添加好友
    TOSessionTypeGroupMessage = 201,    //群消息
    TOSessionTypeToAccount = 301,       //转账到账通知(aa收款，二维码收付款到账)
};

// 整合聊天消息类型、仅供计算Frame时使用
typedef NS_ENUM(NSInteger, TOChatFrameType)
{
    ChatFrameTypeText = 1000,    //文字、语音、视频或语音通话已取消、视频或语音通话通话时长共用
    ChatFrameTypeFile,           //文件
    ChatFrameTypeRedPacket,      //红包（未领取和已领取状态）
    ChatFrameTypePicture,        //图片、视频共用
    ChatFrameTypeBusinessCard,   //名片
    ChatFrameTypeLocation,       //位置
    ChatFrameTypeBQMM,           //表情云
    ChatFrameTypeShare,          //分享
    ChatFrameTypeSystemMessages  //系统消息（群通知）
};

// 消息类型
typedef NS_ENUM(NSInteger, TOMessageType)
{
    TOMessageTypeText = 2,                      //文字
    TOMessageTypePhoto,                         //图片
    TOMessageTypeDocument,                      //文件
    TOMessageTypePeopleJoinGroup = 8,           //有人加入群
    TOMessageTypePeopleExitGroup = 9,           //有人退出群
    TOMessageTypeVoice = 16,                    //语音
    TOMessageTypeRedPacket,                     //红包
    TOMessageTypeTransfer,                      //转账
    TOMessageTypeReceiveRedPacket,              //有人接收红包 领取红包
    TOMessageTypeConfirmTransfer,               //确认转帐收钱通知
    TOMessageTypeTransferReturn,                //转帐退回通知
    TOMessageTypeRedPacketReturn,               //红包过期退还通知
    TOMessageTypeRedPacketReceiveTime = 24,     //红包多少秒被领取完
    TOMessageTypeSystem,                        //系统提示
    TOMessageTypeNameCard = 28,                 //名片
    TOMessageTypePosition,                      //位置
    TOMessageTypeVideo,                         //小视频
    TOMessageTypeSnap = 31,                     //阅后即焚
    TOMessageTypeWithdraw = 32,                 //消息撤回
    TOMessageTypeRead,                          //消息已读
    TOMessageTypeBQMM,                          //表情云
    TOMessageTypeAt,                            //@
    TOMessageTypeFriendsCircleUpdate,           //有好友发布了朋友圈动态
    TOMessageTypeFriendsCircleNewComment,       //好友评论了你的动态，或者回复了你的评论
    TOMessageTypeFriendsCircleNewPraise,        //好友赞了你的朋友圈，或者赞了你参与你的朋友圈
    TOMessageTypeFriendsAccountNotify,          //转账到账通知(aa收款，二维码收付款到账)
    TOMessageTypeAACollection = 40,             //AA收款
    TOMessageTypePayRequest = 41,               //有人要求你付款,(如当别人扫了你的付款码时)
    TOMessageTypeSendMessageRefused = 66,       //被对方加入黑名单后，给对方发消息被拒绝接受
    TOMessageTypeGroupOwnerChanged = 71,      // 变更群主
    TOMessageTypeGroupManegerAdd = 72,       // 添加管理员
    TOMessageTypeGroupManegerCalcel = 73,     // 删除管理员
    TOMessageTypeGroupInviteOpen = 74,       // 开启群邀请确认
    TOMessageTypeGroupInviteClose = 75,      // 关闭群邀请确认
    TOMessageTypeRedPacketForbidden = 76,    // 禁止领红包
    TOMessageTypeRedPacketAllowed = 77,      // 允许领红包
    TOMessageTypeGroupProtectYes = 78,       // 群成员保护
    TOMessageTypeGroupProtectNO = 79,        //  群成员未保护
    TOMessageTypeGroupRedMoneyShow = 80,     // 群红包金额显示
    TOMessageTypeGroupRedMoneyHidden = 81,   //  群红包金额隐藏
    TOMessageTypeBeInvitedJoinGroup = 84,    // 被邀请加群的人收到的消息
    TOMessageTypeTalkForbidden = 87,         // 禁言
    TOMessageTypeTalkAllowed = 88,           // 解禁
    TOMessageTypeInviteJoinGroup = 89,       // 群主创建群给群主发送
    TOMessageTypeGroupMessageClearOn = 92,    //开始群消息清除
    TOMessageTypeGroupMessageClearOff = 93,    //关闭群消息清除
    TOMessageTypeGroupManegerNoSpeack = 99,     // 删除管理员
    TOMessageTypeGroupManegerSpeack = 100,     // 删除管理员
    TOMessageTypeVoiceRequest = 150,            //有人向你发起音频通话
    TOMessageTypeVideoRequest = 151,            //有人向你发起视频通话
    TOMessageTypeAgreeVoiceRequest = 152,       //同意对方音频通话
    TOMessageTypeAgreeVideoRequest = 153,       //同意对方音频通话
    TOMessageTypeRefuseVoiceRequest = 154,      //拒绝对方音频通话
    TOMessageTypeRefuseVideoRequest = 155,      //拒绝对方音频通话
    TOMessageTypeCancleVoiceVideo = 156,        //挂断对方音视频通话
    TOMessageTypeChangeVideoToVoiceRequest = 157,        //对方切换视频通话为音频
    TOMessageTypeChangeVoiceToVideoRequest = 158,        //对方请求切换音频通话为视频
    TOMessageTypeAgreeChangeVoiceToVideoRequest = 159,   //对方同意切换音频通话为视频
    TOMessageTypeRefuseChangeVoiceToVideoRequest = 160,  //对方拒绝切换音频通话为视频
    TOMessageTypeAbnormalCancleVoiceVideo = 161,         //对方异常退出
    TOMessageTypeAbnormalMessageNotify = 162,            //音视频结束通话消息类型
    TOMessageTypeMultiVoiceRequest = 163,                //有人向你发起多人音频通话
    TOMessageTypeMultiVideoRequest = 164,                //有人向你发起多人视频通话
    TOMessageTypeCancleMultiVideoRequest = 165,          //提起人结束多人视频通话
    TOMessageTypeEndMultiVideoRequest = 166,             //最终结束多人视频通话 多人通话销毁
    TOMessageTypeCreateMultiVideoRequest = 167,          //有人创建了多人视频通话
    TOMessageTypeSomeBodyJoinOrExitMultiVideoRequest = 168,        //有人进入或者退出多人视频通话
    TOMessageTypeShutDownSharePosition = 313,        //关闭位置共享
    TOMessageTypeAddNewFriend = 314,                  //同意加好友
    TOMessageTypeNearbyInviteOther=401,                 //身边邀请（显示对方cell）
    TOMessageTypeNearbyDeleteORJoin = 402,          //删除某人、加入群
    TOMessageTypeNearbyInviteMe=406,                 //身边邀请（显示自己cell）
    TOMessageTypeWebTransfor = 501,                   //H5分享
    //TOMessageTypeWebJumpToSponseDetail=504,            //h5分享跳赞助详情
    TOMessageTypeGroupMessageRedDelaly = 700,   //开启红包延迟
    TOMessageTypeGroupMessageRedDelalyOff = 701,    //关闭红包延迟
    TOMessageTypeNearbyQuit=998,                 //退出身边群
    TOMessageTypeNearbyJoin=999,                 //加入身边群
};

// 此枚举: 弹出哪个视图的一个状态值
typedef NS_ENUM(NSInteger , TOShowViewState)
{
    TOShowViewStateVoice = 0,    //录音
    TOShowViewStateKeyBoard,     //键盘
    TOShowViewStateEmojiFace,    //表情
    TOShowViewStateChatTool      //其他
};

#endif /* HHEnumClass_h */
