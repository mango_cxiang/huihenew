//
//  GrandConfig.h
//  XXDNew
//
//  Created by 储翔 on 2016/10/28.
//  Copyright © 2016年 HuiHe. All rights reserved.
//


/**
 一些 常用的宏命令 归档
 */


#ifndef GrandConfig_h
#define GrandConfig_h

/**
 *  字体  7.0版本  屏幕尺寸   机型
 */
//#define Font(measure) [UIFont fontWithName:@"FZLTXHK--GBK1-0" size:measure]
#define Font(measure) [UIFont fontWithName:@"PingFangSC-Regular" size:measure]
#define FontThin(measure) [UIFont systemFontOfSize:measure weight:UIFontWeightThin]
//#define FontBlod(measure) [UIFont fontWithName:@"FZLTHK--GBK1-0" size:measure]
#define FontBlod(measure) [UIFont fontWithName:@"PingFangSC-Regular" size:measure]
#define kAnimationDuration 0.3f

#define IOS7_OR_LATER        [[UIDevice currentDevice].systemVersion floatValue] >= 7.0f
#define IOS8_OR_LATER        [[UIDevice currentDevice].systemVersion floatValue] >= 8.0f
#define IOS9_OR_LATER        [[UIDevice currentDevice].systemVersion floatValue] >= 9.0f
#define IOS10_OR_LATER       [[UIDevice currentDevice].systemVersion floatValue] >= 10.0f
#define IOS11_OR_LATER       @available(iOS 11.0, *)//[[UIDevice currentDevice].systemVersion floatValue] >= 11.0f

#define SCREEN_WIDTH         [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT        [UIScreen mainScreen].bounds.size.height
#define SCREEN_BOUNDS        [UIScreen mainScreen].bounds
#define SELF_WIDTH self.frame.size.width
#define SELF_HEIGHT self.frame.size.height
//由角度转换弧度
#define kDegreesToRadian(x)      (M_PI * (x) / 180.0)
//由弧度转换角度
#define kRadianToDegrees(radian) (radian * 180.0) / (M_PI)

#define kNavBarHeighAndStatusBarHeight (44 + [UIApplication sharedApplication].statusBarFrame.size.height)
#define kStatusBarHeight [UIApplication sharedApplication].statusBarFrame.size.height

# define kAutoAdjustsScrollViewInsetsNo(vc, sc) \
if (IOS11_OR_LATER) {\
if ([sc respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {\
sc.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;\
}\
} else {\
vc.automaticallyAdjustsScrollViewInsets = NO;\
}

#define kFitScrollingContentOffsetAnimation \
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {\
if (scrollView.contentOffset.y != 0) {\
[UIView animateWithDuration:0.35 animations:^{\
scrollView.contentOffset = CGPointZero;\
}];\
}\
}

#define kNormalTableViewReuseHeaderViewWithBackgroundColor(color)\
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {\
    UITableViewHeaderFooterView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"headerView"];\
    if (header == nil) {\
        header = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:@"headerView"];\
        header.contentView.backgroundColor = color;\
    }\
    return header;\
}

#define iPhone4Inch ([UIScreen mainScreen].bounds.size.height == 480.0)
#define iPhone5Inch ([UIScreen mainScreen].bounds.size.height == 568.0)
#define iPhone6Inch ([UIScreen mainScreen].bounds.size.height == 667.0)
#define iPhone6PlusInch  ([UIScreen mainScreen].bounds.size.height == 736.0)

#define iPhoneX ([UIScreen mainScreen].bounds.size.height == 812.f)

//Appdelegate   先取得应用对象在取得AppDelegate类型对象
#define AkrAPPDELEGATE (AppDelegate *)[[UIApplication sharedApplication] delegate]
//NSUserDefaults
#define AkrUSERDEFAULT     [NSUserDefaults standardUserDefaults]
//颜色rgb
#define AkrRGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

//类的初始化
#define Init(Class) [[Class alloc] init]
//底部非安全区域的高度 34
#define kBottomNOSafeArea_Height (iPhoneX ? 34.0 :0.0)
//底部tabbar高度
#define kTabBarHeight ([[UIApplication sharedApplication] statusBarFrame].size.height>20?83:49)
#define ColorManage [HHColorManage sharedHHColorManage]

//导航文字
#define kSetNavTitleAttribute(navTitle)\
self.navigationItem.title = NSLocalizedString(navTitle, nil);\
[self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"FZLTHK--GBK1-0" size:18.0],NSForegroundColorAttributeName:kBlackColor}]

/**
 *  alertView宏
 */
#define  Show_AlterView(responseObject)\
({\
UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"提示", nil)message:NSLocalizedString(responseObject, nil) delegate:self cancelButtonTitle:NSLocalizedString(@"确定", nil) otherButtonTitles:nil,nil];\
[alert show];\
})


/**
 *  单例
 */
#define Singleton_For_Interface(className) \
\
+ (className *)shared##className;


#define Singleton_For_Implemention(className) \
\
+ (className *)shared##className { \
static className *shared##className = nil; \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
shared##className = [[self alloc] init]; \
}); \
return shared##className; \
}

/**
 *  打印宏
 */
#ifdef DEBUG
#define AkrLog(format, ...) do {\
fprintf(stderr, ">------------------------------\n<%s : %d> %s\n",\
[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String],  \
__LINE__, __func__);                                                        \
(NSLog)((format), ##__VA_ARGS__);                                           \
fprintf(stderr, "-----------------------------------\n");\
} while (0)
#else
#define AkrLog(...)
#endif

/**
 *  打电话
 */
#define CALL_PHONE(PHONE) ([[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",PHONE]]])
/**
 *  打开URL
 */
#define OPEN_URL(urlString) ([[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]])


//自定义宏
#define  ViewHeight [[UIScreen mainScreen] bounds].size.height
#define  ViewWidth [[UIScreen mainScreen] bounds].size.width
#define themeColor [UIColor colorWithHexString:@"#3671cf"]
#define whiteBackColor [UIColor colorWithHexString:@"#ffffff"]
#define bordColor [UIColor colorWithHexString:@"#f0f7ff"]
#define blackFontColor [UIColor colorWithHexString:@"#4d4d4d"]
#define lightgrayFontColor [UIColor colorWithHexString:@"#cccccc"]
#define grayBackColor [UIColor colorWithHexString:@"#f5f5f5"]
#define grayFontColor [UIColor colorWithHexString:@"#b2b2b2"]
#define blueSelectColor [UIColor colorWithHexString:@"#428af4"]
#define yellowRemindColor [UIColor colorWithHexString:@"#ff9e36"]
#define redProfitColor [UIColor colorWithHexString:@"#fd4545"]
#define blueBtnFontColor [UIColor colorWithHexString:@"#3f9bff"]
#define grayPacketColor [UIColor colorWithHexString:@"4c4c4c"]
#define kBlackColor AkrRGBACOLOR(77.0,77.0,77.0,1.0)
#define blueSelectColor [UIColor colorWithHexString:@"#428af4"]

#define alphaNaviColor [whiteBackColor colorWithAlphaComponent:0.8]

#define grayTextColor [UIColor colorWithHexString:@"#cfd7e3"]
#define advanceSearchBtnColor [UIColor colorWithHexString:@"#e6e6e6"]  // 高级搜索里的button 主色

#define headerSeparatorColor [UIColor colorWithHexString:kHeaderViewSeparatorColor]
#define searchBarBackgroundColor [UIColor colorWithHexString:kSearchBarBackgroundColor]

#define RGBA_Color(r, g, b, a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:a]
#define RGB_Color(r, g, b) RGBA_Color((r), (g), (b), 1)

#define RGB_RedColor           RGB_Color(253, 69, 69)
#define RGB_QuitRedColor       RGB_Color(252, 126, 126)
#define RGB_UnclickRedColor    RGB_Color(252, 206, 206)
#define RGB_DrakRedColor       RGB_Color(243, 71, 97)
#define RGB_GreenColor         RGB_Color(130, 217, 68)
#define RGB_LightGrayColor     RGB_Color(179, 179, 179)
#define RGB_GrayColor          RGB_Color(128, 128, 128)
#define RGB_MidGrayColor       RGB_Color(100, 100, 100)
#define RGB_DarkGrayColor      RGB_Color(77, 77, 77)
#define RGB_BlueColor          RGB_Color(63, 155, 255)
#define RGB_BlueMaskColor      RGB_Color(79, 163, 255)
#define RGB_LightBlueColor     RGB_Color(178, 214, 255)
#define RGB_FillColor          RGB_Color(250, 252, 255)
#define RGB_OrangeColor        RGB_Color(255, 186, 85)
#define RGB_LineGrayColor      RGB_Color(230, 230, 230)
#define RGB_SystemAlertBgColor RGB_Color(249, 249, 249)
#define RGB_BlackColor         RGB_Color(26, 26, 26)
#define RGB_YellowColor         RGB_Color(255, 252, 242)

//导航栏
#define kNavBarColor [UIColor whiteColor]
#define kRealBlackColor [UIColor blackColor]
#define kBlackColor AkrRGBACOLOR(77.0,77.0,77.0,1.0)
#define kRedColor AkrRGBACOLOR(253.0,69.0,69.0,1.0)
//主题色
#define kThemeColor AkrRGBACOLOR(63.0,155.0,255.0,1.0)

#define k_GradientColorInView(ivar) if ([ivar isKindOfClass:[UIView class]]) {[ivar initWithcolorChangeSWithRect:ivar.bounds startColor:kAThirdLightBlue endColor:kAThirdHeavyBlue];}

///全局验证码长度
#define xxdMaxKeyLenth 4
///全局倒计时长度
#define smsCountTime 60
///全局圆角的数值
#define cornerRadis 3.0


#define RGB_GrayColor_Text              RGB_Color(153, 153, 153)
#define RGB_GreenColor_Text              RGB_Color(7, 193, 96)


#define RGB_BlackColor_NormalLevel            RGB_Color(51, 51, 51)
#define RGB_RedColor_NormalLevel              RGB_Color(255, 91, 91)
//十六进制颜色值转换
#define UIColorFromRGBA(rgbValue , a) [UIColor colorWithRed:((CGFloat)((rgbValue & 0xFF0000) >> 16)) / 255.0 green:((CGFloat)((rgbValue & 0xFF00) >> 8)) / 255.0 blue:((CGFloat)(rgbValue & 0xFF)) / 255.0 alpha:a]
#define UIColorFromRGB(rgbValue) UIColorFromRGBA(rgbValue , 1.0)

#define kiOSVersion [[UIDevice currentDevice] systemVersion].floatValue
//SearchController.SearchBar的高度
#define HHSearchControllerSearchBarHeight 56

/* navigation高度**/
#define SafeAreaTopHeight                           (SCREEN_HEIGHT >= 812.0 ? 88 : 64)
/* 屏幕底部安全区域**/
#define SafeAreaBottomHeight                        (SCREEN_HEIGHT >= 812.0 ? 34 : 0)
/* 状态栏高度**/
#define StatusBarHeight                             (SCREEN_HEIGHT >= 812.0 ? 44 : 20)
/* 状态栏高度差**/
#define StatusBarDifferenceHeight                   ((SCREEN_HEIGHT >= 812.0 || SCREEN_WIDTH >= 812.0)? 24 : 0)
/* tabBar高度**/
#define TabBarHeight                                (SCREEN_HEIGHT >= 812.0 ? 70 : 56)
#define kSemiboldsFont(a)                            [UIFont fontWithName:@"PingFangSC-Semibold" size: a]
#define kLightFont(a)                                [UIFont fontWithName:@"PingFangSC-Light" size: a]

#define kMediumFont(a)                               [UIFont fontWithName:@"PingFangSC-Medium" size: a]

#define kRegularFont(a)                              [UIFont fontWithName:@"PingFangSC-Regular" size: a]
//系统字体
#define SystemFont(x) [UIFont systemFontOfSize:x+[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SystemFontSet"]].floatValue]
#define SystemFontWeightMedium(x) [UIFont systemFontOfSize:x+[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SystemFontSet"]].floatValue weight:UIFontWeightMedium]
#define SystemFontWeightRegular(x) [UIFont systemFontOfSize:x+[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SystemFontSet"]].floatValue weight:UIFontWeightRegular]

//粗体
#define BoldFont(x) [UIFont boldSystemFontOfSize:x+[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SystemFontSet"]].floatValue]


//加载cell
#define LoadCell(cellClassStr) [[[NSBundle mainBundle] loadNibNamed:cellClassStr owner:self options:nil] lastObject]


#define WeakSelf(type)  __weak typeof(type) weak##type = type;
#define StrongSelf(type)  __strong typeof(type) strong##type = type;

#define WeakObj(o) autoreleasepool{} __weak typeof(o) weak##o = o;
#define StrongObj(o) autoreleasepool{} __strong typeof(o) o = weak##o;

//空判断相关
#define isEmptyStr(str) (!str||[str isKindOfClass:[NSNull class]]||[str isEqualToString:@""]) //判断是否空字符串
#define isEmptyArr(arr) (!arr||((NSArray *)arr).count==0) //判断是否空数组

#define PlaceholderImage [UIImage imageNamed:@"head_default"]

#define LoginAgreement  @"https://help.huihe.xyz/registeragreement.html"


/**
 *  字体  7.0版本  屏幕尺寸   机型
 */
//#define Font(measure) [UIFont fontWithName:@"FZLTXHK--GBK1-0" size:measure]
#define Font(measure) [UIFont fontWithName:@"PingFangSC-Regular" size:measure]
#define FontThin(measure) [UIFont systemFontOfSize:measure weight:UIFontWeightThin]
//#define FontBlod(measure) [UIFont fontWithName:@"FZLTHK--GBK1-0" size:measure]
#define FontBlod(measure) [UIFont fontWithName:@"PingFangSC-Regular" size:measure]
#define kAnimationDuration 0.3f

#define IOS7_OR_LATER        [[UIDevice currentDevice].systemVersion floatValue] >= 7.0f
#define IOS8_OR_LATER        [[UIDevice currentDevice].systemVersion floatValue] >= 8.0f
#define IOS9_OR_LATER        [[UIDevice currentDevice].systemVersion floatValue] >= 9.0f
#define IOS10_OR_LATER       [[UIDevice currentDevice].systemVersion floatValue] >= 10.0f
#define IOS11_OR_LATER       @available(iOS 11.0, *)//[[UIDevice currentDevice].systemVersion floatValue] >= 11.0f

#define SCREEN_WIDTH         [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT        [UIScreen mainScreen].bounds.size.height
#define SCREEN_BOUNDS        [UIScreen mainScreen].bounds
#define SELF_WIDTH self.frame.size.width
#define SELF_HEIGHT self.frame.size.height
//由角度转换弧度
#define kDegreesToRadian(x)      (M_PI * (x) / 180.0)
//由弧度转换角度
#define kRadianToDegrees(radian) (radian * 180.0) / (M_PI)

#define kNavBarHeighAndStatusBarHeight (44 + [UIApplication sharedApplication].statusBarFrame.size.height)
#define kStatusBarHeight [UIApplication sharedApplication].statusBarFrame.size.height

# define kAutoAdjustsScrollViewInsetsNo(vc, sc) \
if (IOS11_OR_LATER) {\
if ([sc respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {\
sc.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;\
}\
} else {\
vc.automaticallyAdjustsScrollViewInsets = NO;\
}

#define kFitScrollingContentOffsetAnimation \
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {\
if (scrollView.contentOffset.y != 0) {\
[UIView animateWithDuration:0.35 animations:^{\
scrollView.contentOffset = CGPointZero;\
}];\
}\
}

#define kNormalTableViewReuseHeaderViewWithBackgroundColor(color)\
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {\
    UITableViewHeaderFooterView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"headerView"];\
    if (header == nil) {\
        header = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:@"headerView"];\
        header.contentView.backgroundColor = color;\
    }\
    return header;\
}

#define iPhone4Inch ([UIScreen mainScreen].bounds.size.height == 480.0)
#define iPhone5Inch ([UIScreen mainScreen].bounds.size.height == 568.0)
#define iPhone6Inch ([UIScreen mainScreen].bounds.size.height == 667.0)
#define iPhone6PlusInch  ([UIScreen mainScreen].bounds.size.height == 736.0)

#define iPhoneX ([UIScreen mainScreen].bounds.size.height == 812.f)

//Appdelegate   先取得应用对象在取得AppDelegate类型对象
#define AkrAPPDELEGATE (AppDelegate *)[[UIApplication sharedApplication] delegate]
//NSUserDefaults
#define AkrUSERDEFAULT     [NSUserDefaults standardUserDefaults]
//颜色rgb
#define AkrRGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

//类的初始化
#define Init(Class) [[Class alloc] init]
//底部非安全区域的高度 34
#define kBottomNOSafeArea_Height (iPhoneX ? 34.0 :0.0)
//底部tabbar高度
#define kTabBarHeight ([[UIApplication sharedApplication] statusBarFrame].size.height>20?83:49)
#define ColorManage [HHColorManage sharedHHColorManage]

//导航文字
#define kSetNavTitleAttribute(navTitle)\
self.navigationItem.title = NSLocalizedString(navTitle, nil);\
[self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"FZLTHK--GBK1-0" size:18.0],NSForegroundColorAttributeName:kBlackColor}]
//imageNamed
#define ImageInit(imageName) [UIImage imageNamed:imageName]
/**
 *  alertView宏
 */
#define  Show_AlterView(responseObject)\
({\
UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"提示", nil)message:NSLocalizedString(responseObject, nil) delegate:self cancelButtonTitle:NSLocalizedString(@"确定", nil) otherButtonTitles:nil,nil];\
[alert show];\
})


/**
 *  单例
 */
#define Singleton_For_Interface(className) \
\
+ (className *)shared##className;


#define Singleton_For_Implemention(className) \
\
+ (className *)shared##className { \
static className *shared##className = nil; \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
shared##className = [[self alloc] init]; \
}); \
return shared##className; \
}

/**
 *  打印宏
 */
#ifdef DEBUG
#define AkrLog(format, ...) do {\
fprintf(stderr, ">------------------------------\n<%s : %d> %s\n",\
[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String],  \
__LINE__, __func__);                                                        \
(NSLog)((format), ##__VA_ARGS__);                                           \
fprintf(stderr, "-----------------------------------\n");\
} while (0)
#else
#define AkrLog(...)
#endif

/**
 *  打电话
 */
#define CALL_PHONE(PHONE) ([[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",PHONE]]])
/**
 *  打开URL
 */
#define OPEN_URL(urlString) ([[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]])


//自定义宏
#define  ViewHeight [[UIScreen mainScreen] bounds].size.height
#define  ViewWidth [[UIScreen mainScreen] bounds].size.width
#define themeColor [UIColor colorWithHexString:@"#3671cf"]
#define whiteBackColor [UIColor colorWithHexString:@"#ffffff"]
#define bordColor [UIColor colorWithHexString:@"#f0f7ff"]
#define blackFontColor [UIColor colorWithHexString:@"#4d4d4d"]
#define lightgrayFontColor [UIColor colorWithHexString:@"#cccccc"]
#define grayBackColor [UIColor colorWithHexString:@"#f5f5f5"]
#define grayFontColor [UIColor colorWithHexString:@"#b2b2b2"]
#define blueSelectColor [UIColor colorWithHexString:@"#428af4"]
#define yellowRemindColor [UIColor colorWithHexString:@"#ff9e36"]
#define redProfitColor [UIColor colorWithHexString:@"#fd4545"]
#define blueBtnFontColor [UIColor colorWithHexString:@"#3f9bff"]
#define grayPacketColor [UIColor colorWithHexString:@"4c4c4c"]
#define kBlackColor AkrRGBACOLOR(77.0,77.0,77.0,1.0)
#define blueSelectColor [UIColor colorWithHexString:@"#428af4"]

#define alphaNaviColor [whiteBackColor colorWithAlphaComponent:0.8]

#define grayTextColor [UIColor colorWithHexString:@"#cfd7e3"]
#define advanceSearchBtnColor [UIColor colorWithHexString:@"#e6e6e6"]  // 高级搜索里的button 主色

#define headerSeparatorColor [UIColor colorWithHexString:kHeaderViewSeparatorColor]
#define searchBarBackgroundColor [UIColor colorWithHexString:kSearchBarBackgroundColor]

#define RGBA_Color(r, g, b, a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:a]
#define RGB_Color(r, g, b) RGBA_Color((r), (g), (b), 1)

#define RGB_RedColor           RGB_Color(253, 69, 69)
#define RGB_QuitRedColor       RGB_Color(252, 126, 126)
#define RGB_UnclickRedColor    RGB_Color(252, 206, 206)
#define RGB_DrakRedColor       RGB_Color(243, 71, 97)
#define RGB_GreenColor         RGB_Color(130, 217, 68)
#define RGB_LightGrayColor     RGB_Color(179, 179, 179)
#define RGB_GrayColor          RGB_Color(128, 128, 128)
#define RGB_MidGrayColor       RGB_Color(100, 100, 100)
#define RGB_DarkGrayColor      RGB_Color(77, 77, 77)
#define RGB_BlueColor          RGB_Color(63, 155, 255)
#define RGB_BlueMaskColor      RGB_Color(79, 163, 255)
#define RGB_LightBlueColor     RGB_Color(178, 214, 255)
#define RGB_FillColor          RGB_Color(250, 252, 255)
#define RGB_OrangeColor        RGB_Color(255, 186, 85)
#define RGB_LineGrayColor      RGB_Color(230, 230, 230)
#define RGB_SystemAlertBgColor RGB_Color(249, 249, 249)
#define RGB_BlackColor         RGB_Color(26, 26, 26)
#define RGB_YellowColor         RGB_Color(255, 252, 242)

//导航栏
#define kNavBarColor [UIColor whiteColor]
#define kRealBlackColor [UIColor blackColor]
#define kBlackColor AkrRGBACOLOR(77.0,77.0,77.0,1.0)
#define kRedColor AkrRGBACOLOR(253.0,69.0,69.0,1.0)
//主题色
#define kThemeColor AkrRGBACOLOR(63.0,155.0,255.0,1.0)

#define k_GradientColorInView(ivar) if ([ivar isKindOfClass:[UIView class]]) {[ivar initWithcolorChangeSWithRect:ivar.bounds startColor:kAThirdLightBlue endColor:kAThirdHeavyBlue];}

///全局验证码长度
#define xxdMaxKeyLenth 4
///全局倒计时长度
#define smsCountTime 60
///全局圆角的数值
#define cornerRadis 3.0


#define RGB_GrayColor_Text              RGB_Color(153, 153, 153)
#define RGB_GreenColor_Text              RGB_Color(7, 193, 96)


#define RGB_BlackColor_NormalLevel            RGB_Color(51, 51, 51)
#define RGB_RedColor_NormalLevel              RGB_Color(255, 91, 91)
//十六进制颜色值转换
#define UIColorFromRGBA(rgbValue , a) [UIColor colorWithRed:((CGFloat)((rgbValue & 0xFF0000) >> 16)) / 255.0 green:((CGFloat)((rgbValue & 0xFF00) >> 8)) / 255.0 blue:((CGFloat)(rgbValue & 0xFF)) / 255.0 alpha:a]
#define UIColorFromRGB(rgbValue) UIColorFromRGBA(rgbValue , 1.0)

#define kiOSVersion [[UIDevice currentDevice] systemVersion].floatValue
/* navigation高度**/
#define SafeAreaTopHeight                           (SCREEN_HEIGHT >= 812.0 ? 88 : 64)
/* 屏幕底部安全区域**/
#define SafeAreaBottomHeight                        (SCREEN_HEIGHT >= 812.0 ? 34 : 0)
/* 状态栏高度**/
#define StatusBarHeight                             (SCREEN_HEIGHT >= 812.0 ? 44 : 20)
/* 状态栏高度差**/
#define StatusBarDifferenceHeight                   ((SCREEN_HEIGHT >= 812.0 || SCREEN_WIDTH >= 812.0)? 24 : 0)
/* tabBar高度**/
#define TabBarHeight                                (SCREEN_HEIGHT >= 812.0 ? 70 : 56)
#define kSemiboldsFont(a)               [UIFont fontWithName:@"PingFangSC-Semibold" size: a]
#define kLightFont(a)               [UIFont fontWithName:@"PingFangSC-Light" size: a]

#define kMediumFont(a)               [UIFont fontWithName:@"PingFangSC-Medium" size: a]

#define kRegularFont(a)               [UIFont fontWithName:@"PingFangSC-Regular" size: a]
//系统字体
#define SystemFont(x) [UIFont systemFontOfSize:x+[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SystemFontSet"]].floatValue]
#define SystemFontWeightMedium(x) [UIFont systemFontOfSize:x+[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SystemFontSet"]].floatValue weight:UIFontWeightMedium]
#define SystemFontWeightRegular(x) [UIFont systemFontOfSize:x+[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SystemFontSet"]].floatValue weight:UIFontWeightRegular]

//粗体
#define BoldFont(x) [UIFont boldSystemFontOfSize:x+[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SystemFontSet"]].floatValue]


//加载cell
#define LoadCell(cellClassStr) [[[NSBundle mainBundle] loadNibNamed:cellClassStr owner:self options:nil] lastObject]


#define WeakSelf(type)  __weak typeof(type) weak##type = type;
#define StrongSelf(type)  __strong typeof(type) strong##type = type;

#define WeakObj(o) autoreleasepool{} __weak typeof(o) weak##o = o;
#define StrongObj(o) autoreleasepool{} __strong typeof(o) o = weak##o;

//空字符串处理
#define SafeString(str) [NSString notNullStringWithStr:str]

//空判断相关
#define isEmptyStr(str) (!str||[str isKindOfClass:[NSNull class]]||[str isEqualToString:@""]) //判断是否空字符串
#define isEmptyArr(arr) (!arr||((NSArray *)arr).count==0) //判断是否空数组

#define PlaceholderImage [UIImage imageNamed:@"head_default"]
//头像图片占位符
#define HeadPlaceholderImage [UIImage imageNamed:@"moren_touxiang"]
#define LoginAgreement  @"https://help.huihe.xyz/registeragreement.html"
#define IsBlankWithStr(Str) [NSString isBlankWithStr:Str]
//沙盒目录下Documents文件的路径
#define DocumentsPath [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
//默认sessionId 拼的字符串
#define SESSIONID_MIDDLE @"00000"

#pragma mark --- 通知相关
//停止播放语音通知
#define StopPlayVoiceNotification @"StopPlayVoiceNotification"
//拍摄视频下载成功
#define NOTIFICATION_DELETEFRIEND_VIDEOSUCCESS @"NOTIFICATION_DELETEFRIEND_VIDEOSUCCESS"


#define NIMKit_Dispatch_Async_Main(block)\
if ([NSThread isMainThread]) {\
block();\
} else {\
dispatch_async(dispatch_get_main_queue(), block);\
}

#define PROPORTION fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.width)/375
#define adaptation(value)      (value)*PROPORTION
#define WEAKSELF typeof(self) __weak weakSelf = self;
#define RGB(r, g, b)  [UIColor colorWithRed:r / 255.0f green:g / 255.0f blue:b / 255.0f alpha:1.0f]
#define ShowToast(str) [[HMJShowLoding sharedInstance]showText:str];

#endif /* GrandConfig_h */
