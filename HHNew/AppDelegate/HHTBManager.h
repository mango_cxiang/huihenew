//
//  HHTBManager.h
//  HHNew
//
//  Created by 储翔 on 2020/4/20.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RDVTabBarController;
NS_ASSUME_NONNULL_BEGIN

@interface HHTBManager : NSObject

Singleton_For_Interface(HHTBManager);

@property (nonatomic,strong) RDVTabBarController *tabBarCtr;

- (void)configViewCtrsinWindow:(UIWindow *)window;

@end

NS_ASSUME_NONNULL_END
