//
//  HHTBManager.m
//  HHNew
//
//  Created by 储翔 on 2020/4/20.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHTBManager.h"
#import "RDVTabBarItem.h"
#import "HHNavController.h"
#import "RDVTabBarController.h"
@interface HHTBManager ()<RDVTabBarControllerDelegate>

@end
@implementation HHTBManager
Singleton_For_Implemention(HHTBManager)


//配置ctr
- (void)configViewCtrsinWindow:(UIWindow *)window
{
    
    NSArray *ctrArray, *titleArray;

    ctrArray = @[@"MessageViewController",@"InnovationViewController",@"ActivityViewController",@"MineViewController"];
    
    titleArray = @[NSLocalizedString(@"聊天", nil),NSLocalizedString(@"众创", nil),NSLocalizedString(@"活动", nil),NSLocalizedString(@"我", nil)];
    
    
    NSMutableArray *navArray = [NSMutableArray array];
    for (int i = 0; i < ctrArray.count; i ++) {
        
        Class class = NSClassFromString(ctrArray[i]);
        
        UIViewController *ctr;
        ctr = [[class alloc] init];
        ctr.title = titleArray[i];
        HHNavController *navCtr = [[HHNavController alloc] initWithRootViewController:ctr];
        
        
        [navArray addObject:navCtr];
    }
    
    _tabBarCtr = [[RDVTabBarController alloc] init];
    _tabBarCtr.viewControllers = navArray;
    [_tabBarCtr.tabBar setHeight:(kTabBarHeight)];
    _tabBarCtr.delegate = self;
    [self customerTabBarForCtr:_tabBarCtr];
    
    //加阴影--任海丽编辑
    [_tabBarCtr tabBar].layer.shadowColor = kBlackColor.CGColor;//shadowColor阴影颜色
    [_tabBarCtr tabBar].layer.shadowOffset = CGSizeMake(4,4);//shadowOffset阴影偏移,x向右偏移4，y向下偏移4，默认(0, -3),这个跟shadowRadius配合使用
    [_tabBarCtr tabBar].layer.shadowOpacity = 0.8;//阴影透明度，默认0
    [_tabBarCtr tabBar].layer.shadowRadius = 4;//阴影半径，默认3
    
    window.rootViewController = _tabBarCtr;
}

//自定义tabBar显示
- (void)customerTabBarForCtr:(RDVTabBarController *)tabBarCtr
{
    
    NSArray *imageArray, *selectImageArray;
    imageArray = @[@"tabbar_chat_n",@"tabbar_work_n",@"tabbar_activity_n",@"tabbar_mine_n"];
    selectImageArray = @[@"tabbar_chat_s",@"tabbar_work_s",@"tabbar_activity_s",@"tabbar_mine_s"];
    
    [[[tabBarCtr tabBar] items] enumerateObjectsUsingBlock:^(RDVTabBarItem *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        //选中背景图片
        //        [item setBackgroundSelectedImage:[UIImage imageNamed:@"tab_sel"] withUnselectedImage:nil];
        UIImage *selectedImage = [UIImage imageNamed:selectImageArray[idx]];
        UIImage *normalImage = [UIImage imageNamed:imageArray[idx]];
        obj.backgroundColor = [UIColor whiteColor];
        //选中按钮图片
        [obj setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:normalImage];
        
        NSDictionary *normalAttribute = @{NSFontAttributeName:[UIFont systemFontOfSize:10],NSForegroundColorAttributeName:kBlackColor};
        NSDictionary *selectedAttribute = @{NSFontAttributeName:[UIFont systemFontOfSize:10],NSForegroundColorAttributeName:RGB_Color(7, 193, 96)};
        
        //选中文字属性
        [obj setUnselectedTitleAttributes:normalAttribute];
        [obj setSelectedTitleAttributes:selectedAttribute];
    }];
}

- (void)tabBarController:(RDVTabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    AkrLog(@"已选择tab %@", viewController);

   
}

@end
