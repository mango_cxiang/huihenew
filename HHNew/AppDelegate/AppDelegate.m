//
//  AppDelegate.m
//  HHNew
//
//  Created by 储翔 on 2020/4/20.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "AppDelegate.h"
#import "HHTBManager.h"
#import "LoginVerificationCodeViewController.h"
#import "HHAddressListViewController.h"
#import "HHGroupChatDetailsViewController.h"
#import "HHSingleChatDetailsViewController.h"

#import "HHUserAuthenticationViewController.h"
#import "HHUserCompleteInfoViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
//    [[HHTBManager sharedHHTBManager] configViewCtrsinWindow:self.window];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:[[HHGroupChatDetailsViewController alloc] init]];
    self.window.rootViewController=nav;
    return YES;
}



@end
