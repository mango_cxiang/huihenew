//
//  HHGlobals.h
//  HHNew
//
//  Created by 张 on 2020/4/21.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface HHGlobals : NSObject

+(NSString*)timeStringForListWith:(NSTimeInterval)timesstamp;

/// 检测麦克风权限
+ (BOOL)checkMicrophoneStatus;
/// 检测相机权限
+ (BOOL)checkCameraStatus;
/// 检测相册权限
+ (BOOL)checkPhotoLibraryStatus;
//历史表情
+(NSArray *)getPoiList;
+(void)savePoiList:(NSArray *)poiList;

+(NSArray *)getEmojiDict;
+(void)saveEmojiDict:(NSArray *)EmojiDict;


/// 显示仅文字的AlertControll
+(void)showAlertWithTitle:(NSString *)title;

/// 选择地理位置
+(void)choosePositionCompleted:(void (^)(NSString *Location ,UIImage *Snapshot))complete;
@end

NS_ASSUME_NONNULL_END
