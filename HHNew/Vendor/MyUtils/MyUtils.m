//
//  MyUtils.m
//  huihe
//
//  Created by ye on 2019/6/18.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "MyUtils.h"

@implementation MyUtils

+ (UIButton *)buttonWithFrame:(CGRect)frame
                       target:(id)target
                     selector:(nullable SEL)selector
              backGroundColor:(nullable UIColor *)backColor
                         font:(nullable UIFont *)font
                   titleColor:(nullable UIColor *)titleColor
                        title:(nullable NSString *)title
{
    UIButton* button=[UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    button.titleLabel.font = font;
    [button setBackgroundColor:backColor];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
    
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

+(UILabel*)labelWithFrame:(CGRect)frame font:(nullable UIFont *)font backColor:(nullable UIColor*)backColor  textColor:(nullable UIColor *)textColor title:(nullable NSString*)title
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    if (backColor) {
        label.backgroundColor = backColor;
    }else{
        label.backgroundColor = [UIColor clearColor];
    }
    
    label.text = title;
    //  label.textAlignment = NSTextAlignmentCenter;
    label.textColor = textColor;
    label.font = font;
    return label;
}

+ (UIImageView*)imageViewWithFrame:(CGRect)frame image:(nullable UIImage *)image backColor:(nullable UIColor *)backColor
{
    UIImageView * imageV = [[UIImageView alloc]initWithFrame:frame];
    imageV.image = image;
    imageV.contentMode = UIViewContentModeScaleAspectFit;
    imageV.backgroundColor = backColor;
    return imageV;
}

+ (UITextField*)textFieldWithFrame:(CGRect)frame backColor:(nullable UIColor *)backColor fieldFont:(UIFont *)fieldfont fieldColr:(UIColor *)fieldColor phFont:(UIFont *)phfont phColor:(UIColor *)phColor placeholder:(NSString *)text;
{
    UITextField * textField = [[UITextField alloc]initWithFrame:frame];
    textField.font = fieldfont;
    textField.textColor = fieldColor;
    if (text) {
        NSMutableAttributedString *placeholder = [[NSMutableAttributedString alloc]initWithString:text];
        if (phColor) {
            [placeholder addAttribute:NSForegroundColorAttributeName
                                value:phColor
                                range:NSMakeRange(0, text.length)];
        }
        if (phfont) {
            [placeholder addAttribute:NSFontAttributeName
                                value:phfont
                                range:NSMakeRange(0, text.length)];
        }
        textField.attributedPlaceholder = placeholder;
    }
    
    textField.backgroundColor = backColor;
    return textField;
}

@end
