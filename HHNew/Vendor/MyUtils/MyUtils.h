//
//  MyUtils.h
//  huihe
//
//  Created by ye on 2019/6/18.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyUtils : NSObject

+ (UIButton *)buttonWithFrame:(CGRect)frame
                       target:(id)target
                     selector:(nullable SEL)selector
              backGroundColor:(nullable UIColor *)backColor
                         font:(nullable UIFont *)font
                   titleColor:(nullable UIColor *)titleColor
                        title:(nullable NSString *)title;


+(UILabel*)labelWithFrame:(CGRect)frame font:(nullable UIFont *)font backColor:(nullable UIColor*)backColor  textColor:(nullable UIColor *)textColor title:(nullable NSString*)title;

+ (UIImageView*)imageViewWithFrame:(CGRect)frame image:(nullable UIImage *)image backColor:(nullable UIColor *)backColor;

+ (UITextField*)textFieldWithFrame:(CGRect)frame backColor:(nullable UIColor *)backColor fieldFont:(UIFont *)fieldfont fieldColr:(UIColor *)fieldColor phFont:(UIFont *)phfont phColor:(UIColor *)phColor placeholder:(NSString *)text;

@end

NS_ASSUME_NONNULL_END
