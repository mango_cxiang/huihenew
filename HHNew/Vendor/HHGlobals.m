//
//  HHGlobals.m
//  HHNew
//
//  Created by 张 on 2020/4/21.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGlobals.h"
#import "HMKLocationController.h"

@implementation HHGlobals

+(NSString*)timeStringForListWith:(NSTimeInterval)timesstamp{
    NSString * _timestamp;
    NSTimeInterval timestamp = timesstamp/ 1000;
    time_t now;
    time(&now);
    
    int distance = (int)difftime(now, timestamp);
    if (distance < 0) distance = 0;
    
   if (distance < 60 * 60 * 24) {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
        if ([date isYesterday]) {
            _timestamp = @"昨天";
        }else {
            NSString *string = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
            
            NSRange range = [string rangeOfString:@"a"];
            
            BOOL hasAMPM = range.location != NSNotFound;
            
            if (hasAMPM) {
                NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"HH"];
                NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
                NSString *dayTime = [dateFormatter stringFromDate:date];
                NSString *nowTime1 = @"";
                if ([dayTime integerValue] > 12) {
                    nowTime1 = @"下午";
                }else {
                    nowTime1 = @"上午";
                }
                NSDateFormatter * dateFormatter1 = [[NSDateFormatter alloc] init];
                [dateFormatter1 setDateFormat:@"hh:mm"];
                NSDate *date1 = [NSDate dateWithTimeIntervalSince1970:timestamp];
                NSString *dayTime1 = [dateFormatter1 stringFromDate:date1];
                _timestamp = [NSString stringWithFormat:@"%@%@", nowTime1, dayTime1];
                
            }else {
                NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"HH:mm"];
                NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
                _timestamp = [dateFormatter stringFromDate:date];
            }
            
        }
        
    } else if (distance < 60 * 60 * 24 * 30) {
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"M-d"];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
        _timestamp = [dateFormatter stringFromDate:date];
    } else {
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-M-d"];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
        _timestamp = [dateFormatter stringFromDate:date];
    }
    
    return _timestamp;
}

//检测麦克风
+ (BOOL)checkMicrophoneStatus{
    AVAuthorizationStatus videoAuthStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    if(videoAuthStatus == AVAuthorizationStatusRestricted || videoAuthStatus == AVAuthorizationStatusDenied) {// 未授权
        [HHGlobals showAlertWithTitle:@"麦克风权限受限"];
        return NO;
    }
    return YES;
}
//检测相机
+ (BOOL)checkCameraStatus{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [HHGlobals showAlertWithTitle:@"检测不到相机设备"];
        return NO;
    }
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
        [HHGlobals showAlertWithTitle:@"会合还没有相机权限"];
        return NO;
    }
    return YES;
}
// 检测相册权限
+ (BOOL)checkPhotoLibraryStatus{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusRestricted || status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusNotDetermined) {
         return NO;
    }
    return YES;
}

#define POI_LIST   @"POI_LIST"
+(NSArray *)getPoiList
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *poiList = [defaults objectForKey:POI_LIST];
    return poiList;
}
+(void)savePoiList:(NSArray *)poiList
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:poiList forKey:POI_LIST];
    [defaults synchronize];
}

#define EMOJI_LIST      @"EMOJI_LIST"
+(NSArray *)getEmojiDict{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *EmojiList = [defaults objectForKey:EMOJI_LIST];
    return EmojiList;
}
+(void)saveEmojiDict:(NSArray *)EmojiArr{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:EmojiArr forKey:EMOJI_LIST];
    [defaults synchronize];
}

/// 显示仅文字的AlertControll
+(void)showAlertWithTitle:(NSString *)title
{
    UIAlertController *alter = [UIAlertController alertControllerWithTitle:nil message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *sure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alter addAction:sure];
    UIWindow * window = [[UIApplication sharedApplication].delegate window];
    [window.rootViewController presentViewController:alter animated:YES completion:nil];
}

/// 选择地理位置
+(void)choosePositionCompleted:(void (^)(NSString *Location ,UIImage *Snapshot))complete
{
    HMKLocationController  *locVc = Init(HMKLocationController);
    locVc.locationWithSnapshotBlock = ^(NSString *Location) {
        NSDictionary *locationDic = [Location mj_JSONObject];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        [dic setDictionary:locationDic];
        [dic setObject:Location forKey:@"Location"];
        NSString *str = [NSString stringWithFormat:@"http://restapi.amap.com/v3/staticmap?location=%@,%@&size=350*350&zoom=6&markers=mid,,A:%@,%@&key=d90c0f524a625051a046e3930d5e2ed0",locationDic[@"lut"],locationDic[@"lat"],locationDic[@"lut"],locationDic[@"lat"]];
        [dic setObject:str  forKey:@"url"];
        UIImage *imag = [UIImage new];
        complete([dic mj_JSONString],imag);
    };
    HHNavController *nav = [[HHNavController alloc]initWithRootViewController:locVc];
//    nav.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : BoldFont(18)};
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    UIWindow * window = [[UIApplication sharedApplication].delegate window];
    [window.rootViewController presentViewController:nav animated:YES completion:nil];
}

@end
