//
//  HMKYXTextView.m
//  huihe
//
//  Created by ye on 2019/7/10.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKYXTextView.h"
#import "MyUtils.h"

@interface HMKYXTextView ()<UITextViewDelegate>

@property (nonatomic, assign) NSInteger limitNum;

@property (nonatomic, strong) UILabel * placeL;

@end

@implementation HMKYXTextView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame limitNum:(NSInteger)limitNum
{
    self = [super initWithFrame:frame];
    if (self) {
        _limitNum = limitNum;
        [self initWithSubviews];
    }
    return self;
}

- (void)initWithSubviews
{
    self.backgroundColor = ColorManage.background_color;
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
    
    _placeView = [[UITextView alloc]initWithFrame:CGRectZero];
    //                  CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-15-7)];
    _placeView.backgroundColor = [UIColor clearColor];
    _placeView.delegate = self;
    _placeView.font = kRegularFont(14);
    _placeView.textColor = [UIColor blackColor];
    [self addSubview:_placeView];
    
    
    _placeL = [MyUtils labelWithFrame:CGRectZero font:kRegularFont(14) backColor:nil textColor:ColorManage.text_color title:nil];
    _placeL.enabled = NO;
    _placeL.hidden = YES;
    _placeL.numberOfLines = 0;
    _placeL.backgroundColor = [UIColor clearColor];
    [self addSubview:_placeL];
    
//    _placeL.backgroundColor = [UIColor redColor];
    
    //    CGRectMake(self.frame.size.width-19-60, CGRectGetMaxY(_placeView.frame), 60, 15)
    _remarkLabel = [MyUtils labelWithFrame:CGRectZero font:kRegularFont(14) backColor:nil textColor:ColorManage.text_color title:[NSString stringWithFormat:@"0/%ld",(long)_limitNum]];
    _remarkLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:_remarkLabel];
    
    [_placeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.insets(UIEdgeInsetsMake(10, 10, 10, 10));
    }];
    
    [_placeL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.insets(UIEdgeInsetsMake(11, 13, 20, 10));
    }];
    
    [_remarkLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(self.frame.size.width-60-10);
        make.bottom.equalTo(_placeView.mas_bottom);
        make.width.equalTo(@(60));
        make.height.equalTo(@(15));
    }];
    
}

#pragma mark - textView
- (void)textViewDidChange:(UITextView *)textView {
    
    
    if (!isEmptyStr(_defaultText)) {
        if (textView.text.length == 0) {
            _placeL.text = _defaultText;
        }else{
            _placeL.text = @"";
        }
    }
    
    NSInteger count = textView.text.length;
    _remarkLabel.text = [NSString stringWithFormat:@"%zd/%ld",count,_limitNum];
    
    //字数限制操作
    NSInteger num = 0;
    if (_limitNum) {
        num = _limitNum;
    }else{
        num = 200;
    }
    
    if (textView.text.length >= num) {
        textView.text = [textView.text substringToIndex:num];
        _remarkLabel.text = [NSString stringWithFormat:@"%ld/%ld",(long)num,(long)num];
    }
    _text = textView.text;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (textView.text.length + text.length >_limitNum){
        return NO;
    }else{
        return YES;
    }
    
}

- (void)setText:(NSString *)text
{
    _text = text;
    _placeView.text = text;
    _remarkLabel.text = [NSString stringWithFormat:@"%ld/%ld",text.length,_limitNum];
}

- (void)setDefaultText:(NSString *)defaultText
{
    _defaultText = defaultText;
    if (!isEmptyStr(defaultText)) {
        _placeL.hidden = NO;
        _placeL.text = defaultText;
    }
}

- (void)setBgColor:(UIColor *)bgColor
{
    _bgColor = bgColor;
    self.backgroundColor = bgColor;
}
@end
