//
//  HMKYXTextView.h
//  huihe
//
//  Created by ye on 2019/7/10.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMKYXTextView : UIView

- (instancetype)initWithFrame:(CGRect)frame limitNum:(NSInteger)limitNum;

@property (nonatomic, strong) UITextView * placeView;
@property (nonatomic, copy) NSString * defaultText;
@property (nonatomic, strong)UIColor * bgColor;

@property (nonatomic, strong) UILabel * remarkLabel;

//已输入内容
@property (nonatomic, copy) NSString * text;

@end

NS_ASSUME_NONNULL_END
