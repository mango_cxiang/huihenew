//
//  FYJDPlaceSearchView.m
//  FYJD
//
//  Created by feeyo on 2019/9/12.
//  Copyright © 2019 feeyo. All rights reserved.
//

#import "FYJDSearchView.h"
@interface FYJDSearchView ()<UITextFieldDelegate>

@end


@implementation FYJDSearchView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self loadSubView];
        self.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return self;
}

- (void)loadSubView{
    
    [self addSubview:self.textField];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(15);
        make.top.mas_equalTo(self).offset(8);
        make.right.mas_equalTo(self).offset(-15);
        make.height.equalTo(@(35));
    }];
    
    [self addSubview:self.cancleButton];
    [_cancleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).offset(-15);
        make.top.mas_equalTo(self).offset(8);
        make.bottom.mas_equalTo(self).offset(-5);
        make.width.equalTo(@(45));
    }];
}

- (void)cancelButtonClicked:(UIButton *)btn{
    if(self.searchViewCancelBtnClickBlock)self.searchViewCancelBtnClickBlock();
    [self closeView];
}

- (void)closeView{
    _cancleButton.hidden = YES;
    _textField.text = @"";
    [_textField resignFirstResponder];
    [self.textField mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(15);
        make.top.mas_equalTo(self).offset(8);
        make.right.mas_equalTo(self).offset(-15);
        make.height.equalTo(@(35));
    }];
}

//TODO:点击搜索
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.textField resignFirstResponder];
//    NSString *inputWord = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return YES;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    self.cancleButton.hidden = NO;
    [self.textField mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).offset(-65);
    }];
    return YES;
}

//输入内容
- (void)textContentChange{
    if (self.searchTextContentChanged) {
        self.searchTextContentChanged(self.textField.text);
    }
}

- (UITextField *)textField {
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.delegate = self;
        _textField.placeholder = @"搜索";
        [_textField viewWithRadis:5];
        _textField.backgroundColor = ColorManage.darkBackgroundColor;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.returnKeyType = UIReturnKeySearch;
        UIView *emptyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 20)];
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 2, 15, 15)];
        imgView.image = [UIImage imageNamed:@"search_clear"];
        [emptyView addSubview:imgView];
        _textField.leftView = emptyView;
        _textField.leftViewMode = UITextFieldViewModeAlways;
        _textField.tintColor = RGB_GreenColor;
        _textField.textColor = ColorManage.text_color;
        _textField.font = kRegularFont(14);
        [_textField addTarget:self action:@selector(textContentChange) forControlEvents:UIControlEventEditingChanged];
    }
    return _textField;
}


- (UIButton *)cancleButton{
    if (!_cancleButton) {
        _cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancleButton.hidden = YES;
        [_cancleButton setTitle:@"  取消" forState:UIControlStateNormal];
        _cancleButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [_cancleButton setTitleColor:ColorManage.loginTextColor forState:UIControlStateNormal];
        [_cancleButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancleButton;
}
@end
