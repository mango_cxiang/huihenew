//
//  FYJDPlaceSearchView.h
//  FYJD
//
//  Created by feeyo on 2019/9/12.
//  Copyright © 2019 feeyo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^searchViewCancelBtnClickBlock)(void);
typedef void(^searchTextContentChanged)(NSString *inputWords);

@interface FYJDSearchView : UIView

@property (nonatomic, strong) UITextField *textField;

@property (nonatomic, strong) UIButton *cancleButton;

@property (nonatomic, copy) searchViewCancelBtnClickBlock searchViewCancelBtnClickBlock;
@property (nonatomic, copy) searchTextContentChanged searchTextContentChanged;

- (void)closeView;

@end

NS_ASSUME_NONNULL_END
