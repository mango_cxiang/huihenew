//
//  FYJDTextView.h
//  FYJD
//
//  Created by feeyo on 2019/4/26.
//  Copyright © 2019 feeyo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FYJDTextView : UITextView


/** 占位文字 */
@property (nonatomic, copy) NSString *placeholder;
/** 占位文字颜色 */
@property (nonatomic, strong) UIColor *placeholderColor;

- (float) heightForString:(UITextView *)textView andWidth:(float)width;

@end

NS_ASSUME_NONNULL_END
