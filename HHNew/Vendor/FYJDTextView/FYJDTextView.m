//
//  FYJDTextView.m
//  FYJD
//
//  Created by feeyo on 2019/4/26.
//  Copyright © 2019 feeyo. All rights reserved.
//

#import "FYJDTextView.h"

@implementation FYJDTextView

- (instancetype)init{
    if (self = [super init]) {
        [self loadCustom];
        [self addDoneBtn];
    }
    return self;
}

- (void)loadCustom{
    // 设置默认字体
    self.font = [UIFont systemFontOfSize:14];
    
    // 设置默认颜色
    self.placeholderColor = RGB_Color(170, 175, 181);
    self.tintColor = RGB_Color(19, 197, 106);

    // 使用通知监听文字改变
    [[NSNotificationCenter defaultCenter] addObserver:self   selector:@selector(textDidChange:) name:UITextViewTextDidChangeNotification object:self];
}

- (void)textDidChange:(NSNotification *)note{
    // 会重新调用drawRect:方法
    [self setNeedsDisplay];
}

- (void)addDoneBtn{
    UIToolbar * topView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    [topView setBarStyle:UIBarStyleDefault];
    UIBarButtonItem * helloButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem * btnSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(dismissKeyBoard)];
    NSArray * buttonsArray = [NSArray arrayWithObjects:helloButton,btnSpace,doneButton,nil];
    [topView setItems:buttonsArray];
    [self setInputAccessoryView:topView];
}

- (void)dismissKeyBoard{
    [self resignFirstResponder];
}

- (float) heightForString:(UITextView *)textView andWidth:(float)width{
    CGSize sizeToFit = [textView sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    return sizeToFit.height;
}

/**
 * 每次调用drawRect:方法，都会将以前画的东西清除掉
 */
- (void)drawRect:(CGRect)rect
{
    // 如果有文字，就直接返回，不需要画占位文字
    if (self.hasText) return;
    
    // 属性
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSFontAttributeName] = self.font;
    attrs[NSForegroundColorAttributeName] = self.placeholderColor;
    
    // 画文字
    rect.origin.x = 5;
    rect.origin.y = 8;
    rect.size.width -= 2 * rect.origin.x;
    [self.placeholder drawInRect:rect withAttributes:attrs];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self setNeedsDisplay];
}

#pragma mark - setter
- (void)setPlaceholder:(NSString *)placeholder
{
    _placeholder = [placeholder copy];
    
    [self setNeedsDisplay];
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor
{
    _placeholderColor = placeholderColor;
    
    [self setNeedsDisplay];
}

- (void)setFont:(UIFont *)font
{
    [super setFont:font];
    
    [self setNeedsDisplay];
}

- (void)setText:(NSString *)text
{
    [super setText:text];
    
    [self setNeedsDisplay];
}

- (void)setAttributedText:(NSAttributedString *)attributedText
{
    [super setAttributedText:attributedText];
    
    [self setNeedsDisplay];
}

@end
