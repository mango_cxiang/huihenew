//
//  HMKLocationController.h
//  huihe
//
//  Created by jie.huang on 3/12/18.
//  Copyright © 2018年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^LocationWithSnapshotBlock)(NSString *Location);

@interface HMKLocationController : UIViewController

@property (nonatomic, copy) LocationWithSnapshotBlock locationWithSnapshotBlock;


@end
