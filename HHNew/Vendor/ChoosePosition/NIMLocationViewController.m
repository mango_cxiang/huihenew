//
//  NTESLocationViewController.m
//  NIM
//
//  Created by chris on 15/2/28.
//  Copyright (c) 2015年 Netease. All rights reserved.
//
#import "NIMLocationViewController.h"
#import "NIMKitLocationPoint.h"
#import <AddressBookUI/AddressBookUI.h>
#import <CoreLocation/CoreLocation.h>
#import "BaseViewController.h"

@interface NIMLocationViewController (){
    BOOL  _updateUserLocation;
}

@property(nonatomic,strong) UIBarButtonItem *sendButton;

@property(nonatomic,strong) CLLocationManager *locationManager;

@property(nonatomic,strong) NIMKitLocationPoint *locationPoint;

@property(nonatomic,strong) CLGeocoder * geoCoder;

@end

@implementation NIMLocationViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _locationManager = [[CLLocationManager alloc] init];
        _geoCoder = [[CLGeocoder alloc] init];
    }
    return self;
}

- (instancetype)initWithLocationPoint:(NIMKitLocationPoint *)locationPoint{
    self = [self initWithNibName:nil bundle:nil];
    if (self) {
        _locationPoint = locationPoint;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
    self.mapView.delegate = self;
    [self.view addSubview:self.mapView];
    
    if (self.navigationController.topViewController == self) {
        [self.navigationItem setLeftBarButtonItem:[UIBarButtonItem itemWithImageName:@"fanhui" highImageName:@"fanhui" target:self action:@selector(dismiss:)]];
    }else{
        UIButton * backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [backBtn setImage:ImageInit(@"fanhui") forState:UIControlStateNormal];
        [backBtn setImage:ImageInit(@"fanhui") forState:UIControlStateHighlighted];
        [backBtn addTarget:self action:@selector(dismiss:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:backBtn];
        [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(kStatusBarHeight);
            make.width.height.offset(44);
            make.left.offset(15);
        }];
    }
    
    if (self.locationPoint) {
        MKCoordinateRegion theRegion;
        theRegion.center = self.locationPoint.coordinate;
        theRegion.span.longitudeDelta	= 0.01f;
        theRegion.span.latitudeDelta	= 0.01f;
        [self.mapView addAnnotation:self.locationPoint];
        [self.mapView setRegion:theRegion animated:YES];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(SCREEN_WIDTH - 95, SCREEN_HEIGHT - 200+SafeAreaTopHeight, 80, 80);
        [button setImage:ImageInit(@"icon_mapNavigation") forState:UIControlStateNormal];
        [button addTarget:self action:@selector(mapNavigationButton) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
    }else{
        [self setUpRightNavButton];
        self.locationPoint   = [[NIMKitLocationPoint alloc] init];
        if ([CLLocationManager locationServicesEnabled]) {
            [_locationManager requestAlwaysAuthorization];
            CLAuthorizationStatus status = CLLocationManager.authorizationStatus;
            if (status == kCLAuthorizationStatusRestricted || status == kCLAuthorizationStatusDenied ) {

//                [self.view makeToast:Localized(@"请在设置-隐私里允许程序使用地理位置服务,以便进行导航等功能")
//                            duration:3
//                            position:CSToastPositionCenter];
            }else{
                self.mapView.showsUserLocation = YES;
            }
        }else{
//            [self.view makeToast:Localized(@"请打开地理位置服务")
//                        duration:2
//                        position:CSToastPositionCenter];
        }
    }
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
}

- (void)setUpRightNavButton{
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(onSend:)];
    self.navigationItem.rightBarButtonItem = item;
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor whiteColor];
    self.sendButton = item;
    self.sendButton.enabled = NO;
}
-(void)mapNavigationButton{
    
    UIAlertController *alter = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *sure1 = [UIAlertAction actionWithTitle:@"Apple地图" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //使用自带地图导航
        MKMapItem *currentLocation =[MKMapItem mapItemForCurrentLocation];
        
        MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:self.locationPoint.coordinate addressDictionary:nil]];
        
        [MKMapItem openMapsWithItems:@[currentLocation,toLocation] launchOptions:@{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving,
                                                                                   MKLaunchOptionsShowsTrafficKey:[NSNumber numberWithBool:YES]}];
    }];
    [alter addAction:sure1];
    UIAlertAction *sure2 = [UIAlertAction actionWithTitle:@"高德地图" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //判断是否安装了高德地图，如果安装了高德地图，则使用高德地图导航
        if ( [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"iosamap://"]]) {
            NSLog(@"alertController -- 高德地图");
            NSString *urlsting = [[NSString stringWithFormat:@"iosamap://navi?sourceApplication= &backScheme= &lat=%f&lon=%f&dev=0&style=2",self.locationPoint.coordinate.latitude,self.locationPoint.coordinate.longitude] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            [[UIApplication  sharedApplication]openURL:[NSURL URLWithString:urlsting]];
        }else{
            ShowToast(@"未安装高德地图")
        }
    }];
    [alter addAction:sure2];
    UIAlertAction *sure3 = [UIAlertAction actionWithTitle:@"百度地图" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"baidumap://"]]) {
            NSLog(@"alertController -- 百度地图");
            NSString *urlsting =[[NSString stringWithFormat:@"baidumap://map/direction?origin={{我的位置}}&destination=latlng:%f,%f|name=目的地&mode=driving&coord_type=gcj02",self.locationPoint.coordinate.latitude,self.locationPoint.coordinate.longitude] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlsting]];
        }else{
            ShowToast(@"未安装百度地图")
        }
    }];
    [alter addAction:sure3];
    UIAlertAction *cancle = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alter addAction:cancle];
    [self presentViewController:alter animated:YES completion:^{
    }];
    
    return;
}
- (void)onSend:(id)sender{
//    if ([self.delegate respondsToSelector:@selector(onSendLocation:)]) {
//        [self.delegate onSendLocation:self.locationPoint];
//    }
//    [self.navigationController dismissViewControllerAnimated:YES completion:nil];

    NSLog(@"%@",self.locationPoint);
    
//    UIImage *img = [_mapView takeSnapshot];
    
    // 截图附加选项
    MKMapSnapshotOptions *options = [[MKMapSnapshotOptions alloc] init];
    // 设置截图区域(在地图上的区域,作用在地图)
    options.region = self.mapView.region;
    //    options.mapRect = self.mapView.visibleMapRect;
    
    // 设置截图后的图片大小(作用在输出图像)
    options.size = self.mapView.frame.size;
    // 设置截图后的图片比例（默认是屏幕比例， 作用在输出图像）
    options.scale = [[UIScreen mainScreen] scale];
    
    MKMapSnapshotter *snapshotter = [[MKMapSnapshotter alloc] initWithOptions:options];
    [snapshotter startWithCompletionHandler:^(MKMapSnapshot * _Nullable snapshot, NSError * _Nullable error) {
        if (error) {
            NSLog(@"截图错误：%@",error.localizedDescription);
        }else
        {
            // 设置屏幕上图片显示
            NSMutableDictionary * dic = [NSMutableDictionary dictionary];
            [dic setObject:self.locationPoint.title forKey:@"addr"];
            [dic setObject:[NSString stringWithFormat:@"%f",self.locationPoint.coordinate.latitude] forKey:@"lat"];
            [dic setObject:[NSString stringWithFormat:@"%f",self.locationPoint.coordinate.longitude] forKey:@"lut"];
            self.returnLocationWithSnapshotBlock([dic mj_JSONString],snapshot.image);
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)returnLocationTextWithSnapshot:(ReturnLocationWithSnapshotBlock)block{
    self.returnLocationWithSnapshotBlock = block;
}
#pragma mark - MKMapViewDelegate



- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    if (!_updateUserLocation) {
        return;
    }
    CLLocationCoordinate2D centerCoordinate = mapView.region.center;
    [self reverseGeoLocation:centerCoordinate];
}



- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated

{
    if (!_updateUserLocation) {
        return;
    }
    [_mapView removeAnnotations:_mapView.annotations];
}



- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
    static NSString *reusePin = @"reusePin";
    MKPinAnnotationView * pin = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:reusePin];
    if (!pin) {
        pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reusePin];
    }
    pin.canShowCallout	= YES;
    return pin;

}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    _updateUserLocation = YES;
    MKCoordinateRegion theRegion;
    theRegion.center = userLocation.coordinate;
    theRegion.span.longitudeDelta	= 0.01f;
    theRegion.span.latitudeDelta	= 0.01f;
    [_mapView setRegion:theRegion animated:NO];
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views{
    [_mapView selectAnnotation:self.locationPoint animated:YES];
    UIView * view = [mapView viewForAnnotation:self.mapView.userLocation];
    view.hidden = YES;
}


- (void)reverseGeoLocation:(CLLocationCoordinate2D)locationCoordinate2D{
    if (self.geoCoder.isGeocoding) {
        [self.geoCoder cancelGeocode];
    }
    CLLocation *location = [[CLLocation alloc]initWithLatitude:locationCoordinate2D.latitude
                                                     longitude:locationCoordinate2D.longitude];
    __weak NIMLocationViewController *wself = self;
    self.sendButton.enabled = NO;
    [self.geoCoder reverseGeocodeLocation:location
                      completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil) {
             CLPlacemark *mark = [placemarks lastObject];
             NSString * title  = [wself nameForPlaceMark:mark];
             NIMKitLocationPoint *ponit = [[NIMKitLocationPoint alloc]initWithCoordinate:locationCoordinate2D andTitle:title];
             wself.locationPoint = ponit;
             [wself.mapView addAnnotation:ponit];
             wself.sendButton.enabled = YES;
         } else {
             wself.locationPoint = nil;
         }
    }];
}

- (NSString *)nameForPlaceMark: (CLPlacemark *)mark
{
    NSString *name = ABCreateStringWithAddressDictionary(mark.addressDictionary,YES);
    unichar characters[1] = {0x200e};   //format之后会出现这个诡异的不可见字符，在android端显示会很诡异，需要去掉
    NSString *invalidString = [[NSString alloc]initWithCharacters:characters length:1];
    NSString *formattedName =  [[name stringByReplacingOccurrencesOfString:@"\n" withString:@" "] stringByReplacingOccurrencesOfString:invalidString withString:@""];
    return formattedName;
}

- (void)dismiss:(id)sender
{
    if (self.navigationController.topViewController != self) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}



@end
