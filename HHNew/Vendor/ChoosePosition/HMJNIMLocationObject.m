//
//  HMJNIMLocationObject.m
//  huihe
//
//  Created by 有慧科技 on 2018/4/3.
//  Copyright © 2018年 Six. All rights reserved.
//

#import "HMJNIMLocationObject.h"

@implementation HMJNIMLocationObject
- (instancetype)initWithLatitude:(double)latitude
                       longitude:(double)longitude
                           title:(nullable NSString *)title{
    self = [super init];
    if (self) {
        _latitude  = latitude;
        _longitude = longitude;
        _title     = title;
    }
    return self;
}
@end
