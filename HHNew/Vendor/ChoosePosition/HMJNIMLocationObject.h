//
//  HMJNIMLocationObject.h
//  huihe
//
//  Created by 有慧科技 on 2018/4/3.
//  Copyright © 2018年 Six. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HMJNIMLocationObject : NSObject
/**
 *  位置实例对象初始化方法
 *
 *  @param latitude  纬度
 *  @param longitude 经度
 *  @param title   地理位置描述
 *  @return 位置实例对象
 */
- (instancetype)initWithLatitude:(double)latitude
                       longitude:(double)longitude
                           title:(nullable NSString *)title;

/**
 *  维度
 */
@property (nonatomic, assign, readonly) double latitude;

/**
 *  经度
 */
@property (nonatomic, assign, readonly) double longitude;

/**
 *  标题
 */
@property (nullable, nonatomic, copy, readonly) NSString *title;
@end
