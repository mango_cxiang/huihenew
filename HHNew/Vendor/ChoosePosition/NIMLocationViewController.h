//
//  NTESLocationViewController.h
//  NIM
//
//  Created by chris on 15/2/28.
//  Copyright (c) 2015年 Netease. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class NIMKitLocationPoint;

@protocol NIMLocationViewControllerDelegate <NSObject>

- (void)onSendLocation:(NIMKitLocationPoint *)locationPoint;

@end

typedef void (^ReturnLocationWithSnapshotBlock)(NSString *Location ,UIImage *Snapshot);

@interface NIMLocationViewController : UIViewController<MKMapViewDelegate>

@property(nonatomic,strong) MKMapView *mapView;

@property(nonatomic,weak)   id<NIMLocationViewControllerDelegate> delegate;

@property (nonatomic, copy) ReturnLocationWithSnapshotBlock returnLocationWithSnapshotBlock;

@property (nonatomic,strong) NSString *addressStr;

- (instancetype)initWithLocationPoint:(NIMKitLocationPoint *)locationPoint;

- (void)returnLocationTextWithSnapshot:(ReturnLocationWithSnapshotBlock)block;

@end
