

#import "ZGQTableViewCell.h"
#import "GrandConfig.h"
@implementation ZGQTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addViews];
    }
    return self;
}

- (void)addViews{
//    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
//    self.titleLabel.textColor = RGB_BlackColor_NormalLevel;
    
    [self.contentView addSubview:self.titleLabel];
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = ColorManage.lineViewColor;
    [self.contentView addSubview:self.lineView];
    
}

- (void)layoutSubviews {
    CGFloat contentViewWidth = CGRectGetWidth(self.contentView.bounds);
    CGFloat contentViewHeight = CGRectGetHeight(self.contentView.bounds);
    self.titleLabel.frame = CGRectMake(contentViewWidth * 0.2, 0, contentViewWidth * 0.6, contentViewHeight);
    self.lineView.frame = CGRectMake(0, contentViewHeight - 0.3, contentViewWidth, 0.3);
   
    if([self.titleLabel.text isEqualToString:@"删除"]){
        self.titleLabel.textColor = RGB_RedColor_NormalLevel;
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
