

#import "ZGQActionSheetView.h"
#import "ZGQTableViewCell.h"
#import "UIView+Extension.h"
#import "UIView+RoundCorner.h"
#import "UILabel+HHTextSpace.h"

#define ZGQ_SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define ZGQ_SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define ZGQ_BACKGROUND_COLOR [UIColor colorWithWhite:0.001 alpha:0.4]
#define ZGQ_OPTION_COLOR [UIColor colorWithWhite:0.2 alpha:1.000]

static NSString *const sheetViewCell = @"ZGQSheetViewCell";

typedef void(^selectBlock)(NSInteger index);
typedef void(^cancelBlock)(void);

@interface ZGQActionSheetView ()<UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource>

//半透明背景图
@property (nonatomic, strong) UIView *sheetBackView;
//选项列表和取消按钮父视图
@property (nonatomic, strong) UIView *sheetView;
//选项列表
@property (nonatomic, strong) UITableView *sheetTableView;
//示例图片父视图
@property (nonatomic, strong) UIView *sampleBackView;
//示例图片
@property (nonatomic, strong) UIImageView *sampleImageView;
//示例图片说明标题
@property (nonatomic, strong) UILabel *sampleLabel;
//取消按钮
@property (nonatomic, strong) UIButton *sheetCancelBtn;
//父视图高度
@property (nonatomic, assign) CGFloat sheetViewHeight;
//选中回调
@property (nonatomic, copy) selectBlock selectBlock;
//取消回调
@property (nonatomic, copy) cancelBlock cancelBlock;


@end

@implementation ZGQActionSheetView


- (instancetype)initWithOptions:(NSArray<NSString *>*)options
{
    self = [super init];
    if (self) {
        [self initializeInformation];
        self.options = [NSArray arrayWithArray:options];
    }
    
    return self;
}

- (instancetype)initWithOptions:(NSArray<NSString *> *)options completion:(void (^)(NSInteger))completion cancel:(void (^)(void))cancelBlock {
    self = [super init];
    if (self) {
        [self initializeInformation];
        self.options = [NSArray arrayWithArray:options];
        self.selectBlock = completion;
        self.cancelBlock = cancelBlock;
    }
    return self;
}

- (void)initializeInformation{
    self.maxShowCount = 5;
    self.fontSize = 16.0f;
    self.optionHeight = 56.0f;
    self.optionColor = self.optionColor?self.optionColor:ColorManage.text_color;
    self.gap = 7.0f;
    self.needCancelButton = YES;
    self.cancelTitle = @"取消";
}


- (void)show
{
    if (self.options.count < 1) {
        NSLog(@"###### ZGQActrionSheetViewError!!!,请设置标题数组!!!");
        return;
    }

    self.sheetBackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    self.sheetBackView.backgroundColor = ZGQ_BACKGROUND_COLOR;
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [[UIApplication sharedApplication].keyWindow addSubview:self.sheetBackView];
    
    UITapGestureRecognizer *backTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetCancelAction)];
    backTap.delegate = self;
    [self.sheetBackView addGestureRecognizer:backTap];
    
    self.sheetView = [[UIView alloc]init];
    self.sheetView.backgroundColor = ColorManage.nav_color;
    [self.sheetBackView addSubview:self.sheetView];
    
    NSInteger optionCount = self.options.count;
    NSInteger showCount = optionCount < self.maxShowCount ? optionCount : self.maxShowCount;
    NSInteger isHaveCancelBtn = self.needCancelButton ? 1 : 0;

    self.sheetViewHeight = self.optionHeight * (showCount + isHaveCancelBtn) + (self.gap * isHaveCancelBtn);
    CGFloat top = ZGQ_SCREEN_HEIGHT;
    CGFloat height = self.sheetViewHeight;
    
    // 标题
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 40, 0)];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 0;
    label.font = [UIFont systemFontOfSize:14.0];
    label.textColor = ColorManage.grayTextColor_Deault;
    label.text = self.sampleTitleName;
    [label changeSpaceWithLineSpace:3 WordSpace:0];
    label.textAlignment = NSTextAlignmentCenter;
    [label sizeToFit];
    [label setFrame:CGRectMake(20, 15, SCREEN_WIDTH - 40, ceil(label.frame.size.height))];
    UIView * header = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0)];
    header.backgroundColor = ColorManage.background_color;
    [header addSubview:label];
    header.height = label.frame.size.height + 30;

    if (self.showSampleView) {
          top = top -  header.height;
          height = self.sheetViewHeight + header.height ;
      }
      self.sheetView.frame = CGRectMake(0, top, ZGQ_SCREEN_WIDTH, height+SafeAreaBottomHeight);
      [self.sheetView tt_addRoundedCorners:UIRectCornerTopLeft | UIRectCornerTopRight withRadii:CGSizeMake(12, 12)];
    
    UIView * line = [[UIView alloc] initWithFrame:CGRectMake(0, header.frame.size.height - 0.5, SCREEN_WIDTH, 0.5)];
    line.backgroundColor = ColorManage.lineViewColor;
    [header addSubview:line];
    
    CGFloat bottom = 0;
    if (self.showSampleView) {
        [self.sheetView addSubview:header];
         bottom = header.bottom;
    }

    self.sheetTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, bottom, ZGQ_SCREEN_WIDTH, self.optionHeight * showCount)];
    self.sheetTableView.backgroundColor = ColorManage.background_color;

    [self.sheetView addSubview:self.sheetTableView];
    self.sheetTableView.showsVerticalScrollIndicator = NO;
    self.sheetTableView.bounces = NO;
    self.sheetTableView.separatorStyle = NO;
    self.sheetTableView.rowHeight = self.optionHeight;
    self.sheetTableView.delegate = self;
    self.sheetTableView.dataSource = self;
    [self.sheetTableView registerClass:[ZGQTableViewCell class] forCellReuseIdentifier:sheetViewCell];
    
    if (self.needCancelButton) {
        self.sheetCancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.sheetCancelBtn.frame = CGRectMake(0, height - self.optionHeight, CGRectGetWidth(self.sheetView.bounds), self.optionHeight);
        self.sheetCancelBtn.backgroundColor = ColorManage.background_color;
        [self.sheetCancelBtn setTitle:self.cancelTitle forState:UIControlStateNormal];
        self.sheetCancelBtn.titleLabel.font = [UIFont systemFontOfSize:self.fontSize];
        [self.sheetCancelBtn setTitleColor:ColorManage.text_color forState:UIControlStateNormal];
        [self.sheetCancelBtn addTarget:self action:@selector(sheetCancelAction) forControlEvents:UIControlEventTouchUpInside];
        [self.sheetView addSubview:self.sheetCancelBtn];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, self.sheetCancelBtn.bottom, SCREEN_WIDTH, SafeAreaBottomHeight)];
        view.backgroundColor = ColorManage.background_color;
        [self.sheetView addSubview:view];
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.sheetView.frame = CGRectMake(0, SCREEN_HEIGHT - height - SafeAreaBottomHeight, ZGQ_SCREEN_WIDTH, height+SafeAreaBottomHeight);
        
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.options.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZGQTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sheetViewCell];
    cell.backgroundColor = ColorManage.background_color;
    cell.titleLabel.text = self.options[indexPath.row];
    cell.titleLabel.font = kRegularFont(self.fontSize);
    cell.titleLabel.textColor =  self.optionColor;
    if(indexPath.row == self.options.count-1){
        cell.lineView.hidden = YES;
    }
    UIView * bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, self.optionHeight - 1)];
    bgView.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:0.2];
    cell.selectedBackgroundView = bgView;
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.selectBlock) {
        self.selectBlock(indexPath.row);
    }
    
    if ([self.delegate respondsToSelector:@selector(ZGQActionSheetView:didSelectRowAtIndex:text:)]) {
        [self.delegate ZGQActionSheetView:self didSelectRowAtIndex:indexPath.row text:self.options[indexPath.row]];
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.sheetBackView.alpha = 0;
        self.sheetView.frame = CGRectMake(0, ZGQ_SCREEN_HEIGHT, ZGQ_SCREEN_WIDTH, self.sheetViewHeight);
    } completion:^(BOOL finished) {
        [self.sheetBackView removeFromSuperview];
        [self removeFromSuperview];
    }];
}


- (void)sheetCancelAction
{
    if (self.cancelBlock) {
        self.cancelBlock();
    }
    if ([self.delegate respondsToSelector:@selector(ZGQActionSheetViewdidCancelSelectFrom:)]) {
        [self.delegate ZGQActionSheetViewdidCancelSelectFrom:self];
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.sheetBackView.alpha = 0;
        self.sheetView.frame = CGRectMake(0, ZGQ_SCREEN_HEIGHT, ZGQ_SCREEN_WIDTH, self.sheetViewHeight+SafeAreaBottomHeight);
    } completion:^(BOOL finished) {
        [self.sheetBackView removeFromSuperview];
        [self removeFromSuperview];
    }];
    
}


-(BOOL)gestureRecognizer:(UIGestureRecognizer*)gestureRecognizer shouldReceiveTouch:(UITouch*)touch
{
    if([NSStringFromClass([touch.view class])isEqual:@"UITableViewCellContentView"]){
        return NO;
    }
    return YES;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
