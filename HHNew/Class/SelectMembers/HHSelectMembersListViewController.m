//
//  HHSelectMembersListViewController.m
//  HHNew
//
//  Created by Liubin on 2020/5/6.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHSelectMembersListViewController.h"
#import "SCIndexViewConfiguration.h"
#import "SCIndexView.h"
#import "HHSelectMembersTableView.h"
#import "HHContactsModel.h"
#import "SelectMemberWithSearchView.h"

@interface HHSelectMembersListViewController ()<SCIndexViewDelegate,SelectMemberWithSearchViewDelegate,UITextFieldDelegate>
@property (strong, nonatomic) HHSelectMembersTableView *tableView;
@property (strong, nonatomic) HHSelectMembersTableView *searchResultTableView;

@property (strong, nonatomic) SelectMemberWithSearchView *selectView;

@property (nonatomic, strong) SCIndexView *indexView;
@property (nonatomic, assign) SCIndexViewStyle indexViewStyle;
@property (nonatomic, strong) UIButton *finishBtn;
@property (nonatomic,strong) NSMutableArray *searchResultArr;//搜索结果
@property (nonatomic,strong) NSMutableArray *dataArray;//所有好友
@property (strong, nonatomic) NSMutableArray *membersArray;//选中的成员

@end

@implementation HHSelectMembersListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    [self setupRightBarButtonItem:self.finishBtn];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.searchResultTableView];
    [self.view addSubview:self.indexView];
    [self.view addSubview:self.selectView];
    [self loadData];
    [self handleBlock];
}

- (void)setAddMembers:(BOOL)addMembers{
    _addMembers = addMembers;
    self.title = _addMembers?@"选择好友":@"可删除成员";
}

- (void)rightBarButtonItemAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadData{
    //假数据：产生10个数字+三个随机字母
    NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:0];
   for (NSInteger i =0; i<5; i++) {
        HHContactsModel *model = [[HHContactsModel alloc] init];
        model.nickName = [NSString stringWithFormat:@"%@%ld", @"召唤师", (long)i];
        model.headUrl = @"qunliao";
        [tempArray addObject:model];
    }
    for (NSInteger i =0; i<5; i++) {
        HHContactsModel *model = [[HHContactsModel alloc] init];
        model.nickName = [NSString stringWithFormat:@"%@%ld", @"还有谁", (long)i];
        model.headUrl = @"group";
        [tempArray addObject:model];
    }
    self.dataArray = tempArray;
    self.tableView.dataListArray = tempArray;
    
    NSMutableArray* indexPathArray = [DataHelper getContactListSectionBy:self.tableView.dataListArray];
    // 配置索引数据
    if ([indexPathArray count]>0) {
        NSMutableArray *indexViewDataSource = [NSMutableArray array];
        for (NSString *str in indexPathArray) {
            [indexViewDataSource addObject:str];
        }
        self.indexView.dataSource = indexViewDataSource.copy;
    }
}

- (void)handleBlock{
    WeakSelf(self);
     //点击好友列表tableView
    self.tableView.selectMembersFinishTableView = ^(BOOL select, HHContactsModel * _Nonnull model) {
        if (select) {
            [weakself.membersArray addObject:model];
        }
        else{
            [weakself.membersArray removeObject:model];
        }
        [weakself updateRightBarButtonItem:weakself.membersArray];
        [weakself.selectView updateSubviewsLayout:weakself.membersArray];
        weakself.membersArray = [NSMutableArray arrayWithArray:weakself.membersArray];
    };
    
    //点击搜索结果tableView
    self.searchResultTableView.selectMembersFinishTableView = ^(BOOL select, HHContactsModel * _Nonnull model) {
        weakself.selectView.textfield.text = @"";
        if (select) {
            [weakself.membersArray addObject:model];
        }
        else{
            [weakself.membersArray removeObject:model];
        }
        [weakself updateRightBarButtonItem:weakself.membersArray];
        [weakself.selectView updateSubviewsLayout:weakself.membersArray];
        [weakself clearSearchResult];
        [weakself.tableView reloadData];
    };
}

- (void)finishButtonClick:(UIButton *)btn{
    if (self.finishSelectNewGroupMembers) {
        self.finishSelectNewGroupMembers(self.membersArray,_addMembers);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

//刷新完成按钮状态
- (void)updateRightBarButtonItem:(NSMutableArray *)membersArray {
    if (membersArray.count > 0) {
        _finishBtn.userInteractionEnabled = YES;
        [_finishBtn setTitle:[NSString stringWithFormat:@"完成(%ld)",membersArray.count] forState:(UIControlStateNormal)];
        [_finishBtn setBackgroundColor:ColorManage.greenTextColor];
    }
    else {
        _finishBtn.userInteractionEnabled = NO;
        [_finishBtn setTitle:@"完成" forState:(UIControlStateNormal)];
        [_finishBtn setBackgroundColor:RGB_GrayColor];
    }
}

#pragma mark - SelectMemberWithSearchView delegate
//移除选中的cell
- (void)removeMemberFromSelectArray:(HHContactsModel *)member indexPath:(NSIndexPath *)indexPath {
    [_dataArray enumerateObjectsUsingBlock:^(HHContactsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.nickName isEqualToString:member.nickName]) {
            obj.isChoose = NO;
            [self.membersArray removeObject:obj];
            [self updateRightBarButtonItem:self.membersArray];
            [self.tableView reloadData];

        }
    }];
}

//搜索框输入文本
- (void)textFieldContentChanged:(UITextField *)textField{
    if (textField.text.length <= 0) {
        [self clearSearchResult];
        return;
    }
    _searchResultTableView.hidden = NO;
    _tableView.hidden = YES;
    NSString *inputWord = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];;
    if (!inputWord.length){
        textField.text = @"";
        return;
    }

    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(automaticCompletionWithKeyword:) object:inputWord];
    //获取是否有高亮
    UITextRange *selectedRange = [textField markedTextRange];
    if(!selectedRange){
        [self automaticCompletionWithKeyword:inputWord];
    }
}
- (void)clearSearchResult{
    _searchResultTableView.dataListArray = [NSMutableArray arrayWithCapacity:0];
    _searchResultTableView.hidden = YES;
    _tableView.hidden = NO;
}

//TODO:自动补全接口
- (void)automaticCompletionWithKeyword:(NSString *)inputWord {
    [self.searchResultArr removeAllObjects];
    NSMutableArray *tempResults = [NSMutableArray array];
    NSMutableArray *contactsSource = [NSMutableArray arrayWithArray:self.dataArray];
    NSUInteger searchOptions = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch;

     for (HHContactsModel * model in contactsSource) {
            [tempResults addObject:model];
        }
    for (int i = 0; i < tempResults.count; i++) {
        NSString *storeString = [[(HHContactsModel *)tempResults[i] nickName] pinyinOfName];
        NSString *wordString = [inputWord pinyinOfName];
        NSRange storeRange = NSMakeRange(0, storeString.length);
        if (storeString.length < storeRange.length) {
            continue;
        }
        NSRange foundRange = [storeString rangeOfString:wordString options:searchOptions range:storeRange];
        if (foundRange.length) {
            [self.searchResultArr addObject:tempResults[i]];
        }
    }
    self.searchResultTableView.dataListArray = self.searchResultArr;
}


#pragma mark - Getter
- (NSMutableArray *)membersArray{
    if (!_membersArray) {
        _membersArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _membersArray;
}

- (NSMutableArray *)searchResultArr{
    if (!_searchResultArr) {
        _searchResultArr = [NSMutableArray arrayWithCapacity:0];
    }
    return _searchResultArr;
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArray;
}

- (SCIndexView *)indexView{
    if (!_indexView) {
        _indexView = [[SCIndexView alloc] initWithTableView:self.tableView configuration:[SCIndexViewConfiguration configurationWithIndexViewStyle:self.indexViewStyle]];
        _indexView.translucentForTableViewInNavigationBar = YES;
        _indexView.delegate = self;
    }
    return _indexView;
}

- (HHSelectMembersTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHSelectMembersTableView alloc] initWithFrame:CGRectMake(0, self.selectView.bottom+0.5, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _tableView.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return _tableView;
}

- (SelectMemberWithSearchView *)selectView{
    if (!_selectView) {
        _selectView = [[SelectMemberWithSearchView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, 52)];
        _selectView.delegate = self;        
    }
    return _selectView;
}

- (HHSelectMembersTableView *)searchResultTableView{
    if (!_searchResultTableView) {
        _searchResultTableView = [[HHSelectMembersTableView alloc] initWithFrame:CGRectMake(0, self.selectView.bottom+0.5, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStylePlain];
        _searchResultTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _searchResultTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _searchResultTableView.backgroundColor = ColorManage.grayBackgroundColor;
        _searchResultTableView.hidden = YES;
    }
    return _searchResultTableView;
}



- (UIButton *)finishBtn {
    if (!_finishBtn) {
         _finishBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _finishBtn.frame = CGRectMake(0, 0, 80, 30);
        _finishBtn.layer.masksToBounds = YES;
        _finishBtn.layer.cornerRadius = 3;
        _finishBtn.userInteractionEnabled = NO;
        _finishBtn.timeInterval = 2;
        _finishBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_finishBtn setTitle:@"完成" forState:(UIControlStateNormal)];
        [_finishBtn setBackgroundColor:RGB_Color(222, 223, 224)];
        [_finishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_finishBtn addTarget:self action:@selector(finishButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _finishBtn;
}
@end
