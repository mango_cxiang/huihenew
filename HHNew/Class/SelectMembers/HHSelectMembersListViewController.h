//
//  HHSelectMembersListViewController.h
//  HHNew
//
//  Created by Liubin on 2020/5/6.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^finishSelectNewGroupMembers)(NSMutableArray *array,BOOL add);
@interface HHSelectMembersListViewController : BaseViewController
@property (copy, nonatomic) finishSelectNewGroupMembers finishSelectNewGroupMembers;
@property (assign, nonatomic) BOOL addMembers;

@end

NS_ASSUME_NONNULL_END
