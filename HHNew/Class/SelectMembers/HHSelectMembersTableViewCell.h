//
//  HHAddLimitMembersTableViewCell.h
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HHContactsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface HHSelectMembersTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
- (IBAction)clickSelectBtn:(id)sender;
@property (strong, nonatomic) HHContactsModel * contactsModel;
@property (weak, nonatomic) IBOutlet UIView *lineView;

@end

NS_ASSUME_NONNULL_END
