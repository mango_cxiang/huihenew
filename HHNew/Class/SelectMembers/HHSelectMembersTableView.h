//
//  HHAddLimitMembersTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HHContactsModel;
NS_ASSUME_NONNULL_BEGIN
//最终选中的成员数组
typedef void(^addMembersFinishTableView)(NSMutableArray *membersArray);
//单次选中或取消选中的成员
typedef void(^selectMembersFinishTableView)(BOOL select, HHContactsModel *model);


@interface HHSelectMembersTableView : UITableView

@property (copy, nonatomic) addMembersFinishTableView addMembersFinishTableView;
@property (copy, nonatomic) selectMembersFinishTableView selectMembersFinishTableView;

@property (strong, nonatomic) UILabel *footerView;

@property (strong, nonatomic) NSMutableArray *dataListArray;
@end

NS_ASSUME_NONNULL_END
