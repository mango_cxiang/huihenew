//
//  HHAddLimitMembersTableViewCell.m
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHSelectMembersTableViewCell.h"

@implementation HHSelectMembersTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.headImgView.layer.masksToBounds = YES;
    self.headImgView.layer.cornerRadius = 5;
    
    self.nameLabel.textColor = ColorManage.text_color;
    self.nameLabel.font = kRegularFont(16);
    
    self.lineView.backgroundColor = ColorManage.lineViewColor;
    self.selectBtn.userInteractionEnabled = NO;
}

- (void)setContactsModel:(HHContactsModel *)contactsModel{
    _contactsModel = contactsModel;
    NSString *headUrl = contactsModel.headUrl;
    if([headUrl hasPrefix:@"http"]){
    [self.headImgView sd_setImageWithURL:[NSURL URLWithString:headUrl] placeholderImage:nil options:SDWebImageRefreshCached];
    }else{
      self.headImgView.image = [UIImage imageNamed:headUrl];
    }
    self.nameLabel.text = contactsModel.markName.length >0?contactsModel.markName:contactsModel.nickName;
    if(contactsModel.isChoose){
           [self.selectBtn setImage:[UIImage imageNamed:@"select_yes"] forState:UIControlStateNormal];
       }
       else{
           [self.selectBtn setImage:[UIImage imageNamed:@"select_no"] forState:UIControlStateNormal];
       }
}

- (IBAction)clickSelectBtn:(id)sender {
    
}
@end
