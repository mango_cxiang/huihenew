//
//  HMKPacketListCell.h
//  huihe
//
//  Created by 宋平 on 2019/12/27.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HHPacketListModel;
typedef void(^clickHHPacketListCell)(void);
NS_ASSUME_NONNULL_BEGIN

@interface HHPacketListCell : UITableViewCell
@property(nonatomic,strong) UIImageView *iconImageView;
@property(nonatomic,strong) UILabel *timeLabel;
@property(nonatomic,strong) UILabel *nameLabel;
@property(nonatomic,strong) UILabel *huiheLabel;
@property(nonatomic,strong) UIImageView *packetImage;
@property(nonatomic,strong) UILabel *moneyLabel;
@property(nonatomic,strong) UIButton *clickButton;
@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UILabel *getLabel;

@property(nonatomic,copy) clickHHPacketListCell clickHHPacketListCell;
@property(nonatomic,strong) HHPacketListModel *packModel;

@end

NS_ASSUME_NONNULL_END
