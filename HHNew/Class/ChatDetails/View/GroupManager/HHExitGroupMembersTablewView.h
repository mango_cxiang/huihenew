//
//  HHExitGroupMembersTablewView.h
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HHContactsModel;
NS_ASSUME_NONNULL_BEGIN

typedef void(^clickExitGroupMembersTableViewCell)(NSIndexPath *indexPath,HHContactsModel *model);

@interface HHExitGroupMembersTablewView : UITableView
@property (strong, nonatomic) NSMutableArray *dataListArry;
@property (copy, nonatomic) clickExitGroupMembersTableViewCell clickExitGroupMembersTableViewCell;
@property (nonatomic,strong) UILabel * footerView;

@end

NS_ASSUME_NONNULL_END
