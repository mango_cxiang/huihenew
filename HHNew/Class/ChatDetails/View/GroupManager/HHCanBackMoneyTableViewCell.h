//
//  HHCanBackMoneyTableViewCell.h
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HHRecodModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef void(^clickBackMoneyBtn)(void);
typedef void(^clickGetMoneyBtn)(void);

@interface HHCanBackMoneyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *leftHeadImgView;
@property (weak, nonatomic) IBOutlet UILabel *leftNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *rightImgView;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *sendLabel;
@property (weak, nonatomic) IBOutlet UILabel *getLabel;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *getBtn;
- (IBAction)backMoney:(id)sender;
- (IBAction)getMoney:(id)sender;

@property (strong, nonatomic) HHRecodModel *model;

@property (copy, nonatomic) clickBackMoneyBtn clickBackMoneyBtn;
@property (copy, nonatomic) clickGetMoneyBtn clickGetMoneyBtn;
@property (weak, nonatomic) IBOutlet UIView *lineView;

@end

NS_ASSUME_NONNULL_END
