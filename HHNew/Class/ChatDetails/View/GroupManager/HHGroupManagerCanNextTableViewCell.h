//
//  HHGroupManagerCanNextTableViewCell.h
//  HHNew
//
//  Created by Liubin on 2020/4/27.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HHGroupManagerCanNextTableViewCell : UITableViewCell
@property (nonatomic,strong) UILabel *leftLabel;
@property (nonatomic,strong) UILabel *rightLabel;
@property (nonatomic,strong) UIView *lineView;
@property (nonatomic,strong) UIImageView *nextImgView;

@end

NS_ASSUME_NONNULL_END
