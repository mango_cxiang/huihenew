//
//  HMKGroupManageCell.h
//  huihe
//
//  Created by 宋平 on 2019/11/14.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMKGroupManageCell : UITableViewCell
@property (nonatomic,strong) UILabel *leftLabel;
@property (nonatomic,strong) UISwitch *switchBtn;
@end

NS_ASSUME_NONNULL_END
