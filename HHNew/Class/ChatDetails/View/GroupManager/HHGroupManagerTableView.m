//
//  HHGroupManagerTableView.m
//  HHNew
//
//  Created by Liubin on 2020/4/27.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupManagerTableView.h"
#import "HMKGroupManageCell.h"
#import "HHGroupManagerCanNextTableViewCell.h"
@interface HHGroupManagerTableView ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, copy) NSString *redPactAmount;
@property (nonatomic, copy) NSString *isDelay;
@property (nonatomic, copy) NSString *delayedEntryAmount;
@property (nonatomic, copy) NSString *status;
@end
@implementation HHGroupManagerTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.backgroundColor = ColorManage.grayBackgroundColor;
        self.delegate = self;
        self.dataSource = self;
        [self setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
    }
    return self;
}

- (void)setDataDict:(NSDictionary *)dataDict{
    _dataDict = [NSDictionary dictionaryWithDictionary:dataDict];
    self.status = dataDict[@"status"];
    self.redPactAmount = dataDict[@"delayedEntryAmount"];
    [self reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    if ([[HMJFMDBGroupDataModel sharedInstance] checkUserGroupMemberOwnerGroupId:self.groupModel.groupId  contactId:[NSString acquireUserId]]) {
        return 10;
//    } else {
//        return 9;
//    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 9) {
        return 3;
    }
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==0 ||section==8) {
        return 40;
    }else if(section==1||section==2 || section==3 || section == 7){
        return 30;
    }else if (section==4||section==5 ||section==6 ){
        return 5;
    }
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}

- (UIView *)createViewWithFrame:(CGRect)frame{
    UIView *bgView = [[UIView alloc]initWithFrame:frame];
    bgView.backgroundColor = ColorManage.grayBackgroundColor;
    return bgView;
}

- (UILabel *)createLabelWithFrame:(CGRect)frame{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.numberOfLines = 0;
    label.textColor = ColorManage.grayTextColor_Deault;
    label.font = kRegularFont(13);
    return label;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section==0 || section == 8) {
        UIView * bgView = [self createViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
        UILabel * label = [self createLabelWithFrame:CGRectMake(15, 0, SCREEN_WIDTH-30, 40)];
        label.centerY = bgView.centerY;
        [bgView addSubview:label];
        if (section == 0) {
            label.text = @"启用后，群成员需群主确认才能邀请朋友进群。扫描二维码进群将同时停用。";
        }else {
            label.text = @"开启“红包延迟入账”功能后，已被领取但待入账的红包可申请退回。";
        }
        return bgView;
    }else if (section==1||section==2||section==7 || section==3 || section == 8){
        UIView * bgView = [self createViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
        UILabel * label = [self createLabelWithFrame:CGRectMake(15, 0, SCREEN_WIDTH-30, 20)];
        label.centerY = bgView.centerY;
        [bgView addSubview:label];
        if (section==1) {
            label.text = @"开启后，普通成员间无法通过群聊加好友";
        }else if(section==2){
            label.text = @"开启后，单个红包直接显示金额";
        }else if(section==3){
            label.text = @"开启后，领取的红包将延迟30分钟存入零钱账户";
        }else if(section == 7){
            label.text = @"超过10分钟未被领取的红包";
        }
        return bgView;
    }else if (section==4||section==5||section==6){
        UIView * bgView = [self createViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 5)];
        return bgView;
    }else{
        return nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0||indexPath.section ==1||indexPath.section == 2) {
        HMKGroupManageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HMKGroupManageCell"];
        if (cell==nil) {
            cell = [[HMKGroupManageCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HMKGroupManageCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if (indexPath.section==0) {
//            cell.switchBtn.on = [self.groupModel.joinStatus isEqualToString:@"1"]?YES:NO;
            cell.leftLabel.text = @"群聊邀请确认";
        }else if (indexPath.section==1){
//            cell.switchBtn.on = [self.groupModel.protectStatus isEqualToString:@"1"]?YES:NO;
            cell.leftLabel.text = @"成员保护模式";
        }else if  (indexPath.section==2){
//            cell.switchBtn.on = [self.groupModel.redShowMoney isEqualToString:@"1"]?YES:NO;
            cell.leftLabel.text = @"红包显示金额";
        }else if  (indexPath.section==3){
//            cell.switchBtn.on = [self.groupModel.isDelay isEqualToString:@"1"]?YES:NO;
            cell.leftLabel.text = @"红包延迟入账";
        }
        cell.backgroundColor = ColorManage.darkBackgroundColor;
        [cell.switchBtn addTarget:self action:@selector(switchBtnClick:) forControlEvents:UIControlEventValueChanged];
        return cell;
    }
    else{
        HHGroupManagerCanNextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HHGroupManagerCanNextTableViewCell"];
        if (cell==nil) {
            cell = [[HHGroupManagerCanNextTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HHGroupManagerCanNextTableViewCell"];
        }
        cell.lineView.hidden = YES;
        cell.rightLabel.hidden = YES;
        if (indexPath.section == 3) {
            cell.leftLabel.text = @"红包延迟入账";
//            if ([_status isEqualToString:@"1"]) {
                cell.rightLabel.hidden = NO;
                if ([_redPactAmount integerValue] > 0) {
                    cell.rightLabel.text = [NSString stringWithFormat:@"达%@元延迟入账", _redPactAmount];
                }
                else{
                    cell.rightLabel.text = [NSString stringWithFormat:@""];
                }
//            }
        }else if (indexPath.section == 4) {
            cell.leftLabel.text = @"群成员禁言";
        } else if (indexPath.section == 5) {
            cell.leftLabel.text = @"禁止领取红包";
        } else if (indexPath.section == 6) {
            cell.leftLabel.text = @"消息与记录";
        }else if (indexPath.section==7){
            cell.leftLabel.text = @"未领取的红包";
        }else if (indexPath.section==8){
            cell.leftLabel.text = @"可退回的红包";
        } else{
            cell.lineView.hidden = NO;
            if (indexPath.row == 0) {
                cell.leftLabel.text = @"辅助功能";
            } else if (indexPath.row == 1) {
                cell.leftLabel.text = @"设置管理员";
            } else if (indexPath.row == 2) {
                cell.leftLabel.text = @"群主管理权转让";
            }
        }
        cell.backgroundColor = ColorManage.darkBackgroundColor;
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section <3) {
        return;
    }
    //红包延迟入账
    if (indexPath.section == 3) {
//        if ([_status isEqualToString:@"1"]) {
            [self selectRedTime];
//        }else {
//            [[HMJShowLoding sharedInstance] showText:@"该功能维护中"];
//        }
        return;
    }
    if (self.clickGroupManagerTableView) {
        self.clickGroupManagerTableView(indexPath);
    }
}


-(void)switchBtnClick:(id)sender{
//    HMKGroupManageCell *cell = (HMKGroupManageCell *)[sender superview];
//    NSIndexPath *indexPath = [self indexPathForCell:cell];
//    UISwitch *switchView = (UISwitch *)sender;
//    if (indexPath.section==0) {
//        if ([switchView isOn]) {
//            [self switchOnRequset:@"1" Status:@"1"];
//        }else{
//            [self switchOnRequset:@"1" Status:@"0"];
//        }
//    }else if (indexPath.section==1){
//        if ([switchView isOn]) {
//            [self switchOnRequset:@"2" Status:@"1"];
//        }else{
//            [self switchOnRequset:@"2" Status:@"0"];
//        }
//    }else if (indexPath.section==2){
//        if ([switchView isOn]) {
//            [self switchOnRequset:@"2" Status:@"1"];
//        }else{
//            [self switchOnRequset:@"2" Status:@"0"];
//        }
//    }
//    else{
//
//    }
}

-(void)switchOnRequset:(NSString *)type Status:(NSString *)status{
//    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//    [params setValue:[NSString acquireUserId] forKey:@"userId"];
//    [params setValue:[NSString stringWithFormat:@"%ld",(long)self.groupId] forKey:@"groupId"];
//    [params setValue:type forKey:@"type"];
//    [params setValue:status forKey:@"status"];
//    [HMJBusinessManager TotalGET:@"/group/groupSetting" Parameters:params host:[[AppSetting sharedAppSetting] newServiceUrlStr] success:^(NSDictionary *dic, NSInteger code) {
//        
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//        
//    }];
}

//TODO：设置红包延迟入账
- (void)selectRedTime {
    WeakSelf(self);
    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:0];
    [arr addObject:@"达10元延迟"];
    [arr addObject:@"达50元延迟"];
    [arr addObject:@"达100元延迟"];
    [arr addObject:@"达200元延迟"];
    [arr addObject:@"关闭红包延迟到账"];

    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
//    [param setValue:[NSString acquireUserId] forKey:@"userId"];
//    [param setValue:self.groupModel.groupId forKey:@"groupId"];
    [param setValue:[NSString stringWithFormat:@"%d", 60 * 30 * 1000] forKey:@"delayedEntryTime"];
    NSString *urlStr = @"";
//    NSString *urlStr = [[[AppSetting sharedAppSetting] hostUrlStr] stringByReplacingOccurrencesOfString:@"bqapi/wx" withString:@"hapi"];
    ZGQActionSheetView *sheetView = [[ZGQActionSheetView alloc] initWithOptions:arr completion:^(NSInteger index) {
           switch (index) {
            case 0:{
                       if ([weakself.redPactAmount integerValue] == 10)return;
                       [param setValue:@"10" forKey:@"delayedEntryAmount"];
                       [self sendRequestUrlStr:urlStr param:param];
                   }
                break;
            case 1: {
                       if ([weakself.redPactAmount integerValue] == 50)return;
                       [param setValue:@"50" forKey:@"delayedEntryAmount"];
                       [self sendRequestUrlStr:urlStr param:param];
                   }
                   break;
            case 2: {
                       if ([weakself.redPactAmount integerValue] == 100)return;
                       [param setValue:@"100" forKey:@"delayedEntryAmount"];
                       [self sendRequestUrlStr:urlStr param:param];
                   }
                   break;
            case 3: {
                        if ([weakself.redPactAmount integerValue] == 200)return;
                        [param setValue:@"200" forKey:@"delayedEntryAmount"];
                        [self sendRequestUrlStr:urlStr param:param];
                    }
                   break;
            case 4: {
                if ([weakself.redPactAmount integerValue] == 0)return;
                       [self sendRequestUrlStr:urlStr param:param];
                    }
                break;
            default:
                   break;
               }
           } cancel:^{
               
           }];
    sheetView.showSampleView = YES;
    sheetView.sampleTitleName = @"设置红包延迟金额";
    [sheetView show];
}

- (void)sendRequestUrlStr:(NSString *)urlStr param:(NSDictionary *)param{
//    [HMJBusinessManager TotalPOST:@"/group/setRedPacketDelayed" parameters:param withHost:urlStr withHUDShow:NO withHUDHide:NO mediaDatas:nil success:^(NSDictionary *dic, NSInteger code) {
//        NSString *codeStr = [NSString stringWithFormat:@"%@",[dic objectForKey:@"code"]];
//        //        NSDictionary *dataInfo = [dic objectForKey:@"data"];
//        if (codeStr.integerValue == 1) {
            self.redPactAmount = param[@"delayedEntryAmount"]?param[@"delayedEntryAmount"]:@"";
            [self reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:3]] withRowAnimation:UITableViewRowAnimationNone];
//        }else {
//
//            [[HMJShowLoding sharedInstance] showText:dic[@"data"][@"info"]];
//        }
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//    }];
}


@end
