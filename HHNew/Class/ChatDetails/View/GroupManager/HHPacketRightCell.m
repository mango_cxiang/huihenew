//
//  HMKPacketRightCell.m
//  huihe
//
//  Created by 宋平 on 2019/12/27.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HHPacketRightCell.h"
#import "HHPacketListModel.h"
@implementation HHPacketRightCell
- (instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = ColorManage.darkBackgroundColor;
        [self cellInit];
    }
    return self;
}
-(void)cellInit
{
    //时间
    UILabel * timeLabel = [[UILabel alloc] init];
    timeLabel.font = kRegularFont(14);
    timeLabel.textAlignment = NSTextAlignmentCenter;
    timeLabel.textColor = ColorManage.grayBlackTextColor;
    timeLabel.backgroundColor = [RGB_Color(242, 242, 242) colorWithAlphaComponent:0.4];
    timeLabel.layer.masksToBounds = YES;
    timeLabel.layer.cornerRadius = 3;
    [self addSubview:timeLabel];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top).offset(20);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.height.mas_equalTo(20);
    }];
    self.timeLabel = timeLabel;
    //头像
    UIImageView * iconImageView = [[UIImageView alloc] init];
    iconImageView.layer.masksToBounds = YES;
    iconImageView.layer.cornerRadius = 5;
    iconImageView.userInteractionEnabled = YES;
    [self addSubview:iconImageView];
    [iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-15);
        make.top.mas_equalTo(self.mas_top).offset(50);
        make.width.height.mas_equalTo(40);
    }];
    self.iconImageView = iconImageView;
    UIImageView * packetImage = [[UIImageView alloc] init];
    packetImage.image = [UIImage imageNamed:@"hb_zz_bkg_mine"];
    packetImage.userInteractionEnabled = YES;
    [self addSubview:packetImage];
    [packetImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.iconImageView.mas_left).offset(-5);
        make.top.mas_equalTo(self.iconImageView.mas_top);
        make.width.mas_equalTo(230);
        make.height.mas_equalTo(230/2.6);
    }];
    self.packetImage = packetImage;
    
    UIImageView *smallPacket = [[UIImageView alloc] init];
    smallPacket.image = [UIImage imageNamed:@"hb_bg"];
    smallPacket.userInteractionEnabled = YES;
    smallPacket.clipsToBounds = YES;
    smallPacket.layer.cornerRadius = 2;
    smallPacket.contentMode = UIViewContentModeScaleAspectFill;
//    [smallPacket setContentScaleFactor:[[UIScreen mainScreen] scale]];
    [self.packetImage addSubview:smallPacket];
    [smallPacket mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.packetImage.mas_left).offset(10);
        make.centerY.mas_equalTo(self.packetImage.mas_centerY).offset(-10);
        make.width.mas_equalTo(35);
        make.height.mas_equalTo(45);
    }];
    
    UILabel * title = [[UILabel alloc] init];
    title.lineBreakMode = NSLineBreakByTruncatingTail;
    title.font = kRegularFont(16);
    title.textColor = UIColor.whiteColor;
    [self.packetImage addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(smallPacket.mas_right).offset(10);
        make.right.mas_equalTo(self.packetImage.mas_right).offset(-10);
        make.top.mas_equalTo(self.packetImage.mas_top).offset(12);
        make.height.mas_equalTo(20);
    }];
    self.titleLabel = title;
    
    UILabel * get = [[UILabel alloc] init];
    get.font = kRegularFont(12);
    get.text = @"领取红包";
    get.font = kRegularFont(12);
    get.textColor = UIColor.whiteColor;
    [self.packetImage addSubview:get];
    [get mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(title.mas_left).offset(0);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(5);
        make.height.mas_equalTo(20);
    }];
    self.getLabel = get;
    
    UILabel * huiheLabel = [[UILabel alloc] init];
    huiheLabel.text = @"会合红包";
    huiheLabel.font = kRegularFont(11);
    huiheLabel.textColor = RGB_LightGrayColor;
    [self.packetImage addSubview:huiheLabel];
    [huiheLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.packetImage.mas_left).offset(10);
        make.bottom.mas_equalTo(self.packetImage.mas_bottom);
        make.height.mas_equalTo(20);
    }];
    self.huiheLabel = huiheLabel;
    UILabel * moneyLabel = [[UILabel alloc] init];
    moneyLabel.textColor = ColorManage.darkBackgroundColor;
    moneyLabel.font = kRegularFont(14);
    [self.packetImage addSubview:moneyLabel];
    [moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(smallPacket.mas_right).offset(15);
        make.centerY.mas_equalTo(smallPacket.mas_centerY);
    }];
    self.moneyLabel = moneyLabel;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [self.packetImage addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.packetImage.mas_left);
        make.right.mas_equalTo(self.packetImage.mas_right);
        make.top.mas_equalTo(self.packetImage.mas_top);
        make.bottom.mas_equalTo(self.packetImage.mas_bottom);
    }];
    self.clickButton = button;
}

- (void)setPackModel:(HHPacketListModel *)packModel{
    _packModel = packModel;
    [self.iconImageView xxd_setHeadImageView:self.iconImageView imgUrl:packModel.headUrl];
    //    self.nameLabel.text = packModel.name;
    self.titleLabel.text = packModel.content;
    self.timeLabel.text = [NSString stringWithFormat:@"%@  ",packModel.sendTime];
    self.moneyLabel.text = packModel.redPacketCount;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
