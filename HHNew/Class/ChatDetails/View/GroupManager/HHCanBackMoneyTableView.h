//
//  HHCanBackMoneyTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HHRecodModel;
NS_ASSUME_NONNULL_BEGIN

typedef void(^backMoney)(HHRecodModel *model);
typedef void(^getMoney)(HHRecodModel *model);

@interface HHCanBackMoneyTableView : UITableView
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (copy, nonatomic) backMoney backMoney;
@property (copy, nonatomic) getMoney getMoney;

@end

NS_ASSUME_NONNULL_END
