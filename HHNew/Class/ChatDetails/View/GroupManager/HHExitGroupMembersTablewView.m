//
//  HHExitGroupMembersTablewView.m
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHExitGroupMembersTablewView.h"
#import "HHFriendsTableViewCell.h"
#import "HHContactsModel.h"

@interface HHExitGroupMembersTablewView ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) NSMutableArray *indexPathArray;
@end
@implementation HHExitGroupMembersTablewView
- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.delegate = self;
        self.dataSource = self;
    }
    return self;
}

- (void)setDataListArry:(NSMutableArray *)dataListArry{
    _dataListArry = dataListArry;
    [self createUI:dataListArry.count];
    [self reloadData];
}

- (void)createUI:(NSInteger)count{
    self.footerView.frame = CGRectMake(0, 20, SCREEN_WIDTH,80);
    self.footerView.font = SystemFont(14);
    [self.footerView setTextAlignment:NSTextAlignmentCenter];
    self.footerView.textColor = ColorManage.grayTextColor_Deault;
    self.tableFooterView = self.footerView;
    self.footerView.text = [NSString stringWithFormat:@"%zi位",count];
}

#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataListArry count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellName = @"HHFriendsTableViewCell";
    HHFriendsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if(!cell) {
        cell = LoadCell(@"HHFriendsTableViewCell");
    }
    cell.lineView.hidden = NO;
    cell.nextImgaeView.hidden = YES;
    HHContactsModel *model = self.dataListArry[indexPath.row];
    cell.contactsModel = model;
    cell.selectedBackgroundView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    HHContactsModel *model = self.dataListArry[indexPath.row];
    if (self.clickExitGroupMembersTableViewCell) {
        self.clickExitGroupMembersTableViewCell(indexPath,model);
    }
}

- (NSMutableArray *)indexPathArray{
    if (!_indexPathArray) {
        _indexPathArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _indexPathArray;
}

- (UILabel *)footerView {
    if (!_footerView) {
        _footerView = [[UILabel alloc] init];
    }
    return _footerView;
}

@end
