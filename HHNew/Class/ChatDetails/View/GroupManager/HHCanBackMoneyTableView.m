//
//  HHCanBackMoneyTableView.m
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHCanBackMoneyTableView.h"
#import "HHCanBackMoneyTableViewCell.h"
#import "HHRecodModel.h"
@interface HHCanBackMoneyTableView ()<UITableViewDelegate,UITableViewDataSource>

@end
@implementation HHCanBackMoneyTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.delegate = self;
        self.dataSource = self;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
    }
    return self;
}

- (void)setDataArray:(NSMutableArray *)dataArray{
    _dataArray = dataArray;
    [self reloadData];
}


#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellName = @"HHCanBackMoneyTableViewCell";
    HHCanBackMoneyTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if(!cell) {
        cell = LoadCell(@"HHCanBackMoneyTableViewCell");
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    HHRecodModel *model = self.dataArray[indexPath.section];
    cell.model = model;
    WeakSelf(self)
    cell.clickBackMoneyBtn = ^{
        StrongSelf(weakself);
        if (strongweakself.backMoney) {
            strongweakself.backMoney(model);
        }
    };
    
    cell.clickGetMoneyBtn = ^{
        StrongSelf(weakself);
        if (strongweakself.getMoney) {
            strongweakself.getMoney(model);
        }
    };
    cell.backgroundColor = ColorManage.darkBackgroundColor;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 145;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = ColorManage.grayBackgroundColor;
    return view;
}

@end
