//
//  HHGroupManagerTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/27.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^clickGroupManagerTableView)(NSIndexPath *indexPath);

@interface HHGroupManagerTableView : UITableView
@property (nonatomic, copy) clickGroupManagerTableView clickGroupManagerTableView;
@property (nonatomic, strong) NSDictionary *dataDict;

@end


NS_ASSUME_NONNULL_END
