//
//  HMKGroupManageCell.m
//  huihe
//
//  Created by 宋平 on 2019/11/14.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKGroupManageCell.h"

@implementation HMKGroupManageCell
- (instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self groupManageCell];
    }
    return self;
}
-(void)groupManageCell
{
    self.leftLabel = [[UILabel alloc]init];
    self.leftLabel.textColor = ColorManage.text_color;
    self.leftLabel.font = [UIFont systemFontOfSize:16];
    [self addSubview:self.leftLabel];
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(15);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
    self.switchBtn = [[UISwitch alloc] init];
    self.switchBtn.on = YES;
    [self.switchBtn addTarget:self action:@selector(valueChanged) forControlEvents:(UIControlEventValueChanged)];
    [self addSubview:self.switchBtn];
    [self.switchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-15);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(30);
    }];
//    UIView *line = [[UIView alloc]init];
//    line.backgroundColor = ColorManage.grayBackgroundColor;
//    [self addSubview:line];
//    [line mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.mas_left).offset(15);
//        make.right.mas_equalTo(self.mas_right);
//        make.bottom.mas_equalTo(self.mas_bottom);
//        make.height.mas_equalTo(1);
//    }];
}

-(void)valueChanged
{
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
