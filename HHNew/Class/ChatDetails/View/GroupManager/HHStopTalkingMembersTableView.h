//
//  HHStopTalkingMembersTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/27.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HHContactsModel;
NS_ASSUME_NONNULL_BEGIN

typedef void(^clickUniversalFriendsListTableView)(NSIndexPath *indexPath,HHContactsModel *model);

@interface HHStopTalkingMembersTableView : UITableView

@property (nonatomic, copy) clickUniversalFriendsListTableView clickUniversalFriendsListTableView;
@property (strong, nonatomic) NSMutableArray *dataListArry;
@property (nonatomic,strong) UILabel * footerView;

@end

NS_ASSUME_NONNULL_END
