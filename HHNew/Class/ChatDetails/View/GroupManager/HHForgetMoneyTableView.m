//
//  HHForgetMoneyTableView.m
//  HHNew
//
//  Created by Liubin on 2020/4/30.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHForgetMoneyTableView.h"
#import "HHPacketListCell.h"
#import "HHPacketRightCell.h"
#import "HHPacketListModel.h"
@interface HHForgetMoneyTableView ()<UITableViewDelegate,UITableViewDataSource>

@end
@implementation HHForgetMoneyTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.delegate = self;
        self.dataSource = self;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
    }
    return self;
}

- (void)setDataArray:(NSMutableArray *)dataArray{
    _dataArray = [NSMutableArray arrayWithArray:dataArray];
    [self reloadData];
}


#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WeakSelf(self);
    NSLog(@"行数：%ld",(long)indexPath.row);
    HHPacketListModel *model = self.dataArray[indexPath.row];
    if ([model.userId isEqualToString:@"1"]) {
        HHPacketRightCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HHPacketRightCell"];
        if (cell==nil) {
            cell = [[HHPacketRightCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HHPacketRightCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
//        if ([self.overArr containsObject:model.packetId]) {
//            [cell.packetImage setImage:[UIImage imageNamed:@"hb_zz_bkg_mine_hui"]];
//        }else{
//            cell.packetImage.image = [UIImage imageNamed:@"hb_zz_bkg_mine"];
//        }
        cell.packModel = model;
        cell.clickHHPacketRightCell = ^{
            StrongSelf(weakself);
            if (strongweakself.clickMyPacketForgetMoneyCell) {
                strongweakself.clickMyPacketForgetMoneyCell(model);
            }
        };
        return cell;
    }else{
        HHPacketListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HHPacketListCell"];
        if (cell==nil) {
            cell = [[HHPacketListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HHPacketListCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
//        if ([self.overArr containsObject:model.packetId]) {
//            [cell.packetImage setImage:[UIImage imageNamed:@"hb_zz_bkg_other_hui"]];
//        }else{
//            cell.packetImage.image = [UIImage imageNamed:@"hb_zz_bkg_other"];
//        }
        cell.packModel = model;
        cell.clickHHPacketListCell = ^{
            StrongSelf(weakself);
            if (strongweakself.clickOtherPacketForgetMoneyCell) {
                strongweakself.clickOtherPacketForgetMoneyCell(model);
            }
        };
        
        return cell;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 145;
}



@end
