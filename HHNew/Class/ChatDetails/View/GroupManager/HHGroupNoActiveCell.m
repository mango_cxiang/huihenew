//
//  HMKGroupNoActiveCell.m
//  huihe
//
//  Created by 宋平 on 2019/11/16.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HHGroupNoActiveCell.h"

@implementation HHGroupNoActiveCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self groupNoActiveCell];
        self.selectedBackgroundView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];
    }
    return self;
}
-(void)groupNoActiveCell
{
    self.time = [[UILabel alloc]init];
    self.time.textColor = ColorManage.text_color;
    self.time.font = [UIFont systemFontOfSize:16];
    [self addSubview:self.time];
    [self.time mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(15);
        make.top.mas_equalTo(self.mas_top).offset(10);
        make.height.mas_equalTo(20);
    }];
    self.message = [[UILabel alloc]init];
    self.message.textColor = ColorManage.grayTextColor_Deault;
    self.message.font = [UIFont systemFontOfSize:13];
    [self addSubview:self.message];
    [self.message mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(15);
        make.top.mas_equalTo(self.time.mas_bottom).offset(3);
        make.height.mas_equalTo(20);
    }];
    UIImageView *rightImage = [[UIImageView alloc]init];
    rightImage.image = [UIImage imageNamed:@"next"];
    [self addSubview:rightImage];
    [rightImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).offset(-15);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.width.mas_equalTo(8);
        make.height.mas_equalTo(13);
    }];
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = ColorManage.lineViewColor;
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(15);
        make.right.mas_equalTo(self.mas_right);
        make.bottom.mas_equalTo(self.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
