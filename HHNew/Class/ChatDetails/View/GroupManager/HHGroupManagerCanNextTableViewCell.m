//
//  HHGroupManagerCanNextTableViewCell.m
//  HHNew
//
//  Created by Liubin on 2020/4/27.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupManagerCanNextTableViewCell.h"

@implementation HHGroupManagerCanNextTableViewCell
- (instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self groupManageNextCell];
        self.selectedBackgroundView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];;
    }
    return self;
}
-(void)groupManageNextCell
{
    
    self.leftLabel = [[UILabel alloc]init];
    self.leftLabel.textColor = ColorManage.text_color;
    self.leftLabel.font = [UIFont systemFontOfSize:16];
    [self.contentView addSubview:self.leftLabel];
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(15);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
    
    UIImageView *arrowImg = [MyUtils imageViewWithFrame:CGRectMake(SCREEN_WIDTH-8-15, 21, 8, 13) image:[UIImage imageNamed:@"next"] backColor:nil];
    [self.contentView addSubview:arrowImg];
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = ColorManage.grayBackgroundColor;
    [self.contentView addSubview:self.lineView];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView.mas_left).offset(0);
        make.right.mas_equalTo(self.contentView.mas_right);
        make.bottom.mas_equalTo(self.contentView.mas_bottom);
        make.height.mas_equalTo(1);
    }];
    
    self.rightLabel = [[UILabel alloc]init];
    self.rightLabel.textColor = ColorManage.grayTextColor_Deault;
    self.rightLabel.font = [UIFont systemFontOfSize:16];
    [self.contentView addSubview:self.rightLabel];
    self.rightLabel.hidden = YES;
    [self.rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(arrowImg.mas_left).offset(-15);
        make.centerY.mas_equalTo(self.contentView.mas_centerY);
    }];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
