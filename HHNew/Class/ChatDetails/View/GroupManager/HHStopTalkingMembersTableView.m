//
//  HHStopTalkingMembersTableView.m
//  HHNew
//
//  Created by Liubin on 2020/4/27.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHStopTalkingMembersTableView.h"
#import "DataHelper.h"
#import "HMJGroupMembersTableViewCell.h"
#import "HHContactsModel.h"

@interface HHStopTalkingMembersTableView ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, assign) BOOL translucent;
@property (strong, nonatomic) NSMutableArray *indexPathArray;
@end
@implementation HHStopTalkingMembersTableView


- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.delegate = self;
        self.dataSource = self;
        self.translucent = YES;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
    }
    return self;
}

- (void)setDataListArry:(NSMutableArray *)dataListArry{
    _dataListArry = [DataHelper getContactListDataBy:dataListArry];
    _indexPathArray = [DataHelper getContactListSectionBy:_dataListArry];
    [self createUI:dataListArry.count];
    [self reloadData];
}

- (void)createUI:(NSInteger)count{
    self.footerView.frame = CGRectMake(0, 20, SCREEN_WIDTH,80);
    self.footerView.font = SystemFont(14);
    [self.footerView setTextAlignment:NSTextAlignmentCenter];
    self.footerView.textColor = ColorManage.grayTextColor_Deault;
    self.tableFooterView = self.footerView;
    self.footerView.text = [NSString stringWithFormat:@"%zi位",count];
}

#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.indexPathArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataListArry[section] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellName = @"HHUniversalFiendListTableViewCell";
    HMJGroupMembersTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if(!cell) {
        cell = LoadCell(@"HMJGroupMembersTableViewCell");
    }
    cell.indexPath = indexPath;
    HHContactsModel *model = self.dataListArry[indexPath.section][indexPath.row];
    cell.contacts = model;
    cell.rightLabel.text = @"已禁言";
    cell.rightLabel.hidden = [model.allowSay intValue]>0?NO:YES;
    cell.backgroundColor = ColorManage.darkBackgroundColor;
    cell.selectedBackgroundView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] init];
    header.backgroundColor = ColorManage.grayBackgroundColor;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 200, 30)];
    label.text = [_indexPathArray objectAtIndex:section];
    label.textColor = ColorManage.grayTextColor_Deault;
    label.font = [UIFont systemFontOfSize:14];
    [header addSubview:label];
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    HHContactsModel *model = self.dataListArry[indexPath.section][indexPath.row];
    if (self.clickUniversalFriendsListTableView) {
        self.clickUniversalFriendsListTableView(indexPath,model);
    }
}

- (NSMutableArray *)indexPathArray{
    if (!_indexPathArray) {
        _indexPathArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _indexPathArray;
}

- (UILabel *)footerView {
    if (!_footerView) {
        _footerView = [[UILabel alloc] init];
    }
    return _footerView;
}


@end
