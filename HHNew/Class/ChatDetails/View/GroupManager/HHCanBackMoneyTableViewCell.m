//
//  HHCanBackMoneyTableViewCell.m
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHCanBackMoneyTableViewCell.h"

@implementation HHCanBackMoneyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.leftHeadImgView.layer.masksToBounds = YES;
    self.leftHeadImgView.layer.cornerRadius = 5;
    self.rightImgView.layer.masksToBounds = YES;
    self.rightImgView.layer.cornerRadius = 5;
    self.backBtn.layer.masksToBounds = YES;
    self.backBtn.layer.cornerRadius = 5;
    self.getBtn.layer.masksToBounds = YES;
    self.getBtn.layer.cornerRadius = 5;
    self.lineView.backgroundColor = ColorManage.lineViewColor;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(HHRecodModel *)model{
    [self.leftHeadImgView xxd_setHeadImageView:self.leftHeadImgView imgUrl:@"qunliao"];
    [self.rightImgView xxd_setHeadImageView:self.rightImgView imgUrl:@"group"];
    
    self.leftNameLabel.text = @"深圳世纪芒果合肥分公司产品经理张小龙";
    self.rightNameLabel.text = @"合肥世纪芒果公司产品经理张小龙";
    
    self.sendLabel.text = [NSString stringWithFormat:@"发送：%@",@"2020-04-22 15:50:55"];
    self.getLabel.text = [NSString stringWithFormat:@"领取：%@",@"2020-04-22 15:51:00"];
    self.moneyLabel.text = [NSString stringWithFormat:@"金额：¥%@",@"20"];
}

- (IBAction)backMoney:(id)sender {
    if (self.clickBackMoneyBtn) {
        self.clickBackMoneyBtn();
    }
}

- (IBAction)getMoney:(id)sender {
    if (self.clickGetMoneyBtn) {
        self.clickGetMoneyBtn();
    }
}
@end
