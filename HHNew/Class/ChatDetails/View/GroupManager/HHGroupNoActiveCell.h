//
//  HMKGroupNoActiveCell.h
//  huihe
//
//  Created by 宋平 on 2019/11/16.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HHGroupNoActiveCell : UITableViewCell
@property (nonatomic,strong) UILabel *time;
@property (nonatomic,strong) UILabel *message;

@end

NS_ASSUME_NONNULL_END
