//
//  HHForgetMoneyTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/30.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HHPacketListModel;
NS_ASSUME_NONNULL_BEGIN
typedef void(^clickOtherPacketForgetMoneyCell)(HHPacketListModel *model);
typedef void(^clickMyPacketForgetMoneyCell)(HHPacketListModel *model);
@interface HHForgetMoneyTableView : UITableView
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (nonatomic,strong) NSMutableArray *receivedArray;

@property (copy, nonatomic) clickOtherPacketForgetMoneyCell clickOtherPacketForgetMoneyCell;
@property (copy, nonatomic) clickMyPacketForgetMoneyCell clickMyPacketForgetMoneyCell;


@end

NS_ASSUME_NONNULL_END
