//
//  HHSingleChatDetailsTableView.m
//  HHNew
//
//  Created by Liubin on 2020/5/6.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHSingleChatDetailsTableView.h"
#import "HMJGroupTableViewCell.h"
#import "HMJChatFileTableViewCell.h"

static NSString *detailCell = @"detail";
static NSString *chatCell=@"cell";

@interface HHSingleChatDetailsTableView ()<UITableViewDelegate,UITableViewDataSource,HMJChatFileTableViewCellDelegate>

@end
@implementation HHSingleChatDetailsTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.delegate = self;
        self.dataSource = self;
        self.backgroundColor = ColorManage.darkBackgroundColor;
        UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH,SafeAreaBottomHeight+15)];
        self.tableFooterView = view;
        [self registerNib:[UINib nibWithNibName:@"HMJGroupTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:detailCell];
        [self registerNib:[UINib nibWithNibName:@"HMJChatFileTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:chatCell];

        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
    }
    return self;
}

- (void)setDataArray:(NSMutableArray *)dataArray{
    _dataArray = dataArray;
    [self reloadData];
}

#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return section == 0 ? 2 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == 0) {
        HMJChatFileTableViewCell * chatFileCell = [tableView dequeueReusableCellWithIdentifier:chatCell];
        chatFileCell.lineLabel.hidden = YES;
        if (!chatFileCell) {
            chatFileCell = LoadCell(@"HMJChatFileTableViewCell");
        }
        chatFileCell.delegate = self;
//        chatFileCell.destId = [NSString stringWithFormat:@"%zi",self.destId];
        if (indexPath.row == 0) {
            chatFileCell.leftLable.text = @"消息免打扰";
            chatFileCell.departmentLable.hidden = YES;
//            chatFileCell.switchBtn.on = [[HMJFMDBContactsDataModel sharedInstance] queryContactReceiveTipWithUid:[NSString stringWithFormat:@"%zi",self.destId]];
        } else {
            chatFileCell.leftLable.text = @"置顶聊天";
            chatFileCell.departmentLable.hidden = YES;
//            chatFileCell.switchBtn.on = (self.topFlag.integerValue == 0 ? NO : YES);
        }
        return chatFileCell;
    } else {
        HMJGroupTableViewCell *groupDetailCell = [tableView dequeueReusableCellWithIdentifier:detailCell];
        groupDetailCell.lineView.hidden = YES;
        if (!groupDetailCell) {
            groupDetailCell = LoadCell(@"HMJGroupTableViewCell");
        }
        if (indexPath.section==1) {
            groupDetailCell.nameLable.text= @"查找聊天记录";
            groupDetailCell.groupName.hidden=YES;

        } else if(indexPath.section==2) {
            groupDetailCell.nameLable.text= @"清空聊天记录";
            groupDetailCell.groupName.hidden=YES;
        } else if(indexPath.section==3) {
            groupDetailCell.nameLable.text = @"设置当前聊天背景";
            groupDetailCell.groupName.hidden=YES;
        } else if (indexPath.section==4) {
            groupDetailCell.nameLable.text = @"投诉";
            groupDetailCell.groupName.hidden=YES;
        }
        return groupDetailCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = ColorManage.grayBackgroundColor;
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = ColorManage.grayBackgroundColor;
    return view;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.clickSingleChatDetailsTableViewCell) {
        self.clickSingleChatDetailsTableViewCell(indexPath);
    }
}


@end
