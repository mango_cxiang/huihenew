//
//  HMJChatDetailInfoHeaderView.m
//  huihe
//
//  Created by changle on 2016/12/27.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJChatDetailInfoHeaderView.h"

@interface HMJChatDetailInfoHeaderView ()
@property (nonatomic,strong) UIImageView * iconImageView;
@property (nonatomic,strong) UILabel * nameLabel;
@end

@implementation HMJChatDetailInfoHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = ColorManage.darkBackgroundColor;
        self.userInteractionEnabled = YES;
        
        UIImageView *hearImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 15, 50, 50)];
        hearImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer * tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iconImageViewOnTap:)];
        [hearImageView addGestureRecognizer:tgr];
        [hearImageView viewWithRadis:5];
        hearImageView.image = PlaceholderImage;
        [self addSubview:hearImageView];
        self.iconImageView = hearImageView;
        
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(hearImageView.x, CGRectGetMaxY(hearImageView.frame)+5, hearImageView.width, 20)];
        nameLabel.textColor = ColorManage.black_grayTextColor_Deault;
        self.nameLabel = nameLabel;
        nameLabel.textAlignment = NSTextAlignmentCenter;
        nameLabel.font = kRegularFont(13);
        nameLabel.text = @"昵称";
        [self addSubview:nameLabel];
        self.height = CGRectGetMaxY(nameLabel.frame)+20;
        
        UIButton *addBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(hearImageView.frame)+20, hearImageView.y, 50, 50)];
        [addBtn setImage:[UIImage imageNamed:@"add_members"] forState:UIControlStateNormal];
        [addBtn addTarget:self action:@selector(clickToAdd) forControlEvents:UIControlEventTouchUpInside];
        [addBtn viewWithRadis:5];
        [self addSubview:addBtn];
    }
    return self;
}

- (void)setFriendsModel:(HHContactsModel *)friendsModel{
    [self.iconImageView xxd_setHeadImageView:self.iconImageView imgUrl:friendsModel.headUrl];
    _nameLabel.text = friendsModel.markName.length>0? friendsModel.markName:friendsModel.nickName;
}

- (void)iconImageViewOnTap:(UITapGestureRecognizer *)tgr{
    if ([self.delegate respondsToSelector:@selector(iconImageViewOnTap)]) {
        [self.delegate iconImageViewOnTap];
    }
    if (self.tapUserHeadImage) {
        self.tapUserHeadImage();
    }
}

//点击加号
- (void)clickToAdd{
    if ([self.delegate respondsToSelector:@selector(clickToAdd)]) {
        [self.delegate clickToAdd];
    }
    if (self.clickSingleChatDetailToAdd) {
        self.clickSingleChatDetailToAdd();
    }
}


- (void)creatHeaderViewWithContactId:(NSString *)contactId{
//    HHContactsModel * friendModel = (HHContactsModel *)[[HMJFMDBContactsDataModel sharedInstance] queryDataFromDataBaseWithContactId:contactId].firstObject;
//    [self.iconImageView sd_setImageWithURL:SDImageUrl(friendModel.headUrl) placeholderImage:PlaceholderImage options:SDWebImageRefreshCached];
//    if(friendModel.markName==nil || friendModel.markName.length==0){
//        //                    NSArray *messageArray = [[HMJFMDBMessageDataModel sharedInstance] queryDataFromDataBaseWithmsgId:message.msgId];
//        //                    HMJMessageModel *messageModel = messageArray.firstObject;
//        _nameLabel.text = friendModel.nickName;
//    }else{
//        _nameLabel.text = friendModel.markName;
//    }
}

@end
