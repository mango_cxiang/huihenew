//
//  HHSingleChatDetailsTableView.h
//  HHNew
//
//  Created by Liubin on 2020/5/6.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^clickSingleChatDetailsTableViewCell)(NSIndexPath *indexPath);

@interface HHSingleChatDetailsTableView : UITableView
@property (nonatomic ,strong) NSMutableArray *dataArray;
@property (nonatomic ,copy) clickSingleChatDetailsTableViewCell clickSingleChatDetailsTableViewCell;

@end

NS_ASSUME_NONNULL_END
