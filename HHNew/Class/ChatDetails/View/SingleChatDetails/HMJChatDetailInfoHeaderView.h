//
//  HMJChatDetailInfoHeaderView.h
//  huihe
//
//  Created by changle on 2016/12/27.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HHContactsModel.h"

typedef void(^tapUserHeadImage)(void);
typedef void(^clickSingleChatDetailToAdd)(void);

@protocol HMJChatDetailInfoHeaderViewDelegate <NSObject>

- (void)iconImageViewOnTap;
//点击加号
- (void)clickToAdd;

@end

@interface HMJChatDetailInfoHeaderView : UIView
@property (nonatomic,weak) id<HMJChatDetailInfoHeaderViewDelegate>delegate;
@property (nonatomic,strong) HHContactsModel *friendsModel;


@property (nonatomic, copy) tapUserHeadImage tapUserHeadImage;
@property (nonatomic, copy) clickSingleChatDetailToAdd clickSingleChatDetailToAdd;

- (void)creatHeaderViewWithContactId:(NSString *)contactId;
@end
