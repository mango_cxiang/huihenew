//
//  HHSingleChatRecordTableView.h
//  HHNew
//
//  Created by Liubin on 2020/5/8.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HHSingleChatRecordTableView : UITableView
@property (strong, nonatomic) NSMutableArray *dataListArry;

@end

NS_ASSUME_NONNULL_END
