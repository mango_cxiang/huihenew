//
//  HMJGroupTableViewCell.h
//  huihe
//
//  Created by mysun on 16/3/7.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
//群主管理权转让
@interface HMJGroupTableViewCell : BaseTableViewCell
//名字
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
//最右侧箭头
@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;

//群聊名字
@property (weak, nonatomic) IBOutlet UILabel *groupName;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthLayout;
@property (weak, nonatomic) IBOutlet UIView *lineView;


@end
