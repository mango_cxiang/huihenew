//
//  HMJGroupMemberCollectionViewCell.m
//  huihe
//
//  Created by changle on 2016/12/21.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJGroupMemberCollectionViewCell.h"
@interface HMJGroupMemberCollectionViewCell ()
@property (strong, nonatomic) IBOutlet UIImageView *headImgView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *startImgView;

@property (nonatomic,strong) UILongPressGestureRecognizer * longPressGestureRecognizer;
@end

@implementation HMJGroupMemberCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    //CGFloat padding = 5;
    _headImgView.contentMode = UIViewContentModeScaleAspectFill;
    _headImgView.layer.cornerRadius = 5;
    _headImgView.layer.masksToBounds = YES;
        
    self.nameLabel.font = kRegularFont(12);
    self.nameLabel.textColor = ColorManage.black_grayTextColor_Deault;
    
    self.longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressGestureRecognizer:)];
    [self addGestureRecognizer:self.longPressGestureRecognizer];//为cell添加手势
    self.longPressGestureRecognizer.minimumPressDuration = 1.0;//定义长按识别时长
}

- (void)addImg
{
    _headImgView.image = [UIImage imageNamed:@"add_members"];
    _headImgView.layer.cornerRadius = 0;
    _nameLabel.hidden = YES;
    _startImgView.hidden = YES;

}

- (void)delImg
{
    _headImgView.image = [UIImage imageNamed:@"reduce_members"];
    _headImgView.layer.cornerRadius = 0;
    _nameLabel.hidden = YES;
    _startImgView.hidden = YES;
}

- (void)reset
{
    _nameLabel.hidden = NO;
}

- (void)creatCellWithText:(NSString *)text
{
    self.nameLabel.text = text;
}

- (void)addGanapatiIcon
{
    self.startImgView.hidden = NO;
}

- (void)addManagerIcon
{
    self.startImgView.hidden = NO;
}

- (void)cellCreatWithModel:(HHContactsModel *)model indexPath:(NSIndexPath *)indexPath {
    
    self.longPressGestureRecognizer.view.tag = indexPath.row;

    if([model.headUrl hasPrefix:@"http"]){
        [self.headImgView sd_setImageWithURL:[NSURL URLWithString:model.headUrl] placeholderImage:nil options:SDWebImageRefreshCached];
    }else{
      self.headImgView.image = [UIImage imageNamed:model.headUrl];
    }

//    if ([[HMJFMDBContactsDataModel sharedInstance] isFriendWithContactId:model.contactId]) {
//        HMJContactsModel * friend = (HMJContactsModel *)[[HMJFMDBContactsDataModel sharedInstance] queryDataFromDataBaseNoforBlackWithFriendContactId:model.contactId].firstObject;
//
//        if (friend.markName.length > 0) {
//            self.nameLabel.text = friend.markName;
//        }else {
//            if (model.markName.length > 0) {
//                self.nameLabel.text = model.markName;
//            }else {
//                self.nameLabel.text = model.nickName;
//            }
//        }
//
//
//    } else {

        self.nameLabel.text = model.markName.length > 0 ? model.markName : model.nickName;
//    }

    if (model.role.intValue == 1) {
        self.startImgView.hidden = NO;
        self.startImgView.image = [UIImage imageNamed:@"GroupOwner_1"];
        
    }else if (model.role.intValue == 2) {
        self.startImgView.hidden = NO;
        self.startImgView.image = [UIImage imageNamed:@"GroupOwner_2"];
    } else {
        self.startImgView.hidden = YES;
    }
}


- (void)longPressGestureRecognizer:(UILongPressGestureRecognizer *)gestureRecognizer{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan){
        if ([self.delegate respondsToSelector:@selector(collectionViewCellLongPressGestureRecognizer:)]) {
            [self.delegate collectionViewCellLongPressGestureRecognizer:gestureRecognizer];
        }
    }else {
    }
}

-(void)prepareForReuse
{
    [super prepareForReuse];
}
@end

