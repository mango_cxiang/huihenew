//
//  HHGroupNoticePublisherView.h
//  HHNew
//
//  Created by Liubin on 2020/4/27.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HHGroupNoticePublisherView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (strong, nonatomic) NSDictionary *dataDict;

@end

NS_ASSUME_NONNULL_END
