//
//  HHGroupSearchMembersTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HHContactsModel;
NS_ASSUME_NONNULL_BEGIN
typedef void(^clickSearchMembersTableViewCell)(HHContactsModel *model);
@interface HHGroupSearchMembersTableView : UITableView
@property (nonatomic ,strong) NSMutableArray *dataArray;
@property (nonatomic ,copy) clickSearchMembersTableViewCell clickSearchMembersTableViewCell;
@property (nonatomic, copy) NSString * inputWords;

@end

NS_ASSUME_NONNULL_END
