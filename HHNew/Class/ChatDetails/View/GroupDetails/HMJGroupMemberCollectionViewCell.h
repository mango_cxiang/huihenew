//
//  HMJGroupMemberCollectionViewCell.h
//  huihe
//
//  Created by changle on 2016/12/21.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HHContactsModel.h"

@protocol HMJGroupMemberCollectionViewCellDelegate <NSObject>

- (void)collectionViewCellLongPressGestureRecognizer:(UILongPressGestureRecognizer *)gestureRecognizer;

@end

@interface HMJGroupMemberCollectionViewCell : UICollectionViewCell
@property (nonatomic,weak) id <HMJGroupMemberCollectionViewCellDelegate> delegate;

- (void)creatCellWithText:(NSString *)text;

- (void)addImg;
- (void)delImg;
- (void)reset;
- (void)addGanapatiIcon;
- (void)addManagerIcon;
- (void)cellCreatWithModel:(HHContactsModel *)model indexPath:(NSIndexPath *)indexPath;
@end
