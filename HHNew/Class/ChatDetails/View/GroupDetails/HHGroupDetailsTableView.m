//
//  HHGroupDetailsTableView.m
//  HHNew
//
//  Created by Liubin on 2020/4/26.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupDetailsTableView.h"
#import "HMJGroupTableViewCell.h"
#import "HMJChatFileTableViewCell.h"
#import "HMJAddPeopleTableViewCell.h"
#import "HHNoticeContentTableViewCell.h"
static NSString *Group = @"Group";
static NSString *ChatFile = @"ChatFile";
static NSString *AddPeople = @"AddPeople";
static NSString *NoticeCell = @"NoticeCell";

@interface HHGroupDetailsTableView ()<UITableViewDelegate,UITableViewDataSource,HMJChatFileTableViewCellDelegate>
@property (strong, nonatomic) NSMutableArray *dataArry;
//底部删除按钮View
@property (nonatomic ,strong)UIView *deleteBtnView;
@property (nonatomic  , strong) UIButton * delteBtn;

@end
@implementation HHGroupDetailsTableView


- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.delegate = self;
        self.dataSource = self;
        [self registerNib:[UINib nibWithNibName:@"HMJGroupTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:Group];
        [self registerNib:[UINib nibWithNibName:@"HMJChatFileTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:ChatFile];
        [self registerNib:[UINib nibWithNibName:@"HHNoticeContentTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:NoticeCell];
        [self creatFooterView];
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
       
    }
    return self;
}

- (void)setdataArry:(NSMutableArray *)dataArry{
    _dataArry = dataArry;
    [self reloadData];
}

#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 0) {
//        if([[HMJFMDBGroupDataModel sharedInstance] checkUserGroupMemberOwnerGroupId:[NSString stringWithFormat:@"%ld",(long)self.destId]  contactId:[NSString acquireUserId]] || [[HMJFMDBGroupDataModel sharedInstance] checkUserGroupMemberIdentityGroupId:[NSString stringWithFormat:@"%ld",(long)self.destId]  contactId:[NSString acquireUserId]]){
//            return 4;
//        }else{
//            return 3;
//        }
        return 4;
    } else if (section == 1) {
        return 2;
    } else if (section == 2) {
        return 3;
    } else if (section == 3) {
        return 4;
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HMJGroupTableViewCell *groupcell=[tableView dequeueReusableCellWithIdentifier:Group];
    groupcell = LoadCell(@"HMJGroupTableViewCell");
    UIView * bgView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];
    groupcell.selectedBackgroundView = bgView;
    groupcell.backgroundColor = ColorManage.darkBackgroundColor;
    
    HMJChatFileTableViewCell *chatcell=[tableView dequeueReusableCellWithIdentifier:ChatFile];
    chatcell=LoadCell(@"HMJChatFileTableViewCell");
    chatcell.isGroup = YES;
    chatcell.delegate= self;
    chatcell.backgroundColor = ColorManage.darkBackgroundColor;
//       chatcell.destId = [NSString stringWithFormat:@"%zi",self.destId];

    HHNoticeContentTableViewCell *noticeContentCell=[tableView dequeueReusableCellWithIdentifier:NoticeCell];
    noticeContentCell = LoadCell(@"HHNoticeContentTableViewCell");
    noticeContentCell.selectedBackgroundView = bgView;
    noticeContentCell.backgroundColor = ColorManage.darkBackgroundColor;

    if (indexPath.section==0){
              if (indexPath.row==0){
                  groupcell.nameLable.text = @"群聊名称";
                  groupcell.groupName.text = @"峡谷之巅";
//                  groupcell.groupName.text = self.groupModel.name;
//                  if ([[HMJFMDBGroupDataModel sharedInstance] checkUserGroupMemberOwnerGroupId:[NSString stringWithFormat:@"%ld",(long)self.destId]  contactId:[NSString acquireUserId]] || [[HMJFMDBGroupDataModel sharedInstance] checkUserGroupMemberIdentityGroupId:[NSString stringWithFormat:@"%ld",(long)self.destId]  contactId:[NSString acquireUserId]]) {
//                      groupcell.arrowImage.hidden = NO;
//                  }else {
//                      groupcell.arrowImage.hidden = YES;
//                  }
                  return groupcell;
              }
           if (indexPath.row==1){
               HMJAddPeopleTableViewCell *cell = [HMJAddPeopleTableViewCell cellWithTableView:tableView];
               cell.selectedBackgroundView = bgView;
               cell.backgroundColor = ColorManage.darkBackgroundColor;
               return cell;
           }
        if (indexPath.row == 2){
               if (self.noticeStr.length <= 0) {
                   groupcell.nameLable.text = @"群公告";
                   groupcell.groupName.text = @"未设置";
                   return groupcell;
               }
               else{
                   noticeContentCell.titleLabel.text = @"群公告";
                   //需要用Model缓存文本高度
                   noticeContentCell.contentLabel.text = self.noticeStr;
                   return noticeContentCell;
               }
//                   groupcell.arrowImage.hidden = YES;
//               }else {
//                   groupcell.groupName.hidden = YES;
//               }
           }
               if (indexPath.row == 3){
               
               groupcell.nameLable.text = @"群管理";
               groupcell.groupName.hidden = YES;
               return groupcell;
           }
       }
       
       else
        if (indexPath.section==1){
           if (indexPath.row==0){
               groupcell.nameLable.text = @"我在本群的昵称";
//               for (HMJContactsModel * contactModel in self.groupMemberArray) {
//                   if ([contactModel.contactId isEqualToString:[NSString acquireUserId]]) {
//                       groupcell.groupName.text = contactModel.markName.length > 0 ?contactModel.markName:contactModel.nickName;
//                       break;
//                   }
//               }
               chatcell.switchBtn.hidden=YES;
               return groupcell;
           }else{
               chatcell.leftLable.text= @"显示成员昵称";
               chatcell.departmentLable.hidden=YES;
//               chatcell.switchBtn.on = [self.groupModel.ext boolValue];
               return chatcell;
           }
           
       }else
        if (indexPath.section==2){
           if (indexPath.row==0){
               chatcell.leftLable.text= @"消息免打扰";
//               chatcell.switchBtn.on = [self.groupModel.receiveTip boolValue];
               chatcell.departmentLable.hidden=YES;
           }else if (indexPath.row == 1){
               chatcell.leftLable.text= @"置顶聊天";
//               chatcell.switchBtn.on = (self.topFlag.integerValue == 0 ? NO : YES);
               chatcell.departmentLable.hidden = YES;
           } else {
               chatcell.leftLable.text= @"保存到通讯录";
//               chatcell.switchBtn.on = [self.groupModel.saveLocal boolValue];
               chatcell.departmentLable.hidden = YES;
           }
           return chatcell;
           
       }else
        if(indexPath.section==3){
           if (indexPath.row == 0) {
               groupcell.nameLable.text= @"群合作信息";
               groupcell.groupName.hidden=YES;
           } else if (indexPath.row==1){
               groupcell.nameLable.text= @"查找聊天记录";
               groupcell.groupName.hidden=YES;
           }else if (indexPath.row == 2) {
               groupcell.nameLable.text= @"清空聊天记录";
               groupcell.groupName.hidden=YES;
           } else {
               groupcell.nameLable.text= @"设置当前聊天背景";
               groupcell.arrowImage.hidden=NO;
               groupcell.groupName.hidden=YES;
           }
           return groupcell;
       }else
        if(indexPath.section==4){
           if (indexPath.row==0){
               groupcell.nameLable.text = @"投诉";
               groupcell.arrowImage.hidden=YES;
               groupcell.groupName.hidden=YES;
           }
           return groupcell;
       }
       return nil;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.noticeStr.length >0 && indexPath.section == 0 && indexPath.row == 2) {
        float height = [[HHDefaultTools sharedInstance] heightForString:self.noticeStr andWidth:SCREEN_WIDTH-30];
        return height + 55;
    }
    return 55;
}



-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = ColorManage.grayBackgroundColor;
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = ColorManage.grayBackgroundColor;
    return view;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.clickGroupDetailsTableViewCell) {
        self.clickGroupDetailsTableViewCell(indexPath);
    }
////    if (self.groupModel == nil) {
////        [[HMJShowLoding sharedInstance] showText:@"群已解散"];
////        return;
////    }
//    //私聊群
//    if (indexPath.section==0){
//        if (indexPath.row==0){
//            if ([[HMJFMDBGroupDataModel sharedInstance] checkUserGroupMemberOwnerGroupId:[NSString stringWithFormat:@"%ld",(long)self.destId]  contactId:[NSString acquireUserId]] || [[HMJFMDBGroupDataModel sharedInstance] checkUserGroupMemberIdentityGroupId:[NSString stringWithFormat:@"%ld",(long)self.destId]  contactId:[NSString acquireUserId]]) {
//                HMJGroupNameViewController *groupNameController=[[HMJGroupNameViewController alloc] init];
//                groupNameController.groupName= self.groupModel.name;
//                groupNameController.groupId=self.destId;
//                groupNameController.groupNameBlock=^(NSString *newsGroupName){
//                    self.groupName = newsGroupName;
//                };
//
//                [self.navigationController pushViewController:groupNameController animated:YES];
//            }else {
//                [[HMJShowLoding sharedInstance] showText:@"只有群主或管理员才能修改群名称"];
//            }
//
//
//            return;
//        } else if (indexPath.row == 1){//群二维码
//            HMJGroupQRCodeViewController * qrCodeVc = [[HMJGroupQRCodeViewController alloc]init];
//            qrCodeVc.groupModel = self.groupModel;
//            qrCodeVc.groupModel.groupId = [NSString stringWithFormat:@"%ld",(long)self.destId];
//            [self.navigationController pushViewController:qrCodeVc animated:YES];
//            return;
//        } else if (indexPath.row == 2){//群公告
//            if (self.groupModel.notes.length == 0) {
//                if ([[HMJFMDBGroupDataModel sharedInstance] checkUserGroupMemberOwnerGroupId:[NSString stringWithFormat:@"%ld",(long)self.destId]  contactId:[NSString acquireUserId]] || [[HMJFMDBGroupDataModel sharedInstance] checkUserGroupMemberIdentityGroupId:self.groupModel.groupId contactId:[NSString acquireUserId]]) {
//                    HMJGroupAnnouncementEditViewController * groupAnnouncementEditViewController = [[HMJGroupAnnouncementEditViewController alloc] init];
//                    groupAnnouncementEditViewController.groupId=[self.groupModel.groupId integerValue];
//
//                    [self.navigationController pushViewController:groupAnnouncementEditViewController animated:YES];
//                }else {
//                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"只有群主和管理员才能修改群公告。" message:nil preferredStyle:UIAlertControllerStyleAlert];
//                    [alertController addAction:[UIAlertAction actionWithTitle:@"我知道了" style:UIAlertActionStyleDefault handler:nil]];
//                    [self presentViewController:alertController animated:YES completion:NULL];
//                }
//            }else {
//                HMJHMJNewGroupAnnouncementListViewController * vc = [[HMJHMJNewGroupAnnouncementListViewController alloc]init];
//                vc.groupModel = self.groupModel;
//                NSDictionary *dic = [JSON objectFromJSONString:self.groupModel.notes];
//                HMJGroupAnnouncement *model = [HMJGroupAnnouncement mj_objectWithKeyValues:dic];
//                vc.mentModel = model;
//                [self.navigationController pushViewController:vc animated:YES];
//            }
//
//            return;
//        }else if (indexPath.row == 3){//群管理
//            HMKGroupManagementController * vc = [[HMKGroupManagementController alloc]init];
//            vc.groupId = self.destId;
//            vc.groupModel = self.groupModel;
//            vc.groupMemberArray = [NSArray arrayWithArray:self.groupMemberArray];
//            [self.navigationController pushViewController:vc animated:YES];
//            return;
//        }
//    } else if (indexPath.section == 1){
//        if (indexPath.row == 0) {
//            HMJAloneLineTextFieldViewController *aloneLineTextFieldViewController = [[HMJAloneLineTextFieldViewController alloc]init];
//            aloneLineTextFieldViewController.title = Localized(@"我在本群的昵称");
//            aloneLineTextFieldViewController.groupId = [NSString stringWithFormat:@"%ld",(long)self.destId];
//            aloneLineTextFieldViewController.returnAloneLineTextFieldViewControllerBlock = ^(NSString *markName) {
//                NSMutableArray *arr = [NSMutableArray arrayWithArray:self.groupMemberArray];
//                for (int i = 0 ; i < self.groupMemberArray.count ; i ++) {
//                    HMJContactsModel * contactModel = self.groupMemberArray[i];
//                    if ([contactModel.contactId isEqualToString:[NSString acquireUserId]]) {
//                        HMJContactsModel *model = [[HMJContactsModel alloc]init];
//                        model = contactModel;
//                        model.markName = markName;
//                        [arr replaceObjectAtIndex:i withObject:model];
//                        break;
//                    }
//                }
//                [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
//            };
//            [self.navigationController pushViewController:aloneLineTextFieldViewController animated:YES];
//        }
//
//        return;
//    } else  if (indexPath.section == 3) {
//        if (indexPath.row == 0) {
//            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//            [dict setObject:@"h5_url" forKey:@"key"];
//            [dict setObject:@"groupCooperate" forKey:@"code"];
//            [dict setObject:[NSString acquireUserId] forKey:@"userId"];
//            [HMJBusinessManager GET:@"/config/get" Parameters:dict success:^(NSDictionary *dic, NSInteger code) {
//                NSInteger codeStr = [dic[@"code"] integerValue];
//                if(codeStr==1){
//                    NSString *urlStr = [NSString stringWithFormat:@"%@?userid=%@&nickname=%@&headpic=%@&timestamp=%@&groupId=%@",dic[@"data"][@"detailName"],[NSString acquireUserId],[HMJAccountModel myself].nickName,[HMJAccountModel myself].headUrl,[NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]],self.groupModel.groupId];
//                    urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//                    YXZIKBaseWKWebViewController *resultVC = [[YXZIKBaseWKWebViewController alloc] init];
//                    [resultVC loadWebURLSring:urlStr];
//                    [self.navigationController pushViewController:resultVC animated:YES];
//
//                }
//
//            } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//            }];
//        } else if (indexPath.row==1) {
//            SearchDetailViewController *searchViewController = [[SearchDetailViewController alloc] init];
//            searchViewController.placeHolderText = @"搜索";
//            searchViewController.destId = self.groupModel.groupId;
//            searchViewController.createrId = self.groupModel.createrId;
//            searchViewController.groupModel = self.groupModel;
//            UINavigationController *navigationController =
//            [[UINavigationController alloc] initWithRootViewController:searchViewController];
//            navigationController.modalPresentationStyle = UIModalPresentationFullScreen;
//            [self presentViewController:navigationController
//                               animated:NO
//                             completion:nil];
//            return;
//        } else if (indexPath.row == 2) {
//            UIAlertController *alertSheet = [UIAlertController alertControllerWithTitle:@"确定清空所有聊天记录？" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//            UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                [self deleteMessage];
//            }];
//            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//                NSLog(@"点击了取消");
//            }];
//            [alertSheet addAction:action1];
//            [alertSheet addAction:cancel];
//            [self presentViewController:alertSheet animated:YES completion:nil];
//        } else {
//            HMKChatBackGroundViewController *vc = [[HMKChatBackGroundViewController alloc] init];
//            vc.isFromCurrentSet = YES;
//            vc.ssionId = [NSString stringWithFormat:@"2%@%zi",SESSIONID_MIDDLE,self.destId];
//            [self.navigationController pushViewController:vc animated:YES];
//        }
//    } else  if (indexPath.section == 4) {
//        if (indexPath.row==0){
//            YXZIKBaseWKWebViewController *resultVC = [[YXZIKBaseWKWebViewController alloc] init];
//            resultVC.hidesBottomBarWhenPushed = YES;
//            NSString *urlStr = [NSString stringWithFormat:@"%@?fromid=%@&destid=%@&_t=%ld&type=2",[[AppSetting sharedAppSetting] getComplaint],[NSString acquireUserId],[NSString stringWithFormat:@"%zi",self.destId],(long)[[NSDate date] timeIntervalSince1970]];
//            urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//            [resultVC loadWebURLSring:urlStr];
//            [self.navigationController pushViewController:resultVC animated:YES];
//        }
//    }
}

#pragma mark - 协议 HMJChatFileTableViewCellDelegate
- (void)chatFileTableViewCell:(HMJChatFileTableViewCell *)chatFileTableViewCell didClickOnSwitch:(UISwitch *)sw{
    NSIndexPath * indexPath = [self indexPathForCell:chatFileTableViewCell];
    if (indexPath.section == 1 && indexPath.row == 0){
        //消息免打扰
    }else if (indexPath.section == 1 && indexPath.row == 1){
        //置顶聊天
    }
}

//退出群聊
-(void)clickToDeleteBtn{
    if (self.deleteAndexitGroupChat) {
        self.deleteAndexitGroupChat();
    }
}

#pragma mark -加载视图
//区脚
-(void)creatFooterView{
    [self.deleteBtnView addSubview:self.delteBtn];
    [self.delteBtn addTarget:self action:@selector(clickToDeleteBtn) forControlEvents:UIControlEventTouchUpInside];
    self.tableFooterView = self.deleteBtnView;
    [self.delteBtn setTitle:@"删除并退出群聊" forState:UIControlStateNormal];
}


- (UIButton *)delteBtn{
    if (!_delteBtn) {
        _delteBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, 15, SCREEN_WIDTH-20, 50)];
        _delteBtn.backgroundColor = RGB_RedColor;
        _delteBtn.layer.cornerRadius = 5;
        _delteBtn.layer.masksToBounds = YES;
        [_delteBtn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        _delteBtn.titleLabel.font= kRegularFont(16);
    }
    return _delteBtn;
}


- (UIView *)deleteBtnView{
    if(!_deleteBtnView){
        _deleteBtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 80+SafeAreaBottomHeight)];
    }
    return _deleteBtnView;
}
@end
