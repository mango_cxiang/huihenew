//
//  HMJAddPeopleTableViewCell.m
//  huihe
//
//  Created by mysun on 16/3/3.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJAddPeopleTableViewCell.h"

@interface HMJAddPeopleTableViewCell()


@end

@implementation HMJAddPeopleTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellId = @"HMJAddPeopleTableViewCell";
    HMJAddPeopleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[HMJAddPeopleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    return cell;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}
- (void)createView {
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.font=SystemFont(16);
    nameLabel.text = @"群二维码";
    [self.contentView addSubview:nameLabel];
    
    UIImageView *codeImage = [[UIImageView alloc] init];
    codeImage.image = [UIImage imageNamed:@"qrcode_gray"];
    [self.contentView addSubview:codeImage];
    
    UIImageView *arrow = [[UIImageView alloc] init];
    arrow.image = [UIImage imageNamed:@"next"];
    [self.contentView addSubview:arrow];
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = ColorManage.lineViewColor;
    [self.contentView addSubview:lineView];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.offset(15);
    }];
    
    [arrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.offset(-15);
    }];
    
    [codeImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(arrow.mas_left).offset(-10);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(10);
        make.right.equalTo(self.contentView).offset(0);
        make.height.equalTo(@.5);
    }];
    
}

@end
