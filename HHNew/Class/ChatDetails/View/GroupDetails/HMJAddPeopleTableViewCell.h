//
//  HMJAddPeopleTableViewCell.h
//  huihe
//
//  Created by mysun on 16/3/3.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>
/*
 添加联系人
*/
@interface HMJAddPeopleTableViewCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@property (nonatomic, weak) UIButton *codeBtn;

@end
