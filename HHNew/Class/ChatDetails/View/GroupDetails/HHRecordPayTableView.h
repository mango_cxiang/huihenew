//
//  HHRecordPayTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/30.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^clickRecordPayTableViewCell)(void);

@interface HHRecordPayTableView : UITableView

@property (strong, nonatomic) NSMutableArray *dataArray;
@property (copy, nonatomic) clickRecordPayTableViewCell clickRecordPayTableViewCell;

@end

NS_ASSUME_NONNULL_END
