//
//  HHRecordSearchResultTableViewCell.m
//  HHNew
//
//  Created by Liubin on 2020/5/6.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHRecordSearchResultTableViewCell.h"

@implementation HHRecordSearchResultTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [_iconImgView viewWithRadis:5];
    self.lineView.backgroundColor = ColorManage.lineViewColor;
    self.titleLabel.textColor = ColorManage.text_color;
}

- (void)setSearchRecordModel:(HHContactsModel *)searchRecordModel{
    _searchRecordModel = searchRecordModel;
    self.titleLabel.text = searchRecordModel.nickName;
    self.descLabel.text = searchRecordModel.nickName;
    self.timeLabel.text = searchRecordModel.updatetime;
    [self.iconImgView xxd_setHeadImageView:self.iconImgView imgUrl:searchRecordModel.headUrl];
}

- (void)setInputWords:(NSString *)inputWords{
    NSString * str = _searchRecordModel.nickName;
    self.descLabel.attributedText = [[HHDefaultTools sharedInstance] updateWithKeywordMode:str textColor:ColorManage.grayTextColor_Deault tintStr:inputWords tintColor:RGB_GreenColor];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
