//
//  HMJChatFileTableViewCell.m
//  huihe
//
//  Created by mysun on 16/3/7.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJChatFileTableViewCell.h"
//#import "HMJMessageManager.h"
//#import "HMJContactsManager.h"
//#import "HMJFMDBGroupDataModel.h"
#import "HHContactsModel.h"
@implementation HMJChatFileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = ColorManage.darkBackgroundColor;
    self.leftLable.font = kRegularFont(16);
    self.leftLable.textColor = ColorManage.text_color;
    self.departmentLable.font = kRegularFont(14);
    self.departmentLable.textColor = ColorManage.text_color;;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.lineLabel.backgroundColor = ColorManage.lineViewColor;
    
    [self.switchBtn addTarget:self action:@selector(userSetFriendPrivate) forControlEvents:UIControlEventValueChanged];
}
- (void)userSetFriendPrivate{
    NSString * destType = self.isGroup?@"2":@"1";
    if([self.leftLable.text isEqualToString:@"消息免打扰"]){
//        NSString * receiveTip = _switchBtn.isOn ? @"1" : @"0";
//        [HMJMessageManager userGroupSetReciveTip:receiveTip saveLocal:nil ext:nil groupeId:self.destId destType:destType completed:^(int code, NSDictionary *objc) {
//            if(code == 1){
//                if (destType.integerValue == 1) {
//                    [[HMJFMDBContactsDataModel sharedInstance]updateIgonreWithUid:self.destId receiveTip:receiveTip];
//                }else{
//                    [[HMJFMDBGroupDataModel sharedInstance]updateIgonreWithGroupId:self.destId receiveTip:receiveTip];
//                }
//                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_SESSIONLIST object:nil];
//                //               [[HMJShowLoding sharedInstance] showText:Localized(@"设置成功")];
//            }else{
//                //                [[HMJShowLoding sharedInstance] showText:Localized(@"设置失败")];
//            }
//        }];

    }else if([self.leftLable.text isEqualToString:@"置顶聊天"]){
        if (_switchBtn.isOn) {
//            [HMJMessageManager sessionSetTopWithdestId:self.destId destType:destType completed:^(int code, NSDictionary *objc) {
//                if (code == 1) {
//                    UInt64 recordTime = [[NSDate date] timeIntervalSince1970]*1000;
//                    NSString *timeStamp = [NSString stringWithFormat:@"%llu", recordTime];
//                    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//                    [dic setObject:self.destId forKey:@"destId"];
//                    [dic setObject:destType forKey:@"destType"];
//                    [dic setObject:[NSString acquireUserId] forKey:@"userId"];
//                    [[HMJFMDBTopListDataModel sharedInstance] insertDataToTopListDataBaseWithDic:dic];
//                    [[HMJFMDBMessageDataModel sharedInstance] updataSessionListTopFlagWithSessionId:[NSString stringWithFormat:@"%@%@%@",destType,SESSIONID_MIDDLE,self.destId] lasttime:timeStamp topFlag:@"1"];
//                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_SESSIONLIST object:nil];
//                }
//            }];

        }else{

//            [HMJMessageManager sessionCancleTopWithdestId:self.destId destType:destType completed:^(int code, NSDictionary *objc) {
//                if (code == 1) {
//                    if ([[HMJFMDBTopListDataModel sharedInstance] deleteDataFromTopListDataBaseWithDestId:self.destId destType:destType]) {
//                        UInt64 recordTime = [[NSDate date] timeIntervalSince1970]*1000;
//                        NSString *timeStamp = [NSString stringWithFormat:@"%llu", recordTime];
//                        [[HMJFMDBMessageDataModel sharedInstance] updataSessionListTopFlagWithSessionId:[NSString stringWithFormat:@"%@%@%@",destType,SESSIONID_MIDDLE,self.destId] lasttime:timeStamp topFlag:@"0"];
//                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_SESSIONLIST object:nil];
//                    }
//
//
//                }
//
//            }];

        }

    }  else if([self.leftLable.text isEqualToString:@"显示群成员昵称"]){

//        NSString *showNick = _switchBtn.isOn ? @"1" : @"0";
//        [HMJMessageManager userGroupSetReciveTip:nil saveLocal:nil ext:showNick groupeId:self.destId destType:destType completed:^(int code, NSDictionary *objc) {
//            if(code == 1){
//                [[HMJFMDBGroupDataModel sharedInstance] updateIsShowNickWithGroupId:self.destId showNick:showNick];
//            }
//        }];
//
//        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NOTIFICATION_GroupMemberRoleChanged" object:self];

    }else if([self.leftLable.text isEqualToString:@"保存到通讯录"]){
//        NSString *save = _switchBtn.isOn ? @"1" : @"0";
//        [HMJMessageManager userGroupSetReciveTip:nil saveLocal:save ext:nil groupeId:self.destId destType:destType completed:^(int code, NSDictionary *objc) {
//            if(code == 1){
//                [[HMJFMDBGroupDataModel sharedInstance] updateSaveLocalWithGroupId:self.destId saveLocal:save];
//
//            }
//        }];

    }
}

- (IBAction)clickToSwitch:(UISwitch *)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatFileTableViewCell:didClickOnSwitch:)]){
        [self.delegate chatFileTableViewCell:self didClickOnSwitch:sender];
    }
}

@end
