//
//  HHGroupRecordTypeView.m
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupRecordTypeView.h"

@implementation HHGroupRecordTypeView
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        //        [self loadView];
        self =  [[[NSBundle mainBundle]loadNibNamed:@"HHGroupRecordTypeView" owner:self options:nil] lastObject];
        self.frame = frame;
        self.typeBgView.backgroundColor = ColorManage.darkBackgroundColor;
        self.backgroundColor = ColorManage.darkBackgroundColor;

    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
}

- (IBAction)clickGroupMembersRecordBtn:(id)sender {
    if(self.selectGroupRecordType){
        self.selectGroupRecordType(SearchGroupRecordMembers);
    }
}

- (IBAction)selectDateType:(id)sender {
    if(self.selectGroupRecordType){
           self.selectGroupRecordType(SearchGroupRecordDate);
       }
}

- (IBAction)selectFileType:(id)sender {
    if(self.selectGroupRecordType){
           self.selectGroupRecordType(SearchGroupRecordFile);
       }
}

- (IBAction)selectUrlType:(id)sender {
    if(self.selectGroupRecordType){
           self.selectGroupRecordType(SearchGroupRecordUrl);
       }
}
- (IBAction)selectImageAndVideoType:(id)sender {
    if(self.selectGroupRecordType){
           self.selectGroupRecordType(SearchGroupRecordImageAndVideo);
       }
}

- (IBAction)selectPayType:(id)sender {
    if(self.selectGroupRecordType){
           self.selectGroupRecordType(SearchGroupRecordPay);
       }
}
@end
