//
//  HHGroupAllMembersCollectionView.m
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupAllMembersCollectionView.h"
#import "HMJGroupMemberCollectionViewCell.h"
#import "HHContactsModel.h"

@interface HHGroupAllMembersCollectionView ()<UICollectionViewDelegate,UICollectionViewDataSource>

@end

@implementation HHGroupAllMembersCollectionView

static NSString * const reuseIdentifier = @"Cell";

- (instancetype)initWithFrame:(CGRect)frame collectionViewLayout:(nonnull UICollectionViewLayout *)layout{
    if (self == [super initWithFrame:frame collectionViewLayout:[self createCollectionViewLayout]]) {
//        self.collectionViewLayout = [self createCollectionViewLayout];
       [self registerNib:[UINib nibWithNibName:@"HMJGroupMemberCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:reuseIdentifier];
       [self setBackgroundColor:ColorManage.darkBackgroundColor];
       self.delegate = self;
       self.dataSource = self;
    }
    return self;
}

- (void)setContentArray:(NSMutableArray *)contentArray{
    _contentArray = contentArray;
    [self reloadData];
}

- (UICollectionViewFlowLayout *)createCollectionViewLayout{
    //创建一个流式布局对象
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    
    //设置每个cell的大小
    CGFloat padding = 5;
    layout.itemSize = CGSizeMake((SCREEN_WIDTH - padding * 6)/5.0, (SCREEN_WIDTH - padding * 6)/5.0 + 20);
    
    //设置每个cell间的最小水平间距
    layout.minimumInteritemSpacing = 0;
    
    //设置每个cell间的行间距
    layout.minimumLineSpacing = padding;
    
    //设置每一组距离四周的内边距
    layout.sectionInset = UIEdgeInsetsMake(padding, padding, padding, padding);
    
    return layout;
}

#pragma mark <UICollectionViewDataSource>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    //如果自己是群主就+2，不是就+1
    return self.contentArray.count+2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HMJGroupMemberCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    [cell reset];
    if (indexPath.row == self.contentArray.count+0) {
        [cell addImg];
    } else if (indexPath.row == self.contentArray.count + 1) {
        [cell delImg];
    }else {
        HHContactsModel * model = self.contentArray[indexPath.row];
        [cell cellCreatWithModel:model indexPath:indexPath];
    }
    return cell;
}

#pragma mark <UICollectionViewDelegate>
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    if(indexPath.row == self.contentArray.count){
        if (self.clickAddMembers) {
            self.clickAddMembers();
        }
    }
    if(indexPath.row == self.contentArray.count+1){
        if (self.clickDeleteMembers) {
            self.clickDeleteMembers();
        }
    }
    if(indexPath.row < self.contentArray.count){
        HHContactsModel * model = self.contentArray[indexPath.row];
           if (self.clickAllMembersCollectionViewCell) {
               self.clickAllMembersCollectionViewCell(model);
           }
    }
   
}



@end
