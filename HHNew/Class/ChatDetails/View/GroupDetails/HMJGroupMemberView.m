//
//  HMJGroupMemberView.m
//  huihe
//
//  Created by changle on 2016/12/21.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJGroupMemberView.h"
#import "HMJGroupMemberCollectionViewCell.h"
#import "HHContactsModel.h"
#import "UUButton.h"

@interface HMJGroupMemberView ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic,strong) UICollectionView * groupMemberCollectionView;
@property (nonatomic,strong) UUButton * seeAllMemberBtn;
@property (nonatomic,strong) UIImageView * nextImgView;

@end


@implementation HMJGroupMemberView
static NSString * const reuseIdentifier = @"Cell";

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.groupMemberCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, self.width, self.height - 30) collectionViewLayout:[self collectionViewLayout]];
        [self.groupMemberCollectionView registerNib:[UINib nibWithNibName:@"HMJGroupMemberCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:reuseIdentifier];
//        [self.groupMemberCollectionView setBackgroundColor:ColorManage.blackBackgroundColor];
        self.groupMemberCollectionView.delegate = self;
        self.groupMemberCollectionView.dataSource = self;
        self.groupMemberCollectionView.backgroundColor =  ColorManage.darkBackgroundColor;
        [self addSubview:_groupMemberCollectionView];
        
        self.seeAllMemberBtn = [[UUButton alloc]initWithFrame:CGRectMake(0, self.height - 45, 100, 30)];
        self.seeAllMemberBtn.contentAlignment = UUContentAlignmentCenterImageRight;
        self.seeAllMemberBtn.spacing = 5;
        self.seeAllMemberBtn.centerX = self.centerX;
        [self.seeAllMemberBtn setTitle:@"查看全部成员" forState:UIControlStateNormal];
        [self.seeAllMemberBtn addTarget:self action:@selector(tapSellAllMemberBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.seeAllMemberBtn setTitleColor:ColorManage.grayTextColor_Deault forState:UIControlStateNormal];
        [self.seeAllMemberBtn setImage:[UIImage imageNamed:@"next"] forState:UIControlStateNormal];
        self.seeAllMemberBtn.titleLabel.font = kRegularFont(15);
        [self addSubview:self.seeAllMemberBtn];
        
        self.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return self;
}


- (UICollectionViewFlowLayout *)collectionViewLayout{
    //创建一个流式布局对象
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    
    //设置每个cell的大小
    CGFloat padding = 5;
    layout.itemSize = CGSizeMake((SCREEN_WIDTH - padding * 6)/5.0, (SCREEN_WIDTH - padding * 6)/5.0 + 20);
    
    //设置每个cell间的最小水平间距
    layout.minimumInteritemSpacing = 0;
    
    //设置每个cell间的行间距
    layout.minimumLineSpacing = padding;
    
    //设置每一组距离四周的内边距
    layout.sectionInset = UIEdgeInsetsMake(padding, padding, padding, padding);
    
    return layout;
}

#pragma mark <UICollectionViewDataSource>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSInteger num = 0;
    if (self.contentArray.count > 18) {
        num = 20;
        self.seeAllMemberBtn.hidden = NO;
    }else {
        num = (self.contentArray.count + 2);
        self.seeAllMemberBtn.hidden = YES;
    }
    return num;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HMJGroupMemberCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    [cell reset];
    if (self.contentArray.count > 18) {
        if (indexPath.row == 18) {
            [cell addImg];
        }else if (indexPath.row == 19) {
            [cell delImg];
        }else {
            HHContactsModel * model = self.contentArray[indexPath.row];
            [cell cellCreatWithModel:model indexPath:indexPath];
        }
    }else {
        if (indexPath.row == self.contentArray.count) {
            [cell addImg];
        } else if (indexPath.row == self.contentArray.count + 1) {
            [cell delImg];
        }else {
            HHContactsModel * model = self.contentArray[indexPath.row];
            [cell cellCreatWithModel:model indexPath:indexPath];
        }
    }
    return cell;
}

#pragma mark <UICollectionViewDelegate>
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    if ([self.delegate respondsToSelector:@selector(groupMemberViewBeCollection:)]) {
//        [self.delegate groupMemberViewBeCollection:indexPath];
//    }
    if (self.clickGroupMemberCollectionViewCell) {
        self.clickGroupMemberCollectionViewCell(indexPath);
    }
}

- (void)tapSellAllMemberBtn:(UIButton *)sender{
//    if ([self.delegate respondsToSelector:@selector(tapSellAllMemberBtn)]) {
//        [self.delegate tapSellAllMemberBtn];
//    }
    if (self.clickLookAllMembersBtn) {
        self.clickLookAllMembersBtn();
    }
}

- (void)setContentArray:(NSArray *)contentArray {
    _contentArray = contentArray;
    [self reloadData];
}
- (void)reloadData
{
    self.groupMemberCollectionView.frame = CGRectMake(0, 0, self.width, self.height);
    self.seeAllMemberBtn.frame = CGRectMake(0, self.height-45, 100, 30);
    self.seeAllMemberBtn.centerX = self.centerX;
    [self.groupMemberCollectionView reloadData];
}
- (void)hiddenSeeAllMemberBtn
{
    _seeAllMemberBtn.hidden = YES;
}



@end
