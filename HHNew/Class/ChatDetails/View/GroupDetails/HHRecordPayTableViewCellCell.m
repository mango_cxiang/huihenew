//
//  HHRecordPayTableViewCellCell.m
//  HHNew
//
//  Created by Liubin on 2020/4/30.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHRecordPayTableViewCellCell.h"

@implementation HHRecordPayTableViewCellCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = ColorManage.darkBackgroundColor;
    self.userHeadImgView.layer.cornerRadius = 5;
    self.userHeadImgView.layer.masksToBounds = YES;
    
    self.userTitleLabel.textColor = ColorManage.text_color;
    self.hbBgView.backgroundColor = ColorManage.grayBgColor_SecondLevel;
    self.lineView.backgroundColor = ColorManage.lineViewColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
