//
//  HHGroupNoticePublisherView.m
//  HHNew
//
//  Created by Liubin on 2020/4/27.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupNoticePublisherView.h"

@implementation HHGroupNoticePublisherView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        //        [self loadView];
        self =  [[[NSBundle mainBundle]loadNibNamed:@"HHGroupNoticePublisherView" owner:self options:nil] lastObject];
        self.frame = frame;
    }
    return self;
}

-(void)setDataDict:(NSDictionary *)dataDict{
    _dataDict = dataDict;
    NSString *headUrl = _dataDict[@"icon"];
    if([headUrl hasPrefix:@"http"]){
    [self.headImgView sd_setImageWithURL:[NSURL URLWithString:headUrl] placeholderImage:nil options:SDWebImageRefreshCached];
    }else{
      self.headImgView.image = [UIImage imageNamed:headUrl];
    }
    self.nameLabel.text = [NSString stringWithFormat:@"%@", _dataDict[@"name"]];
    self.timeLabel.text = [NSString stringWithFormat:@"%@", _dataDict[@"time"]];
}

- (void)awakeFromNib{
    [super awakeFromNib];
    self.headImgView.layer.masksToBounds = YES;
    self.headImgView.layer.cornerRadius = 5;
    
    self.nameLabel.font = kRegularFont(17);
    self.nameLabel.textColor = ColorManage.text_color;

    self.timeLabel.font = kRegularFont(14);
    self.timeLabel.textColor = ColorManage.grayTextColor_Deault;
    
    self.lineView.backgroundColor = ColorManage.lineViewColor;
}


@end
