//
//  HMJChatFileTableViewCell.h
//  huihe
//
//  Created by mysun on 16/3/7.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HMJChatFileTableViewCell;
@protocol HMJChatFileTableViewCellDelegate <NSObject>

@optional
//代理方法
- (void)chatFileTableViewCell:(HMJChatFileTableViewCell *)chatFileTableViewCell didClickOnSwitch:(UISwitch *)sw;

@end


//群概况－－显示群成员名称
@interface HMJChatFileTableViewCell : UITableViewCell

@property (nonatomic,weak) id<HMJChatFileTableViewCellDelegate>delegate;
//左侧的lable
@property (weak, nonatomic) IBOutlet UILabel *leftLable;
//群聊名字
@property (weak, nonatomic) IBOutlet UILabel *departmentLable;

@property (weak, nonatomic) IBOutlet UISwitch *switchBtn;

@property (nonatomic, assign) BOOL isGroup;

@property (nonatomic, strong) NSString * destId;
@property (weak, nonatomic) IBOutlet UIView *lineLabel;

@end
