//
//  HMJGroupMemberView.h
//  huihe
//
//  Created by changle on 2016/12/21.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol HMJGroupMemberViewDelegate <NSObject>
//
//- (void)groupMemberViewBeCollection:(NSIndexPath*)indexPath;
//- (void)tapSellAllMemberBtn;
//
//@end

typedef void(^clickGroupMemberCollectionViewCell)(NSIndexPath *indexPath);
typedef void(^clickLookAllMembersBtn)(void);

@interface HMJGroupMemberView : UIView
//@property (nonatomic,weak) id <HMJGroupMemberViewDelegate> delegate;
@property (nonatomic,strong) NSArray * contentArray;
@property (nonatomic,assign) BOOL needAddBtn;
@property (nonatomic, assign) BOOL isOwer;
@property (nonatomic,copy) clickLookAllMembersBtn clickLookAllMembersBtn;
@property (nonatomic,copy) clickGroupMemberCollectionViewCell clickGroupMemberCollectionViewCell;


- (void)reloadData;
- (void)hiddenSeeAllMemberBtn;
@end
