//
//  HMJGroupTableViewCell.m
//  huihe
//
//  Created by mysun on 16/3/7.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJGroupTableViewCell.h"

@implementation HMJGroupTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = ColorManage.darkBackgroundColor;
    self.nameLable.font = SystemFont(16);
    self.nameLable.textColor = ColorManage.text_color;
    self.groupName.textColor= ColorManage.grayTextColor_Deault;
    self.groupName.font=SystemFont(14);
    self.selectionStyle=UITableViewCellSelectionStyleDefault;
    self.lineView.backgroundColor = ColorManage.lineViewColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
