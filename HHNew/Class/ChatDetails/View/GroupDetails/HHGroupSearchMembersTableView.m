//
//  HHGroupSearchMembersTableView.m
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupSearchMembersTableView.h"
#import "HHAddFriendsSearchResultTableViewCell.h"
@interface HHGroupSearchMembersTableView ()<UITableViewDelegate,UITableViewDataSource>

@end
@implementation HHGroupSearchMembersTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.delegate = self;
        self.dataSource = self;
        self.backgroundColor = ColorManage.darkBackgroundColor;
        UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH,SafeAreaBottomHeight+15)];
        self.tableFooterView = view;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
    }
    return self;
}

- (void)setDataArray:(NSMutableArray *)dataArray{
    _dataArray = dataArray;
    [self reloadData];
}

#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellName = @"HHAddFriendsSearchResultTableViewCell";
    HHAddFriendsSearchResultTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if(!cell) {
        cell = LoadCell(@"HHAddFriendsSearchResultTableViewCell");
    }
    cell.contactsModel = _dataArray[indexPath.row];
    NSString *str = cell.contactsModel.markName.length>0?cell.contactsModel.markName:cell.contactsModel.nickName;
    [cell updateTitleLabelWithKeywordMode:str hintStr:self.inputWords];
    cell.backgroundColor = ColorManage.darkBackgroundColor;
    cell.selectedBackgroundView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    HHContactsModel *model = self.dataArray[indexPath.row];
    if (self.clickSearchMembersTableViewCell) {
        self.clickSearchMembersTableViewCell(model);
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
@end
