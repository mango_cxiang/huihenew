//
//  HHRecordPayTableView.m
//  HHNew
//
//  Created by Liubin on 2020/4/30.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHRecordPayTableView.h"
#import "HHRecordPayTableViewCellCell.h"
@interface HHRecordPayTableView ()<UITableViewDelegate,UITableViewDataSource>

@end
@implementation HHRecordPayTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.delegate = self;
        self.dataSource = self;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
    }
    return self;
}

- (void)setDataArray:(NSMutableArray *)dataArray{
    _dataArray = dataArray;
    [self reloadData];
}


#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellName = @"HHRecordPayTableViewCellCell";
    HHRecordPayTableViewCellCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if(!cell) {
        cell = LoadCell(@"HHRecordPayTableViewCellCell");
    }
    cell.backgroundColor = ColorManage.darkBackgroundColor;
    cell.userHeadImgView.image = [UIImage imageNamed:@"group"];
    cell.userTitleLabel.text = @"A张婷宣发了一个红包";
    cell.hbTitleLabel.text = @"恭喜发财，大吉大利";
    cell.hbStatusLabel.text = @"已领取";
    cell.selectedBackgroundView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];
    cell.timeLabel.text = @"2019-04-23";
    cell.backgroundColor = ColorManage.darkBackgroundColor;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = ColorManage.grayBackgroundColor;
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
