//
//  HHGroupAllMembersCollectionView.h
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HHContactsModel;
NS_ASSUME_NONNULL_BEGIN
typedef void(^clickAllMembersCollectionViewCell)(HHContactsModel *model);
typedef void(^clickAddMembers)(void);
typedef void(^clickDeleteMembers)(void);

@interface HHGroupAllMembersCollectionView : UICollectionView
@property (nonatomic,strong) NSMutableArray * contentArray;
@property (nonatomic,copy) clickAllMembersCollectionViewCell clickAllMembersCollectionViewCell;
@property (nonatomic,copy) clickAddMembers clickAddMembers;
@property (nonatomic,copy) clickDeleteMembers clickDeleteMembers;

@end

NS_ASSUME_NONNULL_END
