//
//  HHGroupRecordTypeView.h
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,SearchGroupRecordType){
    SearchGroupRecordMembers,  //群成员
    SearchGroupRecordDate,      //日期
    SearchGroupRecordFile,      //文件
    SearchGroupRecordUrl,       //链接
    SearchGroupRecordImageAndVideo,     //图片和视频
    SearchGroupRecordPay,       //交易
};

NS_ASSUME_NONNULL_BEGIN
typedef void(^selectGroupRecordType)(SearchGroupRecordType type);
@interface HHGroupRecordTypeView : UIView
- (IBAction)clickGroupMembersRecordBtn:(id)sender;
- (IBAction)selectDateType:(id)sender;
- (IBAction)selectFileType:(id)sender;
- (IBAction)selectUrlType:(id)sender;
- (IBAction)selectImageAndVideoType:(id)sender;
- (IBAction)selectPayType:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *typeBgView;

@property (copy, nonatomic) selectGroupRecordType selectGroupRecordType;

@end

NS_ASSUME_NONNULL_END
