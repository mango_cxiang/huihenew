//
//  HHSearchResultTableView.h
//  HHNew
//
//  Created by Liubin on 2020/5/6.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HHContactsModel;

NS_ASSUME_NONNULL_BEGIN
typedef void(^clickRecordSearchResultTableViewCell)(HHContactsModel *model);

@interface HHRecordSearchResultTableView : UITableView
@property (nonatomic ,strong) NSMutableArray *dataArray;
@property (nonatomic ,copy) clickRecordSearchResultTableViewCell clickRecordSearchResultTableViewCell;
@property (nonatomic, copy) NSString * inputWords;

@end

NS_ASSUME_NONNULL_END
