//
//  HHNoticeContentTableViewCell.m
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHNoticeContentTableViewCell.h"
#import "UILabel+HHTextSpace.h"
@implementation HHNoticeContentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.titleLabel.font = kRegularFont(16);
    self.titleLabel.textColor = ColorManage.text_color;
    self.contentLabel.font = kRegularFont(15);
    self.contentLabel.textColor = RGB_GrayColor;
    self.lineView.backgroundColor = ColorManage.lineViewColor;

}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self.contentLabel changeSpaceWithLineSpace:2 WordSpace:0.7];
    self.contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
