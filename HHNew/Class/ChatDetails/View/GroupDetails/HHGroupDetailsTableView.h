//
//  HHGroupDetailsTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/26.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^clickGroupDetailsTableViewCell)(NSIndexPath *indexPath);
typedef void(^deleteAndexitGroupChat)(void);


@interface HHGroupDetailsTableView : UITableView
@property (nonatomic, copy) clickGroupDetailsTableViewCell clickGroupDetailsTableViewCell;
@property (nonatomic, copy) deleteAndexitGroupChat deleteAndexitGroupChat;
@property (nonatomic,copy) NSString * noticeStr;

@end

NS_ASSUME_NONNULL_END
