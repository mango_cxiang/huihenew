//
//  HHSingleChatDetailsViewController.m
//  HHNew
//
//  Created by Liubin on 2020/5/6.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHSingleChatDetailsViewController.h"
#import "HHSingleChatDetailsTableView.h"
#import "HMJChatDetailInfoHeaderView.h"
#import "HHFriendsDetailsViewController.h"
#import "HHChatBgViewController.h"
#import "HHSingleChatRecordViewController.h"
#import "HHSelectMembersListViewController.h"

@interface HHSingleChatDetailsViewController ()
@property (nonatomic,strong) HHSingleChatDetailsTableView * tableView;
@property (nonatomic,strong) HMJChatDetailInfoHeaderView * headerView;
@property (nonatomic,strong) HHContactsModel *friendsModel;
@end

@implementation HHSingleChatDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"聊天详情";

    [self createUI];
    [self loadData];
    [self handleBlock];
}

- (void)createUI{
    [self.view addSubview:self.tableView];
     self.tableView.tableHeaderView = self.headerView;
}

- (void)loadData{
    HHContactsModel *model = [[HHContactsModel alloc] init];
    model.headUrl = @"qunliao";
    model.nickName = @"天锡";
    self.headerView.friendsModel = model;
}

- (void)handleBlock{
    WeakSelf(self);
    _headerView.tapUserHeadImage = ^{
        HHFriendsDetailsViewController *dvc = Init(HHFriendsDetailsViewController);
        [weakself.navigationController pushViewController:dvc animated:YES];
    };
    _headerView.clickSingleChatDetailToAdd = ^{
        [weakself addMembersForCreateGroup];
    };
    _tableView.clickSingleChatDetailsTableViewCell = ^(NSIndexPath * _Nonnull indexPath) {
        if (indexPath.section == 1) {
            [weakself findSingleChatHistory];
        }
        if (indexPath.section == 2) {
            [weakself clearSingleChatHistory];
        }
        if (indexPath.section == 3) {
            [weakself resetGroupChatBgView];
        }
        if (indexPath.section == 4) {
            [weakself sendReport];
        }
        
    };
}
//添加成员，创建群聊
- (void)addMembersForCreateGroup{
    HHSelectMembersListViewController * creatGroupViewController = [[HHSelectMembersListViewController alloc]init];
    creatGroupViewController.addMembers = YES;
    creatGroupViewController.finishSelectNewGroupMembers = ^(NSMutableArray * _Nonnull array,BOOL add) {
        if (!add) {
            return ;
        }
    };
    [self.navigationController pushViewController:creatGroupViewController animated:YES];
}

//查找聊天记录
- (void)findSingleChatHistory{
    HHSingleChatRecordViewController *rvc = [[HHSingleChatRecordViewController alloc] init];
    [self.navigationController pushViewController:rvc animated:YES];
}

//清空聊天记录
- (void)clearSingleChatHistory{
    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:0];
    [arr addObject:@"清空聊天记录"];
    ZGQActionSheetView *sheetView = [[ZGQActionSheetView alloc] initWithOptions:arr completion:^(NSInteger index) {
        if (index == 0) {
            
        }
    } cancel:^{
            
    }];
    sheetView.optionColor = RGB_RedColor;
    [sheetView show];
}

//设置聊天背景
- (void)resetGroupChatBgView{
    HHChatBgViewController *nvc = Init(HHChatBgViewController);
    [self.navigationController pushViewController:nvc animated:YES];
}

//投诉
- (void)sendReport{
    
}

- (HMJChatDetailInfoHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[HMJChatDetailInfoHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 120)];
    }
    return _headerView;
}

- (HHSingleChatDetailsTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHSingleChatDetailsTableView alloc]initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT-SafeAreaTopHeight) style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ColorManage.grayBackgroundColor;
       }
       return _tableView;
}


@end
