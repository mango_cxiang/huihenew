//
//  HHSingleChatRecordViewController.m
//  HHNew
//
//  Created by Liubin on 2020/5/6.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHSingleChatRecordViewController.h"
#import "FYJDSearchView.h"
#import "HHSingleChatRecordTableView.h"
@interface HHSingleChatRecordViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) FYJDSearchView *searchView;
@property (nonatomic, strong) HHSingleChatRecordTableView *tableView;

@end

@implementation HHSingleChatRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"聊天记录";

    [self createUI];
    [self loadData];
    [self handleBlock];
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
}

- (void)createUI{
    [self.view addSubview:self.searchView];
    [self.view addSubview:self.tableView];
}

- (void)loadData{
    NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:0];
    for (NSInteger i =0; i<20; i++) {
        HHContactsModel *model = [[HHContactsModel alloc] init];
        model.nickName = [NSString stringWithFormat:@"%@%ld",[[HHDefaultTools sharedInstance] shuffledAlphabet], (long)i];
        model.headUrl = @"group";
        [tempArray addObject:model];
    }
    self.tableView.dataListArry = tempArray;
}

- (void)handleBlock{
    self.searchView.searchTextContentChanged = ^(NSString * _Nonnull inputWords) {
        
    };
}

- (HHSingleChatRecordTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHSingleChatRecordTableView alloc]initWithFrame:CGRectMake(0, SafeAreaTopHeight+50, SCREEN_WIDTH, SCREEN_HEIGHT-SafeAreaTopHeight-50) style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ColorManage.grayBackgroundColor;
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    }
    return _tableView;
}

- (FYJDSearchView *)searchView{
    if (!_searchView) {
        _searchView = [[FYJDSearchView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, 50)];
    }
    return _searchView;
}

@end
