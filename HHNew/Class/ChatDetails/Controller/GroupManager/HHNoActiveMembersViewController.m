//
//  HHNoActiveMembersViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHNoActiveMembersViewController.h"
#import "HHGroupNoActiveCell.h"
#import "HHTypeNoActiveViewController.h"

@interface HHNoActiveMembersViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) UITableView *tableView;
@end

@implementation HHNoActiveMembersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"不活跃群成员";
    self.view.backgroundColor = ColorManage.background_color;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:self.tableView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HHGroupNoActiveCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HMKGroupNoActiveCell"];
    if (cell==nil) {
        cell = [[HHGroupNoActiveCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HMKGroupNoActiveCell"];
    }
    if (indexPath.row==0) {
        cell.time.text = @"三天不活跃";
        cell.message.text = @"超过三天未使用闲聊的群成员";
    }else if (indexPath.row==1) {
        cell.time.text = @"一周不活跃";
        cell.message.text = @"超过一周未使用闲聊的群成员";
    }else if (indexPath.row==2) {
        cell.time.text = @"一个月不活跃";
        cell.message.text = @"超过一个月未使用闲聊的群成员";
    }
    cell.backgroundColor = ColorManage.darkBackgroundColor;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    HHTypeNoActiveViewController *noActive = [[HHTypeNoActiveViewController alloc]init];
//    noActive.groupId = self.groupId;
    if (indexPath.row==0) {
        noActive.titleStr = @"三天不活跃";
        noActive.queryType = @"1";
    }else if (indexPath.row==1){
        noActive.titleStr = @"一周不活跃";
        noActive.queryType = @"2";
    }else if (indexPath.row==2){
        noActive.titleStr = @"一个月不活跃";
        noActive.queryType = @"3";
    }
    [self.navigationController pushViewController:noActive animated:YES];
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = ColorManage.grayBackgroundColor;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    return _tableView;
}
@end
