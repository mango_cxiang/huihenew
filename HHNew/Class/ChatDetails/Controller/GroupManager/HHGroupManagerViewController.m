//
//  HHGroupManagerViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/27.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupManagerViewController.h"
#import "HHGroupManagerTableView.h"
#import "HHStopTalkingViewController.h"
#import "HHLimitMoneyViewController.h"
#import "HHSetGroupAdminViewController.h"
#import "HHTransferGroupController.h"
#import "HHMsgRecordViewController.h"
#import "HHCanBackMoneyViewController.h"
#import "HHForgetReceiveMoneyViewController.h"

@interface HHGroupManagerViewController ()
@property (strong, nonatomic) HHGroupManagerTableView *tableView;
@end

@implementation HHGroupManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"群管理";
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    [self loadData];
    [self createUI];
    [self handleBlock];
}

- (void)createUI{
    [self.view addSubview:self.tableView];
}

- (void)loadData{
//    WeakSelf(self);
//    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//    [dict setObject:self.groupModel.groupId forKey:@"groupId"];
//    [HMJBusinessManager GET:@"/group/getDelayedEntryStatus" Parameters:dict success:^(NSDictionary *dic, NSInteger code) {
//        NSInteger codeStr = [dic[@"code"] integerValue];
//        if(codeStr==1){
//            NSDictionary *dataDic = dic[@"data"];
//            weakSelf.status = dataDic[@"status"];
//            weakSelf.redPactAmount = dataDic[@"delayedEntryAmount"];
//            weakSelf.tableView.dataDict = dataDic;
//        }
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//    }];
}

- (void)handleBlock{
    WeakSelf(self);
    self.tableView.clickGroupManagerTableView = ^(NSIndexPath * _Nonnull indexPath) {
            //群成员禁言
            if (indexPath.section==4) {
                HHStopTalkingViewController *tvc = [[HHStopTalkingViewController alloc]init];
//                wordsVc.groupID = [NSString stringWithFormat:@"%ld",(long)self.groupId];
//                wordsVc.createrId = self.groupModel.createrId;
                [weakself.navigationController pushViewController:tvc animated:YES];
            }
            //禁止领取红包
            if (indexPath.section==5) {
                HHLimitMoneyViewController * limitVc = [[HHLimitMoneyViewController alloc]init];
//                limitVc.groupID = [NSString stringWithFormat:@"%ld",(long)self.groupId];
//                limitVc.createrId = self.groupModel.createrId;
                [weakself.navigationController pushViewController:limitVc animated:YES];
            }
            //消息与记录
            if (indexPath.section==6){
                HHMsgRecordViewController *messageRecordVC = [[HHMsgRecordViewController alloc]init];
//                messageRecordVC.groupModel = self.groupModel;
//        //        messageRecordVC.groupId = [NSString stringWithFormat:@"%ld",(long)self.groupId];
                [weakself.navigationController pushViewController:messageRecordVC animated:YES];
            }
            //未领取的红包
            if (indexPath.section==7){
                HHForgetReceiveMoneyViewController *packetList = [[HHForgetReceiveMoneyViewController alloc]init];
//                packetList.groupId = [NSString stringWithFormat:@"%ld",(long)self.groupId];
                [weakself.navigationController pushViewController:packetList animated:YES];
            }
            //可退回的红包
            if (indexPath.section==8){
//                if ([_status isEqualToString:@"1"]) {
                    HHCanBackMoneyViewController *packetList = [[HHCanBackMoneyViewController alloc]init];
//                    packetList.groupModel = self.groupModel;
                    [weakself.navigationController pushViewController:packetList animated:YES];
//                }else {
//                    [[HMJShowLoding sharedInstance] showText:@"该功能维护中"];
//                }
            }
            
            if (indexPath.section==9){
                //辅助功能
                if (indexPath.row == 0) {
//                    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//                    [dict setObject:@"h5_url" forKey:@"key"];
//                    [dict setObject:@"groupAuxiliary" forKey:@"code"];
//                    [dict setObject:[NSString acquireUserId] forKey:@"userId"];
//                    [HMJBusinessManager GET:@"/config/get" Parameters:dict success:^(NSDictionary *dic, NSInteger code) {
//                        NSInteger codeStr = [dic[@"code"] integerValue];
//                        if(codeStr==1){
//                            NSString *urlStr = [NSString stringWithFormat:@"%@?userid=%@&nickname=%@&headpic=%@&timestamp=%@&groupId=%@",dic[@"data"][@"detailName"],[NSString acquireUserId],[HMJAccountModel myself].nickName,[HMJAccountModel myself].headUrl,[NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]],[NSString stringWithFormat:@"%ld",(long)self.groupId]];
//                            urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//                            YXZIKBaseWKWebViewController *resultVC = [[YXZIKBaseWKWebViewController alloc] init];
//                            [resultVC loadWebURLSring:urlStr];
//                            [weakself.navigationController pushViewController:resultVC animated:YES];
//                        }
//                    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//                    }];
                }
                //设置管理员
                if (indexPath.row == 1) {
                    HHSetGroupAdminViewController *adminList = [[HHSetGroupAdminViewController alloc]init];
//                    adminList.groupId = [NSString stringWithFormat:@"%ld",(long)self.groupId];
//                    adminList.createrId = self.groupModel.createrId;
                    [weakself.navigationController pushViewController:adminList animated:YES];
                }
                //转让群主
                if (indexPath.row == 2) {
                    HHTransferGroupController * transferVc = [[HHTransferGroupController alloc]init];
//                    transferVc.groupId = [NSString stringWithFormat:@"%ld",(long)self.groupId];
//                    transferVc.createrId = self.groupModel.createrId;
                    [weakself.navigationController pushViewController:transferVc animated:YES];
                }
            }
    };
}

- (HHGroupManagerTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHGroupManagerTableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return _tableView;
}

@end
