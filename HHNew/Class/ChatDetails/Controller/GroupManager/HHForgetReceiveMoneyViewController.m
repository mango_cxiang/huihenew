//
//  HHForgetReceiveMoneyViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/30.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHForgetReceiveMoneyViewController.h"
#import "HHForgetMoneyTableView.h"
#import "HHPacketListModel.h"
@interface HHForgetReceiveMoneyViewController ()
@property (strong, nonatomic) HHForgetMoneyTableView *tableView;

@end

@implementation HHForgetReceiveMoneyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"未领取红包";
    [self.view addSubview:self.tableView];
    self.view.backgroundColor = ColorManage.darkBackgroundColor;
    [self loadData];
    [self handleBlock];
}

- (void)loadData{
    //假数据：
    NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:0];
    for (NSInteger i =0; i<5; i++) {
       HHPacketListModel *model = [[HHPacketListModel alloc] init];
       model.name = @"hhhhh";
       model.userId = @"0";
       model.content = @"祝你生日快乐解决解决军军交接";
       model.headUrl = @"qunliao";
       model.sendTime = @"2020-04-21 15:32";
       model.redPacketContent = @"100";
       [tempArray addObject:model];
    }
    for (NSInteger i =0; i<5; i++) {
       HHPacketListModel *model = [[HHPacketListModel alloc] init];
       model.name = @"he";
        model.userId = @"1";
       model.content = @"红红火火恍恍h哈哈哈哈哈";
       model.headUrl = @"qunliao";
       model.sendTime = @"2020-04-21 15:32";
       model.redPacketContent = @"100";
       [tempArray addObject:model];
    }
    self.tableView.dataArray = tempArray;
}

- (void)handleBlock{
    WeakSelf(self);
    //点击他人的红包
    self.tableView.clickMyPacketForgetMoneyCell = ^(HHPacketListModel * _Nonnull model) {
        
    };
    //点击他人的红包
    self.tableView.clickOtherPacketForgetMoneyCell = ^(HHPacketListModel * _Nonnull model) {
        
    };
    
}

- (HHForgetMoneyTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHForgetMoneyTableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ColorManage.darkBackgroundColor;
    }
    return _tableView;
}


@end
