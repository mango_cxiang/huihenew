//
//  HHExitGroupMembersViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHExitGroupMembersViewController.h"
#import "HHExitGroupMembersTablewView.h"
#import "HHFriendsDetailsViewController.h"

@interface HHExitGroupMembersViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) HHExitGroupMembersTablewView *tableView;

@end

@implementation HHExitGroupMembersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"退群成员列表";
    [self.view addSubview:self.tableView];
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    [self loadData];
    [self handleBlock];
}

- (void)loadData{
    //假数据：
    NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:0];
   for (NSInteger i =0; i<5; i++) {
        HHContactsModel *model = [[HHContactsModel alloc] init];
        model.nickName = [NSString stringWithFormat:@"%@%ld", @"召唤师", (long)i];
        model.headUrl = @"qunliao";
        model.updatetime = @"2020-04-21 15:32:01";
        [tempArray addObject:model];
    }
    self.tableView.dataListArry = tempArray;
}

- (void)handleBlock{
    WeakSelf(self);
    self.tableView.clickExitGroupMembersTableViewCell = ^(NSIndexPath * _Nonnull indexPath, HHContactsModel * _Nonnull model) {
        HHFriendsDetailsViewController *dvc = Init(HHFriendsDetailsViewController);
        [weakself.navigationController pushViewController:dvc animated:YES];
    };
}

- (HHExitGroupMembersTablewView *)tableView{
    if (!_tableView) {
        _tableView = [[HHExitGroupMembersTablewView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return _tableView;
}


@end
