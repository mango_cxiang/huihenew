//
//  HHCanBackMoneyViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHCanBackMoneyViewController.h"
#import "HHRecodModel.h"
#import "HHCanBackMoneyTableView.h"
@interface HHCanBackMoneyViewController ()
@property (strong, nonatomic) HHCanBackMoneyTableView *tableView;

@end

@implementation HHCanBackMoneyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"可退回的红包";
    [self.view addSubview:self.tableView];
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    [self loadData];
    [self handleBlock];
}

- (void)loadData{
    //假数据：
    NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:0];
   for (NSInteger i =0; i<5; i++) {
        HHRecodModel *model = [[HHRecodModel alloc] init];
//        model.nickName = [NSString stringWithFormat:@"%@%ld", @"召唤师", (long)i];
//        model.headUrl = @"qunliao";
//        model.updatetime = @"2020-04-21 15:32:01";
        [tempArray addObject:model];
    }
    self.tableView.dataArray = tempArray;
}

- (void)handleBlock{
    WeakSelf(self);
    self.tableView.backMoney = ^(HHRecodModel * _Nonnull model) {
        [weakself showAlerTitle:@"是否确定退回此红包" backMoney:YES];
    };
    
    self.tableView.getMoney = ^(HHRecodModel * _Nonnull model) {
        [weakself showAlerTitle:@"是否确定直接入账给领取人此红包" backMoney:NO];
    };
}

- (void)showAlerTitle:(NSString *)title backMoney:(BOOL)backMoney{
    UIAlertController *alertSheet = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *backAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"退回红包");
    }];
    UIAlertAction *getAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"领取红包");
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    if (backMoney) {
        [alertSheet addAction:backAction];
    }else{
        [alertSheet addAction:getAction];
    }
    [alertSheet addAction:cancel];
    [self presentViewController:alertSheet animated:YES completion:nil];
}

- (HHCanBackMoneyTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHCanBackMoneyTableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return _tableView;
}

@end
