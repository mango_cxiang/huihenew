//
//  HHTransferGroupController.m
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHTransferGroupController.h"
#import "SCIndexViewConfiguration.h"
#import "SCIndexView.h"
#import "HHIndexMembersListTableView.h"
#import "HHContactsModel.h"
@interface HHTransferGroupController ()<SCIndexViewDelegate>
@property (strong, nonatomic) HHIndexMembersListTableView *tableView;
@property (nonatomic, strong) SCIndexView *indexView;
@property (nonatomic, assign) SCIndexViewStyle indexViewStyle;
@property (strong, nonatomic) NSMutableArray *membersArray;
@end

@implementation HHTransferGroupController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"转让群";
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.indexView];
    [self loadData];
    [self handleBlock];
}

- (void)loadData{
    //假数据：
    NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:0];
   for (NSInteger i =0; i<5; i++) {
        HHContactsModel *model = [[HHContactsModel alloc] init];
        model.nickName = [NSString stringWithFormat:@"%@%ld", @"召唤师", (long)i];
        model.headUrl = @"qunliao";
        [tempArray addObject:model];
    }
    for (NSInteger i =0; i<5; i++) {
        HHContactsModel *model = [[HHContactsModel alloc] init];
        model.nickName = [NSString stringWithFormat:@"%@%ld", @"还有谁", (long)i];
        model.headUrl = @"qunliao";
        [tempArray addObject:model];
    }
    self.tableView.dataListArray = tempArray;
    
    NSMutableArray* indexPathArray = [DataHelper getContactListSectionBy:self.tableView.dataListArray];
    // 配置索引数据
    if ([indexPathArray count]>0) {
        NSMutableArray *indexViewDataSource = [NSMutableArray array];
        for (NSString *str in indexPathArray) {
            [indexViewDataSource addObject:str];
        }
        self.indexView.dataSource = indexViewDataSource.copy;
    }
}

- (void)handleBlock{
    WeakSelf(self);
    self.tableView.clickHHIndexMembersTableViewCell = ^(NSIndexPath * _Nonnull indexPath, HHContactsModel * _Nonnull model) {
        [weakself showAlertVC:model];
    };
}

- (void)showAlertVC:(HHContactsModel *)model{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"是否确定选择为新群主，你将被解除群主身份" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *conform = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self transferGroupRequest:model.contactId];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:conform];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)transferGroupRequest:(NSString *)destId{
//    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//    [params setValue:[NSString acquireUserId] forKey:@"userId"];
//    [params setValue:self.groupId forKey:@"groupId"];
//    [params setValue:destId forKey:@"destId"];
//    [HMJBusinessManager TotalPOST:@"/group/transferGroup" parameters:params withHost:[[AppSetting sharedAppSetting] newServiceUrlStr] withHUDShow:NO withHUDHide:NO mediaDatas:nil success:^(NSDictionary *dic, NSInteger code) {
//        NSInteger codeInt = [dic[@"code"] integerValue];
//        if(codeInt==1){
//            NSInteger index = (NSInteger)[[self.navigationController viewControllers] indexOfObject:self];
//            if (index > 4) {
//                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(index-4)] animated:YES];
//            }else{
//                [self.navigationController popToRootViewControllerAnimated:YES];
//            }
//        }else{
//            [[HMJShowLoding sharedInstance] showText:dic[@"data"][@"info"]];
//        }
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//    }];
}


- (NSMutableArray *)membersArray{
    if (!_membersArray) {
        _membersArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _membersArray;
}

- (SCIndexView *)indexView{
    if (!_indexView) {
        _indexView = [[SCIndexView alloc] initWithTableView:self.tableView configuration:[SCIndexViewConfiguration configurationWithIndexViewStyle:self.indexViewStyle]];
        _indexView.translucentForTableViewInNavigationBar = YES;
        _indexView.delegate = self;
    }
    return _indexView;
}


- (HHIndexMembersListTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHIndexMembersListTableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return _tableView;
}



@end
