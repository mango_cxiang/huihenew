//
//  HHAddLimitMembersViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHAddLimitMembersViewController.h"
#import "SCIndexViewConfiguration.h"
#import "SCIndexView.h"
#import "HHSelectMembersTableView.h"
#import "HHContactsModel.h"
@interface HHAddLimitMembersViewController ()<SCIndexViewDelegate>
@property (strong, nonatomic) HHSelectMembersTableView *tableView;
@property (nonatomic, strong) SCIndexView *indexView;
@property (nonatomic, assign) SCIndexViewStyle indexViewStyle;
@property (strong, nonatomic) NSMutableArray *membersArray;

@end
@implementation HHAddLimitMembersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"添加成员";
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    [self setRightBarButtonItem:@"确定" color:ColorManage.text_color];

    [self.view addSubview:self.tableView];
    [self.view addSubview:self.indexView];
    [self loadData];
    [self handleBlock];
}

- (void)rightBarButtonItemAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    if (self.addLimitMembersFinish) {
        self.addLimitMembersFinish(self.membersArray);
    }
}

- (void)loadData{
    //假数据：产生100个数字+三个随机字母
    NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:0];
   for (NSInteger i =0; i<5; i++) {
        HHContactsModel *model = [[HHContactsModel alloc] init];
        model.nickName = [NSString stringWithFormat:@"%@%ld", @"召唤师", (long)i];
        model.headUrl = @"qunliao";
        [tempArray addObject:model];
    }
    for (NSInteger i =0; i<5; i++) {
        HHContactsModel *model = [[HHContactsModel alloc] init];
        model.nickName = [NSString stringWithFormat:@"%@%ld", @"还有谁", (long)i];
        model.headUrl = @"qunliao";
        [tempArray addObject:model];
    }
    self.tableView.dataListArray = tempArray;
    
    NSMutableArray* indexPathArray = [DataHelper getContactListSectionBy:self.tableView.dataListArray];
    // 配置索引数据
    if ([indexPathArray count]>0) {
        NSMutableArray *indexViewDataSource = [NSMutableArray array];
        for (NSString *str in indexPathArray) {
            [indexViewDataSource addObject:str];
        }
        self.indexView.dataSource = indexViewDataSource.copy;
    }
}

- (void)handleBlock{
    WeakSelf(self);
    self.tableView.addMembersFinishTableView = ^(NSMutableArray * _Nonnull membersArray) {
        weakself.membersArray = [NSMutableArray arrayWithArray:membersArray];
    };
}

- (NSMutableArray *)membersArray{
    if (!_membersArray) {
        _membersArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _membersArray;
}

- (SCIndexView *)indexView{
    if (!_indexView) {
        _indexView = [[SCIndexView alloc] initWithTableView:self.tableView configuration:[SCIndexViewConfiguration configurationWithIndexViewStyle:self.indexViewStyle]];
        _indexView.translucentForTableViewInNavigationBar = YES;
        _indexView.delegate = self;
    }
    return _indexView;
}


- (HHSelectMembersTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHSelectMembersTableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return _tableView;
}

@end
