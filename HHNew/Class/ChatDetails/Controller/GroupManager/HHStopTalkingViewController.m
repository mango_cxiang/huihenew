//
//  HHStopTalkingViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/27.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHStopTalkingViewController.h"
#import "HHStopTalkingMembersTableView.h"
#import "SCIndexViewConfiguration.h"
#import "SCIndexView.h"
#import "HHContactsModel.h"
#import "DataHelper.h"

@interface HHStopTalkingViewController ()<SCIndexViewDelegate>
@property (strong, nonatomic) HHStopTalkingMembersTableView *tableView;
@property (nonatomic, strong) SCIndexView *indexView;
@property (nonatomic, assign) SCIndexViewStyle indexViewStyle;

@end

@implementation HHStopTalkingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"禁言成员";
    // Do any additional setup after loading the view.
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.indexView];
    [self loadData];
    [self handleBlock];
}

- (void)loadData{
    //假数据：产生100个数字+三个随机字母
    NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:0];
    for (NSInteger i =0; i<50; i++) {
        HHContactsModel *model = [[HHContactsModel alloc] init];
        model.nickName = [NSString stringWithFormat:@"%@%ld",[[HHDefaultTools sharedInstance] shuffledAlphabet], (long)i];
        model.headUrl = @"qunliao";
        [tempArray addObject:model];
    }
    self.tableView.dataListArry = tempArray;
    NSMutableArray* indexPathArray = [DataHelper getContactListSectionBy:self.tableView.dataListArry];

    // 配置索引数据
    if ([indexPathArray count]>0) {
        NSMutableArray *indexViewDataSource = [NSMutableArray array];
        for (NSString *str in indexPathArray) {
            [indexViewDataSource addObject:str];
        }
        self.indexView.dataSource = indexViewDataSource.copy;
    }
}

- (void)handleBlock{
    WeakSelf(self);
    
    self.tableView.clickUniversalFriendsListTableView = ^(NSIndexPath * _Nonnull indexPath, HHContactsModel * _Nonnull model) {
        NSArray * arr = @[@"禁言10分钟",@"禁言30分钟",@"禁言60分钟",@"禁言12小时",@"禁言24小时",@"取消禁言"];
        ZGQActionSheetView *sheetView = [[ZGQActionSheetView alloc] initWithOptions:arr completion:^(NSInteger index) {
            switch (index) {
                case 0:
                    {
                        model.allowSay = @"1";
                        [weakself.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    }
                    break;
                case 1:
                    {
                        model.allowSay = @"2";
                        [weakself.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    }
                    break;
                case 2:
                    {
                        model.allowSay = @"3";
                        [weakself.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];

                    }
                    break;
                case 3:
                    {
                        model.allowSay = @"4";
                        [weakself.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];

                    }
                    break;
                case 4:
                    {
                        model.allowSay = @"5";
                        [weakself.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    }
                    break;
                case 5:
                    {
                        model.allowSay = @"0";
                        [weakself.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    }
                    break;
                default:
                    break;
                }
            } cancel:^{
                
            }];
        
        sheetView.maxShowCount = 6;
        sheetView.showSampleView = YES;
        sheetView.sampleTitleName = @"请选择禁言时长";
        [sheetView show];
    };
}


- (HHStopTalkingMembersTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHStopTalkingMembersTableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return _tableView;
}

- (SCIndexView *)indexView{
    if (!_indexView) {
        _indexView = [[SCIndexView alloc] initWithTableView:self.tableView configuration:[SCIndexViewConfiguration configurationWithIndexViewStyle:self.indexViewStyle]];
        _indexView.translucentForTableViewInNavigationBar = YES;
        _indexView.delegate = self;
    }
    return _indexView;
}

@end
