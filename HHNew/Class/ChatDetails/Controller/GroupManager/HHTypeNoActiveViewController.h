//
//  HHTypeNoActiveViewController.h
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HHTypeNoActiveViewController : BaseViewController
@property (copy, nonatomic) NSString *titleStr;
@property (nonatomic , strong) NSString *groupId;
@property (nonatomic , strong) NSString *queryType;

@end

NS_ASSUME_NONNULL_END
