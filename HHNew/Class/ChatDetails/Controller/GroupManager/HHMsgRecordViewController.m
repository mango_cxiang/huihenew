//
//  HHMsgRecordViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHMsgRecordViewController.h"
#import "HMKGroupManageCell.h"
#import "HHExitGroupMembersViewController.h"
#import "HHNoActiveMembersViewController.h"

@interface HHMsgRecordViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) UITableView *tableView;
@end

@implementation HHMsgRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"消息与记录";
    [self.view addSubview:self.tableView];
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==0) {
        return 30;
    }else if(section==1){
        return 10;
    }else{
        return 0.1;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section==0) {
        UIView *footView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
        UILabel *footLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, SCREEN_WIDTH-30, 20)];
        footLabel.centerY = footView.centerY;
        footLabel.text = @"开启后，超过36小时的消息会自动清理";
        footLabel.font = kRegularFont(12);
        footLabel.textAlignment = NSTextAlignmentLeft;
        footLabel.textColor = ColorManage.grayTextColor_Deault;
        [footView addSubview:footLabel];
        return footView;
    }else{
        return [[UIView alloc]init];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *titleArr = [NSArray arrayWithObjects:@"消息定时清理",@"退群成员",@"不活跃成员", nil];
    if (indexPath.section==0) {
        HMKGroupManageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HMKGroupManageCell"];
        if (cell==nil) {
            cell = [[HMKGroupManageCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HMKGroupManageCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.leftLabel.text = titleArr[indexPath.section];
//        if ([self.groupModel.msgClear isEqualToString:@"1"]) {
//            cell.switchBtn.on = YES;
//        }else{
//            cell.switchBtn.on = NO;
//        }
        cell.backgroundColor = ColorManage.darkBackgroundColor;
        [cell.switchBtn addTarget:self action:@selector(switchClick:) forControlEvents:UIControlEventValueChanged];
        return cell;
    }else{
        UITableViewCell *cell = [[UITableViewCell alloc]init];
        UILabel *leftLabel = [[UILabel alloc]init];
        leftLabel.text = titleArr[indexPath.section];
        leftLabel.textColor = ColorManage.text_color;
        leftLabel.font = [UIFont systemFontOfSize:16];
        [cell.contentView addSubview:leftLabel];
        [leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(cell.contentView.mas_left).offset(15);
            make.centerY.mas_equalTo(cell.contentView.mas_centerY);
        }];
        UIImageView *arrowImg = [MyUtils imageViewWithFrame:CGRectMake(SCREEN_WIDTH-8-15, 21, 8, 13) image:[UIImage imageNamed:@"next"] backColor:nil];
        [cell.contentView addSubview:arrowImg];
        UIView *line = [[UIView alloc]init];
        line.backgroundColor = ColorManage.lineViewColor;
        [cell.contentView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(cell.contentView.mas_left).offset(15);
            make.right.mas_equalTo(cell.contentView.mas_right);
            make.bottom.mas_equalTo(cell.contentView.mas_bottom);
            make.height.mas_equalTo(0.5);
        }];
        cell.backgroundColor = ColorManage.darkBackgroundColor;
        cell.selectedBackgroundView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section==1) {
        HHExitGroupMembersViewController *exitGroup = [[HHExitGroupMembersViewController alloc]init];
//        exitGroup.groupId = self.groupModel.groupId;
        [self.navigationController pushViewController:exitGroup animated:YES];
    }else if (indexPath.section==2){
        HHNoActiveMembersViewController *noActiveVc = [[HHNoActiveMembersViewController alloc]init];
//        noActiveVc.groupId = self.groupModel.groupId;
        [self.navigationController pushViewController:noActiveVc animated:YES];
    }
}
-(void)switchClick:(id)sender{
    UISwitch *switchView = (UISwitch *)sender;
    if ([switchView isOn]) {
        [self switchOnRequset:@"4" Status:@"1"];
    }else{
        [self switchOnRequset:@"4" Status:@"0"];
    }
}

-(void)switchOnRequset:(NSString *)type Status:(NSString *)status{
//    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//    [params setValue:[NSString acquireUserId] forKey:@"userId"];
//    [params setValue:self.groupModel.groupId forKey:@"groupId"];
//    [params setValue:type forKey:@"type"];
//    [params setValue:status forKey:@"status"];
//    [HMJBusinessManager TotalGET:@"/group/groupSetting" Parameters:params host:[[AppSetting sharedAppSetting] newServiceUrlStr] success:^(NSDictionary *dic, NSInteger code) {
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//    }];
}


- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = ColorManage.grayBackgroundColor;
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    return _tableView;
}
@end
