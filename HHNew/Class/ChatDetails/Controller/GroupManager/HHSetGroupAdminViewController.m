//
//  HHSetGroupAdminViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHSetGroupAdminViewController.h"
#import "HHAddGroupAdminViewController.h"
#import "HHMembersListTableView.h"
#import "HHContactsModel.h"
@interface HHSetGroupAdminViewController ()
@property (strong, nonatomic) HHMembersListTableView *tableView;
@end

@implementation HHSetGroupAdminViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"管理员列表";
    [self.view addSubview:self.tableView];
    [self setRightBarButtonItem:@"添加" color:ColorManage.text_color];
    [self loadData];
    [self handleBlock];
}


- (void)rightBarButtonItemAction:(id)sender{
    WeakSelf(self)
    HHAddGroupAdminViewController *mvc = Init(HHAddGroupAdminViewController);
    mvc.addGroupAdminFinish = ^(NSMutableArray * _Nonnull membersArray) {
        for (HHContactsModel *model in membersArray) {
            [weakself.tableView.dataArry addObject:model];
        }
        [weakself.tableView reloadData];
    };
    [self.navigationController pushViewController:mvc animated:YES];
}

- (void)loadData{
    //假数据：产生100个数字+三个随机字母
    NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:0];
   for (NSInteger i =0; i<5; i++) {
        HHContactsModel *model = [[HHContactsModel alloc] init];
        model.nickName = [NSString stringWithFormat:@"%@%ld", @"召唤师", (long)i];
        model.headUrl = @"qunliao";
        [tempArray addObject:model];
    }
    self.tableView.dataArry = tempArray;
}

- (void)handleBlock{
    WeakSelf(self);
    self.tableView.clickHHMembersListTableViewCell = ^(NSIndexPath * _Nonnull indexPath, HHContactsModel * _Nonnull model) {
        NSArray * arr = @[@"移除管理员"];
        ZGQActionSheetView *sheetView = [[ZGQActionSheetView alloc] initWithOptions:arr completion:^(NSInteger index) {
            if (index == 0) {
                [weakself.tableView.dataArry removeObjectAtIndex:indexPath.row];
                [weakself.tableView reloadData];
            }
        } cancel:^{
                       
        }];
//        sheetView.showSampleView = YES;
//        sheetView.sampleTitleName = @"取消禁止";
        sheetView.optionColor = RGB_RedColor;
        [sheetView show];
    };
    //左滑删除
    self.tableView.deleteHHMembersListTableViewCell = ^(NSIndexPath * _Nonnull indexPath, HHContactsModel * _Nonnull model) {
        
    };
}



- (HHMembersListTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHMembersListTableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return _tableView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
