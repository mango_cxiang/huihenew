//
//  HHSetGroupAdminViewController.h
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^addGroupAdminFinish)(NSMutableArray *membersArray);

@interface HHAddGroupAdminViewController : BaseViewController
@property (copy, nonatomic) addGroupAdminFinish addGroupAdminFinish;

@end

NS_ASSUME_NONNULL_END
