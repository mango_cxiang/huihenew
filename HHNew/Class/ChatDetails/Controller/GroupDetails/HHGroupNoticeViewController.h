//
//  HHGroupNoticeViewController.h
//  HHNew
//
//  Created by Liubin on 2020/4/27.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^changeGroupNoticeFinished)(NSString *noticeString);
@interface HHGroupNoticeViewController : BaseViewController
@property (nonatomic,copy) NSString * noticeStr;

@property (nonatomic,copy) changeGroupNoticeFinished changeGroupNoticeFinished;

@end

NS_ASSUME_NONNULL_END
