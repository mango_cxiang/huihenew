//
//  HHGroupSearchMembersViewController.h
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "BaseViewController.h"
@class HHContactsModel;
NS_ASSUME_NONNULL_BEGIN
typedef void(^clickGroupSearchMembersResult)(HHContactsModel * _Nonnull model);
@interface HHGroupSearchMembersViewController : BaseViewController<UISearchResultsUpdating>
@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) NSMutableArray *dataListArry;
@property (strong, nonatomic) UINavigationController *nav;

@property (copy, nonatomic) clickGroupSearchMembersResult clickGroupSearchMembersResult;
- (void)clearResultData;

@end

NS_ASSUME_NONNULL_END
