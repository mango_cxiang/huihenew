//
//  HHGroupChatNameViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/26.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupChatNameViewController.h"

@interface HHGroupChatNameViewController ()<UITextFieldDelegate>

@end

@implementation HHGroupChatNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    [self.nameTextField addTarget:self action:@selector(nameContentChanged:) forControlEvents:UIControlEventEditingChanged];
    self.nameTextField.delegate = self;
    self.titleLabel.backgroundColor = ColorManage.background_color;
    self.view.backgroundColor = ColorManage.background_color;
}

- (void)setGroupName:(NSString *)groupName{
    _groupName = groupName;
    
}

- (void)setUserNickName:(NSString *)userNickName{
    _userNickName = userNickName;
   
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.nameTextField becomeFirstResponder];
    [self createUI];
    if (_groupName.length > 0) {
        self.titleLabel.text = @"修改群聊名称";
        self.nameTextField.text = _groupName;
    }
    else{
        self.titleLabel.text = @"修改我在本群的昵称";
        self.nameTextField.text = _userNickName;
    }
}

- (void)createUI{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 10)];
    self.nameTextField.leftViewMode = UITextFieldViewModeAlways;
    self.nameTextField.leftView = view;
    self.finishBtn.enabled = (self.groupName.length > 0)?YES :NO;
    self.titleLabel.textColor = ColorManage.text_color;
    self.titleLabel.font = kSemiboldsFont(20);
    
    self.nameTextField.tintColor = ColorManage.greenTextColor;
    self.nameTextField.backgroundColor = ColorManage.addressHeaderViewTextColor;
    self.nameTextField.font = kRegularFont(14);
    self.nameTextField.textColor = ColorManage.text_color;
    self.nameTextField.layer.cornerRadius = 5;
    self.nameTextField.layer.masksToBounds = YES;

    self.finishBtn.layer.cornerRadius = 5;
    self.finishBtn.layer.masksToBounds = YES;
    self.finishBtn.enabled = NO;
    self.finishBtn.backgroundColor = ColorManage.loginBtnColorDefault;
}


//处理字符动态输入,刷新登录按钮状态
- (void)nameContentChanged:(UITextField *)phfield{
    if (phfield.text.length > 0 ) {
        [self canClickFinishBtn:YES];
    }else{
        [self canClickFinishBtn:NO];
    }
}

- (void)canClickFinishBtn:(BOOL)canClick{
    if (canClick) {
        self.finishBtn.enabled = YES;
        self.finishBtn.backgroundColor = ColorManage.greenTextColor;
        [self.finishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        return;
    }
    self.finishBtn.enabled = NO;
    self.finishBtn.backgroundColor = ColorManage.loginBtnColorDefault;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.nameTextField resignFirstResponder];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.nameTextField resignFirstResponder];
    return YES;
}
@end
