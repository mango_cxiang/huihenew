//
//  HHGroupRecordViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupRecordViewController.h"
#import "SearchDetailView.h"
#import "HHGroupRecordTypeView.h"
#import "HHRecordMembersTypeViewController.h"
#import "HHRecordPayViewController.h"
#import "HHRecordDateViewController.h"
#import "HHRecordImageVideoViewController.h"
#import "HHRecordSearchResultTableView.h"
#import "HHContactsModel.h"

@interface HHGroupRecordViewController ()<SearchDetailViewDelegate>
@property (strong, nonatomic) SearchDetailView *searchDetailView;
@property (strong, nonatomic) HHGroupRecordTypeView *recordTypeView;
@property (strong, nonatomic) HHRecordSearchResultTableView *resultTableView;
@property (strong, nonatomic) NSMutableArray *dataArray;

@end

@implementation HHGroupRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    [self createUI];
    [self handleBlock];
}

- (void)createUI{
    self.navigationController.navigationBar.barTintColor = ColorManage.grayBackgroundColor;
//    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar addSubview:self.searchDetailView];
    [self.view addSubview:self.recordTypeView];
    [self.view addSubview:self.resultTableView];
//    self.searchTagTableView.tableFooterView = [[UIView alloc] init];
//    self.searchTagTableView.backgroundColor = [UIColor whiteColor];
//    self.searchResultTableView.tableFooterView = [[UIView alloc] init];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
    [self.searchDetailView.textField becomeFirstResponder];
    self.searchDetailView.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.searchDetailView.hidden = YES;
    self.navigationController.navigationBar.translucent = YES;
    [self.searchDetailView.textField resignFirstResponder];
}

- (void)handleBlock{
    WeakSelf(self);
    self.recordTypeView.selectGroupRecordType = ^(SearchGroupRecordType type) {
        if (type == SearchGroupRecordMembers) { //群成员
            HHRecordMembersTypeViewController *mtvc = Init(HHRecordMembersTypeViewController);
            [weakself.navigationController pushViewController:mtvc animated:YES];
        }
        if (type == SearchGroupRecordPay) { //交易
            HHRecordPayViewController *pvc = Init(HHRecordPayViewController);
            [weakself.navigationController pushViewController:pvc animated:YES];
        }
        if (type == SearchGroupRecordUrl) { //链接
            [[HMJShowLoding sharedInstance] showText:@"功能开发中"];
        }
        if (type == SearchGroupRecordFile) { //文件
            [[HMJShowLoding sharedInstance] showText:@"功能开发中"];
        }
        if (type == SearchGroupRecordDate) { //日期
            HHRecordDateViewController *pvc = Init(HHRecordDateViewController);
            [weakself.navigationController pushViewController:pvc animated:YES];
        }
        if (type == SearchGroupRecordImageAndVideo) { //图片及视频
            HHRecordImageVideoViewController *vvc = Init(HHRecordImageVideoViewController);
            [weakself.navigationController pushViewController:vvc animated:YES];
        }
    };
}

#pragma mark - SearchDetailViewDelegate
- (void)dismissButtonWasPressedForSearchDetailView:(SearchDetailView *)searchView {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchButtonWasPressedForSearchDetailView:(SearchDetailView *)searchView {
    self.resultTableView.hidden = NO;
    self.recordTypeView.hidden = YES;
    self.resultTableView.inputWords = searchView.textField.text;
    //此处填充数据
    [self loadRecordSearchResultData];
}

- (void)textFieldEditingChangedForSearchDetailView:(SearchDetailView *)searchView {
//    NSLog(@"搜索内容：：：：：：：%@",searchView.textField.text);
//    NSMutableArray<HMJMessageModel*> *dataMode = [[HMJFMDBMessageDataModel sharedInstance]querySessionListWithmsgcontent:searchView.textField.text withSessinId:[NSString stringWithFormat:@"2%@%@",SESSIONID_MIDDLE,self.destId]];
//    self.listArr = dataMode;
//    self.searchTagTableView.hidden  = YES;
//    self.searchResultTableView.hidden = !self.searchTagTableView.hidden;

    if (searchView.textField.text.length <= 0) {
        self.resultTableView.hidden = YES;
        self.recordTypeView.hidden = NO;
        [self.resultTableView.dataArray removeAllObjects];
        self.resultTableView.inputWords = @"";
        return;
    }
}



- (void)loadRecordSearchResultData{
    NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:0];
    for (NSInteger i =0; i<5; i++) {
        HHContactsModel *model = [[HHContactsModel alloc] init];
        model.nickName = [NSString stringWithFormat:@"%@%ld", @"召唤师", (long)i];
        model.headUrl = @"qunliao";
        model.updatetime = @"2020/05/06";
        [tempArray addObject:model];
    }
    self.resultTableView.dataArray = tempArray;

}

- (SearchDetailView *)searchDetailView{
    if (!_searchDetailView) {
        _searchDetailView = [[SearchDetailView alloc] initWithFrame:CGRectMake(0, 3, SCREEN_WIDTH, 30)];
        _searchDetailView.delegate = self;
        [_searchDetailView.textField becomeFirstResponder];
        _searchDetailView.textField.placeholder = @"  搜索";
    }
    return _searchDetailView;
}

- (HHGroupRecordTypeView *)recordTypeView{
    if (!_recordTypeView) {
        _recordTypeView = [[HHGroupRecordTypeView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    }
    return _recordTypeView;
}


- (HHRecordSearchResultTableView *)resultTableView{
    if (!_resultTableView) {
        _resultTableView = [[HHRecordSearchResultTableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
        _resultTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _resultTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _resultTableView.backgroundColor = ColorManage.grayBackgroundColor;
        _resultTableView.hidden = YES;
    }
    return _resultTableView;
}


- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArray;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.searchDetailView.textField resignFirstResponder];
}
@end
