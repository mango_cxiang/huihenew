//
//  HHChatBgViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/27.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHChatBgViewController.h"
#import "HMKChatBgSelectViewController.h"
#import "UIImage+TOResize.h"
#import "JJImagePicker.h"
@interface HHChatBgViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (nonatomic,strong) UIImagePickerController * imagePickerController;

@end

@implementation HHChatBgViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"聊天背景";
    // Do any additional setup after loading the view from its nib.
    self.selectBgLabel.textColor = ColorManage.text_color;
    self.photosLabel.textColor = ColorManage.text_color;
    self.cameraLabel.textColor = ColorManage.text_color;
    
    self.selectBgLabel.font = kRegularFont(16);
    self.photosLabel.font = kRegularFont(16);
    self.cameraLabel.font = kRegularFont(16);
    
//    self.selectBgLabel.backgroundColor = ColorManage.background_color;
//    self.photosLabel.backgroundColor = ColorManage.background_color;
//    self.cameraLabel.backgroundColor = ColorManage.background_color;
    self.lineView.backgroundColor = ColorManage.lineViewColor;
    
    self.selectView.backgroundColor = ColorManage.darkBackgroundColor;
    self.photosView.backgroundColor = ColorManage.darkBackgroundColor;
    self.cameraView.backgroundColor = ColorManage.darkBackgroundColor;
    self.bgScrollView.backgroundColor = ColorManage.grayBackgroundColor;
    self.bgView.backgroundColor = ColorManage.grayBackgroundColor;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)selectGroupBgView:(id)sender {
    HMKChatBgSelectViewController *svc = Init(HMKChatBgSelectViewController);
    UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:svc];
    nvc.modalPresentationStyle = UIModalPresentationFullScreen;

    [self presentViewController:nvc animated:YES completion:^{
    }];
}

- (IBAction)selectFromPhonePhotos:(id)sender {
    JJImagePicker *picker = [[HHDefaultTools sharedInstance] getJJImagePicker];
    picker.aspectRatioPreset = TOCropViewControllerAspectRatioPresetOriginal;
//        WeakSelf(self)
    [picker showImagePickerWithType:JJImagePickerTypePhoto InViewController:self didFinished:^(JJImagePicker *picker, UIImage *image) {
            
    }];
}

- (IBAction)selectFromCamera:(id)sender {
   JJImagePicker *picker = [[HHDefaultTools sharedInstance] getJJImagePicker];
    picker.aspectRatioPreset = TOCropViewControllerAspectRatioPresetOriginal;
//    WeakSelf(self)
    [picker showImagePickerWithType:JJImagePickerTypeCamera InViewController:self didFinished:^(JJImagePicker *picker, UIImage *image) {
        
    }];

}


#pragma mark - 协议 UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [self dismissViewControllerAnimated:YES completion:nil];
    //取得照片
    UIImage * image = info[UIImagePickerControllerOriginalImage];
    image = [UIImage rotateImage:image];
//    if (self.isFromCurrentSet) {
//        [[AppSetting sharedAppSetting] saveBgImageWithSsionId:image ssion:self.ssionId];
//    } else {
//        [[AppSetting sharedAppSetting] saveBgImage:image];
//    }
    
//    HMJPreviewChatBackGroundViewController *vc = [[HMJPreviewChatBackGroundViewController alloc]init];
//    vc.bgImg = image;
//    vc.type = @"0";
//    vc.destId = @"0";
//    vc.returnChatBackGroundViewBlock = ^(NSString *url) {
//
//    };
//    [self pushViewController:vc];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (UIImagePickerController *)imagePickerController{
    if (!_imagePickerController){
        _imagePickerController = [[UIImagePickerController alloc]init];
        _imagePickerController.delegate = self;
        _imagePickerController.allowsEditing = NO;
//        _imagePickerController.navigationBar.tintColor = [UIColor whiteColor];
    }
    return _imagePickerController;
}
@end
