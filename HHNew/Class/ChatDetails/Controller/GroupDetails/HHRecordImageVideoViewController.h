//
//  HMKChatListImageViewController.h
//  huihe
//
//  Created by 储翔 on 2020/3/16.
//  Copyright © 2020 BQ. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HHRecordImageVideoViewController : BaseViewController
@property (copy, nonatomic) NSString *groupId;
@end

NS_ASSUME_NONNULL_END
