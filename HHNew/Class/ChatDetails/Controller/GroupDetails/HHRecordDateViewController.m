//
//  HHRecordDateViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/30.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHRecordDateViewController.h"
#import "CalenderView.h"
#import "HHMessageListModel.h"

@interface HHRecordDateViewController ()<CalenderViewDelete>

@end

@implementation HHRecordDateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"按日期查找";
    
//    NSMutableArray<HHMessageListModel*> *dataAllModel = [[HMJFMDBMessageDataModel sharedInstance] queryDataFromDateSoucrceBaseWithUid:[NSString stringWithFormat:@"2%@%@",SESSIONID_MIDDLE,self.groupId]];
//    HHMessageListModel *model = [dataAllModel firstObject];
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-M-dd"];
    long long time = [[[HHDefaultTools sharedInstance] acquireTimestamp] longLongValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970: time];
    NSDate *lastDate = [NSDate dateWithTimeIntervalSince1970: (time-36000)];

//    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[model.sendTime integerValue] / 1000];
//    HMJMessageModel *lastModel = [dataAllModel lastObject];
//    NSDate *lastDate = [NSDate dateWithTimeIntervalSince1970:[lastModel.sendTime integerValue] / 1000];
    CalenderView *view = [[CalenderView alloc] initWithFrame:self.view.frame startDay:[dateFormatter stringFromDate:date] endDay:[dateFormatter stringFromDate:lastDate]];
//    view.allDataArr = dataAllModel;
    view.delegate = self;
    view.yearMonthFormat = @"%zd年%02zd月";
    view.actvityColor = YES;
    view.showWeekBottomLine = YES;
    [self.view addSubview:view];
}

- (void)calenderView:(NSIndexPath *)indexPath dateString:(NSString *)dateString {
    if (self.selectRecordDate) {
        self.selectRecordDate(dateString);
    }
//    [self.navigationController popViewControllerAnimated:YES];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissView" object:dateString userInfo:nil];
}

@end
