//
//  HMKChatBgSelectViewController.m
//  huihe
//
//  Created by ZY on 2019/11/28.
//  Copyright © 2019 BQ. All rights reserved.
//

#import "HMKChatBgSelectViewController.h"
#import "HMChatBgImageManager.h"
@interface HMKChatBgSelectViewController ()

@property (nonatomic, strong) NSArray *imageDataArray;
@property (nonatomic, strong) UIButton *finishBtn;
@property (nonatomic, weak) UIImageView *currentSelectIcon;
@end

@implementation HMKChatBgSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    self.navigationItem.title = @"选择背景图";
    [self setLeftBarButtonItem:@"取消" color:ColorManage.text_color];
//    [self setRightBarButtonItem:@"完成" color:ColorManage.text_color];
    [self setupRightBarButtonItem:self.finishBtn];
    [self createView];
}
- (void)leftBarButtonItemAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

//点击完成按钮
- (void)finishButtonClick:(UIButton *)btn{
    UIImageView *imageView = [self.view viewWithTag:self.currentSelectIcon.tag - 100];
    //    NSString *imageName = self.imageDataArray[self.currentSelectIcon.tag - 200];
    //    [[AppSetting sharedAppSetting] saveBgImageName:imageName];
        if (self.HMKChatBgSelectViewControllerBlock) {
            self.HMKChatBgSelectViewControllerBlock(imageView.image);
        }
    [self dismissViewControllerAnimated:YES completion:nil];
}

//选中图片
- (void)imageDidClick:(UITapGestureRecognizer *)tap {
    _finishBtn.enabled = YES;
    [_finishBtn setBackgroundColor:ColorManage.greenTextColor];

    UIImageView *image = (UIImageView *)tap.view;
    UIImageView *selectView = [image viewWithTag:image.tag + 100];
    if (selectView == self.currentSelectIcon) {
        return;
    }
    selectView.hidden = NO;
    self.currentSelectIcon.hidden = YES;
    self.currentSelectIcon = selectView;
}

- (void)createView {
    CGFloat leftP = 10;
    CGFloat rightP = 10;
    CGFloat padding = 5;
    int count = 3;
    CGFloat imgW = (SCREEN_WIDTH - leftP - rightP - 2 * padding) / count;
    CGFloat imgH = imgW;
    for (int i = 0; i < self.imageDataArray.count; i ++) {
        NSString *imageName = self.imageDataArray[i];
        UIImageView *image = [[UIImageView alloc] init];
        image.image = [[HMChatBgImageManager sharedInstance] defaultChatBgImage:imageName];
        image.frame = CGRectMake(leftP + (imgW + padding)*(i%count), SafeAreaTopHeight + 20 + (imgH + padding)*(i/count), imgW, imgH);
        image.contentMode = UIViewContentModeScaleAspectFill;
        image.tag = i + 100;
        image.layer.cornerRadius = 4.0;
        image.layer.masksToBounds = YES;
        image.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageDidClick:)];
        [image addGestureRecognizer:tap];
        
        [self.view addSubview:image];
        
        UIImageView *icon = [[UIImageView alloc] init];
        icon.image = [UIImage imageNamed:@"chat_bg_selected"];
        icon.tag = i + 200;
        icon.hidden = YES;
        
        [image addSubview:icon];
        [icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(image);
            make.height.mas_equalTo(imgW * 57.0/320.0);
        }];
        
//        if ([[[AppSetting sharedAppSetting] getAllBgImageName] isEqualToString:imageName]) {
//            icon.hidden = NO;
//            self.currentSelectIcon = icon;
//        }
    }
}

- (NSArray *)imageDataArray {
    if (_imageDataArray == nil) {
        _imageDataArray = @[@"bgImage_01", @"bgImage_02", @"bgImage_03", @"bgImage_04", @"bgImage_05", @"bgImage_06", @"bgImage_07", @"bgImage_08", @"bgImage_09"];
    }
    return _imageDataArray;
}

- (UIButton *)finishBtn {
    if (!_finishBtn) {
         _finishBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _finishBtn.frame = CGRectMake(0, 0, 50, 35);
        _finishBtn.layer.masksToBounds = YES;
        _finishBtn.layer.cornerRadius = 5;
        _finishBtn.enabled = NO;
        _finishBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_finishBtn setTitle:@"完成" forState:(UIControlStateNormal)];
        [_finishBtn setBackgroundColor:RGB_Color(222, 223, 224)];
        [_finishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_finishBtn addTarget:self action:@selector(finishButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _finishBtn;
}
@end
