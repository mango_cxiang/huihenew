//
//  HHGroupChatDetailsViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/26.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupChatDetailsViewController.h"
#import "HHGroupDetailsTableView.h"
#import "HMJGroupMemberView.h"
#import "HHContactsModel.h"
#import "HHGroupChatNameViewController.h"
#import "HHGroupQrCodeViewController.h"
#import "HHGroupNoticeViewController.h"
#import "HHGroupManagerViewController.h"
#import "HHChatBgViewController.h"
#import "HHGroupAllMembersViewController.h"
#import "HHFriendsDetailsViewController.h"
#import "HHGroupRecordViewController.h"
#import "HHSelectMembersListViewController.h"

#define padding 5
#define aloneWIDTH ((SCREEN_WIDTH-padding*6)/5.0 + 20)

@interface HHGroupChatDetailsViewController ()
@property (nonatomic,strong) HHGroupDetailsTableView * tableView;
@property (nonatomic,strong) HMJGroupMemberView * groupMemberView;
@property (nonatomic,strong) NSMutableArray * membersArray;
@property (nonatomic,strong) UIView * topBgView;
@property (nonatomic,copy) NSString * noticeStr;

@end

@implementation HHGroupChatDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createUI];
    [self loadData];
    [self handleBlock];
}

- (void)createUI{
    [self.view addSubview:self.tableView];
    [self.topBgView addSubview:self.groupMemberView];
     self.tableView.tableHeaderView = self.topBgView;
}

- (void)loadData{
    self.noticeStr = @"节日快乐lkdnkad/?n困了就。父类的看来简历看破滴滴拼伞破商品房票度搜分配狗屁屁搜房卡涝的涝死卡罗拉开,,发商就开始看得见阿好哒看见看】【】解决办法开金身嗯呢";
    self.tableView.noticeStr = self.noticeStr;
    
    HHContactsModel *model1 = [[HHContactsModel alloc] init];
    model1.headUrl = @"qunliao";
    model1.nickName = @"天锡";
    model1.role = @"1";
    
    HHContactsModel *model2 = [[HHContactsModel alloc] init];
    model2.headUrl = @"openGroup";
    model2.nickName = @"还有谁";
    model2.role = @"2";
    
    HHContactsModel *model3 = [[HHContactsModel alloc] init];
    model3.headUrl = @"phone";
    model3.nickName = @"加厚加宽";
    
    HHContactsModel *model4 = [[HHContactsModel alloc] init];
    model4.headUrl = @"new_friends";
    model4.nickName = @"健康活动和谐哈哈嗯";
    
    for (NSInteger i = 0; i<10; i++) {
        HHContactsModel *model = [[HHContactsModel alloc] init];
        model.headUrl = @"group";
        model.nickName = @"xx嗯";
        [self.membersArray addObject:model];
    }
    
    [self.membersArray addObject:model1];
    [self.membersArray addObject:model2];
    [self.membersArray addObject:model3];
    [self.membersArray addObject:model4];
    self.groupMemberView.contentArray = [NSArray arrayWithArray:self.membersArray];
    self.title = [NSString stringWithFormat:@"群聊详情(%lu)",(unsigned long)self.membersArray.count];

    [self updateGroupMemberView];
}

//TODO:处理点击群详情cell事件
- (void)handleBlock{
    WeakSelf(self);
    self.tableView.clickGroupDetailsTableViewCell = ^(NSIndexPath * _Nonnull indexPath) {
        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                [weakself changeGroupName];
            }
            if (indexPath.row == 1) {
                [weakself showGroupQrCode];
            }
            if (indexPath.row == 2) {
                [weakself showGroupNotice];
            }
            if (indexPath.row == 3) {
                [weakself showGroupManager];
            }
        }
        if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                [weakself changeUserNickName];
            }
        }
        if (indexPath.section == 3) {
            if (indexPath.row == 0) {
                [weakself groupCooperationMsg];
            }
            if (indexPath.row == 1) {
                [weakself findGroupChatHistory];
            }
            if (indexPath.row == 2) {
                [weakself clearGroupChatHistory];
            }
            if (indexPath.row == 3) {
                [weakself resetGroupChatBgView];
            }
        }
        if (indexPath.section == 4) {
            if (indexPath.row == 0) {
                [weakself sendReport];
            }
        }
    };
    //退出群聊
    self.tableView.deleteAndexitGroupChat = ^{
        [weakself deleteAndexitGroupChat];
    };
    //点击某个cell
    self.groupMemberView.clickGroupMemberCollectionViewCell = ^(NSIndexPath *indexPath) {
        [weakself clickGroupMemberViewBeCollection:indexPath];
    };
    //查看全部成员
    self.groupMemberView.clickLookAllMembersBtn = ^{
        [weakself showAllMembersVC];
    };
}

//全部群成员页面
- (void)showAllMembersVC{
    HHGroupAllMembersViewController *nvc = Init(HHGroupAllMembersViewController);
    nvc.membersArray = self.membersArray;
    [self.navigationController pushViewController:nvc animated:YES];
}

//群名称页面
- (void)changeGroupName{
    HHGroupChatNameViewController *nvc = Init(HHGroupChatNameViewController);
    nvc.groupName = @"峡谷之巅";
    [self.navigationController pushViewController:nvc animated:YES];
}
//群二维码页面
- (void)showGroupQrCode{
    HHGroupQrCodeViewController *nvc = Init(HHGroupQrCodeViewController);
    [self.navigationController pushViewController:nvc animated:YES];
}
//群公告页面
- (void)showGroupNotice{
    WeakSelf(self)
    HHGroupNoticeViewController *nvc = Init(HHGroupNoticeViewController);
    nvc.noticeStr = self.noticeStr;
    [self.navigationController pushViewController:nvc animated:YES];
    nvc.changeGroupNoticeFinished = ^(NSString * _Nonnull noticeString) {
        weakself.noticeStr = noticeString;
        weakself.tableView.noticeStr = noticeString;
        [weakself.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    };
}
//群管理页面
- (void)showGroupManager{
    HHGroupManagerViewController *nvc = Init(HHGroupManagerViewController);
    [self.navigationController pushViewController:nvc animated:YES];
}

//修改我的昵称页面
- (void)changeUserNickName{
    HHGroupChatNameViewController *nvc = Init(HHGroupChatNameViewController);
    nvc.userNickName = @"王者召唤师";
    [self.navigationController pushViewController:nvc animated:YES];
}

//群合作信息
- (void)groupCooperationMsg{
    
}

//查找聊天记录
- (void)findGroupChatHistory{
    HHGroupRecordViewController *nvc = Init(HHGroupRecordViewController);
    [self.navigationController pushViewController:nvc animated:YES];
}

//清空聊天记录
- (void)clearGroupChatHistory{
    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:0];
    [arr addObject:@"清空聊天记录"];
    ZGQActionSheetView *sheetView = [[ZGQActionSheetView alloc] initWithOptions:arr completion:^(NSInteger index) {
        if (index == 0) {
            
        }
    } cancel:^{
            
    }];
    sheetView.optionColor = RGB_RedColor;
    [sheetView show];
}
//设置聊天背景
- (void)resetGroupChatBgView{
    HHChatBgViewController *nvc = Init(HHChatBgViewController);
    [self.navigationController pushViewController:nvc animated:YES];
}
//投诉
- (void)sendReport{
    
}

//退出群聊
- (void)deleteAndexitGroupChat{
    
}

//TODO:根据获得的成员数组更新头部群成员视图
- (void)updateGroupMemberView{
    int line;
    if (self.groupMemberView.contentArray.count > 19) {
        line = 4;
        self.groupMemberView.frame = CGRectMake(0, 0, SCREEN_WIDTH,line * aloneWIDTH +
                                                padding * (line+1)+60);
    }else {
        line = ((self.groupMemberView.contentArray.count+1)%5 == 0?((int)(self.groupMemberView.contentArray.count+1)/5):((int)(self.groupMemberView.contentArray.count+1)/5)+1);
        
        if (self.groupMemberView.contentArray.count == 4 || self.groupMemberView.contentArray.count == 9 || self.groupMemberView.contentArray.count == 14 || self.groupMemberView.contentArray.count == 19) {
            line += 1;
        }
        
        self.groupMemberView.frame = CGRectMake(0, 0, SCREEN_WIDTH,line * aloneWIDTH +
                                                padding * (line+1));
    }
    
    if (self.groupMemberView.contentArray.count > 19) {
        self.topBgView.frame = CGRectMake(0, 0, SCREEN_WIDTH, line * aloneWIDTH + padding * (line+1)+30);
    }else {
        self.topBgView.frame = CGRectMake(0, 0, SCREEN_WIDTH, line * aloneWIDTH + padding * (line+1));
    }
    self.topBgView.frame = self.groupMemberView.frame;
    [self.groupMemberView reloadData];
}

//TODO:成员列表回调，选中某个用户，或者点击添加，点击删除
- (void)clickGroupMemberViewBeCollection:(NSIndexPath *)indexPath{
    if (self.groupMemberView.contentArray.count > 18) {
        if (indexPath.row == 18) { //添加
            if (self.groupMemberView.contentArray.count >= 500) {
                [self alertGroupMembersFull];
                return;
            }
            [self alertAddGroupMembers];
        } else if (indexPath.row == 19) { // 删除
            [self deleteMembers];
        } else {
            [self openUserDetails];
        }
    }else {
        if(indexPath.row == self.groupMemberView.contentArray.count)    { // 添加
            [self alertAddGroupMembers];
        }  else if (indexPath.row == self.groupMemberView.contentArray.count + 1) { // 删除
            [self deleteMembers];
        } else{
            [self openUserDetails];
        }
    }
}

//TODO:提示群人数已满
- (void)alertGroupMembersFull{
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"温馨提示" message:@"此群已满500位成员，暂不支持继续邀请新成员。" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}
//TODO:添加成员
- (void)alertAddGroupMembers{
    NSString *titleStr;
//    if ([self.groupModel.joinStatus isEqualToString:@"0"]) {
        titleStr = @"为减少打扰，你将向对方发送群聊邀请，待同意后才会进入群聊，确认邀请吗？";
//    }else {
//        if ([[HMJFMDBGroupDataModel sharedInstance] checkUserGroupMemberOwnerGroupId:self.groupModel.groupId  contactId:[NSString acquireUserId]] || [[HMJFMDBGroupDataModel sharedInstance] checkUserGroupMemberIdentityGroupId:self.groupModel.groupId contactId:[NSString acquireUserId]]) {
//            titleStr = @"为减少打扰，你将向对方发送群聊邀请，待同意后才会进入群聊，确认邀请吗？";
//        }else {
//            titleStr = @"为减少打扰，群主或管理员通过后，你将向对方发送群聊邀请，待同意后才会进入群聊。";
//        }
//    }
     UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"温馨提示" message:titleStr preferredStyle:UIAlertControllerStyleAlert];
     UIAlertAction *action=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
         HHSelectMembersListViewController * creatGroupViewController = [[HHSelectMembersListViewController alloc]init];
//         creatGroupViewController.title = Localized(@"添加群成员");
//         creatGroupViewController.preaGroupModel = (HMJGroupModel *)[[HMJFMDBGroupDataModel sharedInstance] queryDataFromDataBaseWithGroupId:[NSString stringWithFormat:@"%ld",(long)self.destId]].firstObject;
//         creatGroupViewController.memberArray = [NSArray arrayWithArray:self.memberIdArr];
         creatGroupViewController.addMembers = YES;
         WeakSelf(self);
         creatGroupViewController.finishSelectNewGroupMembers = ^(NSMutableArray * _Nonnull array,BOOL add) {
             if (!add) {
                 return ;
             }
            [weakself.membersArray addObjectsFromArray:array];
            weakself.groupMemberView.contentArray = [NSArray arrayWithArray:weakself.membersArray];
            [weakself updateGroupMemberView];
            [weakself.tableView reloadData];
         };
         [self.navigationController pushViewController:creatGroupViewController animated:YES];
     }];
     UIAlertAction *cancelAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
     }];
      [alert addAction:cancelAction];
     [alert addAction:action];
     [self presentViewController:alert animated:YES completion:nil];
}

//TODO：删除成员
- (void)deleteMembers{
    HHSelectMembersListViewController * creatGroupViewController = [[HHSelectMembersListViewController alloc]init];
    creatGroupViewController.addMembers = NO;
    WeakSelf(self);
    creatGroupViewController.finishSelectNewGroupMembers = ^(NSMutableArray * _Nonnull array,BOOL add) {
        if (add) {
            return ;
        }
        NSIndexSet *indexes = [weakself.membersArray indexesOfObjectsPassingTest:^BOOL(HHContactsModel *obj, NSUInteger idx, BOOL *stop) {
            for (HHContactsModel *model in array) {
                if ([model.nickName isEqualToString:obj.nickName]) {
                    return YES;
                }
            }
            return NO;
        }];
        [weakself.membersArray removeObjectsAtIndexes:indexes];
        weakself.groupMemberView.contentArray = [NSArray arrayWithArray:weakself.membersArray];
        [weakself updateGroupMemberView];
        [weakself.tableView reloadData];
    };
    [self.navigationController pushViewController:creatGroupViewController animated:YES];
}

//TODO：进入用户详情页
- (void)openUserDetails{
    HHFriendsDetailsViewController *dvc = Init(HHFriendsDetailsViewController);
    [self.navigationController pushViewController:dvc animated:YES];
//    HMJContactsModel * contactModel = self.groupMemberArray[indexPath.row];
//    HMKOtherInfoViewController *otherInfo = [[HMKOtherInfoViewController alloc] init];
//    if (![otherInfo.userId isEqualToString:[NSString acquireUserId]]) {
//        otherInfo.joinType = @"5";
//    }
//    otherInfo.hidesBottomBarWhenPushed = YES;
//    otherInfo.userId = contactModel.contactId;
//    otherInfo.groupId = self.groupModel.groupId;
//    [self.navigationController pushViewController:otherInfo animated:YES];
}

- (HMJGroupMemberView *)groupMemberView{
    if (!_groupMemberView) {
        _groupMemberView = [[HMJGroupMemberView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0)];
//        _groupMemberView.delegate = self;
        _groupMemberView.needAddBtn = YES;
    }
    return _groupMemberView;
}

- (HHGroupDetailsTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHGroupDetailsTableView alloc]initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT-SafeAreaTopHeight) style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ColorManage.grayBackgroundColor;
       }
       return _tableView;
}

- (UIView *)topBgView{
    if (!_topBgView) {
        _topBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, aloneWIDTH)];
    }
    return _topBgView;
}

- (NSMutableArray *)membersArray{
    if (!_membersArray) {
        _membersArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _membersArray;
}


@end
