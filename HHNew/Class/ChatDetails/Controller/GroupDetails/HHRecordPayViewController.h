//
//  HHGroupPayViewController.h
//  HHNew
//
//  Created by Liubin on 2020/4/30.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HHRecordPayViewController : BaseViewController
@property (copy, nonatomic) NSString *groupId;

@end

NS_ASSUME_NONNULL_END
