//
//  HHGroupQrCodeViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/26.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupQrCodeViewController.h"

@interface HHGroupQrCodeViewController ()

@end

@implementation HHGroupQrCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"群二维码名片";
    self.navigationController.navigationBar.barTintColor = ColorManage.grayBackgroundColor;
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    self.nameLabel.textColor = ColorManage.text_color;
    self.nameLabel.font = kMediumFont(18);
    
    self.iconImgView.layer.masksToBounds = YES;
    self.iconImgView.layer.cornerRadius = 5;

    self.alertLabel.font = kRegularFont(12);
    self.alertLabel.textColor = ColorManage.grayTextColor_Deault;
    
    self.bgView.layer.masksToBounds = YES;
    self.bgView.layer.cornerRadius = 10;
    self.bgView.backgroundColor = ColorManage.darkBackgroundColor;
    
    self.qrCodeImgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.qrCodeImgView addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)]];
    [self createUI];
}

- (void)createUI{
    self.nameLabel.text = @"峡谷之巅";
    NSString *urlStr = @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1587891788640&di=df206b78bfd8874f4adce7c04fbb1475&imgtype=0&src=http%3A%2F%2Fwx3.sinaimg.cn%2Fmw690%2Fe732ef12ly1gdqzzj4d7cj20m80m8tbm.jpg";
    
    [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:nil options:SDWebImageRefreshCached];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
            UIImage *image = [self creatQRCode:urlStr];
            dispatch_async(dispatch_get_main_queue(), ^{
                //将最终合得的图片显示在UIImageView上
                self.qrCodeImgView.image = image;
            });
        });
    
    
}


- (void)longPress:(UILongPressGestureRecognizer *)longPress{
    if (longPress.state != UIGestureRecognizerStateBegan) {
        return;
    }
    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:0];
    [arr addObject:@"发送给朋友"];
    [arr addObject:@"保存图片"];
    ZGQActionSheetView *sheetView = [[ZGQActionSheetView alloc] initWithOptions:arr completion:^(NSInteger index) {
        switch (index) {
            case 0:
                {
                    
                }
                break;
            case 1:
                {
                UIImageWriteToSavedPhotosAlbum(self.qrCodeImgView.image, self, @selector(image:didFinishSavingWithError:contextInfo:), (__bridge void *)self);
                }
                break;
            default:
                break;
            }
        } cancel:^{
            
        }];
        
    [sheetView show];}


- (UIImage *)creatQRCode:(NSString *)str{
    // 1、创建滤镜对象
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];

    // 恢复滤镜的默认属性
    [filter setDefaults];

    // 2、设置数据
//    NSDictionary *dic = @{@"groupid":_groupModel.groupId,
//                          @"userid":[NSString acquireUserId]};
    NSString *string_data = str;
//    [NSString stringWithFormat:@"b_%@",[JSON stringWithObject:dic]];

    // 将字符串转换成 NSdata (虽然二维码本质上是字符串, 但是这里需要转换, 不转换就崩溃)
    NSData *qrImageData = [string_data dataUsingEncoding:NSUTF8StringEncoding];

    // 设置过滤器的输入值, KVC赋值
    [filter setValue:qrImageData forKey:@"inputMessage"];

    // 3、获得滤镜输出的图像
    CIImage *outputImage = [filter outputImage];

    // 图片小于(27,27),我们需要放大
    outputImage = [outputImage imageByApplyingTransform:CGAffineTransformMakeScale(20, 20)];

    // 4、将CIImage类型转成UIImage类型
    UIImage *start_image = [UIImage imageWithCIImage:outputImage];


    // - - - - - - - - - - - - - - - - 添加中间小图标 - - - - - - - - - - - - - - - -
    // 5、开启绘图, 获取图形上下文 (上下文的大小, 就是二维码的大小)
    UIGraphicsBeginImageContext(start_image.size);

    // 把二维码图片画上去 (这里是以图形上下文, 左上角为(0,0)点
    [start_image drawInRect:CGRectMake(0, 0, start_image.size.width, start_image.size.height)];
    // 6、获取当前画得的这张图片
    UIImage *final_image = UIGraphicsGetImageFromCurrentImageContext();
    // 7、关闭图形上下文
    UIGraphicsEndImageContext();
    // 8、借助UIImageView显示二维码
    return final_image;
}


- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    //    NSLog(@"image = %@, error = %@, contextInfo = %@", image, error, contextInfo);
    if (error) {
        [[HMJShowLoding sharedInstance] showText:@"保存失败"];
    }else{
        [[HMJShowLoding sharedInstance] showText:@"保存成功"];
    }
}

@end
