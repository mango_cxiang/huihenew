//
//  HHGroupChatNameViewController.h
//  HHNew
//
//  Created by Liubin on 2020/4/26.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HHGroupChatNameViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UIButton *finishBtn;

@property (copy, nonatomic) NSString *groupName;

@property (copy, nonatomic) NSString *userNickName;

@end

NS_ASSUME_NONNULL_END
