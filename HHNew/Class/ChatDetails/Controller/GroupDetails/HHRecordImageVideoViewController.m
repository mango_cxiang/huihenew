//
//  HMKChatListImageViewController.m
//  huihe
//
//  Created by 储翔 on 2020/3/16.
//  Copyright © 2020 BQ. All rights reserved.
//

#import "HHRecordImageVideoViewController.h"
#import "YBIBUtilities.h"
//#import "YBImageBrowserTipView.h"
#import "YBImageBrowser.h"
#import "HHMessageListModel.h"
//#import "ObjectCollectionCell.h"
@interface HHRecordImageVideoViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) UICollectionView *collectionView;
@end

@implementation HHRecordImageVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"图片及视频";
    self.view.backgroundColor = ColorManage.background_color;
    [self setUpCollect];
    [self getVideoData];
}

- (void)setUpCollect {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height) collectionViewLayout:layout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.view addSubview:self.collectionView];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [_collectionView registerNib:[UINib nibWithNibName:@"ObjectCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"ObjectCollectionCell"];
    //[_collevtionview registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"UICollectionViewHeader"];
    _collectionView.showsVerticalScrollIndicator = NO;
}

- (void)getVideoData {
//    NSMutableArray<HMJMessageModel*> *dataMode = [[HMJFMDBMessageDataModel sharedInstance] queryDataFromImgOrVideoDataBaseWithSessionId:[NSString stringWithFormat:@"2%@%@",SESSIONID_MIDDLE,self.groupId]];
//    self.dataArr = dataMode;
    [self.collectionView reloadData];
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    //return 3;
    return _dataArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    HMJMessageModel *model=_dataArr[indexPath.row];
//    ObjectCollectionCell *cell = (ObjectCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ObjectCollectionCell" forIndexPath:indexPath];
//    cell.msgModel=model;
    return nil;
    
}

//设置每个item的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake( SCREEN_WIDTH / 3 - 20, SCREEN_WIDTH / 3 - 20);
}


//设置每个item的UIEdgeInsets
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 10);
    
}

////设置每个item垂直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self showBrowserForMixedCaseWithIndex:indexPath.row];
}

- (void)showBrowserForMixedCaseWithIndex:(NSInteger)index {
//
//    NSMutableArray *browserDataArr = [NSMutableArray array];
//
//    [_dataArr enumerateObjectsUsingBlock:^(HHMessageListModel  *_Nonnull model , NSUInteger idx, BOOL * _Nonnull stop) {
//        NSDictionary *dict = [JSON objectFromJSONString:model.content];
//        NSString *videoUrl = [dict objectForKey:@"vedioUrl"];
//        if (videoUrl.length > 0) {
//            YBVideoBrowseCellData *data = [YBVideoBrowseCellData new];
//            data.url = [NSURL URLWithString:videoUrl];
//            data.sourceObject = [self sourceObjAtIdx:idx];
//            [browserDataArr addObject:data];
//        }else {
//            YBImageBrowseCellData *data = [YBImageBrowseCellData new];
//            data.thumbUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?x-oss-process=style/img_just",dict[@"ImageUrl"]]];
//            data.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?x-oss-process=style/img_width",dict[@"ImageUrl"]]];
//            data.sourceObject = [self sourceObjAtIdx:idx];
//            [browserDataArr addObject:data];
//        }
//    }];
//
//    //Type 5 : 自定义 / Custom
//    //    CustomCellData *data = [CustomCellData new];
//    //    data.text = @"Custom Cell";
//    //    [browserDataArr addObject:data];
//
//
//    YBImageBrowser *browser = [YBImageBrowser new];
//    browser.dataSourceArray = browserDataArr;
//    browser.currentIndex = index;
//    [browser show];
}

#pragma mark - Tool

//- (id)sourceObjAtIdx:(NSInteger)idx {
//    ObjectCollectionCell *cell = (ObjectCollectionCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
//    return cell ? cell.images : nil;
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
