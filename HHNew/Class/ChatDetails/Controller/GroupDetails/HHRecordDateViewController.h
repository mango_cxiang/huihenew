//
//  HHRecordDateViewController.h
//  HHNew
//
//  Created by Liubin on 2020/4/30.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^selectRecordDate)(NSString * dateString);

@interface HHRecordDateViewController : BaseViewController
@property (copy, nonatomic) NSString *groupId;

@property (copy, nonatomic) selectRecordDate selectRecordDate;

@end

NS_ASSUME_NONNULL_END
