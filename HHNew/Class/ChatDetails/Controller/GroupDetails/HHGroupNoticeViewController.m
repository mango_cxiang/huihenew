//
//  HHGroupNoticeViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/27.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupNoticeViewController.h"
#import "FYJDTextView.h"
#import "HHGroupNoticePublisherView.h"
@interface HHGroupNoticeViewController ()<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) FYJDTextView *textView;
@property (nonatomic, strong) UIButton *finishBtn;
@property (nonatomic, strong) UIButton *editBtn;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) HHGroupNoticePublisherView *publisherView;

@end

@implementation HHGroupNoticeViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = ColorManage.background_color;
    [self createSubViews];
}


//暂且有公告的形式
- (void)loadData{
    self.textView.text = self.noticeStr;
    
    NSDictionary *dict = @{@"icon":@"qunliao",
                           @"name":@"联盟王者",
                           @"time":@"2020-04-26 18:13:46"
    };
    self.publisherView.dataDict = [NSDictionary dictionaryWithDictionary:dict];
    [self.textView setEditable:NO];
}

//处理数据
- (void)setNoticeStr:(NSString *)noticeStr{
    _noticeStr = noticeStr;
    if (_noticeStr.length <= 0) {
        [self.textView becomeFirstResponder];
        [self.textView setEditable:YES];
        [self setupRightBarButtonItem:self.finishBtn];
    }
    else{
        [self loadData];
        self.tableView.tableHeaderView = self.publisherView;
        [self setupRightBarButtonItem:self.editBtn];
    }
}

#pragma mark - 创建子视图
- (void)createSubViews{
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.view addSubview:self.tableView];
}


//TODO:点击"完成"
- (void)finishButtonClick:(UIButton *)sender{
    if (self.changeGroupNoticeFinished) {
        self.changeGroupNoticeFinished(self.textView.text);
    }
    [self.navigationController popViewControllerAnimated:YES];
//    [HMJContactsManager updateNewNoteWithgroupId:@(self.groupId).stringValue content:self.textView.text completed:^(int code, NSDictionary *objc) {
//        if (code == 1) {
//            [self showText:@"发布成功"];
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [self popViewController];
//            });
//        }
//    }];
}

//TODO:点击编辑
- (void)didEditBtnClick{
//    self.finishBtn.hidden = NO;
    [self setupRightBarButtonItem:self.finishBtn];
//    self.editBtn.hidden = YES;
    [self.textView setEditable:YES];
    [self.textView becomeFirstResponder];
    self.tableView.contentOffset = CGPointMake(0, self.publisherView.height);
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    [cell.contentView addSubview:self.textView];
    cell.backgroundColor = ColorManage.darkBackgroundColor;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SCREEN_HEIGHT-SafeAreaTopHeight-75;
}

- (void)textViewDidChange:(UITextView *)textView{
//    if (textView.text.length <= 0) {
//        _finishBtn.enabled = NO;
//        [_finishBtn setBackgroundColor:RGB_Color(222, 223, 224)];
//    }
//    if (textView.text.length > 0) {
        _finishBtn.userInteractionEnabled = YES;
        [_finishBtn setBackgroundColor:ColorManage.greenTextColor];
//    }
}

- (HHGroupNoticePublisherView *)publisherView{
    if (!_publisherView) {
        _publisherView = [[HHGroupNoticePublisherView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 75)];
        _publisherView.backgroundColor = ColorManage.darkBackgroundColor;
    }
    return _publisherView;
}

- (FYJDTextView *)textView{
    if (!_textView) {
        _textView = [[FYJDTextView alloc] init];
        _textView.frame = CGRectMake(12, 0, SCREEN_WIDTH-24, SCREEN_HEIGHT-SafeAreaTopHeight-75);
        _textView.delegate = self;
        _textView.font = kRegularFont(16);
        _textView.returnKeyType = UIReturnKeyDefault;
        _textView.backgroundColor = ColorManage.darkBackgroundColor;
        _textView.tintColor = ColorManage.greenTextColor;
        _textView.textColor = ColorManage.text_color;
        _textView.placeholder = @"";
    }
    return _textView;
}

- (UIButton *)finishBtn {
    if (!_finishBtn) {
         _finishBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _finishBtn.frame = CGRectMake(0, 0, 50, 35);
        _finishBtn.layer.masksToBounds = YES;
        _finishBtn.layer.cornerRadius = 5;
        _finishBtn.userInteractionEnabled = NO;
        _finishBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_finishBtn setTitle:@"完成" forState:(UIControlStateNormal)];
        [_finishBtn setBackgroundColor:RGB_Color(222, 223, 224)];
        [_finishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_finishBtn addTarget:self action:@selector(finishButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _finishBtn;
}

- (UIButton *)editBtn {
    if (!_editBtn) {
        _editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_editBtn setTitle:@"编辑" forState:UIControlStateNormal];
        [_editBtn setTitleColor:ColorManage.text_color forState:UIControlStateNormal];
        _editBtn.titleLabel.font = kRegularFont(16);
        [_editBtn addTarget:self action:@selector(didEditBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _editBtn;
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = ColorManage.darkBackgroundColor;
    }
    return _tableView;
}
@end
