//
//  HHGroupSearchMembersViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupSearchMembersViewController.h"
#import "HHGroupSearchMembersTableView.h"
#import "HHFriendsDetailsViewController.h"
@interface HHGroupSearchMembersViewController ()
@property (nonatomic, strong) HHGroupSearchMembersTableView *tableView;
@property (nonatomic,strong)NSMutableArray * searchResultArray;

@end

@implementation HHGroupSearchMembersViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    self.definesPresentationContext = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.view addSubview:self.tableView];
    [self handleBlock];
}

- (void)handleBlock{
    //点击搜索结果 cell
    WeakSelf(self);
    self.tableView.clickSearchMembersTableViewCell = ^(HHContactsModel * _Nonnull model) {
        StrongSelf(weakself);
        if (strongweakself.clickGroupSearchMembersResult) {
            strongweakself.clickGroupSearchMembersResult(model);
        }
    };
}

//清楚数据
- (void)clearResultData{
    self.tableView.inputWords = @"";
    [self.searchResultArray removeAllObjects];
    [self.tableView.dataArray removeAllObjects];
    [self.tableView reloadData];
}

#pragma mark - UISearchResultsUpdating
//每输入一个字符都会执行一次
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    searchController.searchResultsController.view.hidden = NO;
    if (searchController.searchBar.searchTextField.text.length <= 0) {
        return;
    }
    NSString *inputWord = [searchController.searchBar.searchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];;
    if (!inputWord.length){
        searchController.searchBar.searchTextField.text = @"";
        return;
    }
    //取消执行 selector
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(automaticCompletionWithKeyword:) object:inputWord];
    //获取是否有高亮
    UITextRange *selectedRange = [searchController.searchBar.searchTextField markedTextRange];
    if(!selectedRange){
        [self automaticCompletionWithKeyword:inputWord];
    }
}

#pragma mark - 自动补全
- (void)automaticCompletionWithKeyword:(NSString *)inputWord {
    [self.searchResultArray removeAllObjects];
    NSMutableArray *tempResults = [NSMutableArray array];
    NSMutableArray *contactsSource = [NSMutableArray arrayWithArray:self.dataListArry];
    NSUInteger searchOptions = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch;

    for (HHContactsModel * model in contactsSource) {
        [tempResults addObject:model];
    }
    for (int i = 0; i < tempResults.count; i++) {
        NSString *storeString = [[(HHContactsModel *)tempResults[i] nickName] pinyinOfName];
        NSString *wordString = [inputWord pinyinOfName];
        NSRange storeRange = NSMakeRange(0, storeString.length);
        if (storeString.length < storeRange.length) {
               continue;
           }
           NSRange foundRange = [storeString rangeOfString:wordString options:searchOptions range:storeRange];
           if (foundRange.length) {
               [self.searchResultArray addObject:tempResults[i]];
           }
       }
    //刷新 tableView
    self.tableView.inputWords = inputWord;
    self.tableView.dataArray = self.searchResultArray;
    [self.tableView reloadData];
}


- (HHGroupSearchMembersTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHGroupSearchMembersTableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-SafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    }
    return _tableView;
}


- (NSMutableArray *)searchResultArray{
    if (!_searchResultArray) {
        _searchResultArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _searchResultArray;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.searchBar.searchTextField resignFirstResponder];
}

@end
