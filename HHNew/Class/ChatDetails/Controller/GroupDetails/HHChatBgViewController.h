//
//  HHChatBgViewController.h
//  HHNew
//
//  Created by Liubin on 2020/4/27.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HHChatBgViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *selectBgLabel;
@property (weak, nonatomic) IBOutlet UILabel *photosLabel;
@property (weak, nonatomic) IBOutlet UILabel *cameraLabel;
- (IBAction)selectGroupBgView:(id)sender;
- (IBAction)selectFromPhonePhotos:(id)sender;
- (IBAction)selectFromCamera:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIScrollView *bgScrollView;
@property (weak, nonatomic) IBOutlet UIStackView *selectBgView;
@property (weak, nonatomic) IBOutlet UIStackView *photoCameraView;
@property (weak, nonatomic) IBOutlet UIView *selectView;
@property (weak, nonatomic) IBOutlet UIView *photosView;
@property (weak, nonatomic) IBOutlet UIView *cameraView;

@end

NS_ASSUME_NONNULL_END
