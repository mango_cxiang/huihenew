//
//  HMKChatBgSelectViewController.h
//  huihe
//
//  Created by ZY on 2019/11/28.
//  Copyright © 2019 BQ. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HMKChatBgSelectViewController : BaseViewController

@property (nonatomic, copy) void(^HMKChatBgSelectViewControllerBlock)(UIImage *image);

@end

NS_ASSUME_NONNULL_END
