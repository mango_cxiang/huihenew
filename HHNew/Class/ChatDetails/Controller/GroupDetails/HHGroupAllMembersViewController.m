//
//  HHGroupAllMembersViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/28.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupAllMembersViewController.h"
#import "HHGroupSearchMembersViewController.h"
#import "HHSearchController.h"
#import "HHGroupAllMembersCollectionView.h"
#import "HHContactsModel.h"
#import "HHFriendsDetailsViewController.h"
#import "HHSelectMembersListViewController.h"
@interface HHGroupAllMembersViewController ()<UISearchControllerDelegate,UISearchBarDelegate>
@property (strong, nonatomic) HHSearchController *searchController;
@property (strong, nonatomic) HHGroupSearchMembersViewController *searchResultVC;
@property (strong, nonatomic) HHGroupAllMembersCollectionView *collectionView;

/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage *searchGrayImage;
/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage *searchWhiteImage;

@end

@implementation HHGroupAllMembersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = ColorManage.background_color;
    self.navigationController.navigationBar.barTintColor = ColorManage.grayBackgroundColor;
    self.definesPresentationContext = YES;
    if (@available(iOS 11.0, *)) {
           self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
       } else {
           self.automaticallyAdjustsScrollViewInsets = NO;
       }
    
    [self createUI];
    [self loadData];
    [self handleBlock];
}

- (void)createUI{
    [self.view addSubview:self.collectionView];
    [self.collectionView addSubview:self.searchController.searchBar];
    self.searchController.searchBar.y = - self.searchController.searchBar.height;
    // 内缩collectionView 的显示内容
    self.collectionView.contentInset = UIEdgeInsetsMake(self.searchController.searchBar.height, 0, 0, 0);
      
    self.searchResultVC.nav = self.navigationController;
    self.searchController.searchResultsUpdater = self.searchResultVC;
    self.searchResultVC.searchBar = self.searchController.searchBar;
}

- (void)setMembersArray:(NSMutableArray *)membersArray{
    _membersArray = membersArray;
    self.title = [NSString stringWithFormat:@"群成员(%lu)",(unsigned long)membersArray.count];
    self.collectionView.contentArray = membersArray;
    self.searchResultVC.dataListArry = self.collectionView.contentArray;
}

- (void)loadData{
//    //假数据：
//       NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:0];
//      for (NSInteger i =0; i<50; i++) {
//           HHContactsModel *model = [[HHContactsModel alloc] init];
//           model.nickName = [NSString stringWithFormat:@"%@%ld", @"召唤师", (long)i];
//           model.headUrl = @"qunliao";
//           [tempArray addObject:model];
//       }
//    self.title = [NSString stringWithFormat:@"群成员（%lu）",(unsigned long)tempArray.count];
//    self.collectionView.contentArray = tempArray;
//    self.searchResultVC.dataListArry = self.collectionView.contentArray;

}

- (void)handleBlock{
    WeakSelf(self);
    self.collectionView.clickAllMembersCollectionViewCell = ^(HHContactsModel * _Nonnull model) {
        HHFriendsDetailsViewController *dvc = Init(HHFriendsDetailsViewController);
        [weakself.navigationController pushViewController:dvc animated:YES];
    };
    //添加成员
    self.collectionView.clickAddMembers = ^{
        [weakself addMembers];
    };
    //删除成员
    self.collectionView.clickDeleteMembers = ^{
        [weakself deleteMembers];
    };
    //点击搜索结果列表
    self.searchResultVC.clickGroupSearchMembersResult = ^(HHContactsModel * _Nonnull model) {
        HHFriendsDetailsViewController *dvc = Init(HHFriendsDetailsViewController);
        [weakself.navigationController pushViewController:dvc animated:YES];
    };
}

- (void)addMembers{
    WeakSelf(self)
    HHSelectMembersListViewController * creatGroupViewController = [[HHSelectMembersListViewController alloc]init];
    creatGroupViewController.addMembers = YES;
    creatGroupViewController.finishSelectNewGroupMembers = ^(NSMutableArray * _Nonnull array,BOOL add) {
        if (!add) {
            return ;
        }
        [weakself.membersArray addObjectsFromArray:array];
        weakself.collectionView.contentArray = weakself.membersArray;
    };
    [self.navigationController pushViewController:creatGroupViewController animated:YES];
}

//TODO：删除成员
- (void)deleteMembers{
    HHSelectMembersListViewController * creatGroupViewController = [[HHSelectMembersListViewController alloc]init];
    creatGroupViewController.addMembers = NO;
    WeakSelf(self);
    creatGroupViewController.finishSelectNewGroupMembers = ^(NSMutableArray * _Nonnull array,BOOL add) {
        if (add) {
            return ;
        }
        NSIndexSet *indexes = [weakself.membersArray indexesOfObjectsPassingTest:^BOOL(HHContactsModel *obj, NSUInteger idx, BOOL *stop) {
            for (HHContactsModel *model in array) {
                if ([model.nickName isEqualToString:obj.nickName]) {
                    return YES;
                }
            }
            return NO;
        }];
        [weakself.membersArray removeObjectsAtIndexes:indexes];
        weakself.collectionView.contentArray = weakself.membersArray;
    };
    [self.navigationController pushViewController:creatGroupViewController animated:YES];
}


#pragma mark - UISearchControllerDelegate
- (void)willPresentSearchController:(UISearchController *)searchController{
    // 4.设置搜索框图标的偏移
    [self.searchController.searchBar setPositionAdjustment:UIOffsetMake(0, 0) forSearchBarIcon:UISearchBarIconSearch];
}

- (void)willDismissSearchController:(UISearchController *)searchController{
//    [self.searchResultVC clearResultData];
    // 4.设置搜索框图标的偏移
    CGFloat offsetX = (SCREEN_WIDTH -60 - 32) / 2;
    [_searchController.searchBar setPositionAdjustment:UIOffsetMake(offsetX, 0) forSearchBarIcon:UISearchBarIconSearch];
}

- (void)didDismissSearchController:(UISearchController *)searchController{
    self.collectionView.contentInset = UIEdgeInsetsMake(self.searchController.searchBar.height, 0, 0, 0);
}


- (HHSearchController *)searchController {
    if (!_searchController) {
        _searchController = [[HHSearchController alloc]initWithSearchResultsController:self.searchResultVC];
        _searchController.delegate = self;
        _searchController.searchBar.backgroundImage = [HHDefaultTools sharedInstance].searchBarGrayImage;
        [_searchController.searchBar setSearchFieldBackgroundImage:[HHDefaultTools sharedInstance].searchBarTextFieldImage forState:UIControlStateNormal];
        _searchController.hidesNavigationBarDuringPresentation = YES;
        _searchController.dimsBackgroundDuringPresentation = NO;
        _searchController.searchBar.placeholder =  @"搜索";
//        // 4.设置搜索框图标的偏移
        CGFloat offsetX = (SCREEN_WIDTH -60 - 32) / 2;
        [_searchController.searchBar setPositionAdjustment:UIOffsetMake(offsetX, 0) forSearchBarIcon:UISearchBarIconSearch];
    }
    return _searchController;
}


- (HHGroupAllMembersCollectionView *)collectionView{
    if (!_collectionView) {
        _collectionView = [[HHGroupAllMembersCollectionView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) collectionViewLayout:[[UICollectionViewFlowLayout alloc]init]];
        _collectionView.alwaysBounceVertical = YES;
    }
    return _collectionView;
}

- (HHGroupSearchMembersViewController *)searchResultVC{
    if (!_searchResultVC) {
        _searchResultVC = [[HHGroupSearchMembersViewController alloc]init];
    }
    return _searchResultVC;
}

@end
