//
//  HHRecordMembersTypeViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/30.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHRecordMembersTypeViewController.h"
#import "SCIndexViewConfiguration.h"
#import "SCIndexView.h"
#import "HHIndexMembersListTableView.h"
#import "HHContactsModel.h"
#import "HHGroupSearchMembersViewController.h"
#import "HHFriendsDetailsViewController.h"
@interface HHRecordMembersTypeViewController ()<SCIndexViewDelegate,UISearchControllerDelegate,UISearchBarDelegate>
@property (strong, nonatomic) HHSearchController *searchController;
@property (strong, nonatomic) HHGroupSearchMembersViewController *searchResultVC;

@property (strong, nonatomic) HHIndexMembersListTableView *tableView;
@property (nonatomic, strong) SCIndexView *indexView;
@property (nonatomic, assign) SCIndexViewStyle indexViewStyle;
@property (strong, nonatomic) NSMutableArray *membersArray;
@property (strong, nonatomic) UIView *tableHeadView;

/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage *searchGrayImage;
/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage *searchWhiteImage;
@end

@implementation HHRecordMembersTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"选择群成员";
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    [self createUI];
    [self loadData];
    [self handleBlock];
}

- (void)createUI{
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.indexView];
    self.definesPresentationContext = YES;
    if (@available(iOS 11.0, *)) {
           self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
       } else {
           self.automaticallyAdjustsScrollViewInsets = NO;
       }
    self.tableView.tableHeaderView = self.tableHeadView;
    [self.tableHeadView addSubview:self.searchController.searchBar];
    self.searchResultVC.nav = self.navigationController;
    self.searchController.searchResultsUpdater = self.searchResultVC;
    self.searchResultVC.searchBar = self.searchController.searchBar;
}

- (void)loadData{
    //假数据：
    NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:0];
   for (NSInteger i =0; i<5; i++) {
        HHContactsModel *model = [[HHContactsModel alloc] init];
        model.nickName = [NSString stringWithFormat:@"%@%ld", @"召唤师", (long)i];
        model.headUrl = @"qunliao";
        [tempArray addObject:model];
    }
    for (NSInteger i =0; i<5; i++) {
        HHContactsModel *model = [[HHContactsModel alloc] init];
        model.nickName = [NSString stringWithFormat:@"%@%ld", @"还有谁", (long)i];
        model.headUrl = @"qunliao";
        [tempArray addObject:model];
    }
    self.tableView.dataListArray = tempArray;
    self.searchResultVC.dataListArry = tempArray;
    NSMutableArray* indexPathArray = [DataHelper getContactListSectionBy:self.tableView.dataListArray];
    // 配置索引数据
    if ([indexPathArray count]>0) {
        NSMutableArray *indexViewDataSource = [NSMutableArray array];
        for (NSString *str in indexPathArray) {
            [indexViewDataSource addObject:str];
        }
        self.indexView.dataSource = indexViewDataSource.copy;
    }
}

- (void)handleBlock{
    WeakSelf(self);
    //点击当前成员
    self.tableView.clickHHIndexMembersTableViewCell = ^(NSIndexPath * _Nonnull indexPath, HHContactsModel * _Nonnull model) {
        HHFriendsDetailsViewController *dvc = Init(HHFriendsDetailsViewController);
        [weakself.navigationController pushViewController:dvc animated:YES];
    };
    //点击搜索结果页
    self.searchResultVC.clickGroupSearchMembersResult = ^(HHContactsModel * _Nonnull model) {
        
    };
}

#pragma mark - SCIndexViewDelegate
- (void)indexView:(SCIndexView *)indexView didSelectAtSection:(NSUInteger)section{
}


- (void)willPresentSearchController:(UISearchController *)searchController{
    // 4.设置搜索框图标的偏移
    [self.searchController.searchBar setPositionAdjustment:UIOffsetMake(0, 0) forSearchBarIcon:UISearchBarIconSearch];
}

- (void)willDismissSearchController:(UISearchController *)searchController{
    [self.searchResultVC clearResultData];
    // 4.设置搜索框图标的偏移
    CGFloat offsetX = (SCREEN_WIDTH -60 - 32) / 2;
    [_searchController.searchBar setPositionAdjustment:UIOffsetMake(offsetX, 0) forSearchBarIcon:UISearchBarIconSearch];
}



- (HHSearchController *)searchController {
    if (!_searchController) {
        _searchController = [[HHSearchController alloc]initWithSearchResultsController:self.searchResultVC];
        _searchController.delegate = self;
        _searchController.searchBar.backgroundImage = [HHDefaultTools sharedInstance].searchBarGrayImage;
        [_searchController.searchBar setSearchFieldBackgroundImage:[HHDefaultTools sharedInstance].searchBarTextFieldImage forState:UIControlStateNormal];
        _searchController.hidesNavigationBarDuringPresentation = YES;
        _searchController.dimsBackgroundDuringPresentation = NO;
        _searchController.searchBar.placeholder =  @"搜索";
//        // 4.设置搜索框图标的偏移
        CGFloat offsetX = (SCREEN_WIDTH -60 - 32) / 2;
        [_searchController.searchBar setPositionAdjustment:UIOffsetMake(offsetX, 0) forSearchBarIcon:UISearchBarIconSearch];
    }
    return _searchController;
}

- (NSMutableArray *)membersArray{
    if (!_membersArray) {
        _membersArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _membersArray;
}

- (SCIndexView *)indexView{
    if (!_indexView) {
        _indexView = [[SCIndexView alloc] initWithTableView:self.tableView configuration:[SCIndexViewConfiguration configurationWithIndexViewStyle:self.indexViewStyle]];
        _indexView.translucentForTableViewInNavigationBar = YES;
        _indexView.delegate = self;
    }
    return _indexView;
}


- (HHIndexMembersListTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHIndexMembersListTableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return _tableView;
}


- (HHGroupSearchMembersViewController *)searchResultVC{
    if (!_searchResultVC) {
        _searchResultVC = [[HHGroupSearchMembersViewController alloc]init];
    }
    return _searchResultVC;
}


- (UIView *)tableHeadView{
    if (!_tableHeadView) {
        _tableHeadView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,HHSearchControllerSearchBarHeight)];
        _tableHeadView.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return _tableHeadView;
}

@end
