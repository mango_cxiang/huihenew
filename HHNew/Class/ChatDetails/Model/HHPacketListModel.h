//
//  HMKPacketListModel.h
//  huihe
//
//  Created by 宋平 on 2019/12/27.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HHPacketListModel : NSObject
@property (nonatomic,copy) NSString *headUrl;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *packetId;
@property (nonatomic,copy) NSString *redPacketAmount;
@property (nonatomic,copy) NSString *redPacketContent;
@property (nonatomic,copy) NSString *redPacketCount;
@property (nonatomic,copy) NSString *redPacketType;
@property (nonatomic,copy) NSString *sendTime;
@property (nonatomic,copy) NSString *userId;
@property (nonatomic,copy) NSString *tableName;
@property (nonatomic,copy) NSString *content;

@end

NS_ASSUME_NONNULL_END
