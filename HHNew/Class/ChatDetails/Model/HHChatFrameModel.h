//
//  HHChatFrameModel.h
//  HHNew
//
//  Created by 张 on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HHChatModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^UserNameBlock)(NSInteger userId);
@interface HHChatFrameModel : NSObject

//数据模型
@property(nonatomic,strong)HHChatModel * chatModel;
//时间Frame
@property(nonatomic,assign,readonly)CGRect timeFrame;
//头像Frame
@property(nonatomic,assign,readonly)CGRect iconFrame;
//名字Fram
@property(nonatomic,assign,readonly)CGRect nameFrame;
//文字Frame
@property(nonatomic,assign,readonly)CGRect textFrame;
//内容ControlFrame
@property(nonatomic,assign,readonly)CGRect controlFrame;
//录音秒数Frame
@property(nonatomic,assign,readonly)CGRect secondsFrame;
//动画语音视图Frame
@property(nonatomic,assign,readonly)CGRect voiceImageViewFrame;

//消息发送转圈Frame
//@property(nonatomic,assign,readonly)CGRect activityIndicatorViewFrame;
//消息未读或已读Frame
//@property(nonatomic,assign,readonly)CGRect readFrame;
//状态，发送成功或者失败
//@property(nonatomic,assign,readonly)CGRect statusFrame;
//cell高度
@property(nonatomic,assign,readonly)CGFloat cellHeight;
//是否显示名字
@property (nonatomic,assign)BOOL isShowName;
//用户名字点击回调
@property(nonatomic,copy)UserNameBlock userNameBlock;

@end

NS_ASSUME_NONNULL_END
