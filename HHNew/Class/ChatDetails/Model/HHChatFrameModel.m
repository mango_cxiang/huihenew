//
//  HHChatFrameModel.m
//  HHNew
//
//  Created by 张 on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHChatFrameModel.h"
#import "PPStickerDataManager.h"
#import <YYText.h>

@implementation HHChatFrameModel

-(void)setChatModel:(HHChatModel *)chatModel
{
    _chatModel = chatModel;
    //小窗弹出时宽度不一定是全屏
    CGFloat width = chatModel.viewWidth!=0?chatModel.viewWidth:SCREEN_WIDTH;
    
    NSString * userId = [NSString acquireUserId];
    NSString * fromId = chatModel.fromId;
    if (chatModel.fromType.integerValue == 2) {
        //群消息
        if ([fromId isEqualToString:userId]) {
            //自己发的不显示自己的名字
            self.isShowName = NO;
        }else{
            //他人发的根据自己设置判断是否显示名字
//            HMJGroupModel *model = [[HMJFMDBGroupDataModel sharedInstance] queryDataFromDataBaseWithGroupId:message.destId].firstObject;
//            self.isShowName = [model.ext boolValue];
            self.isShowName = NO;
        }
    }
    //间隙
    CGFloat padding = 10;
    if (!chatModel.hideTime){
        //时间
        CGFloat timeY = 0;
        NSString * time = chatModel.timeString;
        CGFloat timeHeight = 20;
        CGSize  timeSize = [time sizeWithFont:SystemFont(14) andMaxSize:CGSizeMake(width, timeHeight)];
        CGFloat timeWidth = timeSize.width + 20;
        CGFloat timeX = (width - timeWidth) * 0.5;
        _timeFrame = CGRectMake(timeX, timeY, timeWidth, timeHeight);
    }
    
    if (chatModel.chatFrameType==ChatFrameTypeSystemMessages) {
//        CGSize groupTipSize = [[self jointGroupTipMessage] sizeWithFont:SystemFont(14) andMaxSize:CGSizeMake(SCREEN_WIDTH-40, MAXFLOAT)];
        CGSize groupTipSize = [[self jointGroupTipMessage] getSpaceLabelHeightwithSpeace:8 withFont:SystemFont(14) withWidth:(SCREEN_WIDTH-40)];
        CGFloat groupTipX = (width - (groupTipSize.width+20)) * 0.5;
        _controlFrame = CGRectMake(groupTipX, !chatModel.hideTime?(CGRectGetMaxY(_timeFrame)+15):0, (groupTipSize.width+10), (groupTipSize.height+10)<30?30:(groupTipSize.height+10));
        _cellHeight = CGRectGetMaxY(_controlFrame)+15;
        return;
    }
    //头像、名字  //自己发的,不会显示名字,只计算他人的Frame
    CGFloat iconY = !chatModel.hideTime?(CGRectGetMaxY(_timeFrame)+15):0;
    CGFloat iconWidth = 40;
    CGFloat iconX = [fromId isEqualToString:userId]?(width-padding-iconWidth):padding;
    _iconFrame = CGRectMake(iconX, iconY, iconWidth, iconWidth);
    if (self.isShowName) {
        CGFloat nameWidth = SCREEN_WIDTH-100;
        CGFloat nameX = iconX+iconWidth+15;
        _nameFrame = CGRectMake(nameX, iconY , nameWidth, 18);
    }
    //内容视图
    CGFloat controlWidth = 240;
    CGFloat controlHeight = 0;
    TOChatFrameType chatFrameType = chatModel.chatFrameType;
    if (chatFrameType == ChatFrameTypeText) {
        //文本、语音、视频或语音通话已取消、视频或语音通话通话时长
        if (chatModel.messageType.integerValue == TOMessageTypeText) {
            //文本
            CGSize size = [self calculateTextFrame];
            controlWidth = size.width;
            controlHeight = size.height;
        }
        if (chatModel.messageType.integerValue == TOMessageTypeVoice) {
            //语音
            controlWidth = 100;
            for (int i = 0; i< chatModel.seconds ; i++) {
                if (i != 0) {
                    int width = 10 -i;
                    if (width < 1) {
                        width = 1;
                    }
                    controlWidth += width;
                }
            }
            controlHeight = iconWidth;
            
            _voiceImageViewFrame = CGRectMake([fromId isEqualToString:userId]?(controlWidth-40):10, (controlHeight-15)/2.0, 30, 15);
            _secondsFrame = CGRectMake([fromId isEqualToString:userId]?(controlWidth-40-controlHeight):40, 0, controlHeight, controlHeight);
        }
        
    }else if (chatFrameType == ChatFrameTypeFile) {
        //文件
        controlHeight = 80;
    }else if (chatFrameType == ChatFrameTypeRedPacket) {
        //红包（未领取和已领取状态）
        controlHeight = 85;
    }else if (chatFrameType == ChatFrameTypePicture) {
        //图片、视频
        NSString *strclass = NSStringFromClass([chatModel.content class]);
        if ([strclass isEqualToString:@"__NSCFString"]||[strclass isEqualToString:@"__NSCFConstantString"]) {
            NSString *oneStr = [chatModel.content stringByDeletingPathExtension];
            NSArray *whArr =  [oneStr componentsSeparatedByString:@"-"];
            if (whArr.count >1) {
                NSMutableArray *arry = [NSMutableArray arrayWithArray:whArr];
                [arry removeObjectAtIndex:0];
                CGFloat w = [arry[0] floatValue];
                CGFloat h = [arry[1] floatValue];
                controlWidth = w;
                controlHeight = h;
            }else{
                controlWidth = 130;
                controlHeight = 130;
            }
        }
    }else if (chatFrameType == ChatFrameTypeBusinessCard) {
        //名片
        controlHeight = 95;
    }else if (chatFrameType == ChatFrameTypeLocation) {
        //位置
        controlHeight = 140;
    }else if (chatFrameType == ChatFrameTypeBQMM) {
        //表情云或@信息
        NSDictionary * dataDic = [chatModel.content mj_JSONObject];
        if ([dataDic[@"msgString"] containsString:@"bq_tsj_csc:"]) {
            //位置共享
            
        }else{
            if (!IsBlankWithStr(dataDic[@"msgType"])) {
                //表情云
                controlWidth = 100;
                controlHeight = 100;
            }else{
                //@文字
                CGSize size = [self calculateTextFrame];
                controlWidth = size.width;
                controlHeight = size.height;
            }
        }
    }else if (chatFrameType == ChatFrameTypeShare) {
        //分享
        
    }
    CGFloat controlX = [fromId isEqualToString:userId]?(iconX-5-controlWidth):(CGRectGetMaxX(_iconFrame)+5);
    CGFloat controlY = self.isShowName?(CGRectGetMaxY(_nameFrame)):iconY;
    _controlFrame = CGRectMake(controlX, controlY, controlWidth, controlHeight);
    _cellHeight = CGRectGetMaxY(_controlFrame)+20;
}

#pragma mark --- 计算文本Frame
-(CGSize)calculateTextFrame
{
    CGFloat textX = 20;
    CGFloat textY = 10;
    NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    CGSize textSize = [self.chatModel.attributedContent boundingRectWithSize:CGSizeMake(SCREEN_WIDTH-140, MAXFLOAT) options:options context:nil].size;
    CGFloat textWidth = textSize.width;
    CGFloat textHeight = textSize.height;
    _textFrame = CGRectMake(textX, textY, textWidth, textHeight);
    return CGSizeMake(textWidth + textX * 2, textHeight + textY * 2);
}
#pragma mark --- 拼接群通知消息
-(NSString *)jointGroupTipMessage
{
    NSDictionary *jsonDic;
    NSArray *arr;
    id dic = [self.chatModel.content mj_JSONObject];
    if ([dic isKindOfClass:[NSDictionary class]]) {
        jsonDic = dic;
    }else if([dic isKindOfClass:[NSArray class]]) {
        arr = dic;
    }
    if (jsonDic.allKeys.count == 0 && arr.count == 0) {
        return @"";
    }
    NSString * str = @"";
    NSMutableAttributedString * jointStr = [[NSMutableAttributedString alloc]initWithString:@""];
    if (self.chatModel.messageType.integerValue == TOMessageTypeInviteJoinGroup||self.chatModel.messageType.integerValue == TOMessageTypeBeInvitedJoinGroup) {
        // 群主创建群给群主发送  //被邀请加群的人收到的消息
        NSArray * userList = jsonDic[@"userList"];
        NSMutableArray *nameArray = [NSMutableArray array];
        for (NSDictionary * userDic in userList) {
            [nameArray addObject:[NSString stringWithFormat:@"\"%@\"",userDic[@"nickName"]]];
        }
        if (self.chatModel.messageType.integerValue == TOMessageTypeInviteJoinGroup) {
            str = [NSString stringWithFormat:@"%@ %@ %@", jsonDic[@"msgString"],[nameArray componentsJoinedByString:@"、"],jsonDic[@"endMsgString"]];
        }
        if (self.chatModel.messageType.integerValue == TOMessageTypeBeInvitedJoinGroup) {
            str = [NSString stringWithFormat:@"%@ %@ %@",jsonDic[@"nickName"], jsonDic[@"msgString"],[nameArray componentsJoinedByString:@"、"]];
        }
        jointStr = [[NSMutableAttributedString alloc]initWithString:str];
        jointStr.yy_font = SystemFont(14);
        jointStr.yy_color = RGB_Color(178, 178, 178);
        jointStr.yy_alignment = NSTextAlignmentCenter;
        jointStr.yy_lineSpacing = 8;
        //检测到名字将字符串截取掉 防止用户重名
        NSInteger lastLoc = 0;
        NSString * string = [str mutableCopy];
        for (int i = 0;i<nameArray.count;i++) {
            NSString * name = nameArray[i];
            NSRange range = [string rangeOfString:name];
            string = [string substringFromIndex:(range.location+range.length)];
            range = NSMakeRange(range.location+lastLoc, range.length);
            NSDictionary * userDic = userList[i];
            WeakSelf(self);
            [jointStr yy_setTextHighlightRange:range color:RGB_Color(40, 130, 214) backgroundColor:nil tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
                if (weakself.userNameBlock) {
                    weakself.userNameBlock([userDic[@"userId"] integerValue]);
                }
            }];
            lastLoc = range.location+range.length;
        }
        if (self.chatModel.messageType.integerValue == TOMessageTypeBeInvitedJoinGroup) {
            WeakSelf(self);
            NSRange range = [str rangeOfString:jsonDic[@"nickName"]];
            [jointStr yy_setTextHighlightRange:range color:RGB_Color(40, 130, 214) backgroundColor:nil tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
                if (weakself.userNameBlock) {
                    weakself.userNameBlock([jsonDic[@"userId"] integerValue]);
                }
            }];
        }
    }
    if (self.chatModel.messageType.integerValue == TOMessageTypeGroupInviteOpen||self.chatModel.messageType.integerValue == TOMessageTypeGroupInviteClose||self.chatModel.messageType.integerValue == TOMessageTypeGroupProtectYes||self.chatModel.messageType.integerValue == TOMessageTypeGroupProtectNO||self.chatModel.messageType.integerValue == TOMessageTypeGroupRedMoneyShow||self.chatModel.messageType.integerValue == TOMessageTypeGroupRedMoneyHidden) {
        //群单消息提醒
        str = jsonDic[@"msgString"];
        jointStr = [[NSMutableAttributedString alloc]initWithString:str];
        jointStr.yy_font = SystemFont(14);
        jointStr.yy_color = RGB_Color(178, 178, 178);
        jointStr.yy_alignment = NSTextAlignmentCenter;
    }
    self.chatModel.jointGroupTip = jointStr;
    return str;
}

@end
