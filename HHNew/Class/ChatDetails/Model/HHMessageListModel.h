//
//  HHMessageListModel.h
//  HHNew
//
//  Created by Zxs on 2020/4/21.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HHMessageListModel : NSObject

@property (nonatomic, copy) NSString *discropHide;
//消息分类 [0(个人), 1(讨论组), 2(公司群), 3(部门群), 4(任务), 5(新的朋友)]
//6.17 新增四个消息类型 10活动 11赞助 12合作 13赞助
@property (nonatomic , copy) NSString * fromType;
//目标对象id   "fromType"为1时，"destId"为用户id，否则为群聊id
@property (nonatomic , copy) NSString * destId;
//是否置顶
@property (nonatomic , copy) NSString * topFlag;
//是否消息免打扰
@property (nonatomic , copy) NSString * receiveTip;
//头像url
@property (nonatomic , copy) NSString * headImg;
//最后一条消息
@property (nonatomic , copy) NSString * lastMsg;
//最后操作时间(毫秒数)
@property (nonatomic , copy) NSString * lastTime;
//名称
@property (nonatomic , copy) NSString * name;
//未读消息数
@property (nonatomic , copy) NSString * unReadMessageNum;
//消息类型（1 文本 2 语音 3 图片）
@property (nonatomic , strong) NSNumber * messageType;
//状态（正在发送 发送失败）
@property (nonatomic , copy) NSString * status;
//消息版本号
@property (nonatomic , strong) NSNumber * version;
//数据库（后面加的）
//是否有@
@property (nonatomic , copy) NSString * atFlag;
//隐藏
@property (nonatomic , copy) NSString * hidden;
@property (nonatomic , copy) NSString * sessionId;
//类型1个人聊天 2群聊天 3系统消息 101加好友通知  201群消息  301转账到账通知(aa收款，二维码收付款到账)
@property (nonatomic , strong) NSNumber * sessionType;

@property (nonatomic, copy) NSString *fromName1;
//草稿
@property (nonatomic , copy) NSString * draft;
@property (nonatomic, copy) NSString *fromName; // 新加 某某@了我；

@property (nonatomic, copy) NSString *fromId;

@end

NS_ASSUME_NONNULL_END
