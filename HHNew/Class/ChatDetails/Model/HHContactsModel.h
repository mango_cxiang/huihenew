//
//  HMJContactsModel.h
//  huihe
//
//  Created by Six on 16/4/14.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

/**
 *  联系人_模型
 */
@interface HHContactsModel : NSObject

@property (nonatomic , copy) NSString * contactId;   //好友id
@property (nonatomic , copy) NSString * headUrl;    //好友头像
@property (nonatomic , copy) NSString * markName;   //好友备注
@property (nonatomic , copy) NSString * mobile;     //好友手机号
@property (nonatomic , copy) NSString * sex;        //好友性别
@property (nonatomic , copy) NSString * sign;       //好友个性签名
@property (nonatomic , copy) NSString * nickName;   //好友昵称
@property (nonatomic , copy) NSString * isFriend;
@property (nonatomic , copy) NSString * status;     //好友状态
@property (nonatomic , copy) NSString * birthday;   //好友生日
@property (nonatomic , copy) NSString * city;       //市
@property (nonatomic , copy) NSString * province;   //省
@property (nonatomic , copy) NSString * district;   //地区
@property (nonatomic , copy) NSString * detail;     //详情
@property (nonatomic , copy) NSString * mail;       //邮箱
@property (nonatomic , copy) NSString * showName;   //展示的名字
@property (nonatomic , copy) NSString * receiveTip; //好友免打扰
@property (nonatomic , copy) NSString * isOnline;   //好友在线
@property (nonatomic , copy) NSString * isBlack;    //好友黑名单
@property (nonatomic , copy) NSString * realName;   //真实姓名
@property (nonatomic , copy) NSString * marks;      //分组(弃用)
@property (nonatomic , copy) NSString * groupName;      //分组名称
@property (nonatomic , copy) NSString * remark;     //备注
//临时使用 存储手机联系人头像
@property (nonatomic , copy) NSData * contactImgData;    //好友黑名单
@property (nonatomic , copy) NSString * isRegflag;          //手机联系人判断手机号有没有注册
@property (nonatomic , copy) NSString * distance;          //摇一摇、附近的人距离
@property (nonatomic , copy) NSString * updatetime;        //更新时间
@property (nonatomic , copy) NSString * lat;        //纬度
@property (nonatomic , copy) NSString * lng;        //经度
@property (nonatomic , copy) NSString * feedBackImage;        //朋友圈封面
@property (nonatomic , copy) NSString * feedPrivUser;        //不允许好友看我朋友圈
@property (nonatomic , copy) NSString * feedPrivFriend;        //不看好友的朋友圈
@property (nonatomic , copy) NSString * identity;        //群聊身份 1群主 2管理员  3普通群成员
@property (nonatomic , copy) NSString * iMNo;        //会合号（靓号）
@property (nonatomic , copy) NSString * joinType;       //进群方式 0邀请入群 1群号搜索 2扫码加群
@property (nonatomic , copy) NSString * allowSay; // 禁言-新
@property (nonatomic , copy) NSString * noGetRed; // 禁红包-新
@property (nonatomic , copy) NSString * role;     // 身份-新 1群主 2管理员  3普通群成员
@property (nonatomic, copy) NSString *fuserId; // 邀请人
//是否通过手机号搜索
@property (nonatomic , copy) NSString *byMobile;

- (instancetype)initWithDic:(NSDictionary *)dic;
- (instancetype)initWithGroupMemberDic:(NSDictionary *)dic;
- (instancetype)initWithTagDic:(NSDictionary *)dic;
//- (instancetype)initWithFMResultSet:(FMResultSet *)resultSet;
//- (instancetype)initWithFMResultSetGroupMember:(FMResultSet *)resultSet;
//- (instancetype)initWithFriendsFMResultSet:(FMResultSet *)resultSet;
#pragma mark --- 获取手机联系人
+ (NSMutableArray *)reloadContact;

#pragma mark --- 是否被选中
@property(nonatomic,assign)BOOL isChoose;

@property(nonatomic,strong)NSIndexPath * indexPath;

@end
