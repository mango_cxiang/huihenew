//
//  HHChatModel.m
//  HHNew
//
//  Created by 张 on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHChatModel.h"
#import "PPStickerDataManager.h"

@implementation HHChatModel


-(void)setMessageType:(NSString *)messageType
{
    _messageType = messageType;
    self.chatFrameType = ChatFrameTypeText;
    if ([messageType integerValue] == TOMessageTypePhoto||[messageType integerValue] == TOMessageTypeVideo) {
        self.chatFrameType = ChatFrameTypePicture;
    }
    if ([messageType integerValue] == TOMessageTypeInviteJoinGroup||[messageType integerValue] == TOMessageTypeBeInvitedJoinGroup||[messageType integerValue] == TOMessageTypeGroupInviteOpen||[messageType integerValue] == TOMessageTypeGroupInviteClose||[messageType integerValue] == TOMessageTypeGroupProtectYes||[messageType integerValue] == TOMessageTypeGroupProtectNO||[messageType integerValue] == TOMessageTypeGroupRedMoneyShow||[messageType integerValue] == TOMessageTypeGroupRedMoneyHidden) {
        self.chatFrameType = ChatFrameTypeSystemMessages;
    }
    switch ([messageType integerValue]) {
        case TOMessageTypeNameCard:
        {
            self.chatFrameType = ChatFrameTypeBusinessCard;
        }
            break;
        case TOMessageTypePosition:
        {
            self.chatFrameType = ChatFrameTypeLocation;
        }
            break;
        case TOMessageTypeRedPacket:
        {
            self.chatFrameType = ChatFrameTypeRedPacket;
        }
            break;
        case TOMessageTypeBQMM:
        {
            self.chatFrameType = ChatFrameTypeBQMM;
        }
            break;
        default:
            break;
    }
    //判断是否是系统消息类型、给群提示消息赋值
//    self.isGroupTipMessage =
//    self.groupTipMessage =
}
-(void)setSendTime:(NSString *)sendTime
{
    _sendTime = sendTime;
    if (IsBlankWithStr(_timeString)) {
        self.timeString = [NSString returnDateStrWithTime:[_sendTime doubleValue]];
    }
}

-(NSMutableAttributedString *)attributedContent
{
    if (!_attributedContent) {
        if (IsBlankWithStr(self.content)) {
            return [[NSMutableAttributedString alloc]initWithString:@"会合：消息内容为空"];
        }
        NSDictionary * dataDic = [self.content mj_JSONObject];
        _attributedContent = [[NSMutableAttributedString alloc] initWithString:dataDic[@"msgString"] attributes:@{ NSFontAttributeName:SystemFont(16), NSForegroundColorAttributeName:ColorManage.text_color}];
        [PPStickerDataManager.sharedInstance replaceEmojiForAttributedString:_attributedContent font:SystemFont(16)];
    }
    return _attributedContent;
}

@end
