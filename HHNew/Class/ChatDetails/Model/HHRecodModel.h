//
//  HHRecodModel.h
//  HHNew
//
//  Created by Liubin on 2020/4/29.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HHRecodModel : NSObject
@property (nonatomic,strong) NSString *entryAccountTime;
@property (nonatomic,strong) NSString *packetId;
@property (nonatomic,strong) NSString *redPacketAmount;
@property (nonatomic,strong) NSString *redPacketCount;
@property (nonatomic,strong) NSString *reviceUserId;
@property (nonatomic,strong) NSString *sendTime;
@property (nonatomic,strong) NSString *reviceTime;
@property (nonatomic,strong) NSString *tableName;
@property (nonatomic,strong) NSString *userId;
@end

NS_ASSUME_NONNULL_END
