//
//  HMJCountryViewController.m
//  huihe
//
//  Created by Six on 16/3/29.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJCountryViewController.h"
#import "HMJCountryCell.h"
#import "HMJCountryHeadView.h"

@interface HMJCountryViewController ()<UITableViewDataSource , UITableViewDelegate>

//表单
@property (weak , nonatomic) IBOutlet UITableView * tableView;
//数据源
@property (nonatomic , strong) NSMutableArray * dataSourceArray;

@end

@implementation HMJCountryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //自定制导航条
    [self customNavigationBar];

    //添加本地固定数据
    _dataSourceArray = [NSMutableArray arrayWithArray:[HMJCountryGroupModel addDataToDataSourceArray]];
    
    //处理tableView
    [self dealWithTableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - 自定制导航条
- (void)customNavigationBar
{
    self.title = @"选择国家和地区";
}

#pragma mark - 处理tableView
- (void)dealWithTableView
{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = 60;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.backgroundColor = ColorManage.grayBackgroundColor;
    
    UIView * tableFooterView = [[UIView alloc] init];
    tableFooterView.backgroundColor = ColorManage.grayBackgroundColor;
    self.tableView.tableFooterView = tableFooterView;
}

#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataSourceArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    HMJCountryGroupModel * countryGroup = self.dataSourceArray[section];
    return countryGroup.countryArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * cellName = @"countryCell";
    HMJCountryCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if(!cell)
    {
        cell = LoadCell(@"HMJCountryCell");
    }
    HMJCountryGroupModel * countryGroup = self.dataSourceArray[indexPath.section];
    cell.country = countryGroup.countryArray[indexPath.row];
    cell.backgroundColor = ColorManage.darkBackgroundColor;
    cell.selectedBackgroundView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        cell.separatorInset = UIEdgeInsetsMake(0, 10, 0, 0);
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        cell.layoutMargins = UIEdgeInsetsMake(0, 10, 0, 0);
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    HMJCountryHeadView * countryHeadView = [HMJCountryHeadView headViewWithTableView:tableView];
    countryHeadView.countryGroup = self.dataSourceArray[section];
    countryHeadView.backgroundColor = ColorManage.grayBackgroundColor;
    return countryHeadView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HMJCountryGroupModel * countryGroup = self.dataSourceArray[indexPath.section];
    HMJCountryModel * countryModel = countryGroup.countryArray[indexPath.row];
    if (self.selectCountryBlock)
    {
        self.selectCountryBlock(countryModel.country , countryModel.mobile_prefix);
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
