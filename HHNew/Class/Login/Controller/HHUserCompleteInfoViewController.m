//
//  HHUserCompleteInfoViewController.m
//  HHNew
//
//  Created by Liubin on 2020/5/7.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHUserCompleteInfoViewController.h"
#import "JJImagePicker.h"
#import "HHUserAuthenticationViewController.h"

@interface HHUserCompleteInfoViewController ()

@end

@implementation HHUserCompleteInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"完善个人信息";
    self.view.backgroundColor = ColorManage.background_color;
    self.nickNameView.backgroundColor = ColorManage.darkBackgroundColor;
    self.nickNameLabel.textColor  = ColorManage.text_color;
    
    self.finishBtn.enabled = NO;
    self.finishBtn.backgroundColor = ColorManage.loginBtnColorDefault;
    [self.finishBtn viewWithRadis:5];
    self.finishBtn.timeInterval = 2;
    
    self.nickNameTextField.tintColor = RGB_GreenColor;
    [self.nickNameTextField addTarget:self action:@selector(nickNameContentChanged:) forControlEvents:UIControlEventEditingChanged];
}


- (void)nickNameContentChanged:(UITextField *)field{
    if (field.text.length > 0) {
        [self canClickLoginBtn:YES];
    }else{
        [self canClickLoginBtn:NO];
    }
}

- (void)canClickLoginBtn:(BOOL)canClick{
    if (canClick) {
        self.finishBtn.enabled = YES;
        self.finishBtn.backgroundColor = ColorManage.greenTextColor;
        [self.finishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        return;
    }
    self.finishBtn.enabled = NO;
    self.finishBtn.backgroundColor = ColorManage.loginBtnColorDefault;
}

- (IBAction)clickSetHeadBtn:(id)sender {
    [self updateHeaderImg];
}


- (IBAction)clickFinishBtn:(id)sender {
    HHUserAuthenticationViewController *avc = Init(HHUserAuthenticationViewController);
    [self.navigationController pushViewController:avc animated:YES];
}


- (void)updateHeaderImg{
    JJImagePicker *picker = [[HHDefaultTools sharedInstance] getJJImagePicker];
    picker.aspectRatioPreset = TOCropViewControllerAspectRatioPresetSquare;
    WeakSelf(self)
     [picker actionSheetWithTakePhotoTitle:@"拍照" albumTitle:@"从相册中选择" cancelTitle:@"取消" InViewController:self didFinished:^(JJImagePicker *picker, UIImage *image) {
         StrongSelf(weakself)
         [strongweakself uploadUserIconImage:image];
     }];
}

//TODO:刷新照片
- (void)uploadUserIconImage:(UIImage *)image{
    [self.setHeadBtn setImage:image forState:UIControlStateNormal];
    [self.setHeadBtn viewWithRadis:5];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.nickNameTextField resignFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.nickNameTextField resignFirstResponder];
}
@end
