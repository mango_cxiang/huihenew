//
//  LoginVerificationCodeViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/21.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "LoginVerificationCodeViewController.h"
#import "HMJGetCodeButton.h"
#import "UIButton+ButtonCreat.h"
#import "NSString+PDRegex.h"
#import "NSString+Extension.h"
#import "HMJCountryViewController.h"
#import "HHAgreementView.h"
#import "UIView+Extension.h"
#import "LoginPasswordViewController.h"

@interface LoginVerificationCodeViewController ()

@property (nonatomic , strong) HHAgreementView * agreementView;
@end

@implementation LoginVerificationCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = ColorManage.darkBackgroundColor;
    self.countryBgView.backgroundColor = ColorManage.darkBackgroundColor;
    self.phoneBgView.backgroundColor = ColorManage.darkBackgroundColor;
    self.codeBgView.backgroundColor = ColorManage.darkBackgroundColor;
    self.phoneCodeLabel.textColor  = ColorManage.text_color;
    self.codeLabel.textColor  = ColorManage.text_color;
    
    UIButton * rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    [rightBtn setTitle:@"密码登录" forState:UIControlStateNormal];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [rightBtn setTitleColor:ColorManage.loginTextColor forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightBtnAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    [self createUI];
    [self handleBlock];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidShow:)
                                                name:UIKeyboardWillShowNotification
                                              object:nil];

    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidHidden)
                                                name:UIKeyboardWillHideNotification
                                              object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)createUI{
    [self.contryBtn setTitle:[NSString stringWithFormat:@"%@   %@", @"国家/地区", @"中国"] forState:UIControlStateNormal];
    [self.contryBtn setTitleColor:ColorManage.text_color forState:UIControlStateNormal];
    self.phoneCodeLabel.text = @"+86";
    
    [self.getVerifyCodeBtn creatButtonWithfillet:2 borderColor:ColorManage.greenTextColor borderWidth:1];
    [self.getVerifyCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    
    self.loginBtn.layer.cornerRadius = 5;
    self.loginBtn.layer.masksToBounds = YES;
    self.loginBtn.enabled = NO;
    self.loginBtn.backgroundColor = ColorManage.loginBtnColorDefault;

    [self.view addSubview:self.agreementView];
    
    [self.phoneTextField addTarget:self action:@selector(phoneContentChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.inputVerifyCodeTextField addTarget:self action:@selector(passwordContentChanged:) forControlEvents:UIControlEventEditingChanged];
    
    self.phoneTextField.tintColor = ColorManage.greenTextColor;
    self.inputVerifyCodeTextField.tintColor = ColorManage.greenTextColor;

}

- (void)handleBlock{
    WeakSelf(self);
    self.agreementView.clickAgreement = ^(NSString * _Nonnull webUrl) {
       
    };
}

//键盘即将弹出
- (void)handleKeyboardDidShow:(NSNotification*)paramNotification{
    //获取键盘的高度
    NSDictionary *userInfo = [paramNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    int height = keyboardRect.size.height;
    [UIView animateWithDuration:0.3 animations:^{
        self.agreementView.y = SCREEN_HEIGHT - 50 - height;
    }];
}

- (void)handleKeyboardDidHidden{
    [UIView animateWithDuration:0.3 animations:^{
        self.agreementView.y = SCREEN_HEIGHT - 80;
    }];
}

//TODO:点击密码登录
-(void)rightBtnAction{
    LoginPasswordViewController *lvc = [[LoginPasswordViewController alloc] init];
    lvc.accountStr = self.phoneTextField.text;
    WeakSelf(self);
    lvc.toLoginCodeVC = ^(NSString * _Nonnull phoneStr) {
        weakself.phoneTextField.text = phoneStr;
    };
    [self.navigationController pushViewController:lvc animated:YES];
}


//点击获取验证码
- (IBAction)clickGeVerifyCodeBtn:(id)sender {
    if (self.phoneTextField.text.length <= 0){
        //请输入手机号
        return;
    }
    if (![self.phoneTextField.text isMobileNumIllegal]){
        //请输入正确的手机号
        return;
    }
    [sender timeFailBeginFrom:5];
    [self.phoneTextField resignFirstResponder];
}

//点击选择国家/地区
- (IBAction)clickContryBtn:(id)sender {
    //选择国家
    HMJCountryViewController *countryViewController = [[HMJCountryViewController alloc] init];
    WeakSelf(self)
    countryViewController.selectCountryBlock= ^(NSString * countryName , NSString * phoneCode) {
        [weakself.contryBtn creatButtonWithTitle:[NSString stringWithFormat:@"国家/地区  %@",countryName]];
        weakself.phoneCodeLabel.text = [NSString stringWithFormat:@"+%@",phoneCode];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString * countryCode = phoneCode;
        [defaults setObject:countryCode forKey:@"countryCode"];
        [defaults synchronize];
    };
    [self.navigationController pushViewController:countryViewController animated:YES];
}


//点击登录
- (IBAction)clickLoginBtn:(id)sender {
    if (self.phoneTextField.text.length <= 0 || self.inputVerifyCodeTextField.text.length <=0) {
        //请输入手机号和验证码
        return;
    }
    if (![self.phoneTextField.text isMobileNumIllegal]) {
        //请输入合法的手机号
        return;
    }
    if (![self.inputVerifyCodeTextField.text isNumAndChars]) {
        //请输入正确的验证码
        return;
    }
}

//处理字符动态输入,刷新登录按钮状态
- (void)phoneContentChanged:(UITextField *)phfield{
    if (phfield.text.length > 0 && self.inputVerifyCodeTextField.text.length > 0) {
        [self canClickLoginBtn:YES];
    }else{
        [self canClickLoginBtn:NO];
    }
}

- (void)passwordContentChanged:(UITextField *)psField{
    if (psField.text.length > 0 && self.phoneTextField.text.length > 0) {
        [self canClickLoginBtn:YES];
    }else{
        [self canClickLoginBtn:NO];
    }
}

- (void)canClickLoginBtn:(BOOL)canClick{
    if (canClick) {
        self.loginBtn.enabled = YES;
        self.loginBtn.backgroundColor = ColorManage.greenTextColor;
        [self.loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        return;
    }
    self.loginBtn.enabled = NO;
    self.loginBtn.backgroundColor = ColorManage.loginBtnColorDefault;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.phoneTextField resignFirstResponder];
    [self.inputVerifyCodeTextField resignFirstResponder];
}


- (HHAgreementView *)agreementView{
    if (!_agreementView) {
        _agreementView = [[HHAgreementView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 105, SCREEN_HEIGHT - 80, 210, 45)];
    }
    return _agreementView;
}

@end
