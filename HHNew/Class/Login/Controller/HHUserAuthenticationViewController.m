//
//  HHUserAuthenticationViewController.m
//  HHNew
//
//  Created by Liubin on 2020/5/7.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHUserAuthenticationViewController.h"
#import "HHUserPayPasswordView.h"
#import "NSString+Format.h"

@interface HHUserAuthenticationViewController ()
@property (nonatomic, strong) HHUserPayPasswordView *payPswView;
//大于0，已实名认证
@property (nonatomic, copy) NSString *status;

@end

@implementation HHUserAuthenticationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"实名认证";
    self.view.backgroundColor = ColorManage.background_color;
    self.idView.backgroundColor = ColorManage.darkBackgroundColor;
    self.nameView.backgroundColor = ColorManage.darkBackgroundColor;

    self.nameLabel.textColor  = ColorManage.text_color;
    self.idLabel.textColor  = ColorManage.text_color;
    
    self.finishBtn.enabled = NO;
    self.finishBtn.backgroundColor = ColorManage.loginBtnColorDefault;
    [self.finishBtn viewWithRadis:5];
    self.finishBtn.timeInterval = 2;
    
    self.nameTextField.tintColor = RGB_GreenColor;
    self.idTextField.tintColor = RGB_GreenColor;
    [self.nameTextField addTarget:self action:@selector(nameContentChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.idTextField addTarget:self action:@selector(idContentChanged:) forControlEvents:UIControlEventEditingChanged];
    [self createUI];
}

- (void)createUI{
    _status = @"0";
    if ([_status intValue] > 0) {
        self.nameView.hidden = YES;
        self.idView.hidden = YES;
    }
    [self.view addSubview:self.payPswView];
    [self.payPswView mas_makeConstraints:^(MASConstraintMaker *make) {
        if([_status intValue] > 0){
            make.top.mas_equalTo(self.descLabel.mas_bottom).offset(70);
        }
        else{
            make.top.mas_equalTo(self.idView.mas_bottom).offset(0);
        }
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(55);
    }];
}

- (IBAction)clickFinishBtn:(id)sender {
    if([NSString verificationIsValidWithStr:self.nameTextField.text] == NO ){
        [[HMJShowLoding sharedInstance] showText:@"请输入姓名"];
        return;
    }
    if([NSString verificationIsValidWithStr:self.idTextField.text] == NO){
        [[HMJShowLoding sharedInstance] showText:@"请输入身份证号"];
        return;
    }
    if([NSString verificationIsValidWithStr:self.payPswView.payPswTextField.text] == NO){
        [[HMJShowLoding sharedInstance] showText:@"请输入支付密码"];
        return;
    }
    if([NSString judgeIdentityStringValid:self.idTextField.text] == NO){
        [[HMJShowLoding sharedInstance] showText:@"请输入正确的身份证号"];
        return;
    }
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

//处理字符动态输入,刷新登录按钮状态
- (void)nameContentChanged:(UITextField *)nameField{
    if (nameField.text.length > 0 && self.idTextField.text.length >0) {
        [self canClickLoginBtn:YES];
    }else{
        [self canClickLoginBtn:NO];
    }
}

- (void)idContentChanged:(UITextField *)idField{
    if (idField.text.length > 0 && self.nameTextField.text.length >0) {
        [self canClickLoginBtn:YES];
    }else{
        [self canClickLoginBtn:NO];
    }
}

- (void)canClickLoginBtn:(BOOL)canClick{
    if (canClick) {
        self.finishBtn.enabled = YES;
        self.finishBtn.backgroundColor = ColorManage.greenTextColor;
        [self.finishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        return;
    }
    self.finishBtn.enabled = NO;
    self.finishBtn.backgroundColor = ColorManage.loginBtnColorDefault;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.nameTextField resignFirstResponder];
    [self.idTextField resignFirstResponder];
    [self.payPswView.payPswTextField resignFirstResponder];

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.nameTextField resignFirstResponder];
    [self.idTextField resignFirstResponder];
    [self.payPswView.payPswTextField resignFirstResponder];

}

- (HHUserPayPasswordView *)payPswView{
    if (!_payPswView) {
        _payPswView = [[HHUserPayPasswordView alloc] init];
    }
    return _payPswView;
}


@end
