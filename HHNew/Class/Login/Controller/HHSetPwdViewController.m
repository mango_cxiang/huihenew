//
//  HHSetPwdViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/22.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHSetPwdViewController.h"
#import "HMJGetCodeButton.h"
#import "UIButton+ButtonCreat.h"
#import "NSString+PDRegex.h"
#import "NSString+Extension.h"
#import "UIView+Extension.h"


@interface HHSetPwdViewController ()

@end

@implementation HHSetPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = ColorManage.darkBackgroundColor;
    self.phoneBgView.backgroundColor = ColorManage.darkBackgroundColor;
    self.codeBgView.backgroundColor = ColorManage.darkBackgroundColor;
    self.pswBgView.backgroundColor = ColorManage.darkBackgroundColor;

    self.phoneLabel.textColor  = ColorManage.text_color;
    self.codeLabel.textColor  = ColorManage.text_color;
    self.pswLabel.textColor  = ColorManage.text_color;

    [self setTitle:@"设置密码"];
    [self createUI];
}

- (void)createUI{
    
    [self.getCodeBtn creatButtonWithfillet:2 borderColor:ColorManage.greenTextColor borderWidth:1];
    [self.getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    
    self.finishBtn.layer.cornerRadius = 5;
    self.finishBtn.layer.masksToBounds = YES;
    self.finishBtn.enabled = NO;
    self.finishBtn.backgroundColor = ColorManage.loginBtnColorDefault;
    
    [self.codeTextField addTarget:self action:@selector(codeContentChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.pwdTextField addTarget:self action:@selector(passwordContentChanged:) forControlEvents:UIControlEventEditingChanged];

    self.phoneTextField.text = self.accountStr;
    self.phoneTextField.tintColor = ColorManage.greenTextColor;
    self.codeTextField.tintColor = ColorManage.greenTextColor;
    self.pwdTextField.tintColor = ColorManage.greenTextColor;
}

//获取验证码
- (IBAction)clickGetCodeBtn:(id)sender {
    if (self.phoneTextField.text.length <= 0){
        //请输入手机号
        return;
    }
    if (![self.phoneTextField.text isMobileNumIllegal]){
        //请输入正确的手机号
        return;
    }
    [sender timeFailBeginFrom:5];
    [self.phoneTextField resignFirstResponder];

}


//点击完成
- (IBAction)clickFinishBtn:(id)sender {
    if (self.phoneTextField.text.length <= 0 || self.codeTextField.text.length <=0 || self.pwdTextField.text.length <=0) {
           //请输入手机号和验证码
           return;
       }
       if (![self.phoneTextField.text isMobileNumIllegal]) {
           //请输入合法的手机号
           return;
       }
       if (![self.pwdTextField.text isNumAndChars]) {
           //请输入正确的验证码
           return;
       }
}

//处理字符动态输入,刷新登录按钮状态
- (void)codeContentChanged:(UITextField *)phfield{
    if (phfield.text.length > 0 && self.pwdTextField.text.length > 0) {
        [self canClickLoginBtn:YES];
    }else{
        [self canClickLoginBtn:NO];
    }
}

- (void)passwordContentChanged:(UITextField *)psField{
    if (psField.text.length > 0 && self.codeTextField.text.length > 0) {
        [self canClickLoginBtn:YES];
    }else{
        [self canClickLoginBtn:NO];
    }
}

- (void)canClickLoginBtn:(BOOL)canClick{
    if (canClick) {
        self.finishBtn.enabled = YES;
        self.finishBtn.backgroundColor = ColorManage.greenTextColor;
        [self.finishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        return;
    }
    self.finishBtn.enabled = NO;
    self.finishBtn.backgroundColor = ColorManage.loginBtnColorDefault;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.phoneTextField resignFirstResponder];
    [self.codeTextField resignFirstResponder];
    [self.pwdTextField resignFirstResponder];
}

@end
