//
//  HMJCountryViewController.h
//  huihe
//
//  Created by Six on 16/3/29.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
/**
 *  选择国家和地区
 */
@interface HMJCountryViewController : BaseViewController

// 选择国家 block 回调
@property (nonatomic , copy) void(^selectCountryBlock)(NSString * countryName , NSString * phoneCode);

@end
