//
//  LoginPasswordViewController.h
//  HHNew
//
//  Created by Liubin on 2020/4/22.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^toLoginCodeVC)(NSString *phoneStr);

@interface LoginPasswordViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITextField *accountTextField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *setPwdBtn;
- (IBAction)clickLoginBtn:(id)sender;
- (IBAction)clickSetBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *accountBgView;
@property (weak, nonatomic) IBOutlet UIView *pswBgView;
@property (weak, nonatomic) IBOutlet UILabel *accountLabel;
@property (weak, nonatomic) IBOutlet UILabel *pswLabel;

@property (nonatomic, copy) NSString * accountStr;
@property (nonatomic, copy) toLoginCodeVC toLoginCodeVC;


@end

NS_ASSUME_NONNULL_END
