//
//  HHUserCompleteInfoViewController.h
//  HHNew
//
//  Created by Liubin on 2020/5/7.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HHUserCompleteInfoViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIButton *setHeadBtn;
- (IBAction)clickSetHeadBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *nickNameView;
@property (weak, nonatomic) IBOutlet UITextField *nickNameTextField;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *finishBtn;
- (IBAction)clickFinishBtn:(id)sender;

@end

NS_ASSUME_NONNULL_END
