//
//  LoginPasswordViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/22.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "LoginPasswordViewController.h"
#import "UIButton+ButtonCreat.h"
#import "NSString+PDRegex.h"
#import "NSString+Extension.h"
#import "UIView+Extension.h"
#import "HHAgreementView.h"
#import "LoginVerificationCodeViewController.h"
#import "HHSetPwdViewController.h"

@interface LoginPasswordViewController ()
@property (nonatomic , strong) HHAgreementView * agreementView;

@end

@implementation LoginPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = ColorManage.darkBackgroundColor;
    self.accountBgView.backgroundColor = ColorManage.darkBackgroundColor;
    self.pswBgView.backgroundColor = ColorManage.darkBackgroundColor;
    self.accountLabel.textColor  = ColorManage.text_color;
    self.pswLabel.textColor  = ColorManage.text_color;
    
    // Do any additional setup after loading the view from its nib.
    UIButton * rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    [rightBtn setTitle:@"验证码登录" forState:UIControlStateNormal];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [rightBtn setTitleColor:ColorManage.loginTextColor forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightBtnAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    [self setTitle:@"密码登录"];
    
    [self createUI];
    [self handleBlock];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidShow:)
                                                name:UIKeyboardWillShowNotification
                                              object:nil];

    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidHidden)
                                                name:UIKeyboardWillHideNotification
                                              object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)createUI{
    self.loginBtn.layer.cornerRadius = 5;
    self.loginBtn.layer.masksToBounds = YES;
    self.loginBtn.enabled = NO;
    self.loginBtn.backgroundColor = ColorManage.loginBtnColorDefault;

    [self.view addSubview:self.agreementView];
    
    [self.setPwdBtn setTitleColor:ColorManage.loginTextColor forState:UIControlStateNormal];
    
    self.accountTextField.text = self.accountStr;
    [self.accountTextField addTarget:self action:@selector(phoneContentChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.pwdTextField addTarget:self action:@selector(passwordContentChanged:) forControlEvents:UIControlEventEditingChanged];
    
    self.accountTextField.tintColor = ColorManage.greenTextColor;
    self.pwdTextField.tintColor = ColorManage.greenTextColor;

}

- (void)handleBlock{
    WeakSelf(self);
    self.agreementView.clickAgreement = ^(NSString * _Nonnull webUrl) {
       
    };
}

//键盘即将弹出
- (void)handleKeyboardDidShow:(NSNotification*)paramNotification{
    //获取键盘的高度
    NSDictionary *userInfo = [paramNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    int height = keyboardRect.size.height;
    [UIView animateWithDuration:0.3 animations:^{
        self.agreementView.y = SCREEN_HEIGHT - 50 - height;
    }];
}

- (void)handleKeyboardDidHidden{
    [UIView animateWithDuration:0.3 animations:^{
        self.agreementView.y = SCREEN_HEIGHT - 80;
    }];
}

// TODO:点击验证码登录
-(void)rightBtnAction{
    if (self.toLoginCodeVC) {
        self.toLoginCodeVC(self.accountTextField.text);
    }
    [self.navigationController popViewControllerAnimated:YES];
}


//点击登录
- (IBAction)clickLoginBtn:(id)sender {
    if (self.accountTextField.text.length <= 0 || self.pwdTextField.text.length <=0) {
        //请输入手机号和验证码
        return;
    }
    if (![self.accountTextField.text isMobileNumIllegal]) {
        //请输入合法的手机号
        return;
    }
    if (![self.pwdTextField.text isNumAndChars]) {
        //请输入正确的验证码
        return;
    }
}

//设置密码
- (IBAction)clickSetBtn:(id)sender {
    HHSetPwdViewController *svc = [[HHSetPwdViewController alloc] init];
    svc.accountStr = self.accountTextField.text;
    [self.navigationController pushViewController:svc animated:YES];
//    svc.modalPresentationStyle = UIModalPresentationFullScreen;
//    [self presentViewController:svc animated:YES completion:^{
//        
//    }];
}

//处理字符动态输入,刷新登录按钮状态
- (void)phoneContentChanged:(UITextField *)phfield{
    if (phfield.text.length > 0 && self.pwdTextField.text.length > 0) {
        [self canClickLoginBtn:YES];
    }else{
        [self canClickLoginBtn:NO];
    }
}

- (void)passwordContentChanged:(UITextField *)psField{
    if (psField.text.length > 0 && self.accountTextField.text.length > 0) {
        [self canClickLoginBtn:YES];
    }else{
        [self canClickLoginBtn:NO];
    }
}

- (void)canClickLoginBtn:(BOOL)canClick{
    if (canClick) {
        self.loginBtn.enabled = YES;
        self.loginBtn.backgroundColor = ColorManage.greenTextColor;
        [self.loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        return;
    }
    self.loginBtn.enabled = NO;
    self.loginBtn.backgroundColor = ColorManage.loginBtnColorDefault;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.accountTextField resignFirstResponder];
    [self.pwdTextField resignFirstResponder];
}


- (HHAgreementView *)agreementView{
    if (!_agreementView) {
        _agreementView = [[HHAgreementView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 105, SCREEN_HEIGHT - 80, 210, 45)];
    }
    return _agreementView;
}

@end
