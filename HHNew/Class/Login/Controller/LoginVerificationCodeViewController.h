//
//  LoginVerificationCodeViewController.h
//  HHNew
//
//  Created by Liubin on 2020/4/21.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "BaseViewController.h"
@class HMJGetCodeButton;
NS_ASSUME_NONNULL_BEGIN

@interface LoginVerificationCodeViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIButton *contryBtn;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UILabel *phoneCodeLabel;
@property (weak, nonatomic) IBOutlet HMJGetCodeButton *getVerifyCodeBtn;
@property (weak, nonatomic) IBOutlet UITextField *inputVerifyCodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
- (IBAction)clickLoginBtn:(id)sender;
- (IBAction)clickGeVerifyCodeBtn:(id)sender;
- (IBAction)clickContryBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *countryBgView;
@property (weak, nonatomic) IBOutlet UIView *phoneBgView;

@property (weak, nonatomic) IBOutlet UIView *codeBgView;
@property (weak, nonatomic) IBOutlet UILabel *codeLabel;

@end

NS_ASSUME_NONNULL_END
