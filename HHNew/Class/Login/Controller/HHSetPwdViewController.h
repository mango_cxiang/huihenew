//
//  HHSetPwdViewController.h
//  HHNew
//
//  Created by Liubin on 2020/4/22.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "BaseViewController.h"
@class HMJGetCodeButton;
NS_ASSUME_NONNULL_BEGIN

@interface HHSetPwdViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;
@property (weak, nonatomic) IBOutlet HMJGetCodeButton *getCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *finishBtn;
@property (weak, nonatomic) IBOutlet UIView *phoneBgView;

- (IBAction)clickGetCodeBtn:(id)sender;
- (IBAction)clickFinishBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *codeBgView;
@property (weak, nonatomic) IBOutlet UIView *pswBgView;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *codeLabel;
@property (weak, nonatomic) IBOutlet UILabel *pswLabel;

@property (nonatomic, copy) NSString * accountStr;

@end

NS_ASSUME_NONNULL_END
