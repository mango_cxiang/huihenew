//
//  HMJCountryCell.h
//  huihe
//
//  Created by Six on 16/3/29.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMJCountryModel.h"

/**
 *  国家列表cell
 */
@interface HMJCountryCell : UITableViewCell

/**
 *  国家_模型
 */
@property (nonatomic , strong) HMJCountryModel * country;

@end
