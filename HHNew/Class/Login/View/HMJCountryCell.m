//
//  HMJCountryCell.m
//  huihe
//
//  Created by Six on 16/3/29.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJCountryCell.h"

@interface HMJCountryCell ()

//国家
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;
//编号
@property (weak, nonatomic) IBOutlet UILabel *codeLabel;

//@property (strong, nonatomic) IBOutlet UILabel *countryDefaultLabel;
@end

@implementation HMJCountryCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    //设置cell上的一些属性
    [self setSubViewsOnCell];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

//设置cell上的一些属性
- (void)setSubViewsOnCell
{
//    self.selectionStyle = UITableViewCellSelectionStyleNone;
    //self.countryDefaultLabel.font = SystemFont(12);
    self.codeLabel.font = SystemFont(16);
    self.countryLabel.font = SystemFont(14);
    
    self.codeLabel.textColor = ColorManage.text_color;
    self.countryLabel.textColor = ColorManage.text_color;
}

// 通过模型赋值
- (void)setCountry:(HMJCountryModel *)country
{
    _country = country;
    
    self.countryLabel.text = country.country;
    
    self.codeLabel.text = [NSString stringWithFormat:@"+%@" , country.mobile_prefix];
    
   // self.countryDefaultLabel.text = [NSString stringWithFormat:@"(%@)",country.country];
}

@end
