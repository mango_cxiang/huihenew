//
//  HMJGetCodeButton.m
//  huihe
//
//  Created by changle on 2016/12/26.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJGetCodeButton.h"
#import "GrandConfig.h"
@interface HMJGetCodeButton ()

@property (nonatomic,strong) NSTimer * timer;

@property (nonatomic,assign) NSInteger count;

@end

@implementation HMJGetCodeButton

- (void)timeFailBeginFrom:(NSInteger)timeCount {
    self.count = timeCount;
    self.layer.borderColor =UIColorFromRGB(0x999999).CGColor;
    [self setTitleColor:UIColorFromRGB(0x999999) forState:UIControlStateNormal];
    [self setTitle:[NSString stringWithFormat:@"%@%ld%@",@"剩余", self.count,@"秒"] forState:UIControlStateDisabled];
    self.enabled = NO;
    // 加1个计时器
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
}

- (void)timerFired {
    if (self.count > 1) {
        self.count --;
        [self setTitle:[NSString stringWithFormat:@"%@%ld%@",@"剩余", self.count,@"秒"] forState:UIControlStateDisabled];
        self.layer.borderColor =UIColorFromRGB(0x999999).CGColor;
        [self setTitleColor:UIColorFromRGB(0x999999) forState:UIControlStateNormal];
    } else {
        self.enabled = YES;
        [self setTitle:@"重新获取" forState:UIControlStateNormal];
        self.layer.borderColor =UIColorFromRGB(0x07C160).CGColor;
        [self setTitleColor:UIColorFromRGB(0x07C160) forState:UIControlStateNormal];
        [self.timer invalidate];
    }
}

@end
