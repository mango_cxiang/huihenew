//
//  HHAgreementView.h
//  HHNew
//
//  Created by Liubin on 2020/4/21.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^clickAgreement)(NSString *webUrl);

@interface HHAgreementView : UIView

@property (nonatomic, copy) clickAgreement clickAgreement;
- (instancetype)initWithFrame:(CGRect)frame;

@end

NS_ASSUME_NONNULL_END
