//
//  HHUserPayPasswordView.m
//  HHNew
//
//  Created by Liubin on 2020/5/7.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHUserPayPasswordView.h"

@implementation HHUserPayPasswordView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        //        [self loadView];
        self =  [[[NSBundle mainBundle]loadNibNamed:@"HHUserPayPasswordView" owner:self options:nil] lastObject];
        self.frame = frame;
    }
    return self;
}


- (void)awakeFromNib{
    [super awakeFromNib];
    self.payPswTextField.tintColor = RGB_GreenColor;
    self.bgView.backgroundColor = ColorManage.darkBackgroundColor;
    self.pswTitleLabel.textColor  = ColorManage.text_color;
}

@end
