//
//  HHAgreementView.m
//  HHNew
//
//  Created by Liubin on 2020/4/21.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHAgreementView.h"
#import "UIView+Extension.h"
@interface HHAgreementView()

@end

@implementation HHAgreementView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        [self loadView];
    }
    return self;
}


- (void)loadView{
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 210, 18)];
    titleLabel.font = [UIFont systemFontOfSize:12];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = UIColorFromRGB(0x999999);
    titleLabel.numberOfLines = 0;
    titleLabel.text = @"轻触上面的“登录”按钮即表示您同意";
    [self addSubview:titleLabel];
    
    
    UIButton *sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sendBtn.frame = CGRectMake(self.width / 2 - 80, titleLabel.bottom, 160, 20);
    [sendBtn setTitle:@"《会合软件许可及服务协议》" forState:UIControlStateNormal];
    sendBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [sendBtn setTitleColor:UIColorFromRGB(0x576B95) forState:UIControlStateNormal];
    sendBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [sendBtn addTarget:self action:@selector(btnDidPress:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sendBtn];
}


- (void)btnDidPress:(id)sender {
    if (self.clickAgreement) {
        self.clickAgreement(LoginAgreement);
    }
}
@end
