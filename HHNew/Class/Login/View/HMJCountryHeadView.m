//
//  HMJCountryHeadView.m
//  huihe
//
//  Created by Six on 16/3/29.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJCountryHeadView.h"
#import "UIView+Extension.h"
@interface HMJCountryHeadView ()

//组名
@property (nonatomic , strong) UILabel * nameLabel;

@end

@implementation HMJCountryHeadView

/**
 *  聊天文件_组视图
 *
 *  @param tableView tableView
 *
 *  @return self
 */
+ (instancetype)headViewWithTableView:(UITableView *)tableView
{
    static NSString * identifier = @"countryHeadView";
    HMJCountryHeadView * countryHeadView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:identifier];
    if (!countryHeadView)
    {
        countryHeadView = [[HMJCountryHeadView alloc] initWithReuseIdentifier:identifier];
    }
    return countryHeadView;
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier])
    {
        //创建子视图
        [self createSubViewsOnSelf];
    }
    return self;
}

//创建子视图
- (void)createSubViewsOnSelf
{
    self.contentView.backgroundColor = ColorManage.grayBackgroundColor;
    
    //组名
    UILabel * nameLabel = [[UILabel alloc] init];
    self.nameLabel = nameLabel;
    nameLabel.textColor = ColorManage.text_color;
    nameLabel.textAlignment = NSTextAlignmentLeft;
    nameLabel.font = SystemFont(12);
    [self.contentView addSubview:nameLabel];
}

//通过模型赋值
- (void)setCountryGroup:(HMJCountryGroupModel *)countryGroup
{
    _countryGroup = countryGroup;
    self.nameLabel.text = countryGroup.nameStr;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //组名
    self.nameLabel.frame = CGRectMake(10, 0, self.width - 10, self.height);
}

@end
