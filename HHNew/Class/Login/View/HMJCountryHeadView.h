//
//  HMJCountryHeadView.h
//  huihe
//
//  Created by Six on 16/3/29.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMJCountryGroupModel.h"

/**
 *  国家列表_组视图
 */
@interface HMJCountryHeadView : UITableViewHeaderFooterView

/**
 *  国家列表_组模型
 */
@property (nonatomic , strong) HMJCountryGroupModel * countryGroup;

/**
 *  聊天文件_组视图
 *
 *  @param tableView tableView
 *
 *  @return self
 */
+ (instancetype)headViewWithTableView:(UITableView *)tableView;

@end
