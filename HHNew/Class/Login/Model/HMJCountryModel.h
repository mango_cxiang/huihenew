//
//  HMJCountryModel.h
//  huihe
//
//  Created by Six on 16/3/29.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  国家_模型
 */
@interface HMJCountryModel : NSObject

// 国名
@property (nonatomic , copy) NSString * country;

// 国家手机编号
@property (nonatomic , copy) NSString * mobile_prefix;

@end
