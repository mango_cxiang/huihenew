//
//  HMJCountryGroupModel.m
//  huihe
//
//  Created by Six on 16/3/29.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJCountryGroupModel.h"
#import "HMJCountryModel.h"

@implementation HMJCountryGroupModel

#pragma mark - 懒加载
- (NSMutableArray *)countryArray
{
    if (!_countryArray)
    {
        _countryArray = [NSMutableArray array];
    }
    return _countryArray;
}

#pragma mark - 添加本地固定数据
+ (NSMutableArray *)addDataToDataSourceArray;
{
    NSMutableArray * dataSourceArray = [NSMutableArray array];

    NSString * plistPath = [[NSBundle mainBundle] pathForResource:@"countryList" ofType:@"plist"];
    NSDictionary * dict = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    NSArray * keyArray = [dict.allKeys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return (NSComparisonResult)[obj1 compare:obj2 options:NSNumericSearch];
    }];
    for (NSString * key in keyArray)
    {
        HMJCountryGroupModel * countryGroup = [[HMJCountryGroupModel alloc] init];
        countryGroup.nameStr = key;
        countryGroup.countryArray = [HMJCountryModel mj_objectArrayWithKeyValuesArray:dict[key]];
        [dataSourceArray addObject:countryGroup];
    }
    return dataSourceArray;
}

+ (NSString *)searchDataFromDataSourceWithPhoneCode:(NSString *)phoneCode
{
    NSArray * dataArray = [HMJCountryGroupModel addDataToDataSourceArray];
    for (HMJCountryGroupModel * groupModel in dataArray) {
        for (HMJCountryModel * countryModel in groupModel.countryArray) {
            if ([countryModel.mobile_prefix isEqualToString:phoneCode]) {
                return countryModel.country;
                break;
            }
        }
    }
    return  @"";
}

@end
