//
//  HMJCountryGroupModel.h
//  huihe
//
//  Created by Six on 16/3/29.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  国家列表组_模型
 */
@interface HMJCountryGroupModel : NSObject

// 组名
@property (nonatomic , copy) NSString * nameStr;

// 存放 单个国家模型 的数组
@property (nonatomic , strong) NSMutableArray * countryArray;

#pragma mark - 添加本地固定数据
+ (NSMutableArray *)addDataToDataSourceArray;
#pragma mark - 根据搜索区号回去城市名称
+ (NSString *)searchDataFromDataSourceWithPhoneCode:(NSString *)phoneCode;


@end
