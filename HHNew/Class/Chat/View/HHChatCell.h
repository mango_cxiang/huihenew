//
//  HHChatCell.h
//  HHNew
//
//  Created by 张 on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HHChatFrameModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^ContentClickBlock)(void);
typedef void(^HeadPortraitClickBlock)(void);
@interface HHChatCell : UITableViewCell

@property(nonatomic,strong)HHChatFrameModel * frameModel;

@property(nonatomic,copy)ContentClickBlock contentClickBlock;

@property(nonatomic,copy)HeadPortraitClickBlock headPortraitClickBlock;

@property(nonatomic,assign)BOOL isEdit;

@end

NS_ASSUME_NONNULL_END
