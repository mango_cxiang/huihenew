//
//  HomeMessageCell.h
//  HHNew
//
//  Created by 张 on 2020/4/21.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HHMessageListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeMessageCell : UITableViewCell

@property(nonatomic,strong)HHMessageListModel * messageListModel;

@end

NS_ASSUME_NONNULL_END
