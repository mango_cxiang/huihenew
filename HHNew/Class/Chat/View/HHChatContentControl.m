//
//  HHChatContentControl.m
//  HHNew
//
//  Created by 张 on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHChatContentControl.h"
#import <YYImage.h>
#import <YYLabel.h>
#import "HHChatFrameModel.h"
#import "HMJAudioPlayer.h"
#import "MoviePlayerViewController.h"
#import "NIMKitLocationPoint.h"
#import "NIMLocationViewController.h"

@interface HHChatContentControl ()<HMJAudioPlayerDelegate,XLPhotoBrowserDelegate>

//气泡图片
@property(nonatomic,strong)UIImageView * bubbleImageView;
//文字
@property(nonatomic,strong)UILabel * textLabel;
//照片
@property(nonatomic,strong)UIImageView * photoImageView;
//gif图片
@property(nonatomic,strong)YYAnimatedImageView *gifImageView;
//视频播放照片
@property(nonatomic,strong)UIImageView * videoPlayImageView;
//视频播放的时长
@property(nonatomic,strong)UILabel* videoTimeLabel;
//语音
@property(nonatomic,strong)UIView * voiceBgView;
//语音时长
@property(nonatomic,strong)UILabel* voiceTimeLabel;
//语音播放动画
@property(nonatomic,strong)UIImageView * voiceImageView;
//红包视图背景
@property(nonatomic,strong)UIImageView * redPacketBgImageView;
//小红包
@property(nonatomic,strong)UIImageView * smallRedPacketImageView;
//红包标题
@property(nonatomic,strong)UILabel * redPacketTitleLabel;
//红包状态
@property(nonatomic,strong)UILabel * redPacketStatusLabel;
//红包类型
@property(nonatomic,strong)UILabel * redPacketTypeLabel;
//名片视图背景
@property(nonatomic,strong)UIImageView * nameCardBgImageView;
//名片头像
@property(nonatomic,strong)UIImageView * cardHeadImageView;
//名片名字
@property(nonatomic,strong)UILabel * cardNameLabel;
//名片会合号
@property(nonatomic,strong)UILabel * cardHHNumLabel;
//位置视图背景
@property(nonatomic,strong)UIImageView * positionBgImageView;
//位置名称
@property(nonatomic,strong)UILabel * positionNameLabel;
//具体地址
@property(nonatomic,strong)UILabel * specificPositionLabel;
//位置图片
@property(nonatomic,strong)UIImageView * positionImageView;
//文件
@property(nonatomic,strong)UIView * fileBGView;
//分享
@property(nonatomic,strong)UIView * shareView;
//群消息通知
@property(nonatomic,strong)YYLabel * groupTipLabel;
//是否正在播放语音
@property(nonatomic,assign)BOOL isPlayVoice;
//语音播放器
@property(nonatomic,strong)HMJAudioPlayer * voicePlayer;

@end

@implementation HHChatContentControl

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self addTarget:self action:@selector(contentControlOnClick) forControlEvents:UIControlEventTouchUpInside];
        //气泡
        _bubbleImageView = Init(UIImageView);
        [self addSubview:_bubbleImageView];
        [_bubbleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.offset(0);
        }];
        //文字
        _textLabel = Init(UILabel);
        _textLabel.numberOfLines = 0;
        _textLabel.font = SystemFont(16);
        _textLabel.textColor = ColorManage.text_color;
        [_bubbleImageView addSubview:_textLabel];
        //照片
        _photoImageView = Init(UIImageView);
        [_photoImageView viewWithRadis:5];
        _photoImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_photoImageView];
        [_photoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.offset(0);
        }];
        //GIF图片
        _gifImageView = Init(YYAnimatedImageView);
        [_gifImageView viewWithRadis:5];
        _gifImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_gifImageView];
        [_gifImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.offset(0);
        }];
        //视频播放图片
        _videoPlayImageView = Init(UIImageView);
        _videoPlayImageView.image = [UIImage imageNamed:@"shipingbofang"];
        [_photoImageView addSubview:_videoPlayImageView];
        [_videoPlayImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.offset(0);
        }];
        //视频时长
        _videoTimeLabel = Init(UILabel);
        _videoTimeLabel.font = SystemFont(12);
        _videoTimeLabel.textColor = UIColor.whiteColor;
        [_photoImageView addSubview:_videoTimeLabel];
        [_videoTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.right.offset(-5);
        }];
        //语音
        _voiceBgView = Init(UIView);
        [self addSubview:_voiceBgView];
        //语音时长
        _voiceTimeLabel = Init(UILabel);
        _voiceTimeLabel.font = SystemFont(14);
        _voiceTimeLabel.textColor = ColorManage.text_color;
        [_voiceBgView addSubview:_voiceTimeLabel];
        //语音播放动画
        _voiceImageView = Init(UIImageView);
        _voiceImageView.contentMode = UIViewContentModeScaleAspectFit;
        [_voiceBgView addSubview:_voiceImageView];
        //名片
        _nameCardBgImageView = Init(UIImageView);
        [self addSubview:_nameCardBgImageView];
        [_nameCardBgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.offset(0);
        }];
        //名片头像
        _cardHeadImageView = Init(UIImageView);
        [_cardHeadImageView viewWithRadis:5];
        [_nameCardBgImageView addSubview:_cardHeadImageView];
        [_cardHeadImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(10);
            make.left.offset(15);
            make.width.height.offset(50);
        }];
        //名片名字
        _cardNameLabel = Init(UILabel);
        _cardNameLabel.font = SystemFont(17);
        _cardNameLabel.textColor = ColorManage.text_color;
        [_cardNameLabel setAdjustsFontSizeToFitWidth:YES];
        [_nameCardBgImageView addSubview:_cardNameLabel];
        [_cardNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.cardHeadImageView.mas_right).offset(10);
            make.top.offset(10);
            make.width.offset(150);
            make.height.offset(25);
        }];
        //名片会合号
        _cardHHNumLabel = Init(UILabel);
        _cardHHNumLabel.font = SystemFont(14);
        _cardHHNumLabel.textColor = RGB_Color(153, 153, 153);
        [_cardHHNumLabel setAdjustsFontSizeToFitWidth:YES];
        [_nameCardBgImageView addSubview:_cardHHNumLabel];
        [_cardHHNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.cardNameLabel);
            make.top.equalTo(self.cardNameLabel.mas_bottom);
            make.width.offset(150);
            make.height.offset(25);
        }];
        //个人名片
        UILabel * cardLabel = Init(UILabel);
        cardLabel.font = SystemFont(12);
        cardLabel.textColor = RGB_Color(178, 178, 178);
        cardLabel.text = @"个人名片";
        [_nameCardBgImageView addSubview:cardLabel];
        [cardLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(17);
            make.bottom.offset(-5);
        }];
        //位置
        _positionBgImageView = Init(UIImageView);
        [self addSubview:_positionBgImageView];
        [_positionBgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.offset(0);
        }];
        //位置图片
        _positionImageView = Init(UIImageView);
        _positionImageView.contentMode = UIViewContentModeScaleAspectFill;
        _positionImageView.clipsToBounds = YES;
        [_positionBgImageView addSubview:_positionImageView];
        [_positionImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.offset(0);
            make.width.offset(235);
            make.height.offset(90);
        }];
        //位置名称
        _positionNameLabel = Init(UILabel);
        _positionNameLabel.font = SystemFont(16);
        _positionNameLabel.textColor = ColorManage.text_color;
        [_positionBgImageView addSubview:_positionNameLabel];
        [_positionNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15);
            make.top.offset(10);
            make.width.offset(210);
        }];
        //具体位置
        _specificPositionLabel = Init(UILabel);
        _specificPositionLabel.font = SystemFont(12);
        _specificPositionLabel.textColor = RGB_Color(178, 178, 178);
        [_positionBgImageView addSubview:_specificPositionLabel];
        [_specificPositionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.width.equalTo(self.positionNameLabel);
            make.top.equalTo(self.positionNameLabel.mas_bottom).offset(2);
            make.width.offset(210);
        }];
        //红包
        _redPacketBgImageView = Init(UIImageView);
        [self addSubview:_redPacketBgImageView];
        [_redPacketBgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.offset(0);
        }];
        //小红包
        _smallRedPacketImageView = Init(UIImageView);
        [_redPacketBgImageView addSubview:_smallRedPacketImageView];
        [_smallRedPacketImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.offset(15);
        }];
        //红包标题
        _redPacketTitleLabel = Init(UILabel);
        _redPacketTitleLabel.font = SystemFont(16);
        _redPacketTitleLabel.textColor = UIColor.whiteColor;
        [_redPacketBgImageView addSubview:_redPacketTitleLabel];
        [_redPacketTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.smallRedPacketImageView.mas_right).offset(10);
            make.top.offset(15);
            make.width.offset(160);
        }];
        //红包状态
        _redPacketStatusLabel = Init(UILabel);
        _redPacketStatusLabel.font = SystemFont(12);
        _redPacketStatusLabel.textColor = UIColor.whiteColor;
        [_redPacketBgImageView addSubview:_redPacketStatusLabel];
        [_redPacketStatusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.width.equalTo(self.redPacketTitleLabel);
            make.top.equalTo(self.redPacketTitleLabel.mas_bottom).offset(5);
        }];
        //会合红包
        _redPacketTypeLabel = Init(UILabel);
        _redPacketTypeLabel.font = SystemFont(12);
        _redPacketTypeLabel.textColor = RGB_Color(178, 178, 178);
        [_redPacketBgImageView addSubview:_redPacketTypeLabel];
        [_redPacketTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.smallRedPacketImageView);
            make.bottom.offset(-2);
        }];
        //群消息通知
        _groupTipLabel = Init(YYLabel);
        _groupTipLabel.backgroundColor = ColorManage.time_color;
        [_groupTipLabel viewWithRadis:5];
//        _groupTipLabel.backgroundColor = UIColor.redColor;
        _groupTipLabel.numberOfLines = 0;
        [self addSubview:_groupTipLabel];
    }
    return self;
}
#pragma mark --- setFrameModel
-(void)setFrameModel:(HHChatFrameModel *)frameModel
{
    _frameModel = frameModel;
    [self hideAllSubView];
    self.frame = frameModel.controlFrame;
    //群消息通知
    if (frameModel.chatModel.chatFrameType==ChatFrameTypeSystemMessages) {
        _groupTipLabel.hidden = NO;
        _groupTipLabel.frame = CGRectMake(0, 0, self.width, self.height);
        _groupTipLabel.attributedText = frameModel.chatModel.jointGroupTip;
    }
    //文本、语音分类处理
    if (frameModel.chatModel.chatFrameType==ChatFrameTypeText) {
        BOOL isSelf = [[NSString acquireUserId] isEqualToString:frameModel.chatModel.fromId];
        [self showBubbleImageViewIsSelf:isSelf];
        if (frameModel.chatModel.messageType.integerValue==TOMessageTypeText) {
            //文本
            [self showTextLabel];
        }
        if (frameModel.chatModel.messageType.integerValue==TOMessageTypeVoice) {
            //语音
            //注册停止播放语音通知
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopPlayVoiceNotification:) name:StopPlayVoiceNotification object:nil];
            [self showVoiceViewIsSelf:isSelf];
        }
    }
    //名片
    if (frameModel.chatModel.chatFrameType==ChatFrameTypeBusinessCard) {
        [self showNameCardView];
    }
    //位置
    if (frameModel.chatModel.chatFrameType==ChatFrameTypeLocation) {
        [self showPositionView];
    }
    //红包
    if (frameModel.chatModel.chatFrameType==ChatFrameTypeRedPacket) {
        [self showRedPacketView];
    }
    //图片、视频
    if (frameModel.chatModel.chatFrameType==ChatFrameTypePicture) {
        switch ([frameModel.chatModel.messageType integerValue]) {
            case TOMessageTypePhoto:
            {
                [self showPhotoView];
            }
                break;
            case TOMessageTypeVideo:
            {
                [self showVideoView];
            }
            default:
                break;
        }
    }
    //表情云或@信息
    if (frameModel.chatModel.chatFrameType==ChatFrameTypeBQMM) {
        NSDictionary * dataDic = [frameModel.chatModel.content mj_JSONObject];
        if ([dataDic[@"msgString"] containsString:@"bq_tsj_csc:"]) {
            //位置共享
            
        }else{
            if (!IsBlankWithStr(dataDic[@"msgType"])) {
                //表情云
                if ([[dataDic[@"msgString"] pathExtension] isEqualToString:@"gif"]) {
                    self.gifImageView.hidden = NO;
                    [self.gifImageView sd_setImageWithURL:[dataDic[@"msgString"] mj_url]];
                }else {
                    self.photoImageView.hidden = NO;
                    [self.photoImageView sd_setImageWithURL:[dataDic[@"msgString"] mj_url]];
                }
            }else{
                //@文字
                BOOL isSelf = [[NSString acquireUserId] isEqualToString:frameModel.chatModel.fromId];
                [self showBubbleImageViewIsSelf:isSelf];
                [self showTextLabel];
            }
        }
        
    }
}
#pragma mark --- 隐藏全部视图
-(void)hideAllSubView
{
    _bubbleImageView.hidden      = YES;
    _textLabel.hidden            = YES;
    _photoImageView.hidden       = YES;
    _gifImageView.hidden         = YES;
    _videoPlayImageView.hidden   = YES;
    _videoTimeLabel.hidden       = YES;
    _voiceBgView.hidden          = YES;
    _nameCardBgImageView.hidden  = YES;
    _positionBgImageView.hidden  = YES;
    _redPacketBgImageView.hidden = YES;
    _groupTipLabel.hidden        = YES;
}
#pragma mark --- 显示气泡视图
-(void)showBubbleImageViewIsSelf:(BOOL)isSelf
{
    self.bubbleImageView.hidden = NO;
    UIImage * bubbleImage = [UIImage resizableImageWithName:isSelf?@"message_me":@"message_other"];
    self.bubbleImageView.image = bubbleImage;
}
#pragma mark --- 显示文本视图
-(void)showTextLabel
{
    self.textLabel.hidden = NO;
    self.textLabel.frame = _frameModel.textFrame;
    self.textLabel.attributedText = _frameModel.chatModel.attributedContent;
}
#pragma mark --- 显示语音视图
-(void)showVoiceViewIsSelf:(BOOL)isSelf
{
    self.voiceBgView.hidden = NO;
    self.voiceTimeLabel.frame = _frameModel.secondsFrame;
    self.voiceTimeLabel.text = [NSString stringWithFormat:@"%ld″",_frameModel.chatModel.seconds];
    self.voiceTimeLabel.textAlignment = [_frameModel.chatModel.fromId isEqualToString:[NSString acquireUserId]]?NSTextAlignmentRight:NSTextAlignmentLeft;
    self.voiceImageView.frame = _frameModel.voiceImageViewFrame;
    self.voiceImageView.image = ImageInit(isSelf?@"voice_me":@"voice_other");
    self.voiceImageView.animationDuration = 1;
    self.voiceImageView.animationImages = isSelf?@[ImageInit(@"voice_animation_me1"),ImageInit(@"voice_animation_me2"),ImageInit(@"voice_animation_me3")]:@[ImageInit(@"voice_animation_other1"),ImageInit(@"voice_animation_other2"),ImageInit(@"voice_animation_other3")];
}
#pragma mark --- 显示名片视图
-(void)showNameCardView
{
    BOOL isSelf = [[NSString acquireUserId] isEqualToString:_frameModel.chatModel.fromId];
    self.nameCardBgImageView.hidden = NO;
    UIImage * cardImage = ImageInit(isSelf?@"card_mine":@"card_other");
    self.nameCardBgImageView.image = cardImage;
    NSDictionary * jsonDic = [_frameModel.chatModel.content mj_JSONObject];
    if (jsonDic.allKeys.count == 0) {
        return;
    }
    self.cardNameLabel.text = jsonDic[@"nickName"];
    [self.cardHeadImageView sd_setImageWithURL:[jsonDic[@"headUrl"] mj_url] placeholderImage:HeadPlaceholderImage];
    if (!IsBlankWithStr(jsonDic[@"IM"])) {
        self.cardHHNumLabel.text = [NSString stringWithFormat:@"会合号：%@",jsonDic[@"IM"]];
    }else{
        self.cardHHNumLabel.text = @"";
    }
    [self.cardHeadImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.offset(isSelf?10:15);
    }];
}
#pragma mark --- 显示名片视图
-(void)showPositionView
{
    self.positionBgImageView.hidden = NO;
    BOOL isSelf = [[NSString acquireUserId] isEqualToString:_frameModel.chatModel.fromId];
    UIImage * cardImage = ImageInit(isSelf?@"card_mine":@"card_other");
    self.positionBgImageView.image = cardImage;
    NSDictionary * jsonDic = [_frameModel.chatModel.content mj_JSONObject];
    if (jsonDic.allKeys.count == 0) {
        return;
    }
    self.positionNameLabel.text = jsonDic[@"addr"];
    self.specificPositionLabel.text = jsonDic[@"addr"];
    [self.positionImageView sd_setImageWithURL:[jsonDic[@"url"] mj_url]];
    [self.positionImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.offset(isSelf?0:5);
    }];
}
#pragma mark --- 显示红包视图
-(void)showRedPacketView
{
    NSDictionary * jsonDic = [_frameModel.chatModel.content mj_JSONObject];
    if (jsonDic.allKeys.count == 0) {
        return;
    }
    self.redPacketBgImageView.hidden = NO;
    BOOL isSelf = [[NSString acquireUserId] isEqualToString:_frameModel.chatModel.fromId];
    if (isSelf) {
        if ([_frameModel.chatModel.isUnpack isEqualToString:@"1"]) {
            self.redPacketBgImageView.image = ImageInit(@"redpacket_mine_gray");
        } else {
            self.redPacketBgImageView.image = ImageInit(@"redpacket_mine");
        }
    }else{
        if ([_frameModel.chatModel.isUnpack isEqualToString:@"1"]) {
            self.redPacketBgImageView.image = ImageInit(@"redpacket_other_gray");
        } else {
            self.redPacketBgImageView.image = ImageInit(@"redpacket_other");
        }
    }
    self.smallRedPacketImageView.image = ImageInit([_frameModel.chatModel.isUnpack isEqualToString:@"1"]?@"redpacket_small_gray":@"redpacket_small");
    [self.smallRedPacketImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.offset(isSelf?10:15);
    }];
    NSString * moneyStr = jsonDic[@"msg"];
    if ([[moneyStr substringToIndex:1] isEqualToString:@"¥"]) {
        NSMutableString* str1 = [[NSMutableString alloc]initWithString:moneyStr];
        [str1 insertString:@" " atIndex:1];
        self.redPacketTitleLabel.text = str1;
    }else {
        self.redPacketTitleLabel.text = moneyStr;
    }
    NSString * limitStr = jsonDic[@"limit"];
    self.redPacketTypeLabel.text = !IsBlankWithStr(limitStr) ? @"会合红包（指定）" : @"会合红包";
    self.redPacketStatusLabel.text = [_frameModel.chatModel.isUnpack isEqualToString:@"1"]?@"红包已领取":@"领取红包";
}
#pragma mark --- 显示图片视图
-(void)showPhotoView
{
    if (_frameModel.chatModel.image) {
        self.photoImageView.hidden = NO;
        self.photoImageView.image = _frameModel.chatModel.image;
    }else{
        NSString *pathAbsolut;
        if ([_frameModel.chatModel.content hasPrefix:@"https://"]||[_frameModel.chatModel.content hasPrefix:@"http://"]) {
            pathAbsolut = _frameModel.chatModel.content;
        } else {
            NSDictionary *dic = [_frameModel.chatModel.content mj_JSONObject];
            pathAbsolut = dic[@"ImageUrl"];
        }
        NSArray *array = [pathAbsolut componentsSeparatedByString:@"?"];
        NSString *path = array[0];
        // 判断是否为gif
        NSString *extensionName = path.pathExtension;
        if ([extensionName.lowercaseString isEqualToString:@"gif"]) {
            self.gifImageView.hidden = NO;
            [self.gifImageView sd_setImageWithURL:[_frameModel.chatModel.content mj_url]];
        } else {
            self.photoImageView.hidden = NO;
            NSString *urlimage = [pathAbsolut stringByAppendingString:@"?x-oss-process=style/chat_small"];
            [self.photoImageView sd_setImageWithURL:[urlimage mj_url]];
        }
    }
}
#pragma mark --- 显示视频视图
-(void)showVideoView
{
    self.photoImageView.hidden = NO;
    self.videoPlayImageView.hidden = NO;
    NSDictionary * jsonContent = [_frameModel.chatModel.content mj_JSONObject];
    NSString * vedioUrl = jsonContent[@"vedioUrl"];
    if (!IsBlankWithStr(vedioUrl)) {
//        NSArray *contentPathArr = [vedioUrl componentsSeparatedByString:@"?"];
//        NSString *separatedPath = contentPathArr[0];
//        NSString* fullPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"ChatVideo/%@",[[NSString stringWithFormat:@"%@",separatedPath] componentsSeparatedByString:@"/"].lastObject]];
//        if(![[NSFileManager defaultManager] fileExistsAtPath:fullPath]) {
//            //下载视频到本地
//            //                            [[HMJVideoNSURLCache sharedInstance] download:[NSString stringWithFormat:@"%@",[jsonContent valueForKey:@"vedioUrl"]] videoCacheType:VideoCacheTypeChat];
//        }
    }
    if(!IsBlankWithStr(jsonContent[@"vedioSize"])){
        self.videoTimeLabel.hidden = NO;
        self.videoTimeLabel.text = [NSString returnVideoTime:jsonContent[@"vedioSize"]];
    }
    if (!IsBlankWithStr(jsonContent[@"getVedioBitmapUrl"])) {
        [self.photoImageView sd_setImageWithURL:[jsonContent[@"getVedioBitmapUrl"] mj_url]];
    }
}
#pragma mark --- 点击事件触发
-(void)contentControlOnClick
{
    AkrLog(@"contentControl----%@",self);
    if (_frameModel.chatModel.messageType.integerValue == TOMessageTypeVoice) {
        //语音播放
        [self playVoice];
    }
    if (_frameModel.chatModel.messageType.integerValue == TOMessageTypePhoto) {
        //点击图片
        [self clickPhotoAction];
    }
    if (_frameModel.chatModel.messageType.integerValue == TOMessageTypeVideo) {
        //点击视频
        [self clickVideoAction];
    }
    if (_frameModel.chatModel.messageType.integerValue == TOMessageTypeRedPacket) {
        //点击红包
        [self clickRedPacketAction];
    }
    if (_frameModel.chatModel.messageType.integerValue == TOMessageTypePosition) {
        //点击位置
        [self clickPositionAction];
    }
    if (self.contentClickBlock) {
        self.contentClickBlock();
    }
}
#pragma mark --- 播放语音
-(void)playVoice
{
    //通知已经在播放的停止播放
    [[NSNotificationCenter defaultCenter] postNotificationName:StopPlayVoiceNotification object:nil];
    if (!self.isPlayVoice) {
        self.isPlayVoice = YES;
        //判断语音数据类型
//        _frameModel.chatModel.content
        self.voicePlayer = [HMJAudioPlayer sharedInstance];
        self.voicePlayer.delegate = self;
        if (_frameModel.chatModel.voiceData) {
            [self.voicePlayer playWithVoiceData:_frameModel.chatModel.voiceData];
        }else if (_frameModel.chatModel.audioLocationPath){
            NSString *newPath = @"";
            if (![_frameModel.chatModel.audioLocationPath hasPrefix:@"http"]){
                newPath = [DocumentsPath stringByAppendingPathComponent:[_frameModel.chatModel.audioLocationPath lastPathComponent]];
            }else{
                newPath = [DocumentsPath stringByAppendingPathComponent:[_frameModel.chatModel.audioLocationPath lastPathComponent]];
            }
            NSData *data = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:newPath]];
            [self.voicePlayer playWithVoiceData:data];
        }else{
            NSURL* url = [NSURL URLWithString:_frameModel.chatModel.audioLocationPath];
            NSString *exestr = [[url path] pathExtension];
            if ([exestr isEqualToString:@"mp3"]) {
                WeakSelf(self);
                [[NSURLSession sharedSession] dataTaskWithRequest:[NSURLRequest requestWithURL:url] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                    [weakself.voicePlayer playWithVoiceData:data];
                }];
            }else{
                AkrLog(@"语音未知链接%@",url);
            }
        }
    }else{
        [self audioPlayerDidFinishPlayVoice];
    }
}
// HMJAudioPlayerDelegate
- (void)audioPlayerWillStartPlayVoice{
    if (self.isPlayVoice) {
        [self audioPlayerDidFinishPlayVoice];
    }else{
        self.voiceImageView.hidden = YES;
    }
}
- (void)audioPlayerDidStartPlayVoice{
    self.voiceImageView.hidden = NO;
    [self.voiceImageView startAnimating];
}
- (void)audioPlayerDidFinishPlayVoice{
    self.isPlayVoice = NO;
    [self.voiceImageView stopAnimating];
    [[HMJAudioPlayer sharedInstance] stopPlayVoice];
}
//处理监听触发事件
- (void)stopPlayVoiceNotification:(NSNotification *)notification{
    [self audioPlayerDidFinishPlayVoice];
}
#pragma mark --- 点击图片触发
-(void)clickPhotoAction
{
    if (!self.photoImageView.hidden) {
        //正常图片,从数据库中找到已发送的图片数组
//        NSArray * modelArray =
//        [[HMJFMDBMessageDataModel sharedInstance]queryImageUrlDataFromeDataBaseWithSessionId:message];
//        NSMutableArray * imageUrlArray = [NSMutableArray array];
//        for (HMJMessageModel *item in modelArray) {
//            NSDictionary *dic = [item.content objectFromJSONString];
//            if ([dic[@"ImageUrl"] length] > 0) {
//                [imageUrlArray addObject:dic[@"ImageUrl"]];
//            }else {
//                [imageUrlArray addObject:item.content];
//            }
//        }
//        XLPhotoBrowser *browser = [XLPhotoBrowser showPhotoBrowserWithImages:imageUrlArray currentImageIndex:[self showImageIndex:message messages:modelArray] pageControlHidden:YES];
        //暂时先展示本身
        XLPhotoBrowser *browser = [XLPhotoBrowser showPhotoBrowserWithImages:@[self.photoImageView.image] currentImageIndex:0 pageControlHidden:YES];
        browser.delegate = self;
    }else if(!self.gifImageView.hidden){
        //GIF图片，只展示自己
        [XLPhotoBrowser showPhotoBrowserWithImages:@[self.gifImageView.image] currentImageIndex:0 pageControlHidden:YES];
    }
}
//找到当前图片在数组中的索引
//- (NSInteger)showImageIndex:(HMJMessageModel *)message messages:(NSArray *)messages
//{
//    for (int i = 0; i< messages.count; i++) {
//        HMJMessageModel * msg = messages[i];
//        if ([msg.msgId isEqualToString:message.msgId]) {
//            return i;
//        }
//    }
//    return 0;
//}
//XLPhotoBrowserDelegate
-(void)photoBrowser:(XLPhotoBrowser *)browser clickActionSheetIndex:(NSInteger)actionSheetindex currentImageIndex:(NSInteger)currentImageIndex{
    switch (actionSheetindex) {
        case 0:
            [browser saveCurrentShowImage];
            break;
        case 1:
        {
//            NSString *codeStr = [browser scanQRCodeResult];
//            [HMJChatToolManager scanQRCodeWithController:self CodeStr:codeStr completed:^(int code, NSString *content) {
//                if(code==1000){
//                    [browser dismiss];
//                    NSDictionary *urlDic = [content getParam];
//                    _redBagDic = [[NSMutableDictionary alloc] init];
//                    [_redBagDic setValue:[NSString acquireUserId] forKey:@"userId"];
//                    [_redBagDic setValue:urlDic[@"redid"] forKey:@"senderId"];
//                    [_redBagDic setValue:urlDic[@"userid"] forKey:@"thirdUserId"];
//                    HMKAnnoPopView *popView = [HMKAnnoPopView viewFromxib];
//                    _popView = popView;
//                    [popView.headImageView sd_setImageWithURL:[NSURL URLWithString:urlDic[@"headpic"]] placeholderImage:PlaceholderImage];
//                    popView.nameLabel.text = urlDic[@"nickname"];
//                    popView.slogenLabel.text = @"恭喜发财，大吉大利";
//                    popView.delegate = self;
//                }else{
//                    if (code == 1) {
//                        [browser dismiss];
//                    }else{
//                        [[HMJShowLoding sharedInstance]showText:content];
//                    }
//                }
//            }];
        }
            break;
        default:
            break;
    }
}
#pragma mark --- 点击视频触发
-(void)clickVideoAction
{
    NSDictionary * contentDic = [_frameModel.chatModel.content mj_JSONObject];
    MoviePlayerViewController *vc = [[MoviePlayerViewController alloc]init];
    NSString* fullPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"ChatVideo/%@",[[NSString stringWithFormat:@"%@",contentDic[@"vedioUrl"]] componentsSeparatedByString:@"/"].lastObject]];
    if (fullPath.length > 0 && [[NSFileManager defaultManager] fileExistsAtPath:fullPath]) {
        vc.url = fullPath;
        vc.isLocationVideo = YES;
    }else{
        vc.url = [NSString stringWithFormat:@"%@",contentDic[@"vedioUrl"]];
    }
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:vc animated:YES completion:nil];
}
#pragma mark --- 点击红包触发
-(void)clickRedPacketAction
{
//    NSDictionary * repacketDic = [_frameModel.chatModel.content mj_JSONObject];
//    [HMJMessageManager getRedPacketHistoryWithredPacketId:[NSString stringWithFormat:@"%@",repacketDic[@"redPacketId"]] with:repacketDic[@"tableName"] completed:^(int code, NSDictionary *objc) {
//        if(code == 1) {
//            HMJRedPacketModel * redPacketModel = [[HMJRedPacketModel alloc]initWithDataDic:objc];
//            //如果是已领取或者未领取或者超时
//            if (redPacketModel.status.integerValue == 2 || redPacketModel.status.integerValue == 3 || ([[NSString acquireUserId] isEqualToString:redPacketModel.userId])) {
//                if( redPacketModel.status.integerValue == 3) {
//                    [[HMJShowLoding sharedInstance] showText:Localized(@"红包已过期")];
//                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                        HMJRedPacketDetailViewController * redPacketDetailViewController = [[HMJRedPacketDetailViewController alloc]init];
//                        redPacketDetailViewController.type = @"100";
//                        redPacketDetailViewController.preaRedPacketModel = redPacketModel;
//                        [self.navigationController pushViewController:redPacketDetailViewController animated:YES];
//                    });
//                } else {
//                    HMJRedPacketDetailViewController * redPacketDetailViewController = [[HMJRedPacketDetailViewController alloc]init];
//                    redPacketDetailViewController.preaRedPacketModel = redPacketModel;
//                    redPacketDetailViewController.type = @"100";
//                    [self.navigationController pushViewController:redPacketDetailViewController animated:YES];
//                }
//                // 改变颜色
//                [[HMJFMDBMessageDataModel sharedInstance] updataNewRedPacketMessageStatusToDataBase:message.msgId];
//                [self refreshMsgData];
//            } else {
//                _showRedPacketView = [[NSBundle mainBundle] loadNibNamed:@"HMJShowRedPacketView" owner:nil options:nil].firstObject;
//                _showRedPacketView.frame = CGRectMake(0, 0, Screen_Width, Screen_Height);
//                _showRedPacketView.delegate = self;
//                [_showRedPacketView viewCreatWithModel:message redPacket:redPacketModel];
//                [self.view.window addSubview:_showRedPacketView];
//            }
//        } else {
//            [[HMJShowLoding sharedInstance]showText:Localized(@"获取信息失败")];
//        }
//    }];
}
#pragma mark --- 点击位置触发
-(void)clickPositionAction
{
    NSMutableDictionary *dic = [_frameModel.chatModel.content mj_JSONObject];
    NSString *lat = dic[@"lat"];
    NSString *lng = dic[@"lut"];
    CLLocationCoordinate2D cLLocation = CLLocationCoordinate2DMake([lat floatValue], [lng floatValue]);
    NIMKitLocationPoint *locationPoint = [[NIMKitLocationPoint alloc] initWithCoordinate:cLLocation andTitle:dic[@"address"]];
    NIMLocationViewController *vc = [[NIMLocationViewController alloc] initWithLocationPoint:locationPoint];
    vc.addressStr =[dic objectForKey:@"address"];
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:vc animated:YES completion:nil];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
