//
//  HHChatCell.m
//  HHNew
//
//  Created by 张 on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHChatCell.h"
#import "HHChatContentControl.h"

@interface HHChatCell ()
//时间
@property(nonatomic,strong)UILabel * timeLabel;
//头像
@property(nonatomic,strong)UIImageView * headPortraitIV;
//名字
@property(nonatomic,strong)UILabel * nameLabel;
//内容视图
@property(nonatomic,strong)HHChatContentControl * contentControl;
//语音是否已读
@property(nonatomic,strong)UIView * voiceStatusView;
//消息未读或已读
//@property(nonatomic,strong)UILabel * readLabel;
//数据发送成功或者失败
@property(nonatomic,strong)UIImageView * statusImageView;
//消息发送转圈
@property(nonatomic,strong)UIActivityIndicatorView * activityIndicatorView;
//长按菜单
@property(nonatomic,strong)UIMenuController * menuController;

@end

@implementation HHChatCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = UIColor.clearColor;
        //时间
        _timeLabel = Init(UILabel);
        _timeLabel.font = SystemFont(14);
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        _timeLabel.textColor = RGB_Color(178, 178, 178);
        _timeLabel.backgroundColor = ColorManage.time_color;
        [_timeLabel viewWithRadis:3];
        [self.contentView addSubview:_timeLabel];
        //头像
        _headPortraitIV = Init(UIImageView);
        [_headPortraitIV viewWithRadis:5];
        _headPortraitIV.userInteractionEnabled = YES;
        _headPortraitIV.contentMode = UIViewContentModeScaleAspectFill;
        [_headPortraitIV setContentScaleFactor:[[UIScreen mainScreen] scale]];
        [_headPortraitIV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headPortraitIVOnTap:)]];
        [self.contentView addSubview:_headPortraitIV];
        //名字
        _nameLabel = Init(UILabel);
        _nameLabel.font = SystemFont(12);
        _nameLabel.textColor = RGB_Color(115, 115, 115);
        [self.contentView addSubview:_nameLabel];
        //内容视图
        _contentControl = Init(HHChatContentControl);
        WEAKSELF
        _contentControl.contentClickBlock = ^{
            if (weakSelf.contentClickBlock) {
                weakSelf.contentClickBlock();
            }
        };
        //为气泡按钮添加长按手势 来添加消息删除撤销等功能
        [_contentControl addGestureRecognizer:[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(controlLongPressGestureRecognizer:)]];
        [self.contentView addSubview:_contentControl];
        //发送状态,仅自己发送会有,目前仅有失败状态
        _statusImageView = Init(UIImageView);
        _statusImageView.hidden = YES;
        _statusImageView.image = ImageInit(@"send_failure");
        [self.contentView addSubview:_statusImageView];
        [_statusImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentControl.mas_left).offset(-10);
            make.centerY.equalTo(self.contentControl);
        }];
        //消息发送转圈,仅自己发送会有,（他人发来的应该没有吧？）
        _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        _activityIndicatorView.color = UIColorFromRGBA(0x000000, 0.5);
        [self.contentView addSubview:_activityIndicatorView];
        [_activityIndicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentControl.mas_left).offset(-10);
            make.centerY.equalTo(self.contentControl);
        }];
        //语音是否已读,仅针对接收的语音
        _voiceStatusView = Init(UIView);
        _voiceStatusView.hidden = YES;
        [_voiceStatusView viewWithRadis:4];
        _voiceStatusView.backgroundColor = [UIColor redColor];
        [self.contentView addSubview:_voiceStatusView];
        [_voiceStatusView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentControl.mas_right).offset(5);
            make.top.equalTo(self.contentControl);
            make.width.height.offset(8);
        }];
//        //消息未读或 已读
//        _readLabel = Init(UILabel);
//        _readLabel.font = SystemFont(12);
//        _readLabel.hidden = YES;
//        _readLabel.textAlignment = NSTextAlignmentRight;
//        _readLabel.textColor = UIColorFromRGB(0x878787);
//        [self.contentView addSubview:_readLabel];
    }
    return self;
}
-(void)setFrameModel:(HHChatFrameModel *)frameModel
{
    _frameModel = frameModel;
    //设置时间
    self.timeLabel.hidden = YES;
    if (!frameModel.chatModel.hideTime) {
        self.timeLabel.hidden = NO;
        self.timeLabel.text = frameModel.chatModel.timeString;
        self.timeLabel.frame = frameModel.timeFrame;
    }
    //设置头像、名字
    self.headPortraitIV.hidden = YES;
    self.nameLabel.hidden = YES;
    if (frameModel.chatModel.chatFrameType != ChatFrameTypeSystemMessages) {
        self.headPortraitIV.hidden = NO;
        self.headPortraitIV.frame = frameModel.iconFrame;
        [self.headPortraitIV sd_setImageWithURL:[frameModel.chatModel.imageIconUrl mj_url] placeholderImage:HeadPlaceholderImage];
        if (frameModel.isShowName) {
            self.nameLabel.hidden = NO;
            self.nameLabel.frame = frameModel.nameFrame;
            self.nameLabel.text = frameModel.chatModel.fromName;
        }
    }
    if ([frameModel.chatModel.fromId isEqualToString:[NSString acquireUserId]]) {
        //自己发送的
        if ([SafeString(frameModel.chatModel.status) isEqualToString:@"正在发送"]) {
            [self.activityIndicatorView startAnimating];
        }else{
            [self.activityIndicatorView stopAnimating];
        }
        if ([SafeString(frameModel.chatModel.status) isEqualToString:@"失败"]) {
            _statusImageView.hidden = NO;
        }else{
            _statusImageView.hidden = YES;
        }
    }
    //设置内容
    self.contentControl.frameModel = frameModel;
    _voiceStatusView.hidden = YES;
    if ([frameModel.chatModel.messageType integerValue] == TOMessageTypeVoice) {
        //语音、判断小红点是否显示
        if (![frameModel.chatModel.fromId isEqualToString:[NSString acquireUserId]]) {
            _voiceStatusView.hidden = NO;
        }
    }
    if (_isEdit && ([frameModel.chatModel.fromId isEqualToString:[NSString acquireUserId]]||frameModel.chatModel.chatFrameType == ChatFrameTypeSystemMessages)) {
        self.headPortraitIV.right -= 44;
        self.contentControl.right -= 44;
        self.timeLabel.right -= 44;
    }
}

#pragma mark --- 头像点击手势
-(void)headPortraitIVOnTap:(UITapGestureRecognizer *)tapGestureRecognizer
{
    if (self.headPortraitClickBlock) {
        self.headPortraitClickBlock();
    }
}
#pragma mark --- 长按手势
-(void)controlLongPressGestureRecognizer:(UILongPressGestureRecognizer *)longPressGesture
{
    if(longPressGesture.state == UIGestureRecognizerStateBegan) {//长按开始时触发
        NSLog(@"长按了");
    } else if(longPressGesture.state == UIGestureRecognizerStateEnded) { //长按结束时触发
        
    } else if(longPressGesture.state == UIGestureRecognizerStateChanged){ //按住移动时触发
        
    } else if (longPressGesture.state == UIGestureRecognizerStateCancelled){
        
    }
}


-(UIMenuController *)menuController
{
    if (!_menuController) {
        _menuController = [UIMenuController sharedMenuController];
        _menuController.arrowDirection = UIMenuControllerArrowDown;
        [_menuController setTargetRect:self.contentControl.frame inView:self.contentView];
    }
    return _menuController;
}

@end
