//
//  HomeMessageCell.m
//  HHNew
//
//  Created by 张 on 2020/4/21.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HomeMessageCell.h"

@interface HomeMessageCell ()

@property(nonatomic,strong)UIImageView * headPortraitIV;
@property(nonatomic,strong)UILabel * nameLB;
@property(nonatomic,strong)UIImageView * statusIV;
@property(nonatomic,strong)UILabel * contentLB;
@property(nonatomic,strong)UILabel * timeLB;
@property(nonatomic,strong)UIImageView * closeTipIV;

@end

@implementation HomeMessageCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.contentView.backgroundColor = ColorManage.background_color;
        
        _headPortraitIV = Init(UIImageView);
        _headPortraitIV.layer.cornerRadius = 5;
        _headPortraitIV.layer.masksToBounds = YES;
        [self.contentView addSubview:_headPortraitIV];
        [_headPortraitIV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15);
            make.centerY.offset(0);
            make.width.height.offset(50);
        }];
        _nameLB = Init(UILabel);
        _nameLB.textColor = ColorManage.text_color;
        _nameLB.font = Font(17);
        [self.contentView addSubview:_nameLB];
        [_nameLB mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.headPortraitIV.mas_right).offset(15);
            make.bottom.equalTo(self.mas_centerY).offset(-1.5);
        }];
        _statusIV = Init(UIImageView);
        [self.contentView addSubview:_statusIV];
        [_statusIV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.headPortraitIV.mas_right).offset(10);
            make.top.equalTo(self.mas_centerY).offset(2);
        }];
        _contentLB = Init(UILabel);
        _contentLB.textColor = RGB_Color(153, 153, 153);
        _contentLB.font = Font(14);
        [self.contentView addSubview:_contentLB];
        [_contentLB mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.statusIV.mas_right).offset(5);
            make.top.equalTo(self.mas_centerY).offset(1.5);
            make.right.offset(-45);
        }];
        _timeLB = Init(UILabel);
        _timeLB.textColor = RGB_Color(178, 178, 178);
        _timeLB.font = Font(12);
        [self.contentView addSubview:_timeLB];
        [_timeLB mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-15);
            make.top.offset(15);
        }];
        _closeTipIV = Init(UIImageView);
        _closeTipIV.hidden = YES;
        _closeTipIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_closeTipIV];
        [_closeTipIV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-15);
            make.centerY.equalTo(self.contentLB);
        }];
    }
    return self;
}

-(void)setMessageListModel:(HHMessageListModel *)messageListModel
{
    _messageListModel = messageListModel;
    
    //时间
//    self.timeLabel.text = [HHGlobals timeStringForListWith:[messageListModel.lastTime integerValue]];
    
    _contentLB.text = @"消息消息消息消息消息消息消息消息消息消息消息消息";
    _timeLB.text = @"17:27";
    //头像、名称
    switch (messageListModel.sessionType.integerValue) {
        case 10:
        {
            _headPortraitIV.image = ImageInit(@"huodongtongzhi");
            _nameLB.text = @"活动通知";
        }
            break;
        case 11:
        {
            _headPortraitIV.image = ImageInit(@"zanzhutongzhi");
            _nameLB.text = @"赞助通知";
        }
            break;
        case 12:
        {
            _headPortraitIV.image = ImageInit(@"hezuotongzhi");
            _nameLB.text = @"合作通知";
        }
            break;
        case 13:
        {
            _headPortraitIV.image = ImageInit(@"piaowutongzhi");
            _nameLB.text = @"票务通知";
        }
            break;
        case 101:
        {
            _headPortraitIV.image = ImageInit(@"xindepengyou");
            _nameLB.text = @"新的朋友";
        }
            break;
        case 201:
        {
            _headPortraitIV.image = ImageInit(@"quntongzhi");
            _nameLB.text = @"群通知";
        }
            break;
        case 301:
        {
            _headPortraitIV.image = ImageInit(@"huihezhifu");
            _nameLB.text = @"会合支付";
        }
            break;
        case 401:
        {
            _headPortraitIV.image = ImageInit(@"guanzhuxiaoxi");
            _nameLB.text = @"关注消息";
        }
            break;
        case 500:
        {
            _headPortraitIV.image = ImageInit(@"dingdanxiaoxi");
            _nameLB.text = @"订单消息";
        }
            break;
        case 501:
        {
            _headPortraitIV.image = ImageInit(@"anquantixing");
            _nameLB.text = @"安全提醒";
        }
            break;
        case 502:
        {
            _headPortraitIV.image = ImageInit(@"fuwutongzhi");
            _nameLB.text = @"服务通知";
        }
            break;
        case 503:
        {
            _headPortraitIV.image = ImageInit(@"gongnenggongao");
            _nameLB.text = @"系统通知";
        }
            break;
        default:
        {
            [_headPortraitIV sd_setImageWithURL:[messageListModel.headImg mj_url]];
            _nameLB.text = messageListModel.name;
        }
            break;
    }
    //内容
    if (SafeString(messageListModel.draft).length>0) {
        NSString * text = [NSString stringWithFormat:@"%@%@",@"[草稿]",messageListModel.draft];
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:text];
        NSDictionary *attributeDict = [NSDictionary dictionaryWithObjectsAndKeys: SystemFont(15),NSFontAttributeName,[UIColor redColor],NSForegroundColorAttributeName,nil];
        NSRange rang = [text  rangeOfString:@"[草稿]"];
        [string setAttributes:attributeDict range:rang];
        self.contentLB.attributedText = string;
    }else{
        
    }
    
    
    //发送状态
    if([SafeString(messageListModel.status) isEqualToString:@"0"]) {
        self.statusIV.hidden  = NO;
        self.statusIV.image=[UIImage imageNamed:@"send_ing"];
    } else if ([SafeString(messageListModel.status) isEqualToString:@"2"]) {
        self.statusIV.hidden  = NO;
        self.statusIV.image=[UIImage imageNamed:@"send_failure"];
    } else {
        self.statusIV.hidden  = YES;
    }
}

@end
