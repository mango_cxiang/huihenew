//
//  HHChatContentControl.h
//  HHNew
//
//  Created by 张 on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN
@class HHChatFrameModel;

typedef void(^ContentClickBlock)(void);
@interface HHChatContentControl : UIControl

@property(nonatomic,strong)HHChatFrameModel * frameModel;

@property(nonatomic,copy)ContentClickBlock contentClickBlock;

@end

NS_ASSUME_NONNULL_END
