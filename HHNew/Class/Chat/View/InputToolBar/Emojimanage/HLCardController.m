//
//  HLCardController.m
//
//  Created by HeLin on 2017/10/21.
//  Copyright © 2017年 HL. All rights reserved.
//

#import "HLCardController.h"
#import "HLCardCell.h"
#import "HLCardFlowLayout.h"
#import "HMKHLCardModel.h"
typedef enum : NSUInteger {
    ControllerEditStatusSimple,
    ControllerEditStatusMutable,
} ControllerEditStatus;

@interface HLCardController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    ControllerEditStatus _status;
}

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic,assign) BOOL isEdit;
@property (nonatomic,strong) NSMutableArray<HMKHLCardModel*> *dataMode;
@property (nonatomic,strong) NSMutableArray *selectedArry;
@property(nonatomic,strong) UIButton * leftBtn;
@end

static NSString *cellIden = @"CollectionViewCell";

@implementation HLCardController


-(NSMutableArray *)selectedArry{
    
    if (!_selectedArry) {
     
        _selectedArry = [NSMutableArray array];
    }
    return _selectedArry;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) collectionViewLayout:[[HLCardFlowLayout alloc]init]];
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerClass:[HLCardCell class] forCellWithReuseIdentifier:cellIden];
    }
    return _collectionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"添加的单个表情";
    self.view.backgroundColor = [UIColor whiteColor];
    _status = ControllerEditStatusSimple;
     self.collectionView.allowsSelection = NO;
    self.isEdit = YES;
    
    [self setupInterface];
    
    [self getEmojiData];
    
}
-(void)getEmojiData{
    
//    NSMutableDictionary *dictParamer = [NSMutableDictionary dictionary];
//    [dictParamer setObject:[NSString acquireUserId] forKey:@"appUserId"];
//
//    [HMJBusinessManager GET:@"/im/appCollection/findAppCollectionByUserId" Parameters:dictParamer success:^(NSDictionary *dic, NSInteger code) {
//
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//         self.dataMode = [HMKHLCardModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"info"]];
//            [self.collectionView reloadData];
//        }else{
//            [[HMJShowLoding sharedInstance]showText:dic[@"data"][@"info"]];
//        }
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//
//    }];
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.view.backgroundColor = [UIColor whiteColor];
}

-(void)setupInterface{
    
    CGFloat editBtnW = 57;
    CGFloat editBtnH = 30;
    CGFloat editBtnX = self.view.bounds.size.width - editBtnW;
    CGFloat editBtnY = 20;
    UIButton *editBtn = [[UIButton alloc]initWithFrame:CGRectMake(editBtnX, editBtnY, editBtnW, editBtnH)];
    editBtn.layer.cornerRadius = 3;
    [editBtn setTitle:@"管理" forState:UIControlStateNormal];
    [editBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [editBtn addTarget:self action:@selector(editIDCard:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:editBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    [self.view addSubview:self.collectionView];
    
//    [self.navigationItem setLeftBarButtonItem:[self creatButtonOnNavigationBarWithImage:[UIImage imageNamed:@"back_black"] Title:nil BtnColor:nil frame:CGRectMake(0, 0, 35, 35) tag:1 targer:self action:@selector(DeleteEmoji:)]];
    
    _leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    [_leftBtn setTitle:@"返回" forState:UIControlStateNormal];
    _leftBtn.tag = 1;
    [_leftBtn setTitleColor:ColorManage.text_color forState:UIControlStateNormal];
    _leftBtn.titleLabel.font = Font(17);
    [_leftBtn addTarget:self action:@selector(DeleteEmoji:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftItem = [[UIBarButtonItem alloc]initWithCustomView:_leftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
}

-(void)DeleteEmoji:(UIButton *)btn{
    
    if (btn.tag == 1) {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        
        
         [self.selectedArry removeAllObjects];
        for (NSIndexPath *index in self.collectionView.indexPathsForSelectedItems) {
            
            [self.selectedArry addObject:self.dataMode[index.row].ID];
            
        }
        
        if (self.selectedArry.count == 0) {
            
//            [[HMJShowLoding sharedInstance]showText:@"请选择删除的表情！"];
            return;
        }
        
//        NSMutableDictionary *ParameDict = [NSMutableDictionary dictionary];
//
//        [ParameDict setObject:[self.selectedArry componentsJoinedByString:@","] forKey:@"ids"];
//
//
//        [HMJBusinessManager GET:@"/im/appCollection/isDelete" Parameters:ParameDict success:^(NSDictionary *dic, NSInteger code) {
//
//            [self getEmojiData];
//            [self getemoji];
//            [self.selectedArry removeAllObjects];
//
//        } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//            [self.selectedArry removeAllObjects];
//        }];
        
        
    }
}

-(void)getemoji{
    
    
//    [[HMJShowLoding sharedInstance] showLoading];
//    [HHGlobals saveEmojiDict:nil];
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//
//    [HMJBusinessManager GET:@"/im/appAdd/appAddList" Parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//
//
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//            NSMutableArray *emojiArr = [NSMutableArray array];
//
//            NSArray *DataArry = dic[@"data"][@"info"];
//            for (NSDictionary *dict in DataArry) {
//
//                NSMutableDictionary *Emojidict = [NSMutableDictionary dictionary];
//
//                [Emojidict setObject:dict[@"chat_url"] forKey:@"cover_pic"];
//                [Emojidict setObject:dict[@"id"] forKey:@"EmojiID"];
//
//                NSMutableArray *emoticons = [NSMutableArray array];
//
//                for (NSDictionary *SubDict in dict[@"phiz_subList"]) {
//
//                    NSMutableDictionary *icondict = [NSMutableDictionary dictionary];
//
//                    [icondict setValue:SubDict[@"next_url"] forKey:@"image"];
//                    [icondict setValue:SubDict[@"phiz_desc"] forKey:@"desc"];
//                    [icondict setValue:SubDict[@"master_url"] forKey:@"imageGif"];
//                    [icondict setValue:SubDict[@"id"] forKey:@"EmojiID"];
//
//                    [emoticons addObject:icondict];
//                }
//
//                [Emojidict setValue:emoticons forKey:@"emoticons"];
//
//                [emojiArr addObject:Emojidict];
//            }
//
//            [HHGlobals saveEmojiDict:emojiArr];
//        }
//
//        [self getCollectionemoji];
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
}


-(void)getCollectionemoji{
    
    
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//
//    [HMJBusinessManager GET:@"/im/appAdd/listCollection" Parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//
//
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//
//
//
//            NSArray *DataArry = dic[@"data"][@"info"][@"collection"];
//
//            NSMutableDictionary *Emojidict = [NSMutableDictionary dictionary];
//
//            [Emojidict setObject:dic[@"data"][@"info"][@"heartUrl"] forKey:@"cover_pic"];
//
//            NSMutableArray *emoticons = [NSMutableArray array];
//
//            for (NSDictionary *SubDict in DataArry) {
//
//                NSMutableDictionary *icondict = [NSMutableDictionary dictionary];
//
//                [icondict setValue:SubDict[@"nextUrl"] forKey:@"image"];
//                [icondict setValue:SubDict[@"name"] forKey:@"desc"];
//                [icondict setValue:SubDict[@"imgUrl"] forKey:@"imageGif"];
//                [icondict setValue:SubDict[@"next_id"] forKey:@"EmojiID"];
//
//                [emoticons addObject:icondict];
//            }
//
//            [Emojidict setValue:emoticons forKey:@"emoticons"];
//
//            NSMutableArray *arry = [HHGlobals getEmojiDict].mutableCopy;
//
//            [arry insertObject:Emojidict atIndex:0];
//
//            [HHGlobals saveEmojiDict:arry];
//
//        }
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
}


-(void)editIDCard:(UIButton *)btn{
    
    [self.selectedArry removeAllObjects];
    btn.selected = !btn.selected;
    if (btn.selected) {
        self.collectionView.allowsMultipleSelection = btn.selected;
        self.collectionView.allowsSelection = YES;
        self.isEdit = NO;
        [self.collectionView reloadData];
        btn.backgroundColor = RGB_Color(7, 193, 96);
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitle:@"完成" forState:UIControlStateNormal];
        [_leftBtn setTitle:@"删除" forState:UIControlStateNormal];
        [_leftBtn setTitleColor:UIColor.redColor forState:UIControlStateNormal];
        _leftBtn.tag = 2;
    }else{
       self.isEdit = YES;
       self.collectionView.allowsSelection = NO;
       [self.collectionView reloadData];
       btn.backgroundColor = [UIColor clearColor];
       [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
       [btn setTitle:@"管理" forState:UIControlStateNormal];
        [_leftBtn setTitle:@"返回" forState:UIControlStateNormal];
        [_leftBtn setTitleColor:ColorManage.text_color forState:UIControlStateNormal];
        _leftBtn.tag = 1;
    }
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataMode.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HLCardCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIden forIndexPath:indexPath];
    cell.DataModel = self.dataMode[indexPath.row];
    cell.isEdit = self.isEdit;
    return cell;
}


@end
