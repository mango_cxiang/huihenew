//
//  HLCardCell.h
//
//  Created by HeLin  on 2017/10/21.
//  Copyright © 2017年 HL. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HMKHLCardModel;
@interface HLCardCell : UICollectionViewCell

@property (nonatomic,assign) BOOL isEdit;
@property (nonatomic,strong) UIImageView *imgView;
@property (nonatomic,strong) HMKHLCardModel *DataModel;
@end

