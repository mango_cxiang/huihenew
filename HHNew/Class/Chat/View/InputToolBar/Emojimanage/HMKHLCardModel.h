//
//  HMKHLCardModel.h
//  huihe
//
//  Created by jie.huang on 12/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HMKHLCardModel : NSObject


@property (nonatomic,strong) NSString *createDate;
@property (nonatomic,strong) NSString *ID;
@property (nonatomic,strong) NSString *imgType;
@property (nonatomic,strong) NSString *imgUrl;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *nextUrl;
@property (nonatomic,strong) NSString *next_id;


@end
