//
//  HLCardCell.m
//  Mom
//
//  Created by HeLin  on 2017/10/21.
//  Copyright © 2017年 HL. All rights reserved.
//

#import "HLCardCell.h"
#import "HMKHLCardModel.h"
@interface HLCardCell ()
@property (nonatomic, weak)UIButton *chooseIcon;
@end

@implementation HLCardCell

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupInterface];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupInterface];
    }
    return self;
}

-(void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    self.chooseIcon.selected = selected;
}

-(void)setIsEdit:(BOOL)isEdit{
    
    _isEdit = isEdit;
    self.chooseIcon.hidden = isEdit;
}

-(void)setDataModel:(HMKHLCardModel *)DataModel{
    _DataModel = DataModel;
    
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:DataModel.nextUrl]];
}

-(void)setupInterface{
    
    
    self.imgView = [[UIImageView alloc]initWithFrame:CGRectMake(14, 14, (SCREEN_WIDTH)/4 - 28, (SCREEN_WIDTH)/4 - 28)];
    [self.contentView addSubview:self.imgView];
    UIView *xView = [UIView new];
    xView.frame = CGRectMake((SCREEN_WIDTH)/4 , 0, 0.5, (SCREEN_WIDTH)/4);
    xView.backgroundColor = RGB_Color(220, 220, 220);
    [self addSubview:xView];
    
    UIView *yView = [UIView new];
    yView.frame = CGRectMake(0, (SCREEN_WIDTH)/4, (SCREEN_WIDTH)/4, 0.5);
    yView.backgroundColor = RGB_Color(220, 220, 220);
    [self addSubview:yView];
    
    UIButton *chooseIcon = [[UIButton alloc]init];
    [chooseIcon setImage:[UIImage imageNamed:@"weixuanzhong_0219"] forState:UIControlStateNormal];
    [chooseIcon setImage:[UIImage imageNamed:@"xuanzhong_0219"] forState:UIControlStateSelected];
    self.chooseIcon = chooseIcon;
    [self.contentView bringSubviewToFront:self.chooseIcon];
    [self.contentView addSubview:chooseIcon];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    CGFloat chooseIconW = 20;
    CGFloat chooseIconH = chooseIconW;
    CGFloat chooseIconX = self.frame.size.width - chooseIconW - 8;
    CGFloat chooseIconY = 8;
    self.chooseIcon.frame = CGRectMake(chooseIconX, chooseIconY, chooseIconW, chooseIconH);
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


@end
