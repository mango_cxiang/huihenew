//
//  HMJTextView.m
//  huihe
//
//  Created by Six on 16/3/8.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJTextView.h"
#import "NSMutableAttributedString+Emoji.h"
#import <objc/message.h>

@implementation HMJTextView

#pragma mark - 生命周期 方法
- (void)dealloc
{
    _placeholder = nil;
    _placeholderColor = nil;
    [self removeObserver:self forKeyPath:@"contentSize" context:NULL];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidChangeNotification object:self];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self setup];
    }
    return self;
}

- (instancetype)init
{
    if (self = [super init])
    {
        [self setup];
    }
    return self;
}

- (void)setup
{
    // 根据输入框是否有内容 决定 发送按钮是否可以点击
    self.enablesReturnKeyAutomatically = YES;
    
    [self addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:NULL];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewTextDidChangeNotification:) name:UITextViewTextDidChangeNotification object:self];
    _placeholderColor = UIColorFromRGBA(0x000000, 0.3);
    self.font = SystemFont(16);
    self.textColor = [UIColor blackColor];
    self.backgroundColor = [UIColor whiteColor];
    
    self.scrollIndicatorInsets =  UIEdgeInsetsMake(0, 0, 0, 50);
}

#pragma mark - setter 方法
- (void)setPlaceholder:(NSString *)placeholder
{
    if ([placeholder isEqualToString:_placeholder])
    {
        return;
    }
    _placeholder = placeholder;
    [self setNeedsDisplay];
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor
{
    if ([placeholderColor isEqual:_placeholderColor])
    {
        return;
    }
    _placeholderColor = placeholderColor;
    [self setNeedsDisplay];
}

#pragma mark - 重写 父类方法
- (UIResponder *)nextResponder
{
    return _responder ? _responder : [super nextResponder];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (_responder)
    {
        return NO;
    }
    else
    {
        if (action == @selector(paste:))
        {
            return YES;
        }
        else
        {
            return [super canPerformAction:action withSender:sender];
        }
    }
}

//粘贴功能
- (void)paste:(id)sender
{
    UIPasteboard * pasteboard = [UIPasteboard generalPasteboard];
    [self willChangeValueForKey:@"contentSize"];
    NSString *str = [NSString stringWithFormat:@"%@%@",self.text,pasteboard.string];
    self.attributedText = [NSMutableAttributedString returnEmojiStrWithText:str];
    [self didChangeValueForKey:@"contentSize"];
}

- (void)setText:(NSString *)text
{
    BOOL scrollEnabled = self.scrollEnabled;
    [self setScrollEnabled:YES];
    [super setText:text];
    [self setScrollEnabled:scrollEnabled];
    [self setNeedsDisplay];
}

- (void)setScrollable:(BOOL)isScrollable
{
    [super setScrollEnabled:isScrollable];
}

- (void)setAttributedText:(NSAttributedString *)attributedText
{
    [super setAttributedText:attributedText];
    [self setNeedsDisplay];
}

- (void)setContentInset:(UIEdgeInsets)contentInset
{
    [super setContentInset:contentInset];
    [self setNeedsDisplay];
}

- (void)setFont:(UIFont *)font
{
    [super setFont:font];
    [self setNeedsDisplay];
}

- (void)setTextAlignment:(NSTextAlignment)textAlignment
{
    [super setTextAlignment:textAlignment];
    [self setNeedsDisplay];
}

- (NSUInteger)numberOfLines
{
    NSUInteger lines = self.contentSize.height / self.font.lineHeight;
    return lines;
}

#pragma mark - 通知相关
- (void)textViewTextDidChangeNotification:(NSNotification *)notification
{
    [self setNeedsDisplay];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"contentSize"])
    {
        if ((([self numberOfLines] + 1 > self.lineNum) && (self.frame.size.height >= (self.lineNum * self.font.lineHeight))) || self.lineNum == 0)
        {
            return;
        }
        CGSize contentSize = self.contentSize;
        if (contentSize.height > (self.lineNum * self.font.lineHeight))
        {
            contentSize.height = self.lineNum * self.font.lineHeight;
        }
        if ([self.textViewDelegate respondsToSelector:@selector(textView:willChangeHeight:)])
        {
            [self.textViewDelegate textView:self willChangeHeight:contentSize.height];
        }
        self.height = contentSize.height;
        if ([self.textViewDelegate respondsToSelector:@selector(textView:didChangeHeight:)])
        {
            [self.textViewDelegate textView:self didChangeHeight:contentSize.height];
        }
    }
}

#pragma mark - 绘制
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    if (self.text.length == 0 && self.placeholder)
    {
        CGRect placeHolderRect = CGRectMake(10, 7, rect.size.width, rect.size.height);
        [self.placeholderColor set];
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
        paragraphStyle.alignment = self.textAlignment;
        NSDictionary * dict = @{NSFontAttributeName : self.font , NSForegroundColorAttributeName : self.placeholderColor , NSParagraphStyleAttributeName : paragraphStyle};
        [self.placeholder drawInRect:placeHolderRect withAttributes:dict];
    }
}

@end
