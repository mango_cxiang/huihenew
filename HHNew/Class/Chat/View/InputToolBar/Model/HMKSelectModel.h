//
//  HMKSelectModel.h
//  huihe
//
//  Created by jie.huang on 15/2/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HMKSelectModel : NSObject

@property (nonatomic,strong) NSString *chat_url;

@property (nonatomic,strong) NSString *cover_url;

@property (nonatomic,strong) NSString *ID;

@property (nonatomic,strong) NSString *isAdd;

@property (nonatomic,strong) NSString *master_url;

@property (nonatomic,strong) NSString *name;

@property (nonatomic,strong) NSString *desc;


@end
