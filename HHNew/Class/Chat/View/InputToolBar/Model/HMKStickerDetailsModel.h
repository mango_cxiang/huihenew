//
//  HMKStickerDetailsModel.h
//  huihe
//
//  Created by jie.huang on 1/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HMKStickerDetailsModel : NSObject

@property (nonatomic,strong) NSString *ID;
@property (nonatomic,strong) NSString *isAdd;
@property (nonatomic,strong) NSString *master_type;
@property (nonatomic,strong) NSString *master_url;
@property (nonatomic,strong) NSString *next_url;
@property (nonatomic,strong) NSString *p_id;
@property (nonatomic,strong) NSString *phiz_desc;

@end


