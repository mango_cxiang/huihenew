//
//  HMKStickerMallModel.h
//  huihe
//
//  Created by jie.huang on 14/1/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HMKStickerMallModel : NSObject


@property (nonatomic,strong) NSString *cover_url;
@property (nonatomic,strong) NSString *master_url;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *ID;

@end
