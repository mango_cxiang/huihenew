//
//  HMKStickerCollectModel.h
//  huihe
//
//  Created by jie.huang on 7/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HMKStickerCollectModel : NSObject

@property (nonatomic,strong) NSString *chat_url;

@property (nonatomic,strong) NSString *ID;

@property (nonatomic,strong) NSString *isAdd;

@property (nonatomic,strong) NSString *name;


@end
