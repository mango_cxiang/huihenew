//
//  HMKHeardSelectModel.m
//  huihe
//
//  Created by jie.huang on 15/2/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKHeardSelectModel.h"

@implementation HMKHeardSelectModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"ID" : @"id",
             @"desc" :@"description"
             };
}

@end
