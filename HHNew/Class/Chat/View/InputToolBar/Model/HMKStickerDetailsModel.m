//
//  HMKStickerDetailsModel.m
//  huihe
//
//  Created by jie.huang on 1/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKStickerDetailsModel.h"

@implementation HMKStickerDetailsModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"ID" : @"id"};
}

@end
