//
//  HMKMoreStickerModel.h
//  huihe
//
//  Created by jie.huang on 6/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HMKMoreStickerModel : NSObject

@property (nonatomic,strong) NSString *chat_url;
@property (nonatomic,strong) NSString *cover_url;
@property (nonatomic,strong) NSString *ID;
@property (nonatomic,strong) NSString *isAdd;
@property (nonatomic,strong) NSString *master_url;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *userName;

@end
