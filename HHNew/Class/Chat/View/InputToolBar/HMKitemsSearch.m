//
//  HMKitemsSearch.m
//  huihe
//
//  Created by jie.huang on 26/10/18.
//  Copyright © 2018年 BQ. All rights reserved.
//

#import "HMKitemsSearch.h"

@interface HMKitemsSearch ()<UIGestureRecognizerDelegate>

@property (nonatomic,strong) UIImageView *searchIcon;

@end

@implementation HMKitemsSearch


- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}


-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        self.font = [UIFont systemFontOfSize:15];
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius  = 4;
        self.placeholder = @"搜索表情";
        self.backgroundColor = [UIColor whiteColor];
        self.returnKeyType=UIReturnKeySearch;
        self.layer.borderColor = RGB_Color(240, 240, 240).CGColor;
        self.layer.borderWidth = 0.2;
        
        UIImageView *searchIcon = [[UIImageView alloc] init];
        searchIcon.image = [UIImage imageNamed:@"sous"];
        searchIcon.width = 30;
        searchIcon.height = 30;
        searchIcon.contentMode = UIViewContentModeCenter;
        self.leftView = searchIcon;
        self.leftViewMode = UITextFieldViewModeAlways;
        
    }
    
    return self;
}

+(instancetype)SearchBar
{
    return [[self alloc]init];
}

-(void)rightButtonClicked:(UIButton *)btn{
    
}

@end
