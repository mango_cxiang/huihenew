//
//  HMKAllAddEmojiCell.m
//  huihe
//
//  Created by jie.huang on 7/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKAllAddEmojiCell.h"
#import "HMKAllAddEmojiModel.h"
@implementation HMKAllAddEmojiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.isAddBtn.layer.borderWidth = 0.5;
    self.isAddBtn.layer.borderColor = RGB_Color(26, 173, 25).CGColor;
    self.isAddBtn.layer.cornerRadius = 4;
    [self.isAddBtn setTitleColor:RGB_Color(26, 173, 25) forState:UIControlStateNormal];
    self.isAddBtn.layer.masksToBounds = YES;
}


-(void)setDataMode:(HMKAllAddEmojiModel *)dataMode{
    
    _dataMode = dataMode;
    
    [self.iconImage sd_setImageWithURL:[NSURL URLWithString:dataMode.chat_url]];
    self.Timelable.text = dataMode.createDate;
    self.TitleTextLable.text = dataMode.name;
    if ([dataMode.isAdd isEqualToString:@"1"]) {
        self.isAddBtn.layer.borderColor = RGB_Color(219, 219, 219).CGColor;
        [self.isAddBtn setTitleColor:RGB_Color(219, 219, 219) forState:UIControlStateNormal];
    }else{
       self.isAddBtn.layer.borderColor = RGB_Color(26, 173, 25).CGColor;
        [self.isAddBtn setTitleColor:RGB_Color(26, 173, 25) forState:UIControlStateNormal];
    }
    
    
    
}
@end
