//
//  StickerDetailsHeadView.m
//  huihe
//
//  Created by jie.huang on 1/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "StickerDetailsHeadView.h"

@implementation StickerDetailsHeadView


-(void)awakeFromNib{
    
    [super awakeFromNib];
    
    self.AddBtn.backgroundColor = RGB_Color(7, 193, 96);
    self.AddBtn.layer.cornerRadius = 4;
    self.AddBtn.layer.masksToBounds = YES;
}


@end
