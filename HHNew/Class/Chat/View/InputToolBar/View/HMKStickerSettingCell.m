//
//  HMKStickerSettingCell.m
//  huihe
//
//  Created by jie.huang on 6/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKStickerSettingCell.h"
#import "HMKStickerCollectModel.h"
@implementation HMKStickerSettingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.removeBtn.backgroundColor = RGB_Color(237, 237, 237);
    [self.removeBtn viewWithRadis:3];
}

- (void)setDataMode:(HMKStickerCollectModel *)dataMode{
    
    
    _dataMode = dataMode;
    
    [self.iconimage sd_setImageWithURL:[NSURL URLWithString:dataMode.chat_url]];
    self.TitleLable.text = dataMode.name;
}

@end
