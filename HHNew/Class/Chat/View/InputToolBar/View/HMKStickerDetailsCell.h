//
//  HMKStickerDetailsCell.h
//  huihe
//
//  Created by jie.huang on 1/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMKStickerDetailsModel.h"
@interface HMKStickerDetailsCell : UITableViewCell

@property (nonatomic,strong) NSMutableArray<HMKStickerDetailsModel*> *dataMode;

@end
