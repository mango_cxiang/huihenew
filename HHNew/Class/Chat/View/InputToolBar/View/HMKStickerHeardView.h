//
//  HMKStickerHeardView.h
//  huihe
//
//  Created by jie.huang on 9/1/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
@interface HMKStickerHeardView : UIView
@property (weak, nonatomic) IBOutlet SDCycleScrollView *bannerView;
@property (weak, nonatomic) IBOutlet UIView *HeadSearchView;

@end
