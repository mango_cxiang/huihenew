//
//  HMKSelectionCell.h
//  huihe
//
//  Created by jie.huang on 15/2/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>


@class HMKSelectModel;


@interface HMKSelectionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *TitleLbale;
@property (weak, nonatomic) IBOutlet UILabel *SubTitleLable;
@property (weak, nonatomic) IBOutlet UIButton *AddBtn;

@property (nonatomic,strong) HMKSelectModel *DataModel;


@end
