//
//  HMKStickerSearchCell.h
//  huihe
//
//  Created by jie.huang on 5/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HMKStickerSearchHeadModel;
@class HMKMoreStickerModel;
@interface HMKStickerSearchCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *Titilelable;
@property (weak, nonatomic) IBOutlet UILabel *SubTItleLable;

@property (nonatomic,strong) HMKStickerSearchHeadModel *DataModel;

@property (nonatomic,strong) HMKMoreStickerModel *MoreModel;

@end
