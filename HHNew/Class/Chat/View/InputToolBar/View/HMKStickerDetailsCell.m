//
//  HMKStickerDetailsCell.m
//  huihe
//
//  Created by jie.huang on 1/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKStickerDetailsCell.h"
#import "UIButton+WebCache.h"
#import "ZJAnimationPopView.h"
#import "ZJAlertView.h"
#import "HMKEmojiBtn.h"

@interface HMKStickerDetailsCell ()


@property (nonatomic,strong) ZJAlertView *customView;

@property (nonatomic,strong) NSMutableArray *isAddArry;

@end

@implementation HMKStickerDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (NSMutableArray *)isAddArry{
    
    if (!_isAddArry) {
        
        _isAddArry = [NSMutableArray array];
    }
    return _isAddArry;
}

-(ZJAlertView *)customView{
    
    if (!_customView) {
        
        _customView = [ZJAlertView viewFromxib];
    }
    return _customView;
}

-(void)setDataMode:(NSMutableArray<HMKStickerDetailsModel *> *)dataMode{
    
    _dataMode = dataMode;
    
    
    [self createSquares:dataMode];
}


-(void)createSquares:(NSMutableArray<HMKStickerDetailsModel *> *)squares
{
    
    NSInteger count = squares.count;
    
    [self.isAddArry removeAllObjects];
    
    NSInteger maxColsCount = 4;
    CGFloat buttonW = (self.width - 129)/maxColsCount ;
    CGFloat buttonH = buttonW;
    for (NSInteger i=0; i<count; i++) {
        HMKEmojiBtn *button = [HMKEmojiBtn buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = 100 + i;
        [self addSubview:button];
        button.x = (i % maxColsCount) * buttonW + ((i % maxColsCount)==0? 30:(i % maxColsCount)  * 23 + 30) ;
        button.y = (i/maxColsCount)*buttonH + ((i / maxColsCount)==0? 25 : (i / maxColsCount)* 23 + 25);
        button.width = buttonW;
        button.height = buttonH;
        button.DataModel =squares[i];
        [button sd_setImageWithURL:[NSURL URLWithString:squares[i].next_url] forState:UIControlStateNormal placeholderImage:nil];
    }

}
-(void)buttonClick:(HMKEmojiBtn *)button
{
    [self showPopAnimationWithAnimationStyle:1 Button:button];
}

- (void)showPopAnimationWithAnimationStyle:(NSInteger)style Button:(HMKEmojiBtn *)btn
{
    ZJAnimationPopStyle popStyle = (style == 8) ? ZJAnimationPopStyleCardDropFromLeft : (ZJAnimationPopStyle)style;
    ZJAnimationDismissStyle dismissStyle = (ZJAnimationDismissStyle)style;
    
    // 1.初始化
    ZJAnimationPopView *popView = [[ZJAnimationPopView alloc] initWithCustomView:self.customView popStyle:popStyle dismissStyle:dismissStyle];
    
    // 2.设置属性，可不设置使用默认值，见注解
    // 2.1 显示时点击背景是否移除弹框
    popView.isClickBGDismiss = ![_customView isKindOfClass:[ZJAlertView class]];
    // 2.2 显示时背景的透明度
    popView.popBGAlpha = 0.5f;
    // 2.3 显示时是否监听屏幕旋转
    popView.isObserverOrientationChange = YES;
    
    // 2.6 显示完成回调
    popView.popComplete = ^{
        NSLog(@"显示完成");
    };
    // 2.7 移除完成回调
    popView.dismissComplete = ^{
        NSLog(@"移除完成");
    };
    
    // 3.处理自定义视图操作事件
    [self handleCustomActionEnvent:popView Button:btn];
    
    // 4.显示弹框
    [popView pop];
}

- (void)handleCustomActionEnvent:(ZJAnimationPopView *)popView Button:(HMKEmojiBtn *)btn
{
    // 在监听自定义视图的block操作事件时，要使用弱对象来避免循环引用
    __weak typeof(popView) weakPopView = popView;
    
     NSString *url = btn.DataModel.master_url;
    ZJAlertView *alertView = _customView;
    if ([btn.DataModel.isAdd isEqualToString:@"1"] || [self.isAddArry containsObject:@(btn.tag)]) {
        alertView.AddBtn.enabled = NO;
        [alertView.AddBtn setTitleColor:RGB_Color(233, 233, 233) forState:(UIControlStateNormal)];
        [alertView.AddBtn setTitle:@"已添加" forState:(UIControlStateNormal)];
    }else{
        alertView.AddBtn.enabled = YES;
        [alertView.AddBtn setTitleColor:alertView.quxiaoBtn.titleLabel.textColor forState:(UIControlStateNormal)];
        [alertView.AddBtn setTitle:@"添加" forState:(UIControlStateNormal)];
    }
    [alertView.EmojiImageView sd_setImageWithURL:[url mj_url] placeholderImage:nil];
    alertView.canceSureActionBlock = ^(BOOL isSure) {
        [weakPopView dismiss];
        NSLog(@"点击了%@", isSure ? @"确定" : @"取消");
        if (isSure) {
            
            [self AddEmojiButton:btn];
        }
    };
    
    alertView.closeBlock = ^{
         [weakPopView dismiss];
    };
    
    
}

-(void)AddEmojiButton:(HMKEmojiBtn *)btn{
    
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    [dictparame setObject:btn.DataModel.master_url forKey:@"imgUrl"];
//    [dictparame setObject:btn.DataModel.phiz_desc forKey:@"name"];
//    [dictparame setObject:btn.DataModel.ID forKey:@"next_id"];
//    [dictparame setObject:btn.DataModel.p_id forKey:@"phiz_id"];
//    [dictparame setObject:btn.DataModel.next_url forKey:@"nextUrl"];
//    [dictparame setObject:@1 forKey:@"imgType"];
//
//    [[HMJShowLoding sharedInstance]showLoading];
//    [HMJBusinessManager EmojiPOST:@"/im/appCollection/insertAppCollection" parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//
//        [self.isAddArry addObject:@(btn.tag)];
//        [self getemoji];
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
}

-(void)getemoji{
    
    
//    [[HMJShowLoding sharedInstance] showLoading];
//    [HHGlobals saveEmojiDict:nil];
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//
//    [HMJBusinessManager GET:@"/im/appAdd/appAddList" Parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//
//
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//            NSMutableArray *emojiArr = [NSMutableArray array];
//
//            NSArray *DataArry = dic[@"data"][@"info"];
//            for (NSDictionary *dict in DataArry) {
//
//                NSMutableDictionary *Emojidict = [NSMutableDictionary dictionary];
//
//                [Emojidict setObject:dict[@"chat_url"] forKey:@"cover_pic"];
//                [Emojidict setObject:dict[@"id"] forKey:@"EmojiID"];
//
//                NSMutableArray *emoticons = [NSMutableArray array];
//
//                for (NSDictionary *SubDict in dict[@"phiz_subList"]) {
//
//                    NSMutableDictionary *icondict = [NSMutableDictionary dictionary];
//
//                    [icondict setValue:SubDict[@"next_url"] forKey:@"image"];
//                    [icondict setValue:SubDict[@"phiz_desc"] forKey:@"desc"];
//                    [icondict setValue:SubDict[@"master_url"] forKey:@"imageGif"];
//                    [icondict setValue:SubDict[@"id"] forKey:@"EmojiID"];
//
//                    [emoticons addObject:icondict];
//                }
//
//                [Emojidict setValue:emoticons forKey:@"emoticons"];
//
//                [emojiArr addObject:Emojidict];
//            }
//
//            [HHGlobals saveEmojiDict:emojiArr];
//        }
//
//        [self getCollectionemoji];
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
}


-(void)getCollectionemoji{
    
    
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//    
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    
//    [HMJBusinessManager GET:@"/im/appAdd/listCollection" Parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//        
//        
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//            
//            
//            
//            NSArray *DataArry = dic[@"data"][@"info"][@"collection"];
//            
//            NSMutableDictionary *Emojidict = [NSMutableDictionary dictionary];
//            
//            [Emojidict setObject:dic[@"data"][@"info"][@"heartUrl"] forKey:@"cover_pic"];
//            
//            NSMutableArray *emoticons = [NSMutableArray array];
//            
//            for (NSDictionary *SubDict in DataArry) {
//                
//                NSMutableDictionary *icondict = [NSMutableDictionary dictionary];
//                
//                [icondict setValue:SubDict[@"nextUrl"] forKey:@"image"];
//                [icondict setValue:SubDict[@"name"] forKey:@"desc"];
//                [icondict setValue:SubDict[@"imgUrl"] forKey:@"imageGif"];
//                [icondict setValue:SubDict[@"next_id"] forKey:@"EmojiID"];
//                
//                [emoticons addObject:icondict];
//            }
//            
//            [Emojidict setValue:emoticons forKey:@"emoticons"];
//            
//            NSMutableArray *arry = [HHGlobals getEmojiDict].mutableCopy;
//            
//            [arry insertObject:Emojidict atIndex:0];
//            
//            [HHGlobals saveEmojiDict:arry];
//            
//        }
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//        
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
}




@end
