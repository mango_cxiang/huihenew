//
//  HMKStickerNextView.h
//  huihe
//
//  Created by jie.huang on 4/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HMKStickerNextView : UIView
@property (weak, nonatomic) IBOutlet UIButton *MoreBtn;
@property (weak, nonatomic) IBOutlet UILabel *TitiLabel;
@property (weak, nonatomic) IBOutlet UIView *fenGView;

@end
