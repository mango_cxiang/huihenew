//
//  HMKStickerCell.m
//  huihe
//
//  Created by jie.huang on 9/1/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKStickerCell.h"
#import "HMKStickerMallModel.h"
#import "HMKStickerSearchDetatlsModel.h"
@implementation HMKStickerCell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
//        self.backgroundColor = [UIColor purpleColor];
        
        self.imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, (SCREEN_WIDTH-80)/3, (SCREEN_WIDTH-80)/3)];
        self.imgView.layer.borderWidth = 0.5;
        self.imgView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.imgView.layer.cornerRadius = 10;
        self.imgView.layer.masksToBounds = YES;
        [self addSubview:self.imgView];
        
        self.GifImageView = [[YYAnimatedImageView alloc]initWithFrame:CGRectMake(0, 0, (SCREEN_WIDTH-80)/3, (SCREEN_WIDTH-80)/3)];
        self.GifImageView.layer.borderWidth = 0.5;
        self.GifImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.GifImageView.layer.cornerRadius = 10;
        self.GifImageView.layer.masksToBounds = YES;
        [self addSubview:self.GifImageView];
        
        self.text = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.imgView.frame)+5, (SCREEN_WIDTH-80)/3, 20)];
        self.text.textAlignment = NSTextAlignmentCenter;
        self.text.font = [UIFont systemFontOfSize:13];
        [self addSubview:self.text];
        
    }
    return self;
}

- (void)setDataModel:(HMKStickerMallModel *)DataModel{
    
    _DataModel = DataModel;
    
    self.GifImageView.hidden = YES;
    self.imgView.hidden = NO;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:DataModel.cover_url]];
    self.text.text = DataModel.name;
}

-(void)setDetailsMode:(HMKStickerSearchDetatlsModel *)detailsMode{
    
    _detailsMode = detailsMode;
    
    self.GifImageView.hidden = NO;
    self.imgView.hidden = YES;
    [self.GifImageView sd_setImageWithURL:[detailsMode.master_url mj_url] placeholderImage:nil];
    self.text.text = detailsMode.phiz_desc;
    
}


@end
