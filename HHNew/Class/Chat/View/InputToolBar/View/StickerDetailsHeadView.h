//
//  StickerDetailsHeadView.h
//  huihe
//
//  Created by jie.huang on 1/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StickerDetailsHeadView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *HardImageView;
@property (weak, nonatomic) IBOutlet UILabel *TitleLable;
@property (weak, nonatomic) IBOutlet UILabel *ContentLable;
@property (weak, nonatomic) IBOutlet UIButton *AddBtn;

@end
