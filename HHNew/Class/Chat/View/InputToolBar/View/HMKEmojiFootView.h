//
//  HMKEmojiFootView.h
//  huihe
//
//  Created by jie.huang on 11/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HMKEmojiFootView : UIView
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageview;
@property (weak, nonatomic) IBOutlet UIButton *releaseBtn;
@property (weak, nonatomic) IBOutlet UIButton *jumpNextBtn;

@end
