//
//  HMKStickerSearchCell.m
//  huihe
//
//  Created by jie.huang on 5/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKStickerSearchCell.h"
#import "HMKStickerSearchHeadModel.h"
#import "HMKMoreStickerModel.h"
@implementation HMKStickerSearchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


-(void)setMoreModel:(HMKMoreStickerModel *)MoreModel{
    
    _MoreModel = MoreModel;
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:MoreModel.chat_url]];
    self.Titilelable.text = MoreModel.name;
    self.SubTItleLable.text = MoreModel.userName;
}

-(void)setDataModel:(HMKStickerSearchHeadModel *)DataModel{
    
    _DataModel = DataModel;
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:DataModel.chat_url]];
    self.Titilelable.text = DataModel.name;
    self.SubTItleLable.text = DataModel.userName;
    
}
@end
