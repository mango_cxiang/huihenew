//
//  HMKSelectionHeardView.h
//  huihe
//
//  Created by jie.huang on 15/2/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"

@interface HMKSelectionHeardView : UIView
@property (weak, nonatomic) IBOutlet UILabel *recommendedLable;
@property (weak, nonatomic) IBOutlet UIView *recommendedView;
@property (weak, nonatomic) IBOutlet UIView *TabView;
@property (weak, nonatomic) IBOutlet SDCycleScrollView *BannerView;
@property (weak, nonatomic) IBOutlet UIView *HerdSearchView;
@property (weak, nonatomic) IBOutlet UIView *BottomView;
@property (weak, nonatomic) IBOutlet UILabel *BottomLable;
@property (weak, nonatomic) IBOutlet UIView *RecommendedView;
@property (weak, nonatomic) IBOutlet UILabel *RecLable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *RecHeight;

@end
