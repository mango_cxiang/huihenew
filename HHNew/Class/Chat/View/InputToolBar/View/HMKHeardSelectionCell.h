//
//  HMKHeardSelectionCell.h
//  huihe
//
//  Created by jie.huang on 15/2/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HMKHeardSelectModel;
@interface HMKHeardSelectionCell : UITableViewCell


@property (nonatomic,strong) HMKHeardSelectModel *DataModel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *NameLable;
@property (weak, nonatomic) IBOutlet UILabel *SubLable;
@property (weak, nonatomic) IBOutlet UIButton *AddBtn;

@end
