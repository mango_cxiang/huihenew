//
//  HMKAllAddEmojiCell.h
//  huihe
//
//  Created by jie.huang on 7/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HMKAllAddEmojiModel;
@interface HMKAllAddEmojiCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *TitleTextLable;
@property (weak, nonatomic) IBOutlet UILabel *Timelable;
@property (weak, nonatomic) IBOutlet UIButton *isAddBtn;
@property (nonatomic,strong) HMKAllAddEmojiModel *dataMode;

@end
