//
//  HMKHeardSelectionCell.m
//  huihe
//
//  Created by jie.huang on 15/2/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKHeardSelectionCell.h"
#import "HMKHeardSelectModel.h"
@implementation HMKHeardSelectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.AddBtn.layer.borderWidth = 1;
    self.AddBtn.layer.borderColor = RGB_Color(26, 173, 25).CGColor;
    self.AddBtn.layer.cornerRadius = 4;
    [self.AddBtn setTitleColor:RGB_Color(26, 173, 25) forState:UIControlStateNormal];
    self.AddBtn.layer.masksToBounds = YES;
    
}


- (void)setDataModel:(HMKHeardSelectModel *)DataModel{
    
    _DataModel = DataModel;
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:DataModel.cover_url]];
    self.NameLable.text = DataModel.name;
    self.SubLable.text = DataModel.desc;
}

@end
