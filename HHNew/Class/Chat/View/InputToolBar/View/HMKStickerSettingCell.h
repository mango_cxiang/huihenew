//
//  HMKStickerSettingCell.h
//  huihe
//
//  Created by jie.huang on 6/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HMKStickerCollectModel;
@interface HMKStickerSettingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *removeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *iconimage;
@property (weak, nonatomic) IBOutlet UILabel *TitleLable;

@property (nonatomic,strong) HMKStickerCollectModel *dataMode;

@end
