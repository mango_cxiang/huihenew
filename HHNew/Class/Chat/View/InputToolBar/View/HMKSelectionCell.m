//
//  HMKSelectionCell.m
//  huihe
//
//  Created by jie.huang on 15/2/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKSelectionCell.h"
#import "HMKSelectModel.h"
@implementation HMKSelectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.AddBtn.layer.borderWidth = 0.5;
    self.AddBtn.layer.borderColor = RGB_Color(26, 173, 25).CGColor;
    self.AddBtn.layer.cornerRadius = 4;
    [self.AddBtn setTitleColor:RGB_Color(26, 173, 25) forState:UIControlStateNormal];
    self.AddBtn.layer.masksToBounds = YES;
}



- (void)setDataModel:(HMKSelectModel *)DataModel{
    
    _DataModel = DataModel;
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:DataModel.cover_url]];
    self.TitleLbale.text = DataModel.name;
    self.SubTitleLable.text = DataModel.desc;
}

@end
