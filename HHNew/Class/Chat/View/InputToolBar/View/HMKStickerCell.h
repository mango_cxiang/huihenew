//
//  HMKStickerCell.h
//  huihe
//
//  Created by jie.huang on 9/1/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYImage.h"
@class HMKStickerMallModel;
@class HMKStickerSearchDetatlsModel;
@interface HMKStickerCell : UICollectionViewCell

@property (nonatomic,strong) UIImageView *imgView;

@property (nonatomic,strong) YYAnimatedImageView *GifImageView;

@property (nonatomic,strong) UILabel *text;

@property (nonatomic,strong) HMKStickerMallModel *DataModel;

@property (nonatomic,strong) HMKStickerSearchDetatlsModel *detailsMode;

@end
