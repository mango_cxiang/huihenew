//
//  HMJInputFunctionView.h
//  huihe
//
//  Created by Six on 16/3/8.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMJTextView.h"
#import "HMJRecordView.h"

@class HMJInputFunctionView;

@protocol HMJInputFunctionViewDelegate <NSObject>

@optional

//语音按钮 被点击
-(void)didClickOnVoiceButtonWithSelected:(BOOL)selected;
//表情按钮 被点击
-(void)didClickOnEmojiButtonWithSelected:(BOOL)selected;
//加号按钮 被点击
-(void)didClickOnAddButtonWithSelected:(BOOL)selected;
/**
 *  发送消息内容
 *
 *  @param message 输入框内容
 */
-(void)didSendMesage:(NSString *)message;
/*
 * @某人
 */
-(void)mentioneSomeOne;
/**
 *  录音完成数据回调
 */
-(void)didFinishRecoingVoiceAction:(NSDictionary *)voiceDic;
/// 选择图片回调
/// @param imageArray imageArray description
-(void)didSendImage:(NSArray *)imageArray;
/// 视频回调
/// @param videoUrl videoUrl description
-(void)didSendVideo:(NSURL *)videoUrl;
@end
/**
 *  输入功能视图
 */
@interface HMJInputFunctionView : UIView<HMJTextViewDelegate , UITextViewDelegate>

// 代理
@property (nonatomic , weak) id<HMJInputFunctionViewDelegate> delegate;

@property(nonatomic,assign)BOOL isGroup;
//让监听失效
@property(nonatomic,assign)BOOL isLoseEfficacy;

-(void)hideKeyBoard;

@end
