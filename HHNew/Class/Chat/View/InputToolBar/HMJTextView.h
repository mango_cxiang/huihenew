//
//  HMJTextView.h
//  huihe
//
//  Created by Six on 16/3/8.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HMJTextView;

@protocol HMJTextViewDelegate <NSObject>

@optional
- (void)textView:(HMJTextView *)textView willChangeHeight:(CGFloat)height;
- (void)textView:(HMJTextView *)textView didChangeHeight:(CGFloat)height;

@end

/**
 *  自定制输入框
 */
@interface HMJTextView : UITextView

@property (nonatomic , weak) id<HMJTextViewDelegate> textViewDelegate;
@property (nonatomic , strong) UIResponder * responder;

// 占位符
@property (nonatomic , copy) NSString * placeholder;
// 占位符 颜色
@property (nonatomic , strong) UIColor * placeholderColor;
// 自动增高行数
@property (nonatomic , assign) NSUInteger lineNum;

 @property (nonatomic,strong) UIButton *rightButton;

 @property (nonatomic,strong) UIView *TestView;

@end
