//
//  BaseTableViewController.m
//  huihe
//
//  Created by changle on 2016/12/13.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController ()
@property (nonatomic) UITableViewStyle tableViewStyle;
@end

@implementation BaseTableViewController

- (void)dealloc
{
    
}

- (void)creatWithTableViewStyle:(UITableViewStyle)style
{
    [self.tableView removeFromSuperview];
    self.tableView = nil;
    UITableView * tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:style];
    self.tableView = tableView;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundColor = RGB_Color(247, 247, 247);
    tableView.showsVerticalScrollIndicator = NO;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:self.tableView];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self creatWithTableViewStyle:UITableViewStylePlain];
}

- (void)createTableView
{
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tableView.userInteractionEnabled = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 24;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
    
    switch (indexPath.row) {
        case 0:
        {
            break;
        }
            
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    [cell setSelected:NO animated:YES];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)supportSearchBar
{
    //搜索展示控制器
    self.searchController = [[UISearchController alloc]initWithSearchResultsController:nil];
    self.searchController.searchBar.frame = CGRectMake(0, 0, 0, 44);
    //搜索栏表头视图
    self.tableView.tableHeaderView = self.searchController.searchBar;
    [self.searchController.searchBar sizeToFit];
    //背景颜色
    self.searchController.searchBar.tintColor = RGB_Color(37, 171 , 39);
    [self.searchController.searchBar setBackgroundImage:[UIImage new]];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc]init];
}


#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{

}

-(NSMutableArray *)searchArray
{
    if (!_searchArray)
    {
        _searchArray = [NSMutableArray array];
    }
    return _searchArray;
}

-(NSMutableArray *)dataArray
{
    if (!_dataArray)
    {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}


#pragma mark - 协议 UISearchBarDelegate
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self setNeedsStatusBarAppearanceUpdate];
    [searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [self setNeedsStatusBarAppearanceUpdate];
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

- (void)creatTableViewHeaderView
{
}

- (void)creatTableViewFooterView
{
}

#pragma mark - UISearchResultsUpdating Method
#pragma mark 监听者搜索框中的值的变化

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController

{
    
}

- (void)didDismissSearchController:(UISearchController *)searchController
{
    
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath

{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableViewSepatorSet{
    if ([self respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView  setLayoutMargins:UIEdgeInsetsZero];
    }
}



@end
