//
//  HMJRecordView.m
//  huihe
//
//  Created by Six on 16/3/10.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJRecordView.h"

@interface HMJRecordView ()
{
    UIView *_coverMaskView;
    UIImageView *_yinjieBtn;
    dispatch_source_t _Dtimer;
}
// 定时器
@property (nonatomic , strong) NSTimer * timer;
// 显示图片
@property (nonatomic , strong) UIImageView * imageView;
// 提示文字
@property (nonatomic , strong) UILabel * textLabel;
// 时间
@property (nonatomic , strong) UILabel * timeLabel;

@end

@implementation HMJRecordView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        // 透明浮层
        UIView * bgView = [[UIView alloc] initWithFrame:self.bounds];
        bgView.backgroundColor = UIColorFromRGBA(0x000000, 0.5);
        [bgView viewWithRadis:5];
        [self addSubview:bgView];
        
        // 显示图片
        CGFloat imageViewX = 35;
        CGFloat imageViewY = 25;
        CGFloat imageViewWidth = 38;
        CGFloat imageViewHeight = 65;
        CGFloat itemSpace = 10;
        UIImageView * imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mkf_0430"]];
        self.imageView = imageView;
        imageView.frame = CGRectMake(imageViewX, imageViewY, imageViewWidth, imageViewHeight);
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:imageView];
        
        _coverMaskView = [[UIView alloc] initWithFrame:CGRectMake(imageView.right + itemSpace, imageView.bottom, 2, 4)];
        _coverMaskView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_coverMaskView];

        _yinjieBtn = [[UIImageView alloc] initWithFrame:CGRectMake(imageView.right + itemSpace, imageView.y, 20, 65)];
        _yinjieBtn.image = [UIImage imageNamed:@"volume_0430"];
        [self addSubview:_yinjieBtn];
        
        // 提示文字
        CGFloat textLabelX = 5;
        CGFloat textLabelHeight = 26;
        UILabel * textLabel = [[UILabel alloc] initWithFrame:CGRectMake(textLabelX, self.height - textLabelHeight - textLabelX, self.width - 2 * textLabelX, textLabelHeight)];
        self.textLabel = textLabel;
        textLabel.backgroundColor = [UIColor clearColor];
        textLabel.font = SystemFont(14);
        textLabel.text = @"手指上滑，取消发送";
        textLabel.textColor = UIColorFromRGB(0xffffff);
        textLabel.textAlignment = NSTextAlignmentCenter;
        [textLabel viewWithRadis:2];
        [self addSubview:textLabel];
        
        //提示时间
        UILabel * timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(imageViewX, imageViewY, 85, 96)];
        self.timeLabel = timeLabel;
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.font = SystemFont(80);
        timeLabel.hidden = YES;
        timeLabel.textColor = UIColorFromRGB(0xffffff);
        timeLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:timeLabel];

    }
    return self;
}

// 录音按钮按下
- (void)recordButtonTouchDown
{
    self.imageView.image = [UIImage imageNamed:@"mkf_0430"];
    self.imageView.hidden = NO;
    _coverMaskView.hidden = NO;
    _yinjieBtn.hidden = NO;
    self.textLabel.text = @"手指上滑，取消发送";
    self.textLabel.backgroundColor = [UIColor clearColor];
    self.textLabel.font = SystemFont(14);
    self.timeLabel.hidden = YES;
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(autoChangeImage:) userInfo:nil repeats:YES];
    self.timer = timer;
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    
    
    __block int timeout=60;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _Dtimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_Dtimer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_Dtimer, ^{
        if(timeout<=0){
            dispatch_source_cancel(self->_Dtimer);
            dispatch_async(dispatch_get_main_queue(), ^{
                self.timeLabel.text = @"0";
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                if (timeout < 11) {
                    if(self.timeLabel.hidden==YES){
                        self.timeLabel.hidden = NO;
                        self.imageView.hidden = YES;
                        self->_coverMaskView.hidden = YES;
                        self->_yinjieBtn.hidden = YES;
                    }
                    self.timeLabel.text = [NSString stringWithFormat:@"%d",timeout];
                }
            });
            timeout--;
        }
    });
    dispatch_resume(_Dtimer);
}

// 手指在录音按钮内部时离开
- (void)recordButtonTouchUpInside
{
    if (_Dtimer !=nil) {
        dispatch_source_cancel(_Dtimer);
        _Dtimer = nil;
    }
    [self.timer invalidate];
}

// 手指在录音按钮外部时离开
- (void)recordButtonTouchUpOutside
{
    if (_Dtimer !=nil) {
        dispatch_source_cancel(_Dtimer);
        _Dtimer = nil;
    }
    [self.timer invalidate];
}

// 手指移动到录音按钮内部
- (void)recordButtonDragInside
{
    [self changeItemFrame:NO];
}

// 手指移动到录音按钮外部
- (void)recordButtonDragOutside
{
    [self changeItemFrame:YES];
}


- (void)changeItemFrame:(BOOL)change{
    if(change){
        self.imageView.image = [UIImage imageNamed:@"quxiaofasong_0430"];
        self.imageView.width = 85;
        self.imageView.height = 75;
        _coverMaskView.hidden = change;
        _yinjieBtn.hidden = change;
        self.textLabel.text = @"松开手指，取消发送";
        self.textLabel.backgroundColor = UIColorFromRGB(0x993a3a);
        self.textLabel.font = BoldFont(14);
    }else{
        self.imageView.image = [UIImage imageNamed:@"mkf_0430"];
        self.imageView.width = 38;
        self.imageView.height = 65;
        _coverMaskView.hidden = change;
        _yinjieBtn.hidden = change;
        self.textLabel.text = @"手指上滑，取消发送";
        self.textLabel.backgroundColor = [UIColor clearColor];
        self.textLabel.font = SystemFont(14);
    }
}

- (void)autoChangeImage:(NSTimer *)timer
{
    if (self.avRecorder == nil) {
        return;
    }
    [_avRecorder updateMeters];
    float avg = [self.avRecorder averagePowerForChannel:0];
    float minValue = -30;
    float range = 30;
    float outRange = 120;
    if (avg < minValue) {
        avg = minValue;
    }
    float decibels = (avg + range) / range * outRange;
    _coverMaskView.layer.frame = CGRectMake(0, _yinjieBtn.frame.size.height - decibels * _yinjieBtn.frame.size.height / 100, _yinjieBtn.frame.size.width, _yinjieBtn.frame.size.height);
    [_yinjieBtn.layer setMask:_coverMaskView.layer];
    if (self.avRecorder.currentTime > 60) {
        [self.delegate recordViewReachMaxRecordTime];
        [self resetTimer];
        return;
    }
}
-(void)resetTimer{
    if (!_timer)
        return;
    
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
    
    if (_Dtimer !=nil) {
        dispatch_source_cancel(_Dtimer);
        _Dtimer = nil;
    }
}
@end
