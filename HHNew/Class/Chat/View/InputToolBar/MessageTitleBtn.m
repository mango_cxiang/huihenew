//
//  MessageTitleBtn.m
//  huihe
//
//  Created by jie.huang on 7/11/18.
//  Copyright © 2018年 BQ. All rights reserved.
//

#import "MessageTitleBtn.h"

@implementation MessageTitleBtn

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        // 设置按钮颜色
        [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        [self setBackgroundImage:[UIImage imageNamed:@"switch_black"] forState:UIControlStateSelected];
    }
    return self;
}

- (void)setHighlighted:(BOOL)highlighted {}
@end
