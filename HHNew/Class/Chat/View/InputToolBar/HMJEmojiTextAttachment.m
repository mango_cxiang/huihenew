//
//  HMJEmojiTextAttachment.m
//  huihe
//
//  Created by Six on 16/3/7.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJEmojiTextAttachment.h"

@implementation HMJEmojiTextAttachment

- (CGRect)attachmentBoundsForTextContainer:(NSTextContainer *)textContainer proposedLineFragment:(CGRect)lineFrag glyphPosition:(CGPoint)position characterIndex:(NSUInteger)charIndex
{
    CGRect rect = CGRectZero;
    rect.origin.y = self.descender;
    rect.size.width = lineFrag.size.height;
    rect.size.height = lineFrag.size.height;
    return rect;
}

@end
