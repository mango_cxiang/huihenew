//
//  HMKOneEmojiController.m
//  huihe
//
//  Created by jie.huang on 11/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKOneEmojiController.h"
#import "YYImage.h"
#import "HMKStickerNextController.h"
@interface HMKOneEmojiController ()
@property (weak, nonatomic) IBOutlet YYAnimatedImageView *GifImageView;
@property (weak, nonatomic) IBOutlet UIButton *AddBtn;
@property (weak, nonatomic) IBOutlet UILabel *TitleLable;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ViewBottom;

@end

@implementation HMKOneEmojiController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.Model.phiz_desc;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.AddBtn viewWithRadis:4];
    self.AddBtn.layer.borderWidth = 1;
    if ([self.Model.isAdd isEqualToString:@"1"]) {
        
        self.AddBtn.layer.borderColor = RGB_Color(221, 221, 221).CGColor;
        [self.AddBtn setTitleColor:RGB_Color(221, 221, 221) forState:UIControlStateNormal];
        [self.AddBtn setTitle:@"已添加" forState:UIControlStateNormal];
        self.AddBtn.enabled = NO;
    }else{
        self.AddBtn.layer.borderColor = RGB_Color(53, 53, 53).CGColor;
        [self.AddBtn setTitleColor:RGB_Color(53, 53, 53) forState:UIControlStateNormal];
        [self.AddBtn setTitle:@"添加到表情" forState:UIControlStateNormal];
        self.AddBtn.enabled = YES;
    }
    [self.GifImageView sd_setImageWithURL:[self.Model.master_url mj_url]];
    
    self.TitleLable.text = self.Model.name;
    
    [self.iconImage sd_setImageWithURL:[self.Model.chat_url mj_url]];
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.view.backgroundColor = [UIColor whiteColor];
}

- (IBAction)AddEmoji:(id)sender {
    
//
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    [dictparame setObject:self.Model.master_url forKey:@"imgUrl"];
//    [dictparame setObject:self.Model.phiz_desc forKey:@"name"];
//    [dictparame setObject:self.Model.ID forKey:@"next_id"];
//    [dictparame setObject:self.Model.next_url forKey:@"nextUrl"];
//    [dictparame setObject:self.Model.imgType forKey:@"imgType"];
//    [dictparame setObject:self.Model.p_id forKey:@"phiz_id"];
//
//    [HMJBusinessManager EmojiPOST:@"/im/appCollection/insertAppCollection" parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//
//        self.AddBtn.layer.borderColor = RGB(221, 221, 221).CGColor;
//        [self.AddBtn setTitleColor:RGB(221, 221, 221) forState:UIControlStateNormal];
//        [self.AddBtn setTitle:@"已添加" forState:UIControlStateNormal];
//        self.AddBtn.enabled = NO;
//        [self getemoji];
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//    }];
}
- (IBAction)jumpNext:(id)sender {
    
    HMKStickerNextController *vc = [HMKStickerNextController new];
    vc.StickerID = self.Model.p_id;
    vc.TitleText = self.Model.name;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)getemoji{
    
    
//    [[HMJShowLoding sharedInstance] showLoading];
//    [HHGlobals saveEmojiDict:nil];
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//
//    [HMJBusinessManager GET:@"/im/appAdd/appAddList" Parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//
//
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//            NSMutableArray *emojiArr = [NSMutableArray array];
//
//            NSArray *DataArry = dic[@"data"][@"info"];
//            for (NSDictionary *dict in DataArry) {
//
//                NSMutableDictionary *Emojidict = [NSMutableDictionary dictionary];
//
//                [Emojidict setObject:dict[@"chat_url"] forKey:@"cover_pic"];
//                [Emojidict setObject:dict[@"id"] forKey:@"EmojiID"];
//
//                NSMutableArray *emoticons = [NSMutableArray array];
//
//                for (NSDictionary *SubDict in dict[@"phiz_subList"]) {
//
//                    NSMutableDictionary *icondict = [NSMutableDictionary dictionary];
//
//                    [icondict setValue:SubDict[@"next_url"] forKey:@"image"];
//                    [icondict setValue:SubDict[@"phiz_desc"] forKey:@"desc"];
//                    [icondict setValue:SubDict[@"master_url"] forKey:@"imageGif"];
//                    [icondict setValue:SubDict[@"id"] forKey:@"EmojiID"];
//
//                    [emoticons addObject:icondict];
//                }
//
//                [Emojidict setValue:emoticons forKey:@"emoticons"];
//
//                [emojiArr addObject:Emojidict];
//            }
//
//            [HHGlobals saveEmojiDict:emojiArr];
//        }
//
//        [self getCollectionemoji];
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
}


-(void)getCollectionemoji{
    
    
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//    
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    
//    [HMJBusinessManager GET:@"/im/appAdd/listCollection" Parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//        
//        
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//            
//            
//            
//            NSArray *DataArry = dic[@"data"][@"info"][@"collection"];
//            
//            NSMutableDictionary *Emojidict = [NSMutableDictionary dictionary];
//            
//            [Emojidict setObject:dic[@"data"][@"info"][@"heartUrl"] forKey:@"cover_pic"];
//            
//            NSMutableArray *emoticons = [NSMutableArray array];
//            
//            for (NSDictionary *SubDict in DataArry) {
//                
//                NSMutableDictionary *icondict = [NSMutableDictionary dictionary];
//                
//                [icondict setValue:SubDict[@"nextUrl"] forKey:@"image"];
//                [icondict setValue:SubDict[@"name"] forKey:@"desc"];
//                [icondict setValue:SubDict[@"imgUrl"] forKey:@"imageGif"];
//                [icondict setValue:SubDict[@"next_id"] forKey:@"EmojiID"];
//                
//                [emoticons addObject:icondict];
//            }
//            
//            [Emojidict setValue:emoticons forKey:@"emoticons"];
//            
//            NSMutableArray *arry = [HHGlobals getEmojiDict].mutableCopy;
//            
//            [arry insertObject:Emojidict atIndex:0];
//            
//            [HHGlobals saveEmojiDict:arry];
//            
//        }
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//        
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
}


@end
