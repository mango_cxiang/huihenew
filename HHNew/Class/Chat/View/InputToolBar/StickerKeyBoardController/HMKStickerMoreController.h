//
//  HMKStickerMoreController.h
//  huihe
//
//  Created by jie.huang on 5/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "BaseTableViewController.h"

@interface HMKStickerMoreController : BaseTableViewController

@property (nonatomic,strong) NSString *searchText;
@end
