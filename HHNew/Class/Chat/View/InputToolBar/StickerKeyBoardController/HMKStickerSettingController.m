//
//  HMKStickerSettingController.m
//  huihe
//
//  Created by jie.huang on 6/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKStickerSettingController.h"
#import "HMKStickerSettingCell.h"
#import "HMKStickerCollectModel.h"
#import "HMKAllAddEmojiController.h"
#import "HLCardController.h"
@interface HMKStickerSettingController ()

@property (nonatomic,strong) NSMutableArray<HMKStickerCollectModel*> *dataMode;

@end

@implementation HMKStickerSettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = @"我的表情";
    [self creatWithTableViewStyle:UITableViewStyleGrouped];
    
    [self setRightBarButtonItemWithImageArray:@[@"tianjia"] BeginTag:100];
    
    self.tableView.rowHeight = 50;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cellID"];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([HMKStickerSettingCell class]) bundle:nil] forCellReuseIdentifier:@"AddCellID"];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AddEmoji) name:@"ADDEMOJI_SEARCH" object:nil];
    
    [self getCollectList];
}

- (void)rightBarButtonItemAction:(id)sender{
//    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//    [dict setObject:@"h5_url" forKey:@"key"];
//    [dict setObject:@"phiz_guide" forKey:@"code"];
//    [dict setObject:[NSString acquireUserId] forKey:@"userId"];
//    WEAKSELF
//    [HMJBusinessManager GET:@"/config/get" Parameters:dict success:^(NSDictionary *dic, NSInteger code) {
//        NSInteger codeStr = [dic[@"code"] integerValue];
//        if(codeStr==1){
//            //NSString *urlStr = [NSString stringWithFormat:@"%@",dic[@"data"][@"detailName"]];
//            NSString *urlStr = [NSString stringWithFormat:@"%@?userid=%@&nickname=%@&headpic=%@&timestamp=%@",dic[@"data"][@"detailName"],[NSString acquireUserId],[HMJAccountModel myself].nickName,[HMJAccountModel myself].headUrl,[NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]]];
//            YXZIKBaseWKWebViewController *resultVC = [[YXZIKBaseWKWebViewController alloc] init];
//            resultVC.hidesBottomBarWhenPushed = YES;
//            urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//            [resultVC loadWebURLSring:urlStr];
//            [weakSelf.navigationController pushViewController:resultVC animated:YES];
//        }
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//    }];
}

-(void)AddEmoji{
    
    [self getCollectList];
}
- (void)dealloc
{
     [[NSNotificationCenter defaultCenter]removeObserver:self];
}
- (void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
}

-(void)getCollectList{
    
    
//    NSMutableDictionary *parameDict = [NSMutableDictionary dictionary];
//    
//    [parameDict setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    
//    [HMJBusinessManager GET:@"/im/appAdd/listAppAdd" Parameters:parameDict success:^(NSDictionary *dic, NSInteger code) {
//        self.dataMode = [HMKStickerCollectModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"info"]];
//        [self.tableView reloadData];
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//        
//    }];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.dataMode.count > 0? 3 : 2;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.dataMode.count > 0) {

        return section == 1 ? self.dataMode.count : 1;
    }else{
        return 1;
    }

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.dataMode.count > 0) {
        if (indexPath.section == 2) {
            HMKAllAddEmojiController *vc = [HMKAllAddEmojiController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.section == 0){
            HLCardController *vc = [HLCardController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else{
        if (indexPath.section == 1) {
            HMKAllAddEmojiController *vc = [HMKAllAddEmojiController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.section == 0){
            HLCardController *vc = [HLCardController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (self.dataMode.count > 0) {
        
        if (indexPath.section == 0) {

            UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cellID"];
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            cell.imageView.image = [UIImage imageNamed:@"dangebiaoqing_0219"];
            cell.textLabel.text = @"添加的单个表情";
            return cell;

        }else if (indexPath.section == 2){

            UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cellID"];
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.text = @"整套表情添加记录";
            return cell;
        }else{

            HMKStickerSettingCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"AddCellID"];
            cell.dataMode = self.dataMode[indexPath.row];

            [cell.removeBtn addTarget:self action:@selector(removeMoreClick:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
        
    }else{
     
        if (indexPath.section == 0) {

            UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cellID"];
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            cell.imageView.image = [UIImage imageNamed:@"dangebiaoqing_0219"];
            cell.textLabel.text = @"添加的单个表情";
            return cell;

        }else if (indexPath.section == 1){

            UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cellID"];
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.text = @"整套表情添加记录";
            return cell;
        }
    }
    return nil;
}

-(void)removeMoreClick:(UIButton *)btn{
    
    
//    [[HMJShowLoding sharedInstance]showLoading];
//    NSInteger indext = [self.tableView indexPathForCell:((HMKStickerSettingCell *)[[btn superview]superview])].row;
//    NSMutableDictionary *dictParame = [NSMutableDictionary dictionary];
//    if(self.dataMode.count==0){
//        return;
//    }
//    [dictParame setObject:self.dataMode[indext].ID forKey:@"id"];
//
//    [HMJBusinessManager GET:@"/im/appAdd/isDelete" Parameters:dictParame success:^(NSDictionary *dic, NSInteger code) {
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"REMOVE_SEARCH" object:nil];
//        if(self.dataMode.count!=0){
//          [self.dataMode removeObjectAtIndex:indext];
//        }
//        [self getemoji];
//        [self.tableView reloadData];
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//         [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
//
   
    
}

-(void)getemoji{
    
    
//    [[HMJShowLoding sharedInstance] showLoading];
//    [HHGlobals saveEmojiDict:nil];
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//
//    [HMJBusinessManager GET:@"/im/appAdd/appAddList" Parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//
//
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//            NSMutableArray *emojiArr = [NSMutableArray array];
//
//            NSArray *DataArry = dic[@"data"][@"info"];
//            for (NSDictionary *dict in DataArry) {
//
//                NSMutableDictionary *Emojidict = [NSMutableDictionary dictionary];
//
//                [Emojidict setObject:dict[@"chat_url"] forKey:@"cover_pic"];
//                [Emojidict setObject:dict[@"id"] forKey:@"EmojiID"];
//
//                NSMutableArray *emoticons = [NSMutableArray array];
//
//                for (NSDictionary *SubDict in dict[@"phiz_subList"]) {
//
//                    NSMutableDictionary *icondict = [NSMutableDictionary dictionary];
//
//                    [icondict setValue:SubDict[@"next_url"] forKey:@"image"];
//                    [icondict setValue:SubDict[@"phiz_desc"] forKey:@"desc"];
//                    [icondict setValue:SubDict[@"master_url"] forKey:@"imageGif"];
//                    [icondict setValue:SubDict[@"id"] forKey:@"EmojiID"];
//
//                    [emoticons addObject:icondict];
//                }
//
//                [Emojidict setValue:emoticons forKey:@"emoticons"];
//
//                [emojiArr addObject:Emojidict];
//            }
//
//            [HHGlobals saveEmojiDict:emojiArr];
//        }
//
//        [self getCollectionemoji];
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
}


-(void)getCollectionemoji{
    
    
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//    
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    
//    [HMJBusinessManager GET:@"/im/appAdd/listCollection" Parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//        
//        
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//            
//            
//            
//            NSArray *DataArry = dic[@"data"][@"info"][@"collection"];
//            
//            NSMutableDictionary *Emojidict = [NSMutableDictionary dictionary];
//            
//            [Emojidict setObject:dic[@"data"][@"info"][@"heartUrl"] forKey:@"cover_pic"];
//            
//            NSMutableArray *emoticons = [NSMutableArray array];
//            
//            for (NSDictionary *SubDict in DataArry) {
//                
//                NSMutableDictionary *icondict = [NSMutableDictionary dictionary];
//                
//                [icondict setValue:SubDict[@"nextUrl"] forKey:@"image"];
//                [icondict setValue:SubDict[@"name"] forKey:@"desc"];
//                [icondict setValue:SubDict[@"imgUrl"] forKey:@"imageGif"];
//                [icondict setValue:SubDict[@"next_id"] forKey:@"EmojiID"];
//                
//                [emoticons addObject:icondict];
//            }
//            
//            [Emojidict setValue:emoticons forKey:@"emoticons"];
//            
//            NSMutableArray *arry = [HHGlobals getEmojiDict].mutableCopy;
//            
//            [arry insertObject:Emojidict atIndex:0];
//            
//            [HHGlobals saveEmojiDict:arry];
//            
//        }
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//        
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    if (self.dataMode.count > 0) {
        if (section == 1) {

            return @"聊天面板中的整套表情";
        }
    }
    return nil;

}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (self.dataMode.count > 0) {
        if (section == 0) {
            return 0;
        }else if (section == 1){
            return 44;
        }else{
            return 20;
        }
    }else{
        if (section == 0) {
            return 0;
        }else if (section == 1){
            return 20;
        }
        return 0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

@end
