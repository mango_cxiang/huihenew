//
//  HMKSelectionSticerController.m
//  huihe
//
//  Created by jie.huang on 14/2/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKSelectionSticerController.h"
#import "HMKSelectionCell.h"
#import "HMKSelectModel.h"
#import "HMKSelectionHeardView.h"
#import "SDCycleScrollView.h"
#import "HMKHeardSelectionCell.h"
#import "HMKHeardSelectModel.h"
#import "HMKStickerNextController.h"
#import "HMKStickerSearchController.h"
#import "JKRSearchController.h"
#import "UIViewController+JKRStatusBarStyle.h"

@interface HMKSelectionSticerController ()<SDCycleScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,assign) NSInteger index;

@property (nonatomic,strong) NSMutableArray<HMKSelectModel*> *dataMode;

@property (nonatomic,strong) NSMutableArray<HMKHeardSelectModel*> *HearddataMode;

@property (nonatomic,weak) HMKSelectionHeardView *hearView;

@property (nonatomic,strong) NSMutableArray *heardBannerArry;

@property (nonatomic,strong) UITableView *Heardtableview;

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,strong) NSMutableArray *ArryID;

@property (nonatomic,strong) NSMutableArray *NameArr;

@property (nonatomic, strong) JKRSearchController *searchController;

@property (nonatomic,strong) NSString *SearchText;

@property (nonatomic,weak) HMKStickerSearchController *StickerSearchController;

@property (nonatomic,strong) NSMutableArray *selectedArray;

@end

@implementation HMKSelectionSticerController


static  NSString * const SelectionID = @"SelectionID";


static  NSString * const HeardTabID = @"HeardTabID";


-(NSMutableArray *)heardBannerArry{
    
    if (!_heardBannerArry) {
        
        _heardBannerArry = [NSMutableArray array];
    }
    return _heardBannerArry;
}

-(NSMutableArray *)ArryID{
    
    if (!_ArryID) {
        
        _ArryID = [NSMutableArray array];
    }
    return _ArryID;
}

-(NSMutableArray *)NameArr{
    
    if (!_NameArr) {
        
        _NameArr = [NSMutableArray array];
    }
    return _NameArr;
}

-(NSMutableArray *)selectedArray{
    
    if (!_selectedArray) {
        
        _selectedArray = [NSMutableArray array];
    }
    return _selectedArray;
}

- (JKRSearchController *)searchController {
    if (!_searchController) {
        _searchController = [[JKRSearchController alloc] initWithSearchResultsController:self];
        _searchController.searchBar.placeholder = @"搜索表情";
        UIButton *btn = [[UIButton alloc]initWithFrame:_searchController.searchBar.bounds];
        [_searchController.searchBar addSubview:btn];
        [btn addTarget:self action:@selector(endDetatils) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _searchController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = RGB_Color(239, 239, 244);
    self.tableView = [[UITableView alloc]init];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, kTabBarHeight, 0);
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([HMKSelectionCell class]) bundle:nil] forCellReuseIdentifier:SelectionID];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = 80;
    self.tableView.estimatedRowHeight=80.0f;
    self.tableView.tableHeaderView = self.hearView;
    self.tableView.tableFooterView = [UIView new];
    [self.view addSubview:self.tableView];
    
    
    self.Heardtableview = [[UITableView alloc]init];
    self.Heardtableview.frame = CGRectMake(0, 0, SCREEN_WIDTH, 240);
    [self.Heardtableview registerNib:[UINib nibWithNibName:NSStringFromClass([HMKHeardSelectionCell class]) bundle:nil] forCellReuseIdentifier:HeardTabID];
    self.Heardtableview.delegate = self;
    self.Heardtableview.dataSource = self;
    self.Heardtableview.rowHeight = 80;
    self.Heardtableview.estimatedRowHeight=80.0f;
    self.Heardtableview.tableFooterView = [UIView new];
    [self.hearView.TabView addSubview:self.Heardtableview];
    
    
    
    [self getHeardViewbanner];
    
    [self getStickerData];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreTopics)];
    
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(RemoveEmoji) name:@"REMOVE_SEARCH" object:nil];
    
}

-(void)RemoveEmoji{
    
    [self getHeardViewbanner];
    
    [self getStickerData];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
-(void)endDetatils{
    
    HMKStickerSearchController *vc = [HMKStickerSearchController new];
    [self.navigationController pushViewController: vc animated:YES];
}


-(void)getHeardViewbanner{
    
    
//
//    NSMutableDictionary *parameDict = [NSMutableDictionary dictionary];
//
//    [parameDict setObject:[NSString acquireUserId] forKey:@"appUserId"];
//
//    [HMJBusinessManager GET:@"/bms/appPhiz/findMany" Parameters:parameDict success:^(NSDictionary *dic, NSInteger code) {
//
//
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//
//            [self.heardBannerArry removeAllObjects];
//            [self.ArryID removeAllObjects];
//            [self.NameArr removeAllObjects];
//
//            for (NSDictionary *dict in dic[@"data"][@"info"][@"bannerImage"]) {
//
//                [self.heardBannerArry addObject:dict[@"master_url"]];
//                [self.ArryID addObject:dict[@"id"]];
//                [self.NameArr addObject:dict[@"name"]];
//            }
//            self.HearddataMode = [HMKHeardSelectModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"info"][@"Recommended"]];
//
//        }else{
//            [[HMJShowLoding sharedInstance]showText:dic[@"data"][@"info"]];
//        }
//        if (self.HearddataMode.count > 0) {
//
//            self.hearView.height = 45 + 53 + 53 + self.HearddataMode.count * 80 + (SCREEN_WIDTH * 0.3846);
//            self.Heardtableview.height = self.HearddataMode.count * 80;
//
//            self.Heardtableview.hidden = NO;
//            self.hearView.RecommendedView.hidden = NO;
//
//
//        }else{
//            self.Heardtableview.hidden = YES;
//            self.hearView.RecommendedView.hidden = YES;
//            self.hearView.RecommendedView.hidden = YES;
//            self.hearView.RecLable.hidden = YES;
//
//            self.hearView.height = 45 + 53  + (SCREEN_WIDTH * 0.3846);
//            self.Heardtableview.height = 0;
//            self.hearView.RecHeight.constant = 0;
//            self.hearView.RecLable.height = 0;
//        }
//
//
//        _hearView.BannerView.imageURLStringsGroup = self.heardBannerArry;
//
//        [self.Heardtableview reloadData];
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//    }];
    
    
    
}

-(HMKSelectionHeardView *)hearView{
    
    if (!_hearView) {
     
        _hearView = [HMKSelectionHeardView viewFromxib];
        _hearView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 45 + 53 + 53 + 240 + (SCREEN_WIDTH * 0.3846));
        _hearView.BannerView.delegate = self;
        
        _hearView.HerdSearchView.backgroundColor = RGB_Color(247, 247, 247);
        _hearView.backgroundColor = RGB_Color(247, 247, 247);
        [_hearView addSubview:self.searchController.searchBar];
        self.jkr_lightStatusBar = YES;
    }
    return _hearView;
    
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    HMKStickerNextController *vc = [HMKStickerNextController new];
    vc.StickerID = self.ArryID[index];
    vc.TitleText = self.NameArr[index];
    vc.isPush = NO;
    [self.navigationController pushViewController:vc animated:YES];
    
}


-(void)getStickerData{
    
     [self.selectedArray removeAllObjects];
    self.index = 1;
//    NSMutableDictionary *dictParamet = [NSMutableDictionary dictionary];
//    [dictParamet setObject:@(self.index) forKey:@"pageIndex"];
//    [dictParamet setObject:@(20) forKey:@"pageSize"];
//    [dictParamet setObject:[NSString acquireUserId] forKey:@"appUserId"];
//
//
//    [HMJBusinessManager GET:@"/bms/appPhiz/findMoreMany" Parameters:dictParamet success:^(NSDictionary *dic, NSInteger code) {
//
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//
//            self.dataMode = [HMKSelectModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"info"][@"list"]];
//
//            for (int i = 0; i< self.dataMode.count; i++) {
//                HMKSelectModel *moede = self.dataMode [i];
//                if ([moede.isAdd isEqualToString:@"1"]) {
//                    [_selectedArray addObject:@(100 + i)];
//                }
//            }
//
//            [self.Heardtableview reloadData];
//            [self.tableView reloadData];
//            [self.tableView.mj_footer resetNoMoreData];
//
//        }else{
//             [[HMJShowLoding sharedInstance]showText:dic[@"data"][@"info"]];
//
//        }
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//    }];
}

-(void)loadMoreTopics{
    
    
//    [self.selectedArray removeAllObjects];
    self.index ++;
//    NSMutableDictionary *dictParamet = [NSMutableDictionary dictionary];
//    [dictParamet setObject:@(self.index) forKey:@"pageIndex"];
//    [dictParamet setObject:@(20) forKey:@"pageSize"];
//    [dictParamet setObject:[NSString acquireUserId] forKey:@"appUserId"];
//
//
//    [HMJBusinessManager GET:@"/bms/appPhiz/findMoreMany" Parameters:dictParamet success:^(NSDictionary *dic, NSInteger code) {
//
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//
//            NSArray *moreArry = [HMKSelectModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"info"][@"list"]];
//            [self.dataMode addObjectsFromArray:moreArry];
//            if (moreArry.count < 20) {
//                [self.tableView.mj_footer endRefreshingWithNoMoreData];
//            }else{
//                [self.tableView.mj_footer endRefreshing];
//            }
//
//            for (int i = 0; i< moreArry.count; i++) {
//                HMKSelectModel *moede = moreArry [i];
//                if ([moede.isAdd isEqualToString:@"1"]) {
//                    [_selectedArray addObject:@(100 + i)];
//                }
//            }
//        }else{
//            [[HMJShowLoding sharedInstance]showText:dic[@"data"][@"info"]];
//        }
//
//        [self.tableView reloadData];
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//        [self.tableView.mj_footer endRefreshing];
//
//    }];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([tableView isEqual:self.Heardtableview ]) {
        
        if (self.HearddataMode[indexPath.row]) {
            HMKStickerNextController *vc = [HMKStickerNextController new];
            vc.StickerID = self.HearddataMode[indexPath.row].ID;
            vc.TitleText = self.HearddataMode[indexPath.row].name;
            vc.isPush = NO;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }else{
        HMKStickerNextController *vc = [HMKStickerNextController new];
        vc.StickerID = self.dataMode[indexPath.row].ID;
        vc.TitleText = self.dataMode[indexPath.row].name;
        vc.isPush = NO;
        [self.navigationController pushViewController:vc animated:YES];
    }

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.Heardtableview ]) {
        return self.HearddataMode.count;
    }else{
        return self.dataMode.count;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if ([tableView isEqual:self.Heardtableview ]) {

        HMKHeardSelectionCell *cell = [self.Heardtableview dequeueReusableCellWithIdentifier:HeardTabID];
        cell.DataModel = self.HearddataMode[indexPath.row];
        if ([self.HearddataMode[indexPath.row].isAdd isEqualToString:@"1"]) {
            cell.AddBtn.layer.borderColor = RGB_Color(224, 224, 224).CGColor;
            [cell.AddBtn setTitleColor:RGB_Color(224, 224, 224) forState:UIControlStateNormal];
            [cell.AddBtn setTitle:@"已添加" forState:UIControlStateNormal];
            cell.AddBtn.enabled = NO;
        }else{
            
            cell.AddBtn.layer.borderColor = RGB_Color(26, 173, 25).CGColor;
            [cell.AddBtn setTitleColor:RGB_Color(26, 173, 25) forState:UIControlStateNormal];
            [cell.AddBtn setTitle:@"添加" forState:UIControlStateNormal];
            cell.AddBtn.enabled = YES;
        }
        [cell.AddBtn addTarget:self action:@selector(AddClick:) forControlEvents:UIControlEventTouchUpInside];
        return cell;

    }else{
    
        HMKSelectionCell *cell = [self.tableView dequeueReusableCellWithIdentifier:SelectionID];
        cell.AddBtn.tag =100 + indexPath.row;
        cell.DataModel = self.dataMode[indexPath.row];
        if ([self.selectedArray containsObject:@(cell.AddBtn.tag)]) {
        
            cell.AddBtn.selected = YES;
            cell.AddBtn.enabled = NO;
            cell.AddBtn.layer.borderColor = RGB_Color(224, 224, 224).CGColor;
            [cell.AddBtn setTitleColor:RGB_Color(224, 224, 224) forState:UIControlStateNormal];
            [cell.AddBtn setTitle:@"已添加" forState:UIControlStateNormal];
        }else{
    
            cell.AddBtn.layer.borderColor = RGB_Color(26, 173, 25).CGColor;
            [cell.AddBtn setTitleColor:RGB_Color(26, 173, 25) forState:UIControlStateNormal];
            [cell.AddBtn setTitle:@"添加" forState:UIControlStateNormal];
            cell.AddBtn.enabled = YES;
             cell.AddBtn.selected = NO;
        }
        [cell.AddBtn addTarget:self action:@selector(AddMoreClick:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    
    return nil;
}

-(void)AddClick:(UIButton *)btn{
    
//    [[HMJShowLoding sharedInstance]showLoading];
//     NSInteger indext = [self.Heardtableview indexPathForCell:((HMKHeardSelectionCell *)[[btn superview]superview])].row;
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    [dictparame setObject:self.HearddataMode[indext].chat_url forKey:@"imgUrl"];
//    [dictparame setObject:self.HearddataMode[indext].ID forKey:@"phiz_id"];
//    [dictparame setObject:self.HearddataMode[indext].name forKey:@"name"];
//
//    [HMJBusinessManager EmojiPOST:@"/im/appAdd/addAppPhiz" parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//        if ([dic[@"code"] integerValue] == 1) {
//            [[HMJShowLoding sharedInstance]hiddenLoading];
//            [[HMJShowLoding sharedInstance]showText:@"添加成功"];
//            if (btn.selected) {
//                [_selectedArray addObject:@(btn.tag)];
//            }
//            NSArray *DataArry = dic[@"data"][@"info"];
//            NSDictionary *dict = [DataArry firstObject];
//            NSMutableDictionary *Emojidict = [NSMutableDictionary dictionary];
//
//            [Emojidict setObject:dict[@"chat_url"] forKey:@"cover_pic"];
//            [Emojidict setObject:dict[@"id"] forKey:@"EmojiID"];
//
//            NSMutableArray *emoticons = [NSMutableArray array];
//
//            for (NSDictionary *SubDict in dict[@"phiz_subList"]) {
//
//                NSMutableDictionary *icondict = [NSMutableDictionary dictionary];
//
//                [icondict setValue:SubDict[@"next_url"] forKey:@"image"];
//                [icondict setValue:SubDict[@"phiz_desc"] forKey:@"desc"];
//                [icondict setValue:SubDict[@"master_url"] forKey:@"imageGif"];
//                [icondict setValue:SubDict[@"id"] forKey:@"EmojiID"];
//
//                [emoticons addObject:icondict];
//            }
//
//            [Emojidict setValue:emoticons forKey:@"emoticons"];
//
//            [HHGlobals saveEmojiDictToArr:Emojidict];
//            //        [self getemoji];
//
//            btn.layer.borderColor = RGB(224, 224, 224).CGColor;
//            [btn setTitleColor:RGB(224, 224, 224) forState:UIControlStateNormal];
//            [btn setTitle:@"已添加" forState:UIControlStateNormal];
//            [self.tableView reloadData];
//        }else {
//            [[HMJShowLoding sharedInstance]showText:@"添加失败"];
//            [[HMJShowLoding sharedInstance]hiddenLoading];
//        }
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//        [[HMJShowLoding sharedInstance]showText:@"添加失败"];
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
    
}
-(void)AddMoreClick:(UIButton *)btn{
    

//    NSInteger indext = [self.tableView indexPathForCell:((HMKSelectionCell *)[[btn superview]superview])].row;
//    btn.selected = !btn.isSelected;
//    if (btn.selected) {
//
//        NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//        [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//        [dictparame setObject:self.dataMode[indext].chat_url forKey:@"imgUrl"];
//        [dictparame setObject:self.dataMode[indext].ID forKey:@"phiz_id"];
//        [dictparame setObject:self.dataMode[indext].name forKey:@"name"];
//
//        [[HMJShowLoding sharedInstance]showLoading];
//        [HMJBusinessManager EmojiPOST:@"/im/appAdd/addPhizAppAdd" parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//
//            [[HMJShowLoding sharedInstance]hiddenLoading];
//            NSInteger errorcode = [dic[@"code"] integerValue];
//            if (errorcode == 1) {
//
//                [[HMJShowLoding sharedInstance]showText:@"添加成功"];
//                if (btn.selected) {
//                    [_selectedArray addObject:@(btn.tag)];
//                }
//                [self getemoji];
//                [self.tableView reloadData];
//
//            }else{
//               [[HMJShowLoding sharedInstance]showText:dic[@"data"][@"info"]];
//            }
//        } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//            [[HMJShowLoding sharedInstance]showText:@"添加失败"];
//            [[HMJShowLoding sharedInstance]hiddenLoading];
//        }];
//
//    }
}

#pragma mark 表情。。
-(void)getemoji{
    
    
//    [[HMJShowLoding sharedInstance] showLoading];
//    [HHGlobals saveEmojiDict:nil];
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//
//    [HMJBusinessManager GET:@"/im/appAdd/appAddList" Parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//
//
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//            NSMutableArray *emojiArr = [NSMutableArray array];
//
//            NSArray *DataArry = dic[@"data"][@"info"];
//            for (NSDictionary *dict in DataArry) {
//
//                NSMutableDictionary *Emojidict = [NSMutableDictionary dictionary];
//
//                [Emojidict setObject:dict[@"chat_url"] forKey:@"cover_pic"];
//                [Emojidict setObject:dict[@"id"] forKey:@"EmojiID"];
//
//                NSMutableArray *emoticons = [NSMutableArray array];
//
//                for (NSDictionary *SubDict in dict[@"phiz_subList"]) {
//
//                    NSMutableDictionary *icondict = [NSMutableDictionary dictionary];
//
//                    [icondict setValue:SubDict[@"next_url"] forKey:@"image"];
//                    [icondict setValue:SubDict[@"phiz_desc"] forKey:@"desc"];
//                    [icondict setValue:SubDict[@"master_url"] forKey:@"imageGif"];
//                    [icondict setValue:SubDict[@"id"] forKey:@"EmojiID"];
//
//                    [emoticons addObject:icondict];
//                }
//
//                [Emojidict setValue:emoticons forKey:@"emoticons"];
//
//                [emojiArr addObject:Emojidict];
//            }
//
//            [HHGlobals saveEmojiDict:emojiArr];
//        }
//
//        [self getCollectionemoji];
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
}


-(void)getCollectionemoji{
    
    
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//    
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    
//    [HMJBusinessManager GET:@"/im/appAdd/listCollection" Parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//        
//        
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//            
//            
//            
//            NSArray *DataArry = dic[@"data"][@"info"][@"collection"];
//            
//            NSMutableDictionary *Emojidict = [NSMutableDictionary dictionary];
//            
//            [Emojidict setObject:dic[@"data"][@"info"][@"heartUrl"] forKey:@"cover_pic"];
//            
//            NSMutableArray *emoticons = [NSMutableArray array];
//            
//            for (NSDictionary *SubDict in DataArry) {
//                
//                NSMutableDictionary *icondict = [NSMutableDictionary dictionary];
//                
//                [icondict setValue:SubDict[@"nextUrl"] forKey:@"image"];
//                [icondict setValue:SubDict[@"name"] forKey:@"desc"];
//                [icondict setValue:SubDict[@"imgUrl"] forKey:@"imageGif"];
//                [icondict setValue:SubDict[@"next_id"] forKey:@"EmojiID"];
//                
//                [emoticons addObject:icondict];
//            }
//            
//            [Emojidict setValue:emoticons forKey:@"emoticons"];
//            
//            NSMutableArray *arry = [HHGlobals getEmojiDict].mutableCopy;
//            
//            [arry insertObject:Emojidict atIndex:0];
//            
//            [HHGlobals saveEmojiDict:arry];
//            
//        }
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//        
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
}


@end
