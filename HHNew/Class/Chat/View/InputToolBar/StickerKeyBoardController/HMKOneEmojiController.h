//
//  HMKOneEmojiController.h
//  huihe
//
//  Created by jie.huang on 11/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "BaseViewController.h"
#import "HMKStickerSearchDetatlsModel.h"
@interface HMKOneEmojiController : BaseViewController

@property (nonatomic,strong) HMKStickerSearchDetatlsModel *Model;

@end
