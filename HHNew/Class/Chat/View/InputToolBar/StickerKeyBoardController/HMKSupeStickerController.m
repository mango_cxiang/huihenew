//
//  HMKSupeStickerController.m
//  huihe
//
//  Created by jie.huang on 14/2/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKSupeStickerController.h"
#import "MessageTitleBtn.h"
#import "HMKStickerDetailsController.h"
#import "HMKSelectionSticerController.h"
#import "HMKStickerSettingController.h"

@interface HMKSupeStickerController ()<UIScrollViewDelegate>

@property (nonatomic, weak) MessageTitleBtn *selectedTitleButton;

@property (nonatomic, weak) UIScrollView *scrollView;

@property (nonatomic, weak) UIView *titlesView;

@property (nonatomic,assign) NSInteger index;


@end

@implementation HMKSupeStickerController

- (void)viewDidLoad {
    [super viewDidLoad];

    
    self.navigationItem.leftBarButtonItem = nil;
    
    [self setupChildViewControllers];
    
    [self setupScrollView];
    
    [self setupTitleView];
    
    [self addChildVcView];
    
    [self setLeftBarButtonItem:@"返回"];
    
    [self setRightBarButtonItemWithImageArray:@[@"expression_set"] BeginTag:100];

}

-(void)leftBarButtonItemAction:(id)sender
{
    if(_rightSlider){
      [self.navigationController popViewControllerAnimated:YES];
    }else{
       [self dismissViewControllerAnimated:YES completion:nil];
    }
}
-(void)rightBarButtonItemAction:(id)sender
{
    HMKStickerSettingController *vc = [HMKStickerSettingController new];
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.index) {
        [self addChildVcViewIntoScrollView:self.index];
    }
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    
//     [[NSNotificationCenter defaultCenter] postNotificationName:@"SEARCH_CANCEL_NOTIFICATION" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATA_CANCEL_NOTIFICATION" object:nil];
    
}

-(void)setupChildViewControllers{
    
    HMKSelectionSticerController *Vc1 = [[HMKSelectionSticerController alloc]init];
    [self addChildViewController:Vc1];

    HMKStickerDetailsController *Vc2 = [[HMKStickerDetailsController alloc]init];
    [self addChildViewController:Vc2];
}

-(void)setupScrollView
{
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIScrollView *scrollView = [[UIScrollView alloc]init];
    scrollView.delegate =self;
    scrollView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
    scrollView.pagingEnabled = YES;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.contentSize = CGSizeMake(0, 0);
    [self.view addSubview:scrollView];
    self.scrollView= scrollView;
}

-(void)setupTitleView
{
    UIView *titleView = [[UIView alloc]init];
    
    titleView.backgroundColor = [[UIColor whiteColor]colorWithAlphaComponent:0];
    titleView.layer.borderColor = [UIColor blackColor].CGColor;
    titleView.layer.cornerRadius = 5;
    titleView.clipsToBounds = YES;
    titleView.layer.borderWidth = 1.0;
    titleView.frame = CGRectMake(self.view.width/2-100, 30, 180, 28);
    //    [self.view addSubview:titleView];
    self.navigationItem.titleView = titleView;
    
    self.titlesView = titleView;
    
    NSArray *titles = @[@"精选表情", @"更多表情"];
    NSInteger count = titles.count;
    CGFloat titleBUttonw = titleView.width / 2;
    CGFloat titleButttonh = titleView.height;
    for (NSInteger i=0; i<count; i++) {
        MessageTitleBtn *titleButton = [MessageTitleBtn buttonWithType:UIButtonTypeCustom];
        titleButton.tag =i;
        [titleButton addTarget:self action:@selector(titClick:) forControlEvents:UIControlEventTouchUpInside];
        [titleView addSubview:titleButton];
        
        [titleButton setTitle:titles[i] forState:UIControlStateNormal];
        titleButton.frame = CGRectMake(i * titleBUttonw, 0, titleBUttonw, titleButttonh);
    }
    
    MessageTitleBtn *firstTitleButton = titleView.subviews.firstObject;
    
    [firstTitleButton.titleLabel sizeToFit];
    firstTitleButton.selected= YES;
    self.selectedTitleButton =firstTitleButton;
    
    [self titClick:firstTitleButton];
    
}
-(void)titClick:(MessageTitleBtn *)titleButton
{

    self.index = titleButton.tag;
    self.selectedTitleButton.selected = NO;
    titleButton.selected = YES;
    self.selectedTitleButton = titleButton;
    
    CGPoint offset = self.scrollView.contentOffset;
    offset.x = titleButton.tag * self.scrollView.width;
    [self.scrollView setContentOffset:offset animated:YES];
}

- (void)addChildVcView
{
        
    NSUInteger index = self.scrollView.contentOffset.x  / self.scrollView.width;
    UIViewController *childVc = self.childViewControllers[index];
    if ([childVc isViewLoaded]) return;
    childVc.view.frame = self.scrollView.bounds;
    [self.scrollView addSubview:childVc.view];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [self addChildVcView];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    NSUInteger index = scrollView.contentOffset.x / scrollView.width;
    MessageTitleBtn *titleButton = self.titlesView.subviews[index];
    [self titClick:titleButton];
    [self addChildVcView];
    
}

- (void)addChildVcViewIntoScrollView:(NSUInteger)index
{
    MessageTitleBtn *titleButton = self.titlesView.subviews[index];
    [self titClick:titleButton];
    [self addChildVcView];
}


@end
