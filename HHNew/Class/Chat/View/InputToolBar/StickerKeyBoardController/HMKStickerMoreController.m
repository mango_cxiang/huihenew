//
//  HMKStickerMoreController.m
//  huihe
//
//  Created by jie.huang on 5/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKStickerMoreController.h"
#import "HMKStickerSearchCell.h"
#import "HMKMoreStickerModel.h"
#import "HMKitemsSearch.h"
#import "HMKStickerNextController.h"
@interface HMKStickerMoreController ()<UITextFieldDelegate>

@property (nonatomic,assign) NSInteger index;

@property (nonatomic,strong) NSMutableArray<HMKMoreStickerModel*> *dataMode;

@property (nonatomic,strong) HMKitemsSearch *searchBar;

@end

@implementation HMKStickerMoreController


static NSString * const MoreCellID =@"MoreCellID";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.searchBar = [HMKitemsSearch SearchBar];
    self.searchBar.width = SCREEN_WIDTH - 100;
    self.searchBar.height = 35;
    self.searchBar.delegate = self;
    self.searchBar.text = self.searchText;
    self.navigationItem.titleView = self.searchBar;
   
    [self creatWithTableViewStyle:UITableViewStylePlain];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([HMKStickerSearchCell class]) bundle:nil] forCellReuseIdentifier:MoreCellID];
    self.tableView.rowHeight = 80;
    self.tableView.estimatedRowHeight=80.0f;
    self.tableView.tableFooterView = [UIView new];
    
//     [self.navigationItem setLeftBarButtonItem:[self creatButtonOnNavigationBarWithImage:[UIImage imageNamed:@"back_black"] Title:@"" BtnColor:RGB(88, 108, 149) frame:CGRectMake(0, 0, 35, 35) tag:0 targer:self action:@selector(back)]];
    
    [self getStickerData];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreTopics)];
}

//-(void)back{
//    
//    [self.navigationController popViewControllerAnimated:YES];
//}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.searchBar resignFirstResponder];
    [self getStickerData];;
    
    return YES;
}


-(void)getStickerData{
    
    self.index = 1;
//    NSMutableDictionary *ParameDict = [NSMutableDictionary dictionary];
//    [ParameDict setObject:@(self.index) forKey:@"pageIndex"];
//    [ParameDict setObject:@(20) forKey:@"pageSize"];
//    [ParameDict setObject:self.searchBar.text forKey:@"describe"];
//    [ParameDict setObject:[NSString acquireUserId] forKey:@"appUserId"];
//
//    [HMJBusinessManager GET:@"/bms/appPhiz/findMoreManyByName" Parameters:ParameDict success:^(NSDictionary *dic, NSInteger code) {
//
//        self.dataMode = [HMKMoreStickerModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"info"][@"list"]];
//
//        [self.tableView reloadData];
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//    }];
}

-(void)loadMoreTopics{
    
    self.index ++;
//    NSMutableDictionary *ParameDict = [NSMutableDictionary dictionary];
//    [ParameDict setObject:@(self.index) forKey:@"pageIndex"];
//    [ParameDict setObject:@(20) forKey:@"pageSize"];
//    [ParameDict setObject:self.searchBar.text forKey:@"describe"];
//    [ParameDict setObject:[NSString acquireUserId] forKey:@"appUserId"];
//
//    [HMJBusinessManager GET:@"/bms/appPhiz/findMoreManyByName" Parameters:ParameDict success:^(NSDictionary *dic, NSInteger code) {
//
//        NSArray *moreArry = [HMKMoreStickerModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"info"][@"list"]];
//        [self.dataMode addObjectsFromArray:moreArry];
//
//        [self.tableView reloadData];
//
//        [self.tableView.mj_footer endRefreshing];
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//        [self.tableView.mj_footer endRefreshing];
//    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataMode.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    HMKStickerNextController *vc = [HMKStickerNextController new];
    vc.StickerID = self.dataMode[indexPath.row].ID;
    vc.TitleText = self.dataMode[indexPath.row].name;
    vc.isPush = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    HMKStickerSearchCell *cell = [self.tableView dequeueReusableCellWithIdentifier:MoreCellID];
    cell.MoreModel = self.dataMode[indexPath.row];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc] init];
    header.backgroundColor = [UIColor whiteColor];
    
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor lightGrayColor];
    label.width = 200;
    label.x = 10;
    label.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    UIView *lineview = [UIView new];
    lineview.frame = CGRectMake(0, 43, SCREEN_WIDTH, 0.3);
    lineview.backgroundColor = RGB_Color(220, 220, 220);
    [header addSubview:lineview];
    [header addSubview:label];
    label.text = @"表情专辑";
    return header;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 44;
}






@end
