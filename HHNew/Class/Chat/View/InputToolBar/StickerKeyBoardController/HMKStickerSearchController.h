//
//  HMKStickerSearchController.h
//  huihe
//
//  Created by jie.huang on 1/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "BaseViewController.h"

@interface HMKStickerSearchController : BaseViewController


@property (nonatomic,strong) NSString *SearchContentText;
@property (nonatomic,assign) BOOL isexit;

@end
