//
//  HMKStickerDetailsController.m
//  huihe
//
//  Created by jie.huang on 8/1/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKStickerDetailsController.h"
#import "HMKStickerHeardView.h"
#import "HMKStickerCell.h"
#import "HMKStickerMallModel.h"
#import "SDCycleScrollView.h"
#import "HMKStickerNextController.h"
#import "JKRSearchController.h"
#import "UIViewController+JKRStatusBarStyle.h"
#import "HMKStickerSearchController.h"
#import "HMKStickerSearchController.h"


#define kSafeAreaNavHeight (([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125,2436), [[UIScreen mainScreen] currentMode].size) : NO) ? 88 : 64)

@interface HMKStickerDetailsController ()<UICollectionViewDataSource,UICollectionViewDelegate,SDCycleScrollViewDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic,assign) NSInteger index;

@property (nonatomic,strong) NSMutableArray<HMKStickerMallModel*> *dataMode;

@property (nonatomic,strong) NSMutableArray *heardBannerArry;

@property (nonatomic,strong) NSMutableArray *ArryID;

@property (nonatomic,strong) NSMutableArray *NameArr;

@property (nonatomic, strong) JKRSearchController *searchController;

@property (nonatomic,strong) NSString *SearchText;

@property (nonatomic,weak) HMKStickerSearchController *StickerSearchController;


@end

@implementation HMKStickerDetailsController

{
    HMKStickerHeardView *_headerView; //广告栏
}


-(NSMutableArray *)heardBannerArry{
    
    if (!_heardBannerArry) {
        
        _heardBannerArry = [NSMutableArray array];
    }
    return _heardBannerArry;
}
-(NSMutableArray *)ArryID{
    
    if (!_ArryID) {
     
        _ArryID = [NSMutableArray array];
    }
    return _ArryID;
}

-(NSMutableArray *)NameArr{
    
    if (!_NameArr) {
        
        _NameArr = [NSMutableArray array];
    }
    return _NameArr;
}

- (JKRSearchController *)searchController {
    if (!_searchController) {
        _searchController = [[JKRSearchController alloc] initWithSearchResultsController:self];
        _searchController.searchBar.placeholder = @"搜索表情";
        UIButton *btn = [[UIButton alloc]initWithFrame:_searchController.searchBar.bounds];
        [_searchController.searchBar addSubview:btn];
        [btn addTarget:self action:@selector(endDetatils) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _searchController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"表情商店";
    
    _headerView = [HMKStickerHeardView viewFromxib];
    _headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, (SCREEN_WIDTH * 0.3846) + 45 );
    _headerView.bannerView.delegate = self;
    _headerView.HeadSearchView.backgroundColor = RGB_Color(247, 247, 247);
    [_headerView addSubview:self.searchController.searchBar];
    self.jkr_lightStatusBar = YES;
    [self.view addSubview:self.collectionView];
    
    
    [self getHeardViewbanner];
    
    [self getStickerData];
    
    self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreTopics)];
    
    
}

-(void)endDetatils{
    
    HMKStickerSearchController *vc = [HMKStickerSearchController new];
    [self.navigationController pushViewController: vc animated:YES];
}




-(void)getHeardViewbanner{
    
//    [HMJBusinessManager GET:@"/bms/appPhiz/findALLManyMoreOfCover" Parameters:nil success:^(NSDictionary *dic, NSInteger code) {
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//
//            for (NSDictionary *dict in dic[@"data"][@"info"]) {
//
//                [self.heardBannerArry addObject:dict[@"master_url"]];
//                [self.ArryID addObject:dict[@"id"]];
//                [self.NameArr addObject:dict[@"name"]];
//            }
//        }else{
//
//           [[HMJShowLoding sharedInstance]showText:dic[@"data"][@"info"]];
//        }
//
//         _headerView.bannerView.imageURLStringsGroup = self.heardBannerArry;
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//    }];
    
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    HMKStickerNextController *vc = [HMKStickerNextController new];
    vc.StickerID = self.ArryID[index];
    vc.TitleText = self.NameArr[index];
    vc.isPush = NO;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)getStickerData{
    
    self.index = 1;
//    NSMutableDictionary *dictParamet = [NSMutableDictionary dictionary];
//    [dictParamet setObject:@(self.index) forKey:@"pageIndex"];
//    [dictParamet setObject:@(15) forKey:@"pageSize"];
//    [dictParamet setObject:[NSString acquireUserId] forKey:@"appUserId"];
//
//
//    [HMJBusinessManager GET:@"/bms/appPhiz/findALLManyMore" Parameters:dictParamet success:^(NSDictionary *dic, NSInteger code) {
//
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//
//            self.dataMode = [HMKStickerMallModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"info"][@"list"]];
//
//            [self.collectionView.mj_footer resetNoMoreData];
//            [self.collectionView reloadData];
//        }else{
//             [[HMJShowLoding sharedInstance]showText:dic[@"data"][@"info"]];
//        }
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//    }];
    
}

-(void)loadMoreTopics{
    
    self.index ++;
//    NSMutableDictionary *dictParamet = [NSMutableDictionary dictionary];
//    [dictParamet setObject:@(self.index) forKey:@"pageIndex"];
//    [dictParamet setObject:@(15) forKey:@"pageSize"];
//    [dictParamet setObject:[NSString acquireUserId] forKey:@"appUserId"];
//
//
//    [HMJBusinessManager GET:@"/bms/appPhiz/findALLManyMore" Parameters:dictParamet success:^(NSDictionary *dic, NSInteger code) {
//
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//
//            NSArray *moreArry = [HMKStickerMallModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"info"][@"list"]];
//            [self.dataMode addObjectsFromArray:moreArry];
//            if (moreArry.count == 0) {
//                [self.collectionView.mj_footer endRefreshingWithNoMoreData];
//            }else{
//                [self.collectionView.mj_footer endRefreshing];
//            }
//            [self.collectionView reloadData];
//
//        }else{
//            [[HMJShowLoding sharedInstance]showText:dic[@"data"][@"info"]];
//        }
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//         [self.collectionView.mj_footer endRefreshing];
//
//    }];
}

- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        
        flowLayout.headerReferenceSize = CGSizeMake(SCREEN_WIDTH, (SCREEN_WIDTH * 0.3846) + 55);//头部大小
        
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) collectionViewLayout:flowLayout];
        
        //定义每个UICollectionView 的大小
        flowLayout.itemSize = CGSizeMake((SCREEN_WIDTH-80)/3, (SCREEN_WIDTH-80)/3 + 30);
        //定义每个UICollectionView 横向的间距
        flowLayout.minimumLineSpacing = 5;
        //定义每个UICollectionView 纵向的间距
        flowLayout.minimumInteritemSpacing = 20;
        //定义每个UICollectionView 的边距距
        flowLayout.sectionInset = UIEdgeInsetsMake(20, 20, 5, 20);//上左下右
        
        //注册cell和ReusableView（相当于头部）
        [_collectionView registerClass:[HMKStickerCell class] forCellWithReuseIdentifier:@"cell"];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeardView"];
        
        //设置代理
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        
        //背景颜色
        _collectionView.backgroundColor = [UIColor whiteColor];
        //自适应大小
        _collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
    }
    return _collectionView;
}

#pragma mark 定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataMode.count;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identify = @"cell";
    HMKStickerCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
    cell.DataModel = self.dataMode[indexPath.row];
    
    [cell sizeToFit];

    return cell;
}

#pragma mark 头部显示的内容
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:
                                            UICollectionElementKindSectionHeader withReuseIdentifier:@"HeardView" forIndexPath:indexPath];
    
    [headerView addSubview:_headerView];
    return headerView;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"选择%ld",indexPath.item);
    
    HMKStickerNextController *vc = [HMKStickerNextController new];
    vc.StickerID = self.dataMode[indexPath.row].ID;
    vc.TitleText = self.dataMode[indexPath.row].name;
    vc.isPush = NO;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)popViewController{
    
    [self dismissViewControllerAnimated:YES completion:nil];
  
}


@end
