//
//  HMKStickerNextController.h
//  huihe
//
//  Created by jie.huang on 28/2/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "BaseViewController.h"

@interface HMKStickerNextController : UITableViewController

@property (nonatomic,strong) NSString *StickerID;

@property (nonatomic,strong) NSString *TitleText;

@property (nonatomic,assign) BOOL isPush;

@end
