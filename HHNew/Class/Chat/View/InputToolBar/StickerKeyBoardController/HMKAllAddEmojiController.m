//
//  HMKAllAddEmojiController.m
//  huihe
//
//  Created by jie.huang on 7/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKAllAddEmojiController.h"
#import "HMKAllAddEmojiCell.h"
#import "HMKAllAddEmojiModel.h"
#import "HMKStickerNextController.h"
@interface HMKAllAddEmojiController ()

@property (nonatomic,assign) NSInteger index;

@property (nonatomic,strong) NSMutableArray<HMKAllAddEmojiModel*> *dataMode;

@property (nonatomic,strong) NSMutableArray *selectedArray;

@end

@implementation HMKAllAddEmojiController

-(NSMutableArray *)selectedArray{
    
    if (!_selectedArray) {
        
        _selectedArray = [NSMutableArray array];
    }
    return _selectedArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    
    
    self.title = @"整套表情添加记录";
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([HMKAllAddEmojiCell class]) bundle:nil] forCellReuseIdentifier:@"AllAddCellID"];
    
      self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreTopics)];
    self.tableView.rowHeight = 80;
    self.tableView.estimatedRowHeight = 80;
    [self getEmoji];
    
    
}

-(void)getEmoji{
    
    
    [self.selectedArray removeAllObjects];
    
    self.index = 1;
//    NSMutableDictionary *dictParame = [NSMutableDictionary dictionary];
//    [dictParame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    [dictParame setObject:@(self.index) forKey:@"pageIndex"];
//    [dictParame setObject:@(20) forKey:@"pageSize"];
//
//    [HMJBusinessManager GET:@"/im/appAdd/AllappAddList" Parameters:dictParame success:^(NSDictionary *dic, NSInteger code) {
//        NSLog(@"%@",dic);
//        self.dataMode = [HMKAllAddEmojiModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"info"][@"list"]];
//        for (int i = 0; i< self.dataMode.count; i++) {
//            HMKAllAddEmojiModel *moede = self.dataMode [i];
//            if ([moede.isAdd isEqualToString:@"1"]) {
//                [_selectedArray addObject:@(100 + i)];
//            }
//        }
//        [self.tableView.mj_footer resetNoMoreData];
//        [self.tableView reloadData];
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//    }];
}

-(void)loadMoreTopics{
    
//    [self.selectedArray removeAllObjects];
    
    self.index ++;
//    NSMutableDictionary *dictParame = [NSMutableDictionary dictionary];
//
//    [dictParame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    [dictParame setObject:@(self.index) forKey:@"pageIndex"];
//    [dictParame setObject:@(20) forKey:@"pageSize"];
//
//    [HMJBusinessManager GET:@"/im/appAdd/AllappAddList" Parameters:dictParame success:^(NSDictionary *dic, NSInteger code) {
//        NSArray<HMKAllAddEmojiModel*>  *moreArry = [HMKAllAddEmojiModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"info"][@"list"]];
//        [self.dataMode addObjectsFromArray:moreArry];
//
//        for (int i = 0; i< moreArry.count; i++) {
//            HMKAllAddEmojiModel *moede = moreArry [i];
//            if ([moede.isAdd isEqualToString:@"1"]) {
//                [_selectedArray addObject:@(100 + i)];
//            }
//        }
//
//        if (moreArry.count < 20) {
//            [self.tableView.mj_footer endRefreshingWithNoMoreData];
//        }else{
//            [self.tableView.mj_footer endRefreshing];
//        }
//        [self.tableView reloadData];
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//        [self.tableView.mj_footer endRefreshing];
//    }];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.dataMode.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    HMKStickerNextController *vc = [HMKStickerNextController new];
    vc.StickerID = self.dataMode[indexPath.row].ID;
    vc.TitleText = self.dataMode[indexPath.row].name;
    [self.navigationController pushViewController:vc animated:YES];
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    HMKAllAddEmojiCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"AllAddCellID"];
    cell.dataMode = self.dataMode[indexPath.row];
    cell.isAddBtn.tag =100 + indexPath.row;
    if ([self.selectedArray containsObject:@(cell.isAddBtn.tag)]) {
        
        cell.isAddBtn.selected = YES;
        cell.isAddBtn.enabled = NO;
        cell.isAddBtn.layer.borderColor = RGB_Color(224, 224, 224).CGColor;
        [cell.isAddBtn setTitleColor:RGB_Color(224, 224, 224) forState:UIControlStateNormal];
        [cell.isAddBtn setTitle:@"已添加" forState:UIControlStateNormal];
    }else{
        
        cell.isAddBtn.layer.borderColor = RGB_Color(26, 173, 25).CGColor;
        [cell.isAddBtn setTitleColor:RGB_Color(26, 173, 25) forState:UIControlStateNormal];
        cell.isAddBtn.enabled = YES;
        cell.isAddBtn.selected = NO;
    }
    [cell.isAddBtn addTarget:self action:@selector(AddMoreClick:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
    
}
-(void)AddMoreClick:(UIButton *)btn{
    
    
    NSInteger indext = [self.tableView indexPathForCell:((HMKAllAddEmojiCell *)[[btn superview]superview])].row;
    btn.selected = !btn.isSelected;
    if (btn.selected) {
        
//        NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//        [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//        [dictparame setObject:self.dataMode[indext].chat_url forKey:@"imgUrl"];
//        [dictparame setObject:self.dataMode[indext].ID forKey:@"phiz_id"];
//        [dictparame setObject:self.dataMode[indext].name forKey:@"name"];
//
//        [[HMJShowLoding sharedInstance]showLoading];
//        [HMJBusinessManager EmojiPOST:@"/im/appAdd/addPhizAppAdd" parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//
//            [[HMJShowLoding sharedInstance]hiddenLoading];
//            NSInteger errorcode = [dic[@"code"] integerValue];
//            if (errorcode == 1) {
//
//                [[HMJShowLoding sharedInstance]showText:@"添加成功"];
//                if (btn.selected) {
//                    [_selectedArray addObject:@(btn.tag)];
//                }
//                [self getemoji];
//                [self.tableView reloadData];
//
//            }else{
//                [[HMJShowLoding sharedInstance]showText:dic[@"data"][@"info"]];
//            }
//             [[NSNotificationCenter defaultCenter] postNotificationName:@"ADDEMOJI_SEARCH" object:nil];
//        } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//            [[HMJShowLoding sharedInstance]showText:@"添加失败"];
//            [[HMJShowLoding sharedInstance]hiddenLoading];
//        }];
        
    }
}

-(void)getemoji{
    
    
//    [[HMJShowLoding sharedInstance] showLoading];
//    [HHGlobals saveEmojiDict:nil];
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//
//    [HMJBusinessManager GET:@"/im/appAdd/appAddList" Parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//
//
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//            NSMutableArray *emojiArr = [NSMutableArray array];
//
//            NSArray *DataArry = dic[@"data"][@"info"];
//            for (NSDictionary *dict in DataArry) {
//
//                NSMutableDictionary *Emojidict = [NSMutableDictionary dictionary];
//
//                [Emojidict setObject:dict[@"chat_url"] forKey:@"cover_pic"];
//                [Emojidict setObject:dict[@"id"] forKey:@"EmojiID"];
//
//                NSMutableArray *emoticons = [NSMutableArray array];
//
//                for (NSDictionary *SubDict in dict[@"phiz_subList"]) {
//
//                    NSMutableDictionary *icondict = [NSMutableDictionary dictionary];
//
//                    [icondict setValue:SubDict[@"next_url"] forKey:@"image"];
//                    [icondict setValue:SubDict[@"phiz_desc"] forKey:@"desc"];
//                    [icondict setValue:SubDict[@"master_url"] forKey:@"imageGif"];
//                    [icondict setValue:SubDict[@"id"] forKey:@"EmojiID"];
//
//                    [emoticons addObject:icondict];
//                }
//
//                [Emojidict setValue:emoticons forKey:@"emoticons"];
//
//                [emojiArr addObject:Emojidict];
//            }
//
//            [HHGlobals saveEmojiDict:emojiArr];
//        }
//
//        [self getCollectionemoji];
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
}


-(void)getCollectionemoji{
    
    
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//    
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    
//    [HMJBusinessManager GET:@"/im/appAdd/listCollection" Parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//        
//        
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//            
//            
//            
//            NSArray *DataArry = dic[@"data"][@"info"][@"collection"];
//            
//            NSMutableDictionary *Emojidict = [NSMutableDictionary dictionary];
//            
//            [Emojidict setObject:dic[@"data"][@"info"][@"heartUrl"] forKey:@"cover_pic"];
//            
//            NSMutableArray *emoticons = [NSMutableArray array];
//            
//            for (NSDictionary *SubDict in DataArry) {
//                
//                NSMutableDictionary *icondict = [NSMutableDictionary dictionary];
//                
//                [icondict setValue:SubDict[@"nextUrl"] forKey:@"image"];
//                [icondict setValue:SubDict[@"name"] forKey:@"desc"];
//                [icondict setValue:SubDict[@"imgUrl"] forKey:@"imageGif"];
//                [icondict setValue:SubDict[@"next_id"] forKey:@"EmojiID"];
//                
//                [emoticons addObject:icondict];
//            }
//            
//            [Emojidict setValue:emoticons forKey:@"emoticons"];
//            
//            NSMutableArray *arry = [HHGlobals getEmojiDict].mutableCopy;
//            
//            [arry insertObject:Emojidict atIndex:0];
//            
//            [HHGlobals saveEmojiDict:arry];
//            
//        }
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//        
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
}



@end
