//
//  HMKStickerSearchController.m
//  huihe
//
//  Created by jie.huang on 1/3/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKStickerSearchController.h"
//#import "UIButton+CenterImageAndTitle.h"
#import "HMKStickerCell.h"
#import "HMKStickerNextView.h"
#import "HMKStickerSearchHeadModel.h"
#import "HMKStickerSearchDetatlsModel.h"
#import "HMKStickerSearchCell.h"
#import "HMKStickerNextController.h"
#import "HMKitemsSearch.h"
#import "HMKStickerMoreController.h"
#import "HMKOneEmojiController.h"

@interface HMKStickerSearchController ()<UICollectionViewDataSource ,UICollectionViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic,strong) NSMutableArray<HMKStickerSearchHeadModel*> *HeaddataMode;

@property (nonatomic,strong) NSMutableArray<HMKStickerSearchDetatlsModel*> *detailsMode;

@property (nonatomic,assign) NSInteger index;

@property (nonatomic,assign) CGFloat High;

@property (nonatomic,strong) UITableView *Heardtableview;

@property (nonatomic,strong) HMKStickerNextView *headerView;

@property (nonatomic,strong) UICollectionViewFlowLayout *flowLayout;

@property (nonatomic,strong) UIView *lineview;

@property (nonatomic,strong) UILabel *StickerLabel;

@property (nonatomic,strong) HMKitemsSearch *searchBar;

@property (nonatomic,strong) UILabel *isLable;

@end

@implementation HMKStickerSearchController



static NSString * const CellID =@"CellID";

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self showBackButton:NO];
    self.searchBar = [HMKitemsSearch SearchBar];
    self.searchBar.width = SCREEN_WIDTH - 55;
    self.searchBar.height = 35;
    self.searchBar.delegate = self;
    self.navigationItem.titleView = self.searchBar;

     self.Heardtableview = [[UITableView alloc]init];
    [self.Heardtableview registerNib:[UINib nibWithNibName:NSStringFromClass([HMKStickerSearchCell class]) bundle:nil] forCellReuseIdentifier:CellID];
    self.Heardtableview.delegate = self;
    self.Heardtableview.dataSource = self;
    self.Heardtableview.rowHeight = 80;
    self.Heardtableview.estimatedRowHeight=80.0f;
    self.Heardtableview.tableFooterView = [UIView new];
    
    
    self.flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [self setLeftBarButtonItem:@"取消"];
    
    _headerView = [HMKStickerNextView viewFromxib];
    [_headerView.MoreBtn horizontalCenterTitleAndImage];
    [_headerView.MoreBtn addTarget:self action:@selector(Moreclick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.collectionView];
    
    
    _lineview = [UIView new];
    _lineview.backgroundColor = RGB_Color(191, 191, 191);
    [self.collectionView addSubview:_lineview];
    
    self.StickerLabel = [UILabel new];
    self.StickerLabel.text = @"表情";
    
    self.isLable = [[UILabel alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT/2 - 50, SCREEN_WIDTH, 21)];
    self.isLable.textAlignment = NSTextAlignmentCenter;
    self.isLable.textColor = RGB_Color(132, 132, 132);
    self.isLable.text = @"无结果";
    self.isLable.hidden = YES;
    [self.view addSubview:self.isLable];
    
   
    [self.collectionView addSubview:self.StickerLabel];
}

-(void)Moreclick{
    
   
    HMKStickerMoreController *vc = [HMKStickerMoreController new];
    vc.searchText = self.searchBar.text;
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)leftBarButtonItemAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [IQKeyboardManager sharedManager].enable = NO;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = NO;
    
}

//-(void)endSearch{
//    
//    self.detailsMode = nil;
//    self.HeaddataMode = nil;
//    self.High = 0;
//    _headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, self.High );
//    _lineview.frame = CGRectMake(20, self.High +53, SCREEN_WIDTH -20, 0);
//     self.StickerLabel.frame = CGRectMake(15, self.High + 16, 100, 0);
//    
//    _flowLayout.headerReferenceSize = CGSizeMake(SCREEN_WIDTH,0);//头部大小
//    self.Heardtableview.frame = CGRectMake(0, 53, SCREEN_WIDTH, 0);
//    [_headerView addSubview:self.Heardtableview];
//    self.collectionView.mj_footer = nil;
//    [self.collectionView reloadData];
//    [self.Heardtableview reloadData];
//}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [self.searchBar resignFirstResponder];
    [self searchData:textField.text];
    
    return YES;
    
}

-(void)searchData:(NSString *)content{
    
//    if (content.length == 0)  return ;
//    
//    
//    NSMutableDictionary *ParameDict = [NSMutableDictionary dictionary];
//    
//    [ParameDict setObject:content forKey:@"describe"];
//    [ParameDict setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    
//    [HMJBusinessManager GET:@"/bms/appPhiz/findManyByName" Parameters:ParameDict success:^(NSDictionary *dic, NSInteger code) {
//        
//         self.HeaddataMode = [HMKStickerSearchHeadModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"info"]];
//        self.High = self.HeaddataMode.count > 0 ? self.HeaddataMode.count * 80 + 53 : 0;
//        
//        [self GetData:content];
//        
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//        
//    }];
    
}

-(void)GetData:(NSString *)content{
    
    self.index = 1;
//    NSMutableDictionary *ParameDict = [NSMutableDictionary dictionary];
//    [ParameDict setObject:@(self.index) forKey:@"pageIndex"];
//    [ParameDict setObject:@(20) forKey:@"pageSize"];
//    [ParameDict setObject:content forKey:@"describe"];
//    [ParameDict setObject:[NSString acquireUserId] forKey:@"appUserId"];
//
//    [HMJBusinessManager GET:@"/bms/appPhiz/findMoreManyBySub" Parameters:ParameDict success:^(NSDictionary *dic, NSInteger code) {
//
//        self.detailsMode = [HMKStickerSearchDetatlsModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"info"][@"list"]];
//
//        _headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, self.High );
//        _lineview.frame = CGRectMake(20, self.High +53, SCREEN_WIDTH -20, 0.3);
//
//        self.StickerLabel.frame = CGRectMake(20, self.High + 16, 100, 22);
//
//        CGFloat Y = self.High == 0 ? 0 : 53;
//        if (self.High == 0) {
//            _headerView.MoreBtn.hidden = YES;
//            _headerView.TitiLabel.hidden = YES;
//            _headerView.fenGView.hidden = YES;
//        }else{
//            _headerView.MoreBtn.hidden = NO;
//            _headerView.TitiLabel.hidden = NO;
//            _headerView.fenGView.hidden = NO;
//        }
//        if (self.detailsMode.count > 0) {
//            _lineview.hidden = NO;
//            _StickerLabel.hidden = NO;
//            self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreTopics)];
//        }else{
//            _lineview.hidden = YES;
//            _StickerLabel.hidden = YES;
//             self.collectionView.mj_footer = nil;
//
//        }
//        _flowLayout.headerReferenceSize = CGSizeMake(SCREEN_WIDTH,self.High + 53);//头部大小
//        self.Heardtableview.frame = CGRectMake(0, Y, SCREEN_WIDTH, self.High - Y);
//        [_headerView addSubview:self.Heardtableview];
//
//        if (self.detailsMode.count > 0 || self.HeaddataMode.count > 0) {
//            self.isLable.hidden = YES;
//        }else{
//            self.isLable.hidden = NO;
//        }
//
//
//        [self.collectionView reloadData];
//        [self.Heardtableview reloadData];
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//    }];
    
}
-(void)loadMoreTopics{
    
     self.index ++;
//    NSMutableDictionary *ParameDict = [NSMutableDictionary dictionary];
//    [ParameDict setObject:@(self.index) forKey:@"pageIndex"];
//    [ParameDict setObject:@(20) forKey:@"pageSize"];
//    [ParameDict setObject:self.searchBar.text forKey:@"describe"];
//    [ParameDict setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    
//    [HMJBusinessManager GET:@"/bms/appPhiz/findMoreManyBySub" Parameters:ParameDict success:^(NSDictionary *dic, NSInteger code) {
//        NSArray *moreArry = [HMKStickerSearchDetatlsModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"info"][@"list"]];
//        [self.detailsMode addObjectsFromArray:moreArry];
//        [self.collectionView reloadData];
//        [self.collectionView.mj_footer endRefreshing];
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//         [self.collectionView.mj_footer endRefreshing];
//    }];
        
}

- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        

        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) collectionViewLayout:_flowLayout];
        _flowLayout.itemSize = CGSizeMake((SCREEN_WIDTH-80)/3, (SCREEN_WIDTH-80)/3 + 30);
        _flowLayout.minimumLineSpacing = 20;
        _flowLayout.minimumInteritemSpacing = 20;
        _flowLayout.sectionInset = UIEdgeInsetsMake(20, 20, 20, 20);
        
        [_collectionView registerClass:[HMKStickerCell class] forCellWithReuseIdentifier:@"cell"];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeardView"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
    }
    return _collectionView;
}




-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    HMKStickerNextController *vc = [HMKStickerNextController new];
    vc.StickerID = self.HeaddataMode[indexPath.row].ID;
    vc.TitleText = self.HeaddataMode[indexPath.row].name;
    vc.isPush = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return self.HeaddataMode.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    HMKStickerSearchCell *cell = [self.Heardtableview dequeueReusableCellWithIdentifier:CellID];
    cell.DataModel = self.HeaddataMode[indexPath.row];
    return cell;
}


#pragma mark 定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.detailsMode.count;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identify = @"cell";
    HMKStickerCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
    cell.detailsMode = self.detailsMode[indexPath.row];
    
    [cell sizeToFit];
    
    return cell;
}

#pragma mark 头部显示的内容
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:
                                            UICollectionElementKindSectionHeader withReuseIdentifier:@"HeardView" forIndexPath:indexPath];

    [headerView addSubview:_headerView];
    return headerView;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    HMKOneEmojiController *vc = [HMKOneEmojiController new];
    vc.Model = self.detailsMode[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
