//
//  HMKSupeStickerController.h
//  huihe
//
//  Created by jie.huang on 14/2/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "BaseViewController.h"

@interface HMKSupeStickerController : BaseViewController
//是否是右滑抽屉跳转进来
@property (nonatomic, assign) BOOL rightSlider;
@end
