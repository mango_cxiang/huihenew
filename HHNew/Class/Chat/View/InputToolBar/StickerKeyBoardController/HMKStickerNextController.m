//
//  HMKStickerNextController.m
//  huihe
//
//  Created by jie.huang on 28/2/19.
//  Copyright © 2019年 BQ. All rights reserved.
//

#import "HMKStickerNextController.h"
#import "StickerDetailsHeadView.h"
#import "HMKStickerDetailsCell.h"
#import "HMKStickerDetailsModel.h"
#import "HMKEmojiFootView.h"
@interface HMKStickerNextController ()


@property (nonatomic,weak) StickerDetailsHeadView *TableHeadView;

@property (nonatomic,strong) NSMutableArray<HMKStickerDetailsModel*> *dataMode;

@property (nonatomic,weak) StickerDetailsHeadView *HeadView;

@property (nonatomic,strong) HMKEmojiFootView *FootView;

@end

@implementation HMKStickerNextController

static  NSString * const cellID = @"cellID";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = self.TitleText;
    
    [self.tableView registerClass:[HMKStickerDetailsCell class] forCellReuseIdentifier:cellID];
    HMKEmojiFootView *FootView = [HMKEmojiFootView viewFromxib];
    FootView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 165);
    self.FootView = FootView;
    self.tableView.tableFooterView = [UIView new];
    
    StickerDetailsHeadView *headView = [StickerDetailsHeadView viewFromxib];
    headView.frame = CGRectMake(0, 0, SCREEN_WIDTH,  SCREEN_WIDTH * 0.6154 + 100);
    self.TableHeadView = headView;
    self.HeadView = headView;
    self.tableView.tableHeaderView = headView;
    
    [self getSticker];
}

-(void)getSticker{
    
//    NSMutableDictionary *dictParamet = [NSMutableDictionary dictionary];
//    [dictParamet setObject:self.StickerID forKey:@"id"];
//    [dictParamet setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    [HMJBusinessManager GET:@"/bms/appPhiz/findALLManyById" Parameters:dictParamet success:^(NSDictionary *dic, NSInteger code) {
//
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//             self.dataMode = [HMKStickerDetailsModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"info"][@"phiz_subList"]];
//            [self.TableHeadView.HardImageView sd_setImageWithURL:[NSURL URLWithString:dic[@"data"][@"info"][@"master_url"]]];
//            self.TableHeadView.TitleLable.text = dic[@"data"][@"info"][@"name"];
//            self.TableHeadView.ContentLable.text = dic[@"data"][@"info"][@"description"];
//            self.FootView.nameLable.text = dic[@"data"][@"info"][@"name"];
//            [self.FootView.iconImageview sd_setImageWithURL:[NSURL URLWithString:dic[@"data"][@"info"][@"chat_url"]]];
//
//            if ([dic[@"data"][@"info"][@"isAdd"] isEqualToString:@"1"]) {
//                [self.HeadView.AddBtn setTitle:@"已添加" forState:UIControlStateNormal];
//                self.HeadView.AddBtn.backgroundColor = RGB(224, 224, 224);
//                self.HeadView.AddBtn.enabled = NO;
//            }else{
//                [self.HeadView.AddBtn setTitle:@"添加" forState:UIControlStateNormal];
//            }
//            [self.HeadView.AddBtn addTarget:self action:@selector(AddClick:) forControlEvents:UIControlEventTouchUpInside];
//        }else{
//            [[HMJShowLoding sharedInstance]showText:dic[@"data"][@"info"]];
//        }
//        [self.tableView reloadData];
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//    }];
}

-(void)AddClick:(UIButton *)btn{
    
//    [[HMJShowLoding sharedInstance]showLoading];
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    [dictparame setObject:self.StickerID forKey:@"phiz_id"];
//
//    [HMJBusinessManager EmojiPOST:@"/im/appAdd/addPhizAppAdd" parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//        [[HMJShowLoding sharedInstance]showText:@"添加成功"];
//        [btn setTitle:@"已添加" forState:UIControlStateNormal];
//        btn.backgroundColor = RGB(224, 224, 224);
//        btn.enabled = NO;
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"REMOVE_SEARCH" object:nil];
//        [self getemoji];
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//        [[HMJShowLoding sharedInstance]showText:@"添加失败"];
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
    
}

-(void)getemoji{
//    [[HMJShowLoding sharedInstance] showLoading];
//    [HHGlobals saveEmojiDict:nil];
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    [HMJBusinessManager GET:@"/im/appAdd/appAddList" Parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//
//
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//            NSMutableArray *emojiArr = [NSMutableArray array];
//
//            NSArray *DataArry = dic[@"data"][@"info"];
//            for (NSDictionary *dict in DataArry) {
//
//                NSMutableDictionary *Emojidict = [NSMutableDictionary dictionary];
//
//                [Emojidict setObject:dict[@"chat_url"] forKey:@"cover_pic"];
//                [Emojidict setObject:dict[@"id"] forKey:@"EmojiID"];
//
//                NSMutableArray *emoticons = [NSMutableArray array];
//
//                for (NSDictionary *SubDict in dict[@"phiz_subList"]) {
//
//                    NSMutableDictionary *icondict = [NSMutableDictionary dictionary];
//
//                    [icondict setValue:SubDict[@"next_url"] forKey:@"image"];
//                    [icondict setValue:SubDict[@"phiz_desc"] forKey:@"desc"];
//                    [icondict setValue:SubDict[@"master_url"] forKey:@"imageGif"];
//                    [icondict setValue:SubDict[@"id"] forKey:@"EmojiID"];
//
//                    [emoticons addObject:icondict];
//                }
//
//                [Emojidict setValue:emoticons forKey:@"emoticons"];
//
//                [emojiArr addObject:Emojidict];
//            }
//
//            [HHGlobals saveEmojiDict:emojiArr];
//        }
//
//        [self getCollectionemoji];
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
}


-(void)getCollectionemoji{
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    [HMJBusinessManager GET:@"/im/appAdd/listCollection" Parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//            NSArray *DataArry = dic[@"data"][@"info"][@"collection"];
//            
//            NSMutableDictionary *Emojidict = [NSMutableDictionary dictionary];
//            
//            [Emojidict setObject:dic[@"data"][@"info"][@"heartUrl"] forKey:@"cover_pic"];
//            
//            NSMutableArray *emoticons = [NSMutableArray array];
//            
//            for (NSDictionary *SubDict in DataArry) {
//                
//                NSMutableDictionary *icondict = [NSMutableDictionary dictionary];
//                
//                [icondict setValue:SubDict[@"nextUrl"] forKey:@"image"];
//                [icondict setValue:SubDict[@"name"] forKey:@"desc"];
//                [icondict setValue:SubDict[@"imgUrl"] forKey:@"imageGif"];
//                [icondict setValue:SubDict[@"next_id"] forKey:@"EmojiID"];
//                
//                [emoticons addObject:icondict];
//            }
//            
//            [Emojidict setValue:emoticons forKey:@"emoticons"];
//            
//            NSMutableArray *arry = [HHGlobals getEmojiDict].mutableCopy;
//            
//            [arry insertObject:Emojidict atIndex:0];
//            
//            [HHGlobals saveEmojiDict:arry];
//            
//        }
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//        
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//        [[HMJShowLoding sharedInstance]hiddenLoading];
//    }];
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
   
    return (self.dataMode.count%4 == 0 ? self.dataMode.count/4 : self.dataMode.count/4 + 1) * ((SCREEN_WIDTH - 3) / 4) ;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HMKStickerDetailsCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.dataMode = self.dataMode;
    return cell;
}



@end
