//
//  BaseTableViewController.h
//  huihe
//
//  Created by changle on 2016/12/13.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseTableViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate , UISearchControllerDelegate ,UISearchResultsUpdating ,UIScrollViewDelegate>
{
    BOOL isSearch;
}
@property (nonatomic, strong) NSMutableArray *searchArray;
@property (nonatomic, strong) NSMutableArray *dataArray;

//搜索展示控制器
@property (nonatomic , strong) UISearchController * searchController;

@property (strong, nonatomic) UITableView *tableView;

- (void)creatWithTableViewStyle:(UITableViewStyle)style;

- (void)supportSearchBar;

- (void)creatTableViewHeaderView;
- (void)creatTableViewFooterView;

@end
