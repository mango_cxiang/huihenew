//
//  HMJEmojiFaceView.m
//  huihe
//
//  Created by Six on 16/3/2.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJEmojiFaceView.h"

// 单个表情尺寸大小
#define kEmojiSize  CGSizeMake(44, 44)
// 内容面板与边界距离
#define kEmojiInset UIEdgeInsetsZero
// pageControl 距离底部的高度
#define kPageControlBottomInset 0
// 行距
#define kLineSpace 0
// 间距
#define kEmojiSpace 0
// 表情按钮基准 tag值
#define ButtonTag 1234
// 滚动视图高度
#define ScrollViewHeight 175

@interface HMJEmojiFaceView ()

// 滚动视图
@property (nonatomic , strong) UIScrollView * scrollView;
// 滚动小圆点(pageControl)
@property (nonatomic , strong) UIPageControl * pageControl;
// 表情数据
@property (nonatomic , strong) NSDictionary * emojiDict;
// 排序数组
@property (nonatomic , strong) NSArray * sortArray;

@end

@implementation HMJEmojiFaceView

+ (NSDictionary *)localEmojiDict
{
    NSString * path = [[NSBundle mainBundle] pathForResource:@"Face" ofType:@"plist"];
    NSDictionary * dict = [NSDictionary dictionaryWithContentsOfFile:path];
    return dict;
}

- (NSDictionary *)emojiDict
{
    if (!_emojiDict)
    {
        _emojiDict = [[self class] localEmojiDict];
    }
    return _emojiDict;
}

- (NSArray *)sortArray
{
    if (!_sortArray)
    {
        NSArray * sortArray = [self.emojiDict.allValues sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2 options:NSNumericSearch];
        }];
        _sortArray = sortArray;
    }
    return _sortArray;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        //创建子视图
        [self createSubViews];
    }
    return self;
}

- (instancetype)init
{
    if (self = [super init])
    {
        //创建子视图
        [self createSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        //创建子视图
        [self createSubViews];
    }
    return self;
}

// 创建子视图
- (void)createSubViews
{
    self.backgroundColor = UIColorFromRGB(0xebebeb);
    
    // 滚动视图
    UIScrollView * scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
    scrollView.height = ScrollViewHeight;
    self.scrollView = scrollView;
    scrollView.pagingEnabled = YES;
    scrollView.backgroundColor = UIColorFromRGB(0xf4f4f5);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.delegate = self;
    [self addSubview:scrollView];
    
    CGSize contentSize = CGSizeMake(scrollView.width, scrollView.height);
    // 计算每行显示的最大表情数量
    int maxEmotionsNumberInRow = (contentSize.width - kEmojiInset.left - kEmojiInset.right + kEmojiSpace) / (kEmojiSize.width + kEmojiSpace);
    // 计算每列显示的最大表情数量
    int maxEmotionNumberInColumn = (contentSize.height - kEmojiInset.top - kEmojiInset.bottom + kLineSpace) / (kEmojiSize.height + kLineSpace);
    //计算每一行表情之间的实际距离
    float interEmotionSpacing = (contentSize.width - maxEmotionsNumberInRow * kEmojiSize.width - kEmojiInset.left - kEmojiInset.right) / (CGFloat)(maxEmotionsNumberInRow - 1);
    //计算每一列表情之间的实际距离
    float lineEmotionSpacing = (contentSize.height - 35 - maxEmotionNumberInColumn * kEmojiSize.height - kEmojiInset.top - kEmojiInset.bottom) / (CGFloat)(maxEmotionNumberInColumn - 1);
    
    //创建 表情面板
    int emotionCount = (int)self.emojiDict.count;
    int currentEmotionLine = 0;
    int emotionIndexOffset = 0;
    for (int index = 0; index < emotionCount; index++)
    {
        currentEmotionLine = (index + emotionIndexOffset) / maxEmotionsNumberInRow;
        
        //当达到每页最后一排的最后一列时，增加偏移量以便空出位置放置删除按键
        if ((index + emotionIndexOffset) % (maxEmotionNumberInColumn * maxEmotionsNumberInRow) == maxEmotionsNumberInRow * maxEmotionNumberInColumn - 1)
        {
            //添加删除按键
            UIButton * deleteButton = [self createDeleteButton];
            CGRect deleteButtonRect = CGRectMake(0, 0, kEmojiSize.width, kEmojiSize.height);
            deleteButtonRect.origin.x = kEmojiInset.left + (index + emotionIndexOffset) % maxEmotionsNumberInRow * (interEmotionSpacing + kEmojiSize.width) + contentSize.width * (currentEmotionLine / maxEmotionNumberInColumn);
            deleteButtonRect.origin.y = kEmojiInset.top + currentEmotionLine % maxEmotionNumberInColumn * (kEmojiSize.height + lineEmotionSpacing);
            deleteButton.frame = deleteButtonRect;
            [self.scrollView addSubview:deleteButton];
            emotionIndexOffset++;
            currentEmotionLine = (index + emotionIndexOffset) / maxEmotionsNumberInRow;
        }
        
        UIButton * emojiButton = [self createEmojiButtonWithIndex:index];
        CGRect buttonFrame = CGRectMake(0, 0, kEmojiSize.width, kEmojiSize.height);
        buttonFrame.origin.x = kEmojiInset.left + (index + emotionIndexOffset) % maxEmotionsNumberInRow * (interEmotionSpacing + kEmojiSize.width) + contentSize.width * (currentEmotionLine / maxEmotionNumberInColumn);
        buttonFrame.origin.y = kEmojiInset.top + currentEmotionLine % maxEmotionNumberInColumn * (kEmojiSize.height + lineEmotionSpacing);
        emojiButton.frame = buttonFrame;
        [self.scrollView addSubview:emojiButton];
    }
    
    //如果最后一页未满屏，则在最后一页的最后一个表情后面加上删除键
    if ((emotionCount + emotionIndexOffset) % (maxEmotionNumberInColumn * maxEmotionsNumberInRow))
    {
        //添加删除按键
        UIButton * deleteButton = [self createDeleteButton];
        CGRect deleteButtonRect = CGRectMake(0, 0, kEmojiSize.width, kEmojiSize.height);
        deleteButtonRect.origin.x = kEmojiInset.left + (emotionCount + emotionIndexOffset) % maxEmotionsNumberInRow * (interEmotionSpacing + kEmojiSize.width) + contentSize.width * (currentEmotionLine / maxEmotionNumberInColumn);
        deleteButtonRect.origin.y = kEmojiInset.top + currentEmotionLine % maxEmotionNumberInColumn * (kEmojiSize.height + lineEmotionSpacing);
        deleteButton.frame = deleteButtonRect;
        [self.scrollView addSubview:deleteButton];
    }
    
    //计算并设置scrollview的contentSize
    self.scrollView.contentSize = CGSizeMake(contentSize.width * (currentEmotionLine / maxEmotionNumberInColumn + 1), 0);

    // 滚动小圆点(pageControl)
    UIPageControl * pageControl = [[UIPageControl alloc] init];
    self.pageControl = pageControl;
    pageControl.numberOfPages = currentEmotionLine / maxEmotionNumberInColumn + 1;
    pageControl.currentPage = 0;
    pageControl.currentPageIndicatorTintColor = UIColorFromRGB(0x8c9094);
    pageControl.pageIndicatorTintColor = UIColorFromRGB(0xcbcbcb);
    CGSize pageControlSize = [pageControl sizeForNumberOfPages:pageControl.numberOfPages];
    CGRect pageControlRect = CGRectMake((contentSize.width - pageControlSize.width) / 2.f,
                                        contentSize.height - pageControlSize.height - kPageControlBottomInset,
                                        pageControlSize.width,
                                        pageControlSize.height);

    pageControl.frame = pageControlRect;
    [self addSubview:pageControl];
    
    //创建底部表情视图
    UIView * faceView = [[UIView alloc] initWithFrame:CGRectMake(0, scrollView.height, 104 / 2, self.height - scrollView.height)];
    faceView.backgroundColor = UIColorFromRGB(0xf4f4f5);
    [self addSubview:faceView];
    CGFloat faceImageViewWidth = 44 / 2;
    UIImageView * faceImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"f1"]];
    faceImageView.frame = CGRectMake((faceView.width - faceImageViewWidth) / 2, (faceView.height - faceImageViewWidth) / 2, faceImageViewWidth, faceImageViewWidth);
    [faceView addSubview:faceImageView];
    
    // 发送按钮
    UIButton * sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sendButton = sendButton;
    sendButton.frame = CGRectMake(self.width - 60, faceView.y, 60, faceView.height);
    sendButton.backgroundColor = UIColorFromRGB(0xffffff);
    [sendButton setTitle:@"发送" forState:UIControlStateNormal];
    [sendButton setTitleColor:UIColorFromRGB(0x505050) forState:UIControlStateNormal];
    [sendButton setTitleColor:UIColorFromRGB(0x505050) forState:UIControlStateHighlighted];
    sendButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    sendButton.titleLabel.font = SystemFont(16);
    [sendButton addTarget:self action:@selector(sendButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sendButton];
}

//创建 表情按键
- (UIButton *)createEmojiButtonWithIndex:(NSInteger)index
{
    UIButton * emojiButton = [UIButton buttonWithType:UIButtonTypeCustom];
    emojiButton.tag = ButtonTag + index;
    [emojiButton addTarget:self action:@selector(emojiButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [emojiButton setImage:[UIImage imageNamed:self.sortArray[index]] forState:UIControlStateNormal];
    [emojiButton setImage:[UIImage imageNamed:self.sortArray[index]] forState:UIControlStateHighlighted];
    return emojiButton;
}

// 创建 删除按钮
- (UIButton *)createDeleteButton
{
    UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [deleteButton addTarget:self action:@selector(deleteButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [deleteButton setImage:[UIImage imageNamed:@"emoji_delete_button"] forState:UIControlStateNormal];
    [deleteButton setImage:[UIImage imageNamed:@"emoji_delete_button"] forState:UIControlStateHighlighted];
    return deleteButton;
}

// 获取表情名
- (NSString *)getEmojiNameStrWithIndex:(NSInteger)index
{
    if (index >= self.emojiDict.count)
    {
        return nil;
    }
    return self.sortArray[index];
}

- (NSString *)getEmojiFaceStrWithIndex:(NSInteger)index
{
    if (index >= self.emojiDict.count)
    {
        return nil;
    }
    NSArray * keyArray = [self.emojiDict keysSortedByValueWithOptions:NSSortConcurrent usingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    return keyArray[index];
}

#pragma mark - 按钮点击事件
// 表情按钮 被点击
- (void)emojiButtonOnClick:(UIButton *)button
{
    NSInteger emojiButtonIndex = button.tag - ButtonTag;
    NSString * emojiNameStr = [self getEmojiNameStrWithIndex:emojiButtonIndex];
    NSString * emojiFaceStr = [self getEmojiFaceStrWithIndex:emojiButtonIndex];
    if (self.delegate && [self.delegate respondsToSelector:@selector(emojiFaceView:didClickOnEmojiButton:emojiNameStr:emojiFaceStr:)])
    {
        [self.delegate emojiFaceView:self didClickOnEmojiButton:button emojiNameStr:emojiNameStr emojiFaceStr:emojiFaceStr];
    }
}

// 删除按钮 被点击
- (void)deleteButtonOnClick:(UIButton *)button
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(emojiFaceView:didClickOnDeleteButton:)])
    {
        [self.delegate emojiFaceView:self didClickOnDeleteButton:button];
    }
}

// 发送按钮 被点击
- (void)sendButtonOnClick:(UIButton *)button
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(emojiFaceView:didClickOnSendButton:)])
    {
        [self.delegate emojiFaceView:self didClickOnSendButton:button];
    }
}

#pragma mark - 协议 UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.pageControl.currentPage = scrollView.contentOffset.x / scrollView.width;
}

@end
