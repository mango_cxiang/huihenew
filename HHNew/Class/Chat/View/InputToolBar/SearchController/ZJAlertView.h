//
//  ZJAlertView.h
//  ZJAnimationPopViewDemo
//
//  Created by abner on 2017/8/15.
//  Copyright © 2017年 Abnerzj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYImage.h"
@interface ZJAlertView : UIView

@property (nonatomic, copy) void(^canceSureActionBlock)(BOOL isSure);
@property (nonatomic, copy) void(^closeBlock)(void);
@property (weak, nonatomic) IBOutlet YYAnimatedImageView *EmojiImageView;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIButton *AddBtn;
@property (weak, nonatomic) IBOutlet UIButton *quxiaoBtn;

@end
