//
//  HMJInputFunctionView.m
//  huihe
//
//  Created by Six on 16/3/8.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJInputFunctionView.h"
#import "NSString+Extension.h"
#import "NSMutableAttributedString+Emoji.h"
#import <AVFoundation/AVFoundation.h>
#import "NSAttributedString+PPAddition.h"
#import "PPStickerKeyboard.h"
#import "PPUtil.h"
#import "PPStickerDataManager.h"
#import "HMJChatToolView.h"
#import "HMJEmojiFaceView.h"
#import "NIMKitMediaFetcher.h"
#import "HHChatFrameModel.h"
#import "HDJCameraViewController.h"
#import "UIView+LLXAlertPop.h"
#import "HHSendRedPacketViewController.h"

// 此枚举: 不同按钮的tag区分
typedef NS_ENUM(NSInteger , TOButtonTag)
{
    TOButtonTagVoice = 0,
    TOButtonTagEmoji,
    TOButtonTagAdd,
};

// 按钮宽度
#define ButtonWidth 27
// 按钮高度
#define ButtonHeight 27
// 输入框 高度值
#define TextViewH 40

#define InputFunctionViewHeight 56
// 表情面板 高度
#define EmojiFaceViewHeight 210
@interface HMJInputFunctionView ()<HMJRecordViewDelegate,PPStickerKeyboardDelegate,HMJChatToolViewDelegate,HDJCameraViewControllerDelegate,UIDocumentPickerDelegate>
{
    CGPoint _startPoint;
}

// 聊天工具视图  用于显示发送图片，位置，文件等
@property(nonatomic,strong)HMJChatToolView * chatToolView;
//表情键盘
@property (nonatomic, strong)PPStickerKeyboard * stickerKeyboard;
// 弹出视图的动画时间
@property (nonatomic , assign) NSTimeInterval duration;
// 键盘 弹出高度
@property (nonatomic , assign) CGRect keyboardRect;
// 工具条高度
@property (nonatomic , assign) CGRect inputFunctionViewRect;
// 是否点击
@property (nonatomic , assign) BOOL isClick;
// 语音按钮
@property (nonatomic , strong) UIButton * voiceButton;
// 按住录音按钮
@property (nonatomic , strong) UIButton * pressVoiceButton;
// 录音时的附加页面
@property (nonatomic , strong) HMJRecordView * recordView;
// 输入框
@property (nonatomic , strong) HMJTextView * textView;
// 表情按钮
@property (nonatomic , strong) UIButton * emojiButton;
// 加号按钮
@property (nonatomic , strong) UIButton * addButton;
// 文件名
@property (nonatomic , copy) NSString * fileName;
// 录音路径
@property (nonatomic , strong) NSURL * fileUrl;
//// 录音器
//@property (nonatomic , strong) AVAudioRecorder * recorder;
// 播放器
@property (nonatomic , strong) AVAudioPlayer * player;
// 录音和播放
@property (nonatomic , strong) AVAudioSession * audioSession;
//图片选择器
@property(nonatomic  , strong) NIMKitMediaFetcher * mediaFetcher;
//拍摄控制器
@property(nonatomic  , strong) HDJCameraViewController * shootingVC;
//文件选择器
@property(nonatomic  , strong) UIDocumentPickerViewController * documentPickerVC;

@end
@implementation HMJInputFunctionView

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self creatUIWithFram:frame];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrameNotification:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrameNotification:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}
-(void)creatUIWithFram:(CGRect)frame{
    self.backgroundColor = ColorManage.inputBar_color;
    
    UIView *viewlin = [[UIView alloc]init];
    viewlin.frame = CGRectMake(0, 0, SCREEN_WIDTH, 0.4);
    viewlin.backgroundColor = RGB_Color(220, 220, 220);
    [self addSubview:viewlin];
    
    
    CGFloat buttonSpace = 15.0;
    
    // 语音按钮
    UIButton * voiceButton = [UIButton createButtonWithNormalImageName:@"chat_voice" highlightedImageName:@"chat_voice" selectedImageName:@"chat_keyboard"];
    self.voiceButton = voiceButton;
//    voiceButton.frame = CGRectMake(voiceButtonX, btnY, ButtonWidth, ButtonHeight);
    voiceButton.tag = TOButtonTagVoice;
    [voiceButton addTarget:self action:@selector(buttonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:voiceButton];
    [voiceButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.left.offset(buttonSpace);
        make.width.offset(ButtonWidth);
        make.height.offset(ButtonHeight);
    }];

    // 加号按钮
    UIButton * addButton = [UIButton createButtonWithNormalImageName:@"chat_add" highlightedImageName:@"chat_add" selectedImageName:@"chat_add"];
    self.addButton = addButton;
//    addButton.frame = CGRectMake(CGRectGetWidth(frame) - ButtonWidth-voiceButtonX, btnY, ButtonWidth, ButtonHeight);
    addButton.tag = TOButtonTagAdd;
    [addButton addTarget:self action:@selector(buttonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:addButton];
    [addButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-buttonSpace);
        make.centerY.offset(0);
        make.width.offset(ButtonWidth);
        make.height.offset(ButtonHeight);
    }];
    
    // 表情按钮
    UIButton * emojiButton = [UIButton createButtonWithNormalImageName:@"chat_expression" highlightedImageName:@"chat_expression" selectedImageName:@"chat_keyboard"];
    self.emojiButton = emojiButton;
//    emojiButton.frame = CGRectMake(addButton.x-ButtonWidth-buttonSpace, btnY, ButtonWidth, ButtonHeight);
    emojiButton.tag = TOButtonTagEmoji;
    [emojiButton addTarget:self action:@selector(buttonOnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:emojiButton];
    [emojiButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.addButton.mas_left).offset(-buttonSpace);
        make.centerY.offset(0);
        make.width.offset(ButtonWidth);
        make.height.offset(ButtonHeight);
    }];
    
    //输入框
    CGFloat textViewX = ButtonWidth+2*buttonSpace;
    CGFloat textViewWidth = CGRectGetWidth(frame) - 3*ButtonWidth - 5*buttonSpace;
    CGFloat textViewY = (CGRectGetHeight(frame) - TextViewH)/2;
    _textView = [[HMJTextView alloc] initWithFrame:CGRectMake(textViewX, textViewY, textViewWidth, TextViewH)];
    _textView.backgroundColor = [UIColor whiteColor];
    _textView.font = SystemFont(16);
    _textView.lineNum = 5;
    _textView.textViewDelegate = self;
    _textView.delegate = self;
    [_textView viewWithRadis:4];
    _textView.returnKeyType = UIReturnKeySend;
    [self addSubview:_textView];
    
    // 按住录音按钮
    UIButton * pressVoiceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.pressVoiceButton = pressVoiceButton;
    pressVoiceButton.frame = _textView.frame;
    pressVoiceButton.hidden = YES;
    [pressVoiceButton setBackgroundImage:[UIImage NYimageWithColor:UIColorFromRGB(0xffffff)] forState:UIControlStateNormal];
    [pressVoiceButton setBackgroundImage:[UIImage NYimageWithColor:RGB_Color(220, 221, 220)] forState:UIControlStateHighlighted];
    [pressVoiceButton viewWithRadis:4];
    pressVoiceButton.titleLabel.font = SystemFont(17);
    [pressVoiceButton setTitle:@"按住 说话" forState:UIControlStateNormal];
    [pressVoiceButton setTitle:@"松开 结束" forState:UIControlStateHighlighted];
    [pressVoiceButton setTitleColor:RGB_Color(85, 85, 85) forState:UIControlStateNormal];
    [pressVoiceButton addTarget:self action:@selector(pressVoiceButtonTouchDown:) forControlEvents:UIControlEventTouchDown];
    [pressVoiceButton addTarget:self action:@selector(pressVoiceButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    [pressVoiceButton addTarget:self action:@selector(pressVoiceButtonTouchUpOutside:) forControlEvents:UIControlEventTouchUpOutside];
    [pressVoiceButton addTarget:self action:@selector(pressVoiceButtonTouchUpOutside:) forControlEvents: UIControlEventTouchCancel];
    [pressVoiceButton addTarget:self action:@selector(pressVoiceButtonTouchDragExit:) forControlEvents:UIControlEventTouchDragExit];
    [pressVoiceButton addTarget:self action:@selector(pressVoiceButtonTouchDragEnter:) forControlEvents:UIControlEventTouchDragEnter];
    [self addSubview:pressVoiceButton];
}

#pragma mark - 按钮点击事件
- (void)buttonOnClick:(UIButton *)button
{
    switch (button.tag)
    {
        case TOButtonTagVoice:
        {
            button.selected = !button.selected;
            
            self.isClick = button.selected;
            self.emojiButton.selected = NO;
            self.addButton.selected = NO;
            self.pressVoiceButton.hidden = !button.selected;
            self.textView.hidden = button.selected;
            if (button.selected){
                //键盘失去响应，toolbar回到底部，chatTool隐藏
                [self.textView resignFirstResponder];
                // 记录底层视图动态高度
                self.inputFunctionViewRect = self.frame;
                // 恢复底层视图默认高度
                CGRect inputFunctionViewRect = self.frame;
                inputFunctionViewRect.size.height = InputFunctionViewHeight;
                self.frame = inputFunctionViewRect;
                [self inputFunctionViewAutoChangeWithRect:CGRectZero inputFunctionViewRect:self.frame duration:self.duration state:TOShowViewStateVoice];
                self.keyboardRect = self.frame;
            }else{
                //键盘失去响应，toolbar回到底部，chatTool隐藏
                self.textView.inputView = nil;
                [self.textView becomeFirstResponder];
                [self inputFunctionViewAutoChangeWithRect:self.keyboardRect inputFunctionViewRect:self.inputFunctionViewRect duration:self.duration state:TOShowViewStateVoice];
            }
            if ([self.delegate respondsToSelector:@selector(didClickOnVoiceButtonWithSelected:)])
            {
                [self.delegate didClickOnVoiceButtonWithSelected:button.selected];
            }
        }
            break;
            
        case TOButtonTagEmoji:
        {
            button.selected = !button.selected;
            self.addButton.selected = NO;
            self.voiceButton.selected = NO;
            self.pressVoiceButton.hidden = YES;
            self.textView.hidden = NO;
            
            if (button.selected){
                self.textView.inputView = self.stickerKeyboard;
                [self.textView becomeFirstResponder];
                [self.textView reloadInputViews];
            } else {
                self.textView.inputView = nil;
                [self.textView becomeFirstResponder];
                [self.textView reloadInputViews];
            }
            if ([self.delegate respondsToSelector:@selector(didClickOnEmojiButtonWithSelected:)])
            {
                [self.delegate didClickOnEmojiButtonWithSelected:button.selected];
            }
        }
            break;
            
        case TOButtonTagAdd:
        {
//            if (button.selected) {
//                return;
//            }
            button.selected = !button.selected;
            self.emojiButton.selected = NO;
            self.voiceButton.selected = NO;
            self.pressVoiceButton.hidden = YES;
            self.textView.hidden = NO;
            if (button.selected){
                self.isClick = YES;
                [self.textView resignFirstResponder];
//                self.textView.inputView = self.chatToolView;
                [self inputFunctionViewAutoChangeWithRect:self.chatToolView.frame inputFunctionViewRect:self.frame duration:self.duration state:TOShowViewStateChatTool];
                self.keyboardRect = CGRectMake(self.chatToolView.frame.origin.x, self.chatToolView.frame.origin.y, SCREEN_WIDTH, self.chatToolView.height);
            }else{
                self.textView.inputView = nil;
                [self.textView becomeFirstResponder];
                [self.textView reloadInputViews];
            }
            if ([self.delegate respondsToSelector:@selector(didClickOnAddButtonWithSelected:)])
            {
                [self.delegate didClickOnAddButtonWithSelected:button.selected];
            }
        }
            break;
        default:
            break;
    }
}
#pragma mark --- HMJChatToolViewDelegate
-(void)chatToolView:(HMJChatToolView *)chatToolView didClickOnButtonWithIndex:(NSUInteger)index
{
    switch (index) {
        case 0:
        {
            //照片
            [self checkImagePickerView];
        }
            break;
        case 1:
        {
            //拍摄
            [self checkImagePickerVideo];
        }
            break;
        case 2:
        {
            //通话
            [self checkCall];
        }
            break;
        case 3:
        {
            //位置
            [self checkLocation];
        }
            break;
        case 4:
        {
            //红包
            [self checkRedPacket];
        }
            break;
        case 5:
        {
            //名片
            
        }
            break;
        case 6:
        {
            //文件
            [self checkFile];
        }
            break;
        case 7:
        {
            //收藏
            
        }
            break;
            
        default:
            break;
    }
}
#pragma mark -  选择图片(包括动态图)
- (void)checkImagePickerView
{
    WeakSelf(self)
    self.mediaFetcher.mediaTypes = @[(NSString *)kUTTypeImage,(NSString *)kUTTypeMovie];
    [self.mediaFetcher fetchPhotoFromLibrary:^(NSArray *images, NSArray *imageModel,NSString *path, PHAssetMediaType type) {
        switch (type) {
            case PHAssetMediaTypeImage:
            {
                if ([weakself.delegate respondsToSelector:@selector(didSendImage:)]) {
                    [weakself.delegate didSendImage:images];
                }
            }
                break;
            case PHAssetMediaTypeVideo:
            {
                if(path==nil){
                    ShowToast(@"请从iCloud下载后发送")
                    return ;
                }
                NSDictionary *opts = [NSDictionary dictionaryWithObject:@(NO) forKey:AVURLAssetPreferPreciseDurationAndTimingKey];
                AVURLAsset *urlAsset = [AVURLAsset URLAssetWithURL:[NSURL fileURLWithPath:path] options:opts]; // 初始化视频媒体文件
                double second = urlAsset.duration.value / urlAsset.duration.timescale;
                if (second > 20) {
                    ShowToast(@"视频超过20秒，请重新选择")
                    return ;
                }
                if ([self.delegate respondsToSelector:@selector(didSendVideo:)]) {
                    [self.delegate didSendVideo:[NSURL fileURLWithPath:path]];
                }
            }
                break;
            default:
                return;
        }
    }];
}
#pragma mark -- 拍摄
- (void)checkImagePickerVideo
{
    if ([HHGlobals checkCameraStatus]) {
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:self.shootingVC animated:YES completion:nil];
        [self.shootingVC fetchVideoMediaFromHDFetchResult:^(NSString *path, HDJMediaType mediaType) {
            if (mediaType == HDJMediaTypeVideo) {
                //拍摄视频处理
                if ([self.delegate respondsToSelector:@selector(didSendVideo:)]) {
                    [self.delegate didSendVideo:[NSURL fileURLWithPath:path]];
                }
            }
        }];
    }
}
//HDJCameraViewControllerDelegate
- (void)cameraViewController:(HDJCameraViewController *)camera didFinishMediaWithType:(HDJMediaType)mediaType withInfo:(NSDictionary<NSString *,id> *)info{
    if(mediaType==HDJMediaTypeImage){
        //拍摄图片处理
        UIImage* image = info[HDJCameraViewControllerImage];
        if ([self.delegate respondsToSelector:@selector(didSendImage:)]) {
            [self.delegate didSendImage:@[image]];
        }
    }
}
#pragma mark --- 通话
-(void)checkCall
{
    
//    AFNetworkReachabilityManager *mgr = [AFNetworkReachabilityManager sharedManager];
//    if (mgr.isReachable) {
//        NSMutableArray *contactArr = (NSMutableArray *)[[HMJFMDBContactsDataModel sharedInstance] queryDataFromDataBaseWithContactId:[NSString stringWithFormat:@"%zi",self.destId]];
//        HMJContactsModel *model = [contactArr firstObject];
        //判断是否显示拨打电话
        NSArray *arrayTitle = @[@"  语音通话",@"  视频通话", @"  拨打电话"];
        UIColor *color1 = RGB_Color(0, 0, 0);//KColor(88, 168, 243);
        UIColor *color2 = RGB_Color(0, 0, 0);//KColor(88, 168, 243);
        UIColor *color3 = RGB_Color(0, 0, 0);//KColor(88, 168, 243);
        NSMutableArray *color = [NSMutableArray arrayWithCapacity:0];
        [color addObject:color1];
        [color addObject:color2];
        [color addObject:color3];
        UIViewController * vc = (UIViewController *)self.delegate;
        [vc.view createAlertViewTitleArray:arrayTitle textColor:color font:SystemFont(17) actionBlock:^(UIButton * _Nullable button, NSInteger didRow) {
            if (didRow == 0) {
                //语音通话
//                AgoraVoiceVideoViewController * vc = [[AgoraVoiceVideoViewController alloc]init];
//                vc.agoraVoiceVideoStyle = AgoraVoiceVideoStyleVoiceLoading;
//                NSString *content = [NSString stringWithFormat:@"%@%zi,%@",[NSString stringWithFormat:@"%ld", (long)[[NSDate  date] timeIntervalSince1970]],self.destId,[NSString acquireUserId]];
//                vc.roomName = self.title;
//                vc.roomId = content;
//                vc.destId = [NSString stringWithFormat:@"%zi",self.destId];
//                WeakSelfType weakSelf = self;
//                [vc returnDidFinishAgoraVoiceVideoBlock:^(AgoraVoiceVideoBlockStyle type, NSString *time) {
//                    [weakSelf addMessageDidFinishAgoraVoiceVideoBlock:type time:time];
//                }];
//                vc.modalPresentationStyle = UIModalPresentationFullScreen;
//                [self presentViewController:vc animated:YES completion:nil];
//                // 创建自己发送的发起语音消息
//                [[AgoraVoiceVideoManager sharedManager] agoraVoiceVideoManagerSendMessageContent:content conversationKey:[NSString stringWithFormat:@"%ld",(long)self.destId] messageType:TOMessageTypeVoiceRequest];
            }else if (didRow == 1){
                //视频通话
                
            }else if (didRow == 2){
                //拨打电话
                
            }
            
        }];
        
//    }else{
//        [HHGlobals showAlertWithTitle:@"当前网络不可用，无法通话，检查网络设置后再试。"];
//    }
}
#pragma mark --- 位置
-(void)checkLocation
{
    WEAKSELF
    NSArray *arrayTitle = @[@"发送位置",@"开始位置共享"];
    UIColor *color1 = RGB(0, 0, 0);
    UIColor *color2 = RGB(0, 0, 0);
    NSMutableArray *color = [NSMutableArray arrayWithCapacity:0];
    [color addObject:color1];
    [color addObject:color2];
    UIViewController * vc = (UIViewController *)self.delegate;
    [vc.view createAlertViewTitleArray:arrayTitle textColor:color font:[UIFont systemFontOfSize:17*PROPORTION] actionBlock:^(UIButton * _Nullable button, NSInteger didRow) {
        if (didRow==0) {
            //发送位置
            [HHGlobals choosePositionCompleted:^(NSString * _Nonnull Location, UIImage * _Nonnull Snapshot) {
                if (Location) {
//                    [weakSelf creatMessageFrameModelWithContent:Location message:TOMessageFromMe messageType:TOMessageTypePosition];
                }else{
                }
            }];
        }else{
            //位置共享
            ShowToast(@"该功能维护中")
        }
    }];
}
#pragma mark --- 红包
-(void)checkRedPacket
{
    HHSendRedPacketViewController * vc = Init(HHSendRedPacketViewController);
    vc.isGroup = self.isGroup;
    UIViewController * selfVC = (UIViewController *)self.delegate;
    [selfVC.navigationController pushViewController:vc animated:YES];
}
#pragma mark --- 文件
-(void)checkFile
{
    UIViewController * selfVC = (UIViewController *)self.delegate;
    [selfVC presentViewController:self.documentPickerVC animated:YES completion:nil];
}
//UIDocumentPickerDelegate
- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentsAtURLs:(NSArray<NSURL *> *)urls {
    //获取授权
    BOOL fileUrlAuthozied = [urls.firstObject startAccessingSecurityScopedResource];
    if (fileUrlAuthozied) {
        //通过文件协调工具来得到新的文件地址，以此得到文件保护功能
        NSFileCoordinator *fileCoordinator = [[NSFileCoordinator alloc] init];
        NSError *error;
        WEAKSELF
        [fileCoordinator coordinateReadingItemAtURL:urls.firstObject options:0 error:&error byAccessor:^(NSURL *newURL) {
            //读取文件
            NSString *fileName = [newURL lastPathComponent];
            NSError *error = nil;
            NSData *fileData = [NSData dataWithContentsOfURL:newURL options:NSDataReadingMappedIfSafe error:&error];
            if (error) {
                [[HMJShowLoding sharedInstance]showText:@"解析出错！"];
            } else {
//                [HMJBusinessManager OSSuploadfile:@[fileData] fileName:BQ_CHAT_PATH isAsync:YES format:[newURL pathExtension] complete:^(NSArray<NSString *> *names) {
//
//                    NSMutableDictionary *Datadict = [NSMutableDictionary dictionary];
//                    [Datadict setObject:fileName forKey:@"fileName"];
//                    [Datadict setObject:[self sizeStr:[fileData length]] forKey:@"fileSize"];
//                    [Datadict setObject:names[0] forKey:@"fileUrl"];
//                    HMJFileObject *model = [HMJFileObject yy_modelWithDictionary:Datadict];
//                    NSString *content = [(HMJFileObject *)model yy_modelToJSONString];
//                    [weakSelf creatMessageFrameModelWithContent:content message:TOMessageFromMe messageType:TOMessageTypeDocument];
//                } fail:^{
//
//                }];
            }
        }];
        [urls.firstObject stopAccessingSecurityScopedResource];
    } else {
        //授权失败
    }
}

#pragma mark ---  按住录音按钮 相关点击事件
/**
*  按下录音按钮开始录音
*/
- (void)pressVoiceButtonTouchDown:(UIButton *)button
{
    if ([self IsRecord]){
        if(self.recordView.avRecorder&&self.recordView.avRecorder.recording){
            return;
        }
        //停止播放语音
        [[NSNotificationCenter defaultCenter] postNotificationName:StopPlayVoiceNotification object:nil];
        [self.pressVoiceButton addTarget:self action:@selector(pressVoiceButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        NSError * error;
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
        [[AVAudioSession sharedInstance] setActive:YES error:&error];
        int32_t randomId;
        arc4random_buf(&randomId, sizeof(int32_t));
        NSString * fileName = [NSString stringWithFormat:@"%d.wav" , randomId];
        NSString * filePath = [DocumentsPath stringByAppendingPathComponent:fileName];
        self.fileUrl = [NSURL fileURLWithPath:filePath];
        NSDictionary *recordSetting = [[NSDictionary alloc] initWithObjectsAndKeys:
                                       [NSNumber numberWithFloat: 16000], AVSampleRateKey, //采样率
                                       [NSNumber numberWithInt: kAudioFormatLinearPCM],AVFormatIDKey,
                                       [NSNumber numberWithInt:16],AVLinearPCMBitDepthKey,//采样位数 默认 16
                                       [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,//通道的数目
                                       
                                       nil];
        
        AVAudioRecorder * recorder = [[AVAudioRecorder alloc] initWithURL:self.fileUrl settings:recordSetting error:&error];
        if (!error){
            [recorder prepareToRecord];
            recorder.meteringEnabled = YES;
            [recorder peakPowerForChannel:0];
            [recorder record];
        }
        self.recordView.avRecorder = recorder;
        [self.recordView recordButtonTouchDown];
        self.recordView.hidden = NO;
    }
}
/**
*  手指向上滑动取消录音
*/
- (void)pressVoiceButtonTouchUpOutside:(UIButton *)button
{
    [self.recordView recordButtonTouchUpOutside];
    self.recordView.hidden = YES;
    self.recordView.avRecorder.delegate = nil;
    if (self.recordView.avRecorder.recording)
    {
        [self.recordView.avRecorder stop];
    }
    self.recordView.avRecorder = nil;
}
/**
*  松开手指完成录音
*/
- (void)pressVoiceButtonTouchUpInside:(UIButton *)button
{
    self.pressVoiceButton.enabled = NO;
    self.pressVoiceButton.enabled = YES;
    [self finishRecoingVoice];
}
/**
*  当手指离开按钮的范围内时
*/
- (void)pressVoiceButtonTouchDragExit:(UIButton *)button
{
    [self.recordView recordButtonDragOutside];
}
/**
*  当手指再次进入按钮的范围内时
*/
- (void)pressVoiceButtonTouchDragEnter:(UIButton *)button
{
    [self.recordView recordButtonDragInside];
}
// --- HMJRecordViewDelegate 录音达到最大时长后自动发出语音
-(void)recordViewReachMaxRecordTime{
    [self.pressVoiceButton removeTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
    [self finishRecoingVoice];
}
// --- 录音结束方法
-(void)finishRecoingVoice
{
    [self.recordView recordButtonTouchUpInside];
    self.recordView.hidden = YES;
    [self.recordView.avRecorder stop];
    self.recordView.avRecorder = nil;
    AVURLAsset * asset = [AVURLAsset URLAssetWithURL:self.fileUrl options:nil];
    if(asset==nil){
        return;
    }
    CMTime duration = asset.duration;
    Float64 recordSeconds = CMTimeGetSeconds(duration);
    if (recordSeconds <= 1)
    {
        ShowToast(@"说话时间太短")
        return;
    }
    NSString *durantion = [NSString stringWithFormat:@"%ld",(long)recordSeconds];
    NSString * fileName = [self.fileUrl lastPathComponent];
    NSString * filePath = [DocumentsPath stringByAppendingPathComponent:fileName];
    NSURL *urlpath = [NSURL fileURLWithPath:filePath];
    NSMutableDictionary * voiceDic = [NSMutableDictionary dictionary];
    [voiceDic setObject:[urlpath path] forKey:@"url"];
    [voiceDic setObject:durantion forKey:@"time"];
    [voiceDic setObject:@"" forKey:@"str"];
    if ([self.delegate respondsToSelector:@selector(didFinishRecoingVoiceAction:)])
    {
        [self.delegate didFinishRecoingVoiceAction:voiceDic];
    }
}
// --- 检测是否可以录音
- (BOOL)IsRecord
{
    __block BOOL mayRecord = YES;
    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending)
    {
        AVAudioSession * audioSession = [AVAudioSession sharedInstance];
        if ([audioSession respondsToSelector:@selector(requestRecordPermission:)])
        {
            [audioSession performSelector:@selector(requestRecordPermission:) withObject:^(BOOL granted) {
                mayRecord = granted;
            }];
        }
    }
    return mayRecord;
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.voiceButton.selected = NO;
    //self.emojiButton.selected = NO;
    self.addButton.selected = NO;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@""]){
        NSRange selectRange = textView.selectedRange;
        if (selectRange.length > 0){
            //用户长按选择文本时不处理
            return YES;
        }
        // 判断删除的是一个@中间的字符就整体删除
        NSMutableString *string = [NSMutableString stringWithString:textView.text];
        NSArray *matches = [self findAllAt];
        BOOL inAt = NO;
        NSInteger index = range.location;
        for (NSTextCheckingResult *match in matches){
            NSRange newRange = NSMakeRange(match.range.location + 1, match.range.length - 1);
            if (NSLocationInRange(range.location, newRange)){
                inAt = YES;
                index = match.range.location;
                [string replaceCharactersInRange:match.range withString:@""];
                break;
            }
        }
        if (inAt){
            textView.text = string;
            textView.selectedRange = NSMakeRange(index, 0);
            return NO;
        }
    }
    if ([text isEqualToString:@"\n"]){
        if (!IsBlankWithStr(textView.text)){
            if ([self.delegate respondsToSelector:@selector(didSendMesage:)]){
                NSString *contentStr = [@{@"msgString":[self plainText]} mj_JSONString];
                [self.delegate didSendMesage:contentStr];
            }
            textView.attributedText = nil;
            textView.text = nil;
        }
        return NO;
    }
    if ([text isEqualToString:@"@"]) {
        if ([self.delegate respondsToSelector:@selector(mentioneSomeOne)]){
            textView.text = [NSString stringWithFormat:@"%@%@", textView.text, text];
            [self.delegate mentioneSomeOne];
        }
    }
    return YES;
}
- (void)textViewDidChangeSelection:(UITextView *)textView
{
    CGRect rect = [textView caretRectForPosition:textView.selectedTextRange.start];
    CGFloat value = textView.contentOffset.y + textView.bounds.size.height - textView.contentInset.bottom - textView.contentInset.top;
    CGFloat overflow = rect.origin.y + rect.size.height - value;
    if ([self plainText].length == 0) {
        rect.size.height = rect.size.height + 2;
    }
    if (overflow > 0)
    {
        CGPoint offset = textView.contentOffset;
        offset.y += overflow + self.textView.y ;
        [UIView animateWithDuration:0.2 animations:^{
            textView.contentOffset = offset;
        }];
    }
}
-(void)textViewDidChange:(UITextView *)textView{
    if (!self.textView.text.length) {
        return;
    }
    UITextRange *markedTextRange = [self.textView markedTextRange];
    UITextPosition *position = [self.textView positionFromPosition:markedTextRange.start offset:0];
    if (position) {
        return;     // 正处于输入拼音还未点确定的中间状态
    }
    NSRange selectedRange = self.textView.selectedRange;
    
    NSMutableAttributedString *attributedComment = [[NSMutableAttributedString alloc] initWithString:self.plainText attributes:@{ NSFontAttributeName: SystemFont(16), NSForegroundColorAttributeName: [UIColor pp_colorWithRGBString:@"#3B3B3B"]}];
    // 匹配表情
    [PPStickerDataManager.sharedInstance replaceEmojiForAttributedString:attributedComment font:SystemFont(16)];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5.0;
    [attributedComment addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:attributedComment.pp_rangeOfAll];
    
    NSUInteger offset = self.textView.attributedText.length - attributedComment.length;
    self.textView.attributedText = attributedComment;
    self.textView.selectedRange = NSMakeRange(selectedRange.location - offset, 0);
    [self.textView scrollRangeToVisible:self.textView.selectedRange];
}
- (NSArray<NSTextCheckingResult *> *)findAllAt{
    // 找到文本中所有的@
    NSString *string = self.textView.text;
    NSString *kATRegular = @"@[\\u4e00-\\u9fa5\\w\\-\\_]+ ";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:kATRegular options:NSRegularExpressionCaseInsensitive error:nil];
    NSArray *matches = [regex matchesInString:string options:NSMatchingReportProgress range:NSMakeRange(0, [string length])];
    return matches;
}
#pragma mark --- PPStickerKeyboardDelegate
- (void)stickerKeyboard:(PPStickerKeyboard *)stickerKeyboard didClickEmoji:(PPEmoji *)emoji {
    if (emoji.imageGif.length > 0) {
        //表情
        if ([self.delegate respondsToSelector:@selector(didSendMesage:)]) {
            NSString *contentStr = [@{@"msgString":emoji.imageGif , @"msgType" : @"emoji" ,@"emojiName" : emoji.emojiDescription , @"EmojiID" : emoji.EmojiID} mj_JSONString];
            [self.delegate didSendMesage:contentStr];
        }
    }else{
        if (!emoji) {
            return;
        }
        UIImage *emojiImage = [UIImage imageNamed:[@"Sticker.bundle" stringByAppendingPathComponent:emoji.imageName]];
        if (!emojiImage) {
            return;
        }
        NSRange selectedRange = self.textView.selectedRange;
        NSString *emojiString = [NSString stringWithFormat:@"[%@]", emoji.emojiDescription];
        NSMutableAttributedString *emojiAttributedString = [[NSMutableAttributedString alloc] initWithString:emojiString];
        [emojiAttributedString pp_setTextBackedString:[PPTextBackedString stringWithString:emojiString] range:emojiAttributedString.pp_rangeOfAll];
        
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:self.textView.attributedText];
        [attributedText replaceCharactersInRange:selectedRange withAttributedString:emojiAttributedString];
        self.textView.attributedText = attributedText;
        self.textView.selectedRange = NSMakeRange(selectedRange.location + emojiAttributedString.length, 0);
        [self textViewDidChange:self.textView];
    }
}
- (void)stickerKeyboardDidClickDeleteButton:(PPStickerKeyboard *)stickerKeyboard {
    NSRange selectedRange = self.textView.selectedRange;
    if (selectedRange.location == 0 && selectedRange.length == 0) {
        return;
    }
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:self.textView.attributedText];
    if (selectedRange.length > 0) {
        [attributedText deleteCharactersInRange:selectedRange];
        self.textView.attributedText = attributedText;
        self.textView.selectedRange = NSMakeRange(selectedRange.location, 0);
    } else {
        NSUInteger deleteCharactersCount = 1;
        NSString *emojiPattern1 = @"[\\u2600-\\u27BF\\U0001F300-\\U0001F77F\\U0001F900-\\U0001F9FF]";
        NSString *emojiPattern2 = @"[\\u2600-\\u27BF\\U0001F300-\\U0001F77F\\U0001F900–\\U0001F9FF]\\uFE0F";
        NSString *emojiPattern3 = @"[\\u2600-\\u27BF\\U0001F300-\\U0001F77F\\U0001F900–\\U0001F9FF][\\U0001F3FB-\\U0001F3FF]";
        NSString *emojiPattern4 = @"[\\rU0001F1E6-\\U0001F1FF][\\U0001F1E6-\\U0001F1FF]";
        NSString *pattern = [[NSString alloc] initWithFormat:@"%@|%@|%@|%@", emojiPattern4, emojiPattern3, emojiPattern2, emojiPattern1];
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:kNilOptions error:NULL];
        NSArray<NSTextCheckingResult *> *matches = [regex matchesInString:attributedText.string options:kNilOptions range:NSMakeRange(0, attributedText.string.length)];
        for (NSTextCheckingResult *match in matches) {
            if (match.range.location + match.range.length == selectedRange.location) {
                deleteCharactersCount = match.range.length;
                break;
            }
        }
        [attributedText deleteCharactersInRange:NSMakeRange(selectedRange.location - deleteCharactersCount, deleteCharactersCount)];
        self.textView.attributedText = attributedText;
        self.textView.selectedRange = NSMakeRange(selectedRange.location - deleteCharactersCount, 0);
    }
    [self textViewDidChange:self.textView];
}
- (void)stickerKeyboardDidClickSendButton:(PPStickerKeyboard *)stickerKeyboard {
    if ([self plainText].length == 0) {
        return;
    }
    //发送消息
    if ([self.delegate respondsToSelector:@selector(didSendMesage:)]) {
        NSString *contentStr = [@{@"msgString":[self plainText]} mj_JSONString];
        [self.delegate didSendMesage:contentStr];
    }
    self.textView.text = nil;
}
#pragma mark - 协议 HMJTextViewDelegate
- (void)textView:(HMJTextView *)textView didChangeHeight:(CGFloat)height
{
    CGFloat currentHeight;
    if ([self plainText].length == 0) {
         currentHeight= 51;
        self.textView.height = 36;
    }else{
         currentHeight = height + self.textView.y * 2;
    }
    CGRect rect = self.frame;
    CGFloat changeHeight = (currentHeight - rect.size.height);
    rect.origin.y = rect.origin.y - changeHeight;
    rect.size.height = currentHeight;
    self.frame = rect;
}
- (NSString *)plainText
{
    return [self.textView.attributedText pp_plainTextForRange:NSMakeRange(0, self.textView.attributedText.length)];
}
#pragma mark ---  监听键盘弹出事件
- (void)keyboardWillChangeFrameNotification:(NSNotification *)notification
{
    if (self.isLoseEfficacy) {
        return;
    }
    NSDictionary * userInfoDict = notification.userInfo;
    CGRect rect = [userInfoDict[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSTimeInterval duration = [userInfoDict[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    self.duration = duration;
    if ([notification.name isEqualToString:UIKeyboardWillShowNotification])
    {
        // 记录键盘弹出的高度
        self.keyboardRect = rect;
        [self inputFunctionViewAutoChangeWithRect:rect inputFunctionViewRect:self.frame duration:duration state:TOShowViewStateKeyBoard];
    }
    else if([notification.name isEqualToString:UIKeyboardWillHideNotification])
    {
        if (!self.isClick) {
            [self inputFunctionViewAutoChangeWithRect:CGRectZero inputFunctionViewRect:self.frame duration:duration state:TOShowViewStateKeyBoard];
        }
    }
}
#pragma mark ---  动画弹出视图（通过修改坐标）
- (void)inputFunctionViewAutoChangeWithRect:(CGRect)rect inputFunctionViewRect:(CGRect)inputFunctionViewRect duration:(NSTimeInterval)duration state:(TOShowViewState)state
{
    [UIView animateWithDuration:duration animations:^{
        CGFloat inputFunctionViewY = SCREEN_HEIGHT - CGRectGetHeight(rect) - CGRectGetHeight(inputFunctionViewRect);
        if (inputFunctionViewY+SafeAreaBottomHeight+CGRectGetHeight(inputFunctionViewRect)>=SCREEN_HEIGHT) {
            inputFunctionViewY = SCREEN_HEIGHT-SafeAreaBottomHeight-CGRectGetHeight(inputFunctionViewRect);
        }
        self.frame = CGRectMake(0, inputFunctionViewY , CGRectGetWidth(self.frame), CGRectGetHeight(inputFunctionViewRect));
        if (state!=TOShowViewStateChatTool) {
            self.chatToolView.frame = CGRectMake(0, SCREEN_HEIGHT, CGRectGetWidth(self.frame), CGRectGetHeight(self.chatToolView.frame));
        }else{
            self.chatToolView.frame = CGRectMake(0, CGRectGetMaxY(self.frame), CGRectGetWidth(self.chatToolView.frame), CGRectGetHeight(self.chatToolView.frame));
        }
        switch (state)
        {
            case TOShowViewStateVoice:
            {
                
            }
                break;
            case TOShowViewStateKeyBoard:
            {
                self.isClick = NO;
            }
                break;
            case TOShowViewStateEmojiFace:
            {
                self.isClick = NO;
            }
                break;
            case TOShowViewStateChatTool:
            {
                self.isClick = NO;
            }
                break;
            default:
                break;
        }
    } completion:^(BOOL finished) {
        
        
    }];
}
#pragma mark --- 隐藏键盘
-(void)hideKeyBoard
{
    if (self.textView.isFirstResponder) {
        [self.textView resignFirstResponder];
    }
    if (self.chatToolView.y!=SCREEN_HEIGHT) {
        [self inputFunctionViewAutoChangeWithRect:CGRectZero inputFunctionViewRect:self.frame duration:self.duration==0?0.3:self.duration state:TOShowViewStateKeyBoard];
        self.addButton.selected = NO;
    }
}
#pragma mark - 懒加载
- (HMJRecordView *)recordView
{
    if (!_recordView)
    {
        _recordView = [[HMJRecordView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 150) / 2, (SCREEN_HEIGHT - 150) / 2, 150, 150)];
        _recordView.delegate = self;
        _recordView.hidden = YES;
        [[UIApplication sharedApplication].keyWindow addSubview:_recordView];
//        [self addSubview:_recordView];
    }
    return _recordView;
}
-(PPStickerKeyboard *)stickerKeyboard
{
    if (!_stickerKeyboard) {
        _stickerKeyboard = Init(PPStickerKeyboard);
        _stickerKeyboard.backgroundColor = ColorManage.inputBar_color;
        _stickerKeyboard.frame = CGRectMake(0, 0, SCREEN_WIDTH, EmojiFaceViewHeight+SafeAreaBottomHeight);
        _stickerKeyboard.delegate = self;
    }
    return _stickerKeyboard;
}
-(HMJChatToolView *)chatToolView
{
    if (!_chatToolView) {
        _chatToolView = [[HMJChatToolView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.frame)+SafeAreaBottomHeight, SCREEN_WIDTH, EmojiFaceViewHeight+SafeAreaBottomHeight) chatType:1];
//        if([[NSString acquireUserId] integerValue] == _destId){
//            chatToolView.isMyself = YES;
//        }else{
//            chatToolView.isMyself = NO;
//        }
        _chatToolView.isMyself = NO;
        _chatToolView.delegate = self;
        UIViewController * vc = (UIViewController *)self.delegate;
        [vc.view addSubview:_chatToolView];
        [vc.view bringSubviewToFront:_chatToolView];
    }
    return _chatToolView;
}
-(NIMKitMediaFetcher *)mediaFetcher
{
    if (!_mediaFetcher) {
        _mediaFetcher                 = Init(NIMKitMediaFetcher);
        _mediaFetcher.limit           = 9;
        _mediaFetcher.allowPickingGif = YES;
        _mediaFetcher.autoDismiss     = YES;
    }
    return _mediaFetcher;
}
-(HDJCameraViewController *)shootingVC
{
    if (!_shootingVC) {
        _shootingVC = Init(HDJCameraViewController);
        _shootingVC.timeLimit = 15;//视频的拍摄时长限制。不填或默认为10秒
        _shootingVC.delegate = self;//设置代理，用于获取媒体的回调
        _shootingVC.saveMediaIntoAlbum = YES;//是否保存拍摄结果。默认：不保存
        _shootingVC.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    return _shootingVC;
}
-(UIDocumentPickerViewController *)documentPickerVC
{
    if (!_documentPickerVC) {
        NSArray *documentTypes = @[@"public.content"];
        _documentPickerVC = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:documentTypes inMode:UIDocumentPickerModeOpen];
        //设置代理
        _documentPickerVC.delegate = self;
        //设置模态弹出方式
        _documentPickerVC.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    return _documentPickerVC;
}
@end
