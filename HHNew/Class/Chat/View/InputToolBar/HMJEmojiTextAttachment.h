//
//  HMJEmojiTextAttachment.h
//  huihe
//
//  Created by Six on 16/3/7.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HMJEmojiTextAttachment : NSTextAttachment

@property (nonatomic , assign) CGFloat descender;
@property (nonatomic , copy) NSString * faceStr;

@end
