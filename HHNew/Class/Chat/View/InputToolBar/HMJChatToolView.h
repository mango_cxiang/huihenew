//
//  HMJChatToolView.h
//  huihe
//
//  Created by Six on 16/3/7.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HMJChatToolView;

@protocol HMJChatToolViewDelegate <NSObject>

@optional
// 点击某一个按钮
- (void)chatToolView:(HMJChatToolView *)chatToolView didClickOnButtonWithIndex:(NSUInteger)index;

@end

/**
 *  聊天工具 视图
 */
@interface HMJChatToolView : UIView<UIScrollViewDelegate>
//给自己发消息
@property (nonatomic, assign) BOOL isMyself;
//给身边发消息
@property (nonatomic, assign) BOOL isNearby;
// 代理
@property (nonatomic , weak) id<HMJChatToolViewDelegate> delegate;
- (instancetype)initWithFrame:(CGRect)frame chatType:(NSInteger)chatType;
@end
