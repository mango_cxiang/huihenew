//
//  HMJChatToolView.m
//  huihe
//
//  Created by Six on 16/3/7.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJChatToolView.h"



// pageControl 距离底部的高度
#define kPageControlBottomInset 0
// 行距
#define kLineSpace 0
// 间距
#define kEmojiSpace 0




@interface HMJChatToolView ()

// 滚动视图
@property (nonatomic , strong) UIScrollView * scrollView;
// 滚动小圆点(pageControl)
@property (nonatomic , strong) UIPageControl * pageControl;

@property (nonatomic, assign) NSInteger chatType;
@end

@implementation HMJChatToolView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        //创建子视图
        [self createSubViewsWithType:1];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame chatType:(NSInteger)chatType
{
    if (self = [super initWithFrame:frame])
    {
        //创建子视图
        [self createSubViewsWithType:chatType];
    }
    return self;

}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        //创建子视图
        [self createSubViewsWithType:1];
    }
    return self;
}

- (instancetype)init
{
    if (self = [super init])
    {
        //创建子视图
        [self createSubViewsWithType:1];
    }
    return self;
}

- (void)setIsMyself:(BOOL)isMyself{
    _isMyself = isMyself;
    NSArray * imageArray;
    NSArray * nameArray;
    if (_chatType == 1) {//个人聊天
        
        if(isMyself){
            imageArray = @[@"tupian",
                           @"paizhao",
                           @"weizhi",
                           @"mingpian",
                           @"wenjian",
                           @"shoucang",];
            
            nameArray = @[@"照片" ,
                          @"拍摄" ,
                          @"位置" ,
                          @"名片" ,
                          @"文件" ,
                          @"收藏",
                          ];
        }else{
            imageArray = @[@"tupian",
                           @"paizhao",
                           @"shiping",
                           @"weizhi",
                           @"hongbao",
//                           @"zhuanzhang",
                           @"mingpian",
                           @"wenjian",
                           @"shoucang",];
            
            nameArray = @[@"照片" ,
                          @"拍摄" ,
                          @"通话" ,
                          @"位置" ,
                          @"红包" ,
//                          @"转账") ,
                          @"名片" ,
                          @"文件" ,
                          @"收藏",
                          ];
        }
        
    }
    else if (_chatType == 2)//群聊天
    {
        
        if(_isNearby){
            imageArray = @[
                           @"tupian",
                           @"paizhao",
                           @"weizhi",
                           @"hongbao",
                           @"mingpian",
                           @"wenjian",
                           @"shoucang",
                           ];
            nameArray = @[
                          @"照片" ,
                          @"拍摄" ,
                          @"位置" ,
                          @"红包" ,
                          @"名片" ,
                          @"文件",
                          @"收藏"];
        }else{
            imageArray = @[@"tupian",
                           @"paizhao",
                           @"shiping",
                           @"weizhi",
                           @"hongbao",
                           //                           @"zhuanzhang",
                           @"mingpian",
                           @"wenjian",
                           @"shoucang",];
            nameArray = @[
                          @"照片" ,
                          @"拍摄" ,
                          @"通话" ,
                          @"位置" ,
                          @"红包" ,
                          //                      @"收款") ,
                          @"名片" ,
                          @"文件",
                          @"收藏"];
        }
    }else{
        imageArray = @[@"paizhao",
                       @"tupian",
                       @"shoucang",
                       @"weizhi",
                       @"mingpian",
                       @"message_speechInput"];
        
        nameArray = @[@"拍照" ,
                      @"相册" ,
                      @"收藏" ,
                      @"位置" ,
                      @"名片",
                      @"语音输入"];
    }
    
    [self creatMenuWithimageArray:imageArray nameArray:nameArray];
    
    
    CGSize contentSize;
    if (imageArray.count <= 8) {
        contentSize = CGSizeMake(self.width * ( imageArray.count / 8), self.scrollView.height);
        self.pageControl.numberOfPages =  imageArray.count / 8;
        self.pageControl.hidden = YES;
    }else{
        contentSize = CGSizeMake(self.width * (1 + imageArray.count / 8), self.scrollView.height);
        self.pageControl.numberOfPages = 1 + imageArray.count / 8;
        self.pageControl.hidden = NO;
    }
  
    //计算并设置scrollview的contentSize
    self.scrollView.contentSize = CGSizeMake(contentSize.width , 0);
    
   
}

// 创建子视图
- (void)createSubViewsWithType:(NSInteger)chatType
{
    _chatType = chatType;
    self.backgroundColor = ColorManage.inputBar_color;
    // 滚动视图
    UIScrollView * scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
    scrollView.height = self.height;
    self.scrollView = scrollView;
    scrollView.pagingEnabled = YES;
    scrollView.backgroundColor = ColorManage.inputBar_color;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.delegate = self;
    [self addSubview:scrollView];
    
    
    
    // 滚动小圆点(pageControl)
    UIPageControl * pageControl = [[UIPageControl alloc] init];
    self.pageControl = pageControl;
    //pageControl.numberOfPages = 1 + imageArray.count / 8;
    pageControl.currentPage = 0;
    pageControl.currentPageIndicatorTintColor = UIColorFromRGB(0x8c9094);
    pageControl.pageIndicatorTintColor = UIColorFromRGB(0xcbcbcb);
    CGSize pageControlSize = [pageControl sizeForNumberOfPages:pageControl.numberOfPages];
    CGRect pageControlRect = CGRectMake((scrollView.width - pageControlSize.width) / 2.f,
                                        scrollView.height - pageControlSize.height - kPageControlBottomInset-SafeAreaBottomHeight,
                                        pageControlSize.width,
                                        pageControlSize.height);
    pageControl.frame = pageControlRect;
    [self addSubview:pageControl];
}
-(void)creatMenuWithimageArray:(NSArray *)imageArray nameArray:(NSArray *)nameArray{
    NSInteger num = 4;
    CGFloat viewWidth = 60;
    CGFloat viewHeight = 60 + 5 + 11;
    CGFloat spaceX = (self.width - num * viewWidth) / (num + 1);
    CGFloat viewX = (self.width - num * viewWidth) / (num + 1);
    CGFloat spaceY = 15;
    CGFloat viewY = 15;
    CGFloat labelHeight = 22 / 2;
    for (NSInteger i = 0; i < imageArray.count; i++)
    {

        viewX  = spaceX +(i % num) * (spaceX + viewWidth) + (i / 8) * self.width;
        viewY = spaceY + (i / num) % 2 * (spaceY + viewHeight) ;
        
        UIView * view = [[UIView alloc] initWithFrame:CGRectMake(viewX, viewY, viewWidth, viewHeight)];
        
        view.tag = 1234 + i;
        view.userInteractionEnabled = YES;
        [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewOnTap:)]];
        [_scrollView addSubview:view];
        
        //图片
        UIImageView * imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageArray[i]]];
        imageView.frame = CGRectMake(0, 0, view.width, view.width);
        [view addSubview:imageView];
        
        //名称
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(imageView.x, imageView.y + imageView.height + 5 , imageView.width, labelHeight)];
        label.text = nameArray[i];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = SystemFont(11);
        label.textColor = UIColorFromRGB(0x6f7378);
        [view addSubview:label];
        

    }
}
#pragma mark - 视图被敲击 手势 触发事件
- (void)viewOnTap:(UITapGestureRecognizer *)tapGestureRecognizer
{
    UIView * view = tapGestureRecognizer.view;
    NSUInteger index = view.tag - 1234;
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatToolView:didClickOnButtonWithIndex:)])
    {
        [self.delegate chatToolView:self didClickOnButtonWithIndex:index];
    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    AkrLog(@"%f %f",scrollView.contentOffset.x ,scrollView.frame.size.width);
    _pageControl.currentPage = page;
}
@end
