//
//  HMJEmojiFaceView.h
//  huihe
//
//  Created by Six on 16/3/2.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HMJEmojiFaceView;

@protocol HMJEmojiFaceViewDelegate <NSObject>

@optional
//表情按钮 被点击
- (void)emojiFaceView:(HMJEmojiFaceView *)emojiFaceView didClickOnEmojiButton:(UIButton *)emojiButton emojiNameStr:(NSString *)emojiNameStr emojiFaceStr:(NSString *)emojiFaceStr;
//删除按钮 被点击
- (void)emojiFaceView:(HMJEmojiFaceView *)emojiFaceView didClickOnDeleteButton:(UIButton *)deleteButton;
//发送按钮 被点击
- (void)emojiFaceView:(HMJEmojiFaceView *)emojiFaceView didClickOnSendButton:(UIButton *)sendButton;

@end

/**
 *  表情控件
 */
@interface HMJEmojiFaceView : UIView<UIScrollViewDelegate>

// 发送按钮
@property (nonatomic , strong) UIButton * sendButton;
// 代理
@property (nonatomic , weak) id<HMJEmojiFaceViewDelegate> delegate;

+ (NSDictionary *)localEmojiDict;

@end
