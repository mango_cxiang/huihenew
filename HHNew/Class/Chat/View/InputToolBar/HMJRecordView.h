//
//  HMJRecordView.h
//  huihe
//
//  Created by Six on 16/3/10.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
@protocol HMJRecordViewDelegate <NSObject>

- (void)recordViewReachMaxRecordTime;
@end
/**
 *  录音视图
 */
@interface HMJRecordView : UIView
@property (nonatomic , strong)  AVAudioRecorder * avRecorder;
@property (nonatomic,weak) id <HMJRecordViewDelegate>delegate;
// 录音按钮按下
- (void)recordButtonTouchDown;
// 手指在录音按钮内部时离开
- (void)recordButtonTouchUpInside;
// 手指在录音按钮外部时离开
- (void)recordButtonTouchUpOutside;
// 手指移动到录音按钮内部
- (void)recordButtonDragInside;
// 手指移动到录音按钮外部
- (void)recordButtonDragOutside;

-(void)resetTimer;
@end
