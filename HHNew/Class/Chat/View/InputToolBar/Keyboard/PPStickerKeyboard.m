//
//  PPSktickerKeyboard.m
//  PPStickerKeyboard
//
//  Created by Vernon on 2018/1/14.
//  Copyright © 2018年 Vernon. All rights reserved.
//

#import "PPStickerKeyboard.h"
#import "PPEmojiPreviewView.h"
#import "PPStickerPageView.h"
#import "PPStickerDataManager.h"
#import "PPUtil.h"
#import "HMKStickerDetailsController.h"
#import <SDWebImage/UIButton+WebCache.h>
#import "HMKSupeStickerController.h"
static CGFloat const PPStickerTopInset = 10.0;
static CGFloat const PPStickerScrollViewHeight = 132.0;
static CGFloat const PPKeyboardPageControlTopMargin = 10.0;
static CGFloat const PPKeyboardPageControlHeight = 7.0;
static CGFloat const PPKeyboardPageControlBottomMargin = 6.0;
static CGFloat const PPKeyboardCoverButtonWidth = 50.0;
static CGFloat const PPKeyboardCoverButtonHeight = 44.0;
static CGFloat const PPPreviewViewWidth = 92.0;
static CGFloat const PPPreviewViewHeight = 137.0;

static NSString *const PPStickerPageViewReuseID = @"PPStickerPageView";

@interface PPStickerKeyboard () <PPStickerPageViewDelegate, PPQueuingScrollViewDelegate, UIInputViewAudioFeedback>
@property (nonatomic, strong) NSArray<PPSticker *> *stickers;
@property (nonatomic, strong) PPQueuingScrollView *queuingScrollView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) NSArray<PPSlideLineButton *> *stickerCoverButtons;
@property (nonatomic, strong) PPSlideLineButton *sendButton;
@property (nonatomic, strong) UIScrollView *bottomScrollableSegment;
@property (nonatomic, strong) UIView *bottomBGView;
@property (nonatomic, strong) PPEmojiPreviewView *emojiPreviewView;
@property (nonatomic,strong) UIButton *AddBtn;
@end

@implementation PPStickerKeyboard {
    NSUInteger _currentStickerIndex;
}

- (instancetype)init
{
    self = [self initWithFrame:CGRectZero];
    return self;
}

-(void)dealloc{
    
     [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        _currentStickerIndex = 1;
         _stickers = [[PPStickerDataManager sharedInstance] withLoadarr].copy;
        if (_stickers.count == 0) {
            [self getStickers];
        }
//        self.backgroundColor = [UIColor pp_colorWithRGBString:@"#F4F4F4"];
        [self addSubview:self.queuingScrollView];
        [self addSubview:self.pageControl];
        [self addSubview:self.bottomBGView];
        [self addSubview:self.sendButton];
        [self addSubview:self.bottomScrollableSegment];
        [self addSubview:self.AddBtn];
        [self changeStickerToIndex:1];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdataSticker) name:@"UPDATA_CANCEL_NOTIFICATION" object:nil];
        
        
    }
    return self;
}

-(void)UpdataSticker{
    
    _stickers = [[PPStickerDataManager sharedInstance] withLoadarr].copy;
    [self changeStickerToIndex:1];
   
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SEARCH_CANCEL_NOTIFICATION" object:nil];
}
- (void)getStickers {
    
//    NSMutableDictionary *dictparame = [NSMutableDictionary dictionary];
//    [dictparame setObject:[NSString acquireUserId] forKey:@"appUserId"];
//    [HMJBusinessManager GET:@"/im/appAdd/appAddList" Parameters:dictparame success:^(NSDictionary *dic, NSInteger code) {
//        NSInteger errorcode = [dic[@"code"] integerValue];
//        if (errorcode == 1) {
//            NSMutableArray *emojiArr = [NSMutableArray array];
//
//            NSArray *DataArry = dic[@"data"][@"info"];
//            for (NSDictionary *dict in DataArry) {
//
//                NSMutableDictionary *Emojidict = [NSMutableDictionary dictionary];
//
//                [Emojidict setObject:dict[@"chat_url"] forKey:@"cover_pic"];
//                [Emojidict setObject:dict[@"id"] forKey:@"EmojiID"];
//
//                NSMutableArray *emoticons = [NSMutableArray array];
//
//                for (NSDictionary *SubDict in dict[@"phiz_subList"]) {
//
//                    NSMutableDictionary *icondict = [NSMutableDictionary dictionary];
//
//                    [icondict setValue:SubDict[@"next_url"] forKey:@"image"];
//                    [icondict setValue:SubDict[@"phiz_desc"] forKey:@"desc"];
//                    [icondict setValue:SubDict[@"master_url"] forKey:@"imageGif"];
//                    [icondict setValue:SubDict[@"id"] forKey:@"EmojiID"];
//
//                    [emoticons addObject:icondict];
//                }
//
//                [Emojidict setValue:emoticons forKey:@"emoticons"];
//
//                [emojiArr addObject:Emojidict];
//            }
//
//            [HHGlobals  saveEmojiDict:emojiArr];
//
//            _stickers = [self stickerArray];
//
//            [self reloadScrollableSegment];
//        }
//
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//        _stickers = [[PPStickerDataManager sharedInstance] withLoadarr].copy;
//
//        [self reloadScrollableSegment];
//    }];
}

- (NSArray *)stickerArray{
    NSString *path = [NSBundle.mainBundle pathForResource:@"InnerStickersInfo" ofType:@"plist"];
    if (!path) {
        return nil;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"lishi-ssz" forKey:@"cover_pic"];
    NSMutableArray *arr= [[HHGlobals getPoiList] mutableCopy];
    
    for (int i = 0; i <arr.count ; i++ ) {
        NSString *string = arr[i][@"image"];
        
        for (int j = i+1; j < arr.count; j ++) {
            
            NSString *jstring = arr[j][@"image"];
            if([string isEqualToString:jstring]){
                
                [arr removeObjectAtIndex:j];
                j -= 1;
            }
        }
    }
    [HHGlobals savePoiList:arr];
    
    if (arr.count >0) {
        
        if (arr.count > 20) {
            [dict setObject:[NSArray arrayWithArray:[[NSArray arrayWithArray:arr] subarrayWithRange:NSMakeRange(0, 20)]] forKey:@"emoticons"];
        }else{
            [dict setObject:[NSArray arrayWithArray:arr] forKey:@"emoticons"];
        }
    }
    NSMutableArray *array = [[NSMutableArray alloc] initWithContentsOfFile:path].mutableCopy;
    
    [array insertObject:dict atIndex:0];
    
    NSArray *arricon = [HHGlobals getEmojiDict];
    
    for (NSDictionary *icondict in arricon) {
        
        [array addObject:icondict];
    }
    
    NSMutableArray<PPSticker *> *stickers = [[NSMutableArray alloc] init];
    for (NSDictionary *stickerDict in array) {
        PPSticker *sticker = [[PPSticker alloc] init];
        sticker.coverImageName = stickerDict[@"cover_pic"];
        NSArray *emojiArr = stickerDict[@"emoticons"];
        NSMutableArray<PPEmoji *> *emojis = [[NSMutableArray alloc] init];
        for (NSDictionary *emojiDict in emojiArr) {
            PPEmoji *emoji = [[PPEmoji alloc] init];
            emoji.imageName = emojiDict[@"image"];
            emoji.emojiDescription = emojiDict[@"desc"];
            emoji.imageGif = emojiDict[@"imageGif"];
            emoji.EmojiID = emojiDict[@"EmojiID"];
            [emojis addObject:emoji];
        }
        sticker.emojis = emojis;
        [stickers addObject:sticker];
    }
    return stickers;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    

    self.queuingScrollView.contentSize = CGSizeMake([self numberOfPageForSticker:[self stickerAtIndex:_currentStickerIndex]] * CGRectGetWidth(self.bounds), PPStickerScrollViewHeight);
    self.queuingScrollView.frame = CGRectMake(0, PPStickerTopInset, CGRectGetWidth(self.bounds), PPStickerScrollViewHeight);
    self.pageControl.frame = CGRectMake(0, CGRectGetMaxY(self.queuingScrollView.frame) + PPKeyboardPageControlTopMargin, CGRectGetWidth(self.bounds), PPKeyboardPageControlHeight);
    
    self.bottomScrollableSegment.contentSize = CGSizeMake(self.stickerCoverButtons.count * PPKeyboardCoverButtonWidth , PPKeyboardCoverButtonHeight );
    self.bottomScrollableSegment.frame = CGRectMake(PPKeyboardCoverButtonWidth, CGRectGetHeight(self.bounds) - PPKeyboardCoverButtonHeight - PP_SAFEAREAINSETS(self).bottom, CGRectGetWidth(self.bounds) - PPKeyboardCoverButtonWidth - PPKeyboardCoverButtonWidth, PPKeyboardCoverButtonHeight);
    
    self.sendButton.frame = CGRectMake(CGRectGetWidth(self.bounds) - PPKeyboardCoverButtonWidth, CGRectGetMinY(self.bottomScrollableSegment.frame), PPKeyboardCoverButtonWidth, PPKeyboardCoverButtonHeight);
    self.bottomBGView.frame = CGRectMake(0, CGRectGetMinY(self.bottomScrollableSegment.frame), CGRectGetWidth(self.frame), PPKeyboardCoverButtonHeight + PP_SAFEAREAINSETS(self).bottom);
     self.AddBtn.frame = CGRectMake(0, CGRectGetHeight(self.bounds) - PPKeyboardCoverButtonHeight - PP_SAFEAREAINSETS(self).bottom, PPKeyboardCoverButtonWidth, PPKeyboardCoverButtonHeight);
    
    [self reloadScrollableSegment];
    
}

- (CGFloat)heightThatFits
{
    CGFloat bottomInset = 0;
    if (@available(iOS 11.0, *)) {
        bottomInset = UIApplication.sharedApplication.delegate.window.safeAreaInsets.bottom;
    }
    return PPStickerTopInset + PPStickerScrollViewHeight + PPKeyboardPageControlTopMargin + PPKeyboardPageControlHeight + PPKeyboardPageControlBottomMargin + PPKeyboardCoverButtonHeight  + bottomInset;
}

#pragma mark - getter / setter

- (PPQueuingScrollView *)queuingScrollView
{
    if (!_queuingScrollView) {
        _queuingScrollView = [[PPQueuingScrollView alloc] init];
        _queuingScrollView.delegate = self;
        _queuingScrollView.pagePadding = 0;
        _queuingScrollView.alwaysBounceHorizontal = NO;
    }
    return _queuingScrollView;
}

-(UIButton *)AddBtn{
    
    if (!_AddBtn) {
        _AddBtn = [[UIButton alloc]init];
        [_AddBtn setImage:[UIImage imageNamed:@"tianjia"] forState:UIControlStateNormal];
        [_AddBtn addTarget:self action:@selector(addclick) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _AddBtn;
}

-(void)addclick{
    
    HMKSupeStickerController *StickerVc = [[HMKSupeStickerController alloc] init];
    HHNavController *nav = [[HHNavController alloc] initWithRootViewController:StickerVc];
//    nav.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor] , NSFontAttributeName : BoldFont(18)};
     [nav.navigationBar setShadowImage:[[UIImage alloc] init]];
    nav.modalPresentationStyle = UIModalPresentationFullScreen;

    [[[UIApplication sharedApplication].delegate window].rootViewController presentViewController:nav animated:YES completion:nil];
}

- (UIPageControl *)pageControl
{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] init];
        _pageControl.hidesForSinglePage = YES;
        _pageControl.currentPageIndicatorTintColor = [UIColor pp_colorWithRGBString:@"#F5A623"];
        _pageControl.pageIndicatorTintColor = [UIColor pp_colorWithRGBString:@"#BCBCBC"];
    }
    return _pageControl;
}

- (PPSlideLineButton *)sendButton
{
    if (!_sendButton) {
        _sendButton = [[PPSlideLineButton alloc] init];
        [_sendButton setTitle:@"发送" forState:UIControlStateNormal];
        [_sendButton setTitleColor:[UIColor pp_colorWithRGBString:@"#2196F3"] forState:UIControlStateNormal];
        _sendButton.linePosition = PPSlideLineButtonPositionLeft;
        _sendButton.lineColor = [UIColor pp_colorWithRGBString:@"#D1D1D1"];
        [_sendButton addTarget:self action:@selector(sendAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sendButton;
}

- (UIScrollView *)bottomScrollableSegment
{
    if (!_bottomScrollableSegment) {
        _bottomScrollableSegment = [[UIScrollView alloc] init];
        _bottomScrollableSegment.showsHorizontalScrollIndicator = NO;
        _bottomScrollableSegment.showsVerticalScrollIndicator = NO;
    }
    return _bottomScrollableSegment;
}

- (UIView *)bottomBGView
{
    if (!_bottomBGView) {
        _bottomBGView = [[UIView alloc] init];
        _bottomBGView.backgroundColor = ColorManage.inputBar_color;
    }
    return _bottomBGView;
}

- (PPEmojiPreviewView *)emojiPreviewView
{
    if (!_emojiPreviewView) {
        _emojiPreviewView = [[PPEmojiPreviewView alloc] init];
    }
    return _emojiPreviewView;
}

#pragma mark - private method

- (PPSticker *)stickerAtIndex:(NSUInteger)index
{
    if (self.stickers && index < self.stickers.count) {

        return [[PPStickerDataManager sharedInstance] withLoadarr].copy[index];
    }
    return nil;
}

- (NSUInteger)numberOfPageForSticker:(PPSticker *)sticker
{
    if (!sticker) {
        return 0;
    }

    
    NSUInteger numberOfPage;
    if (_currentStickerIndex == 0 || _currentStickerIndex == 1) {
           numberOfPage = (sticker.emojis.count / 20) + ((sticker.emojis.count % 20 == 0) ? 0 : 1);
    }else{
         numberOfPage = (sticker.emojis.count / 8) + ((sticker.emojis.count % 8 == 0) ? 0 : 1);
    }
    return numberOfPage;
}

- (void)reloadScrollableSegment
{
    for (UIButton *button in self.stickerCoverButtons) {
        [button removeFromSuperview];
    }
    self.stickerCoverButtons = nil;

    if (!self.stickers || !self.stickers.count) {
        return;
    }

    NSMutableArray *stickerCoverButtons = [[NSMutableArray alloc] init];
    for (NSUInteger index = 0, max = self.stickers.count; index < max; index++) {
        PPSticker *sticker = self.stickers[index];
        if (!sticker) {
            return;
        }

        PPSlideLineButton *button = [[PPSlideLineButton alloc] init];
        button.tag = index;
        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
        button.linePosition = PPSlideLineButtonPositionRight;
        button.lineColor = [UIColor pp_colorWithRGBString:@"#D1D1D1"];
        button.backgroundColor = (_currentStickerIndex == index ? [UIColor pp_colorWithRGBString:@"#EDEDED"] : [UIColor clearColor]);
        
        if ([sticker.coverImageName isUrl]) {

            [button sd_setImageWithURL:[NSURL URLWithString:sticker.coverImageName] forState:UIControlStateNormal];
        }else{
            [button setImage:[self emojiImageWithName:sticker.coverImageName] forState:UIControlStateNormal];
        }
        [button addTarget:self action:@selector(changeSticker:) forControlEvents:UIControlEventTouchUpInside];
        [self.bottomScrollableSegment addSubview:button];
        [stickerCoverButtons addObject:button];
        button.frame = CGRectMake(index * PPKeyboardCoverButtonWidth, 0, PPKeyboardCoverButtonWidth, PPKeyboardCoverButtonHeight);
    }
    self.stickerCoverButtons = stickerCoverButtons;
}

- (UIImage *)emojiImageWithName:(NSString *)name
{
    if (!name.length) {
        return nil;
    }

    return [UIImage imageNamed:[@"Sticker.bundle" stringByAppendingPathComponent:name]];
}

- (void)changeStickerToIndex:(NSUInteger)toIndex
{
    if (toIndex >= self.stickers.count) {
        return;
    }

    PPSticker *sticker = [self stickerAtIndex:toIndex];
    if (!sticker) {
        return;
    }

    _currentStickerIndex = toIndex;

    PPStickerPageView *pageView = [self queuingScrollView:self.queuingScrollView pageViewForStickerAtIndex:0];
    [self.queuingScrollView displayView:pageView];

    [self reloadScrollableSegment];
}

#pragma mark - target / action

- (void)changeSticker:(UIButton *)button
{
    [self changeStickerToIndex:button.tag];
}

- (void)sendAction:(PPSlideLineButton *)button
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(stickerKeyboardDidClickSendButton:)]) {
        [self.delegate stickerKeyboardDidClickSendButton:self];
    }
}

#pragma mark - PPQueuingScrollViewDelegate

- (void)queuingScrollViewChangedFocusView:(PPQueuingScrollView *)queuingScrollView previousFocusView:(UIView *)previousFocusView
{
    PPStickerPageView *currentView = (PPStickerPageView *)self.queuingScrollView.focusView;
    self.pageControl.currentPage = currentView.pageIndex;
}

- (UIView<PPReusablePage> *)queuingScrollView:(PPQueuingScrollView *)queuingScrollView viewBeforeView:(UIView *)view
{
    return [self queuingScrollView:queuingScrollView pageViewForStickerAtIndex:((PPStickerPageView *)view).pageIndex - 1];
}

- (UIView<PPReusablePage> *)queuingScrollView:(PPQueuingScrollView *)queuingScrollView viewAfterView:(UIView *)view
{
    return [self queuingScrollView:queuingScrollView pageViewForStickerAtIndex:((PPStickerPageView *)view).pageIndex + 1];
}

- (PPStickerPageView *)queuingScrollView:(PPQueuingScrollView *)queuingScrollView pageViewForStickerAtIndex:(NSUInteger)index
{
    PPSticker *sticker = [self stickerAtIndex:_currentStickerIndex];
    if (!sticker) {
        return nil;
    }

    NSUInteger numberOfPages = [self numberOfPageForSticker:sticker];
    self.pageControl.numberOfPages = numberOfPages;
    if (index >= numberOfPages) {
        return nil;
    }

    PPStickerPageView *pageView = [queuingScrollView reusableViewWithIdentifer:PPStickerPageViewReuseID];
    if (!pageView) {
        pageView = [[PPStickerPageView alloc] initWithReuseIdentifier:PPStickerPageViewReuseID];
        pageView.delegate = self;
    }
    pageView.pageIndex = index;
    if (_currentStickerIndex == 0 || _currentStickerIndex == 1) {
         pageView.PPStickerPageViewMaxEmojiCount =20;
    }else{
        pageView.PPStickerPageViewMaxEmojiCount =8;
    }
    [pageView configureWithSticker:sticker];
    return pageView;
}

#pragma mark - PPStickerPageViewDelegate

- (void)stickerPageView:(PPStickerPageView *)stickerPageView didClickEmoji:(PPEmoji *)emoji
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(stickerKeyboard:didClickEmoji:)]) {
        [[UIDevice currentDevice] playInputClick];
        [self.delegate stickerKeyboard:self didClickEmoji:emoji];
    }
}

- (void)stickerPageViewDidClickDeleteButton:(PPStickerPageView *)stickerPageView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(stickerKeyboardDidClickDeleteButton:)]) {
        [[UIDevice currentDevice] playInputClick];
        [self.delegate stickerKeyboardDidClickDeleteButton:self];
    }
}

- (void)stickerPageView:(PPStickerPageView *)stickerKeyboard showEmojiPreviewViewWithEmoji:(PPEmoji *)emoji buttonFrame:(CGRect)buttonFrame
{
    if (!emoji) {
        return;
    }

    self.emojiPreviewView.emoji = emoji;

    CGRect buttonFrameAtKeybord = CGRectMake(buttonFrame.origin.x, PPStickerTopInset + buttonFrame.origin.y, buttonFrame.size.width, buttonFrame.size.height);
    self.emojiPreviewView.frame = CGRectMake(CGRectGetMidX(buttonFrameAtKeybord) - PPPreviewViewWidth / 2, UIScreen.mainScreen.bounds.size.height - CGRectGetHeight(self.bounds) + CGRectGetMaxY(buttonFrameAtKeybord) - PPPreviewViewHeight, PPPreviewViewWidth, PPPreviewViewHeight);

    UIWindow *window = [UIApplication sharedApplication].windows.lastObject;
    if (window) {
        [window addSubview:self.emojiPreviewView];
    }
}

- (void)stickerPageViewHideEmojiPreviewView:(PPStickerPageView *)stickerKeyboard
{
    [self.emojiPreviewView removeFromSuperview];
}

#pragma mark - UIInputViewAudioFeedback

- (BOOL)enableInputClicksWhenVisible
{
    return YES;
}

@end
