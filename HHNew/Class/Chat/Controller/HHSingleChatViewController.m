//
//  HHSingleChatViewController.m
//  HHNew
//
//  Created by 张 on 2020/4/22.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHSingleChatViewController.h"
#import "HMJInputFunctionView.h"
#import "HHChatCell.h"
#import "HHChatRefreshHeader.h"
#import "HHChatMessageManager.h"

#define InputFunctionViewHeight 56

#define pageSize 27

@interface HHSingleChatViewController ()<UITableViewDelegate,UITableViewDataSource,HMJInputFunctionViewDelegate>

@property(nonatomic,strong)UIImageView * chatBackgroundIV;
@property(nonatomic,strong)UITableView * chatTableView;
@property(nonatomic,strong)NSMutableArray * chatArray;
// 用于显示发送消息类型控制的工具条，在底部
@property (nonatomic,strong)HMJInputFunctionView * inputFunctionView;

@property(nonatomic,strong)UIControl * hideKeyBoardControl;

@end

@implementation HHSingleChatViewController

-(void)dealloc{
    [self.inputFunctionView removeObserver:self forKeyPath:@"frame"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = ColorManage.nav_color;
    [self setRightBarButtonItemWithImageArray:@[@"chat_more"] BeginTag:1000];
    self.title = @"Bonle";
    
    [self.view addSubview:self.chatBackgroundIV];
    //监听inputFunctionView Frame改变
    [self.inputFunctionView addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
    
    [self.chatTableView.mj_header beginRefreshing];
}
#pragma mark --- 加载数据
-(void)loadData
{
    NSMutableArray *tempDataArray = [NSMutableArray array];
    for (int i = 0; i<pageSize; i++) {
        HHChatModel * chatModel = Init(HHChatModel);
        chatModel.destId = @"";
        if (i%2==0) {
            chatModel.fromId = @"1";
            chatModel.hideTime = NO;
        }else{
            chatModel.fromId = @"2";
            chatModel.hideTime = YES;
        }
        if (i<2) {
            chatModel.messageType = @"16";
            chatModel.seconds = arc4random()%20+1;
        }else if (i<4) {
            chatModel.messageType = @"29";
            chatModel.content = @"{\"addr\":\"繁华大道与莲花路交叉口向南200米尚泽大都会\",\"lat\":31.772592,\"lut\":117.22938,\"url\":\"https://down.beiquanren.com/chat/20200427/9005120_1587953579004_263.png\"}";
        }else if (i<6) {
            chatModel.messageType = @"28";
            chatModel.content = @"{  \"headUrl\" : \"https:\\/\\/down.beiquanren.com\\/head\\/acc6f11cf94356197ca4398ebf58ff75.jpg?_t=1587112560689&x-oss-process=style\\/head_132\",  \"contactId\" : \"9008246\",  \"nickName\" : \"Lyon\",  \"IM\" : \"hh_49947709645\"}";
        }else if (i<10) {
            chatModel.messageType = @"17";
            chatModel.content = @"{\"redPacketId\":17,\"tableName\":\"im_redpacket_202004_4\",\"msg\":\"会合红包测试\",\"limit\":\"\",\"redPacketType\":\"2\"}";
            chatModel.isUnpack = i%2==0?@"1":@"0";
        }else if (i<12) {
            chatModel.messageType = @"3";
            chatModel.content = @"https://down.beiquanren.com/chat/20200428/9008242_1588039417761_439-130-97.jpg";
        }else if (i<14) {
            chatModel.messageType = @"30";
            chatModel.content = @"{  \"vedioUrl\" : \"https:\\/\\/down.beiquanren.com\\/chat\\/20200428\\/9005120_1588040832647_544.mp4\",  \"getVedioBitmapUrl\" : \"https:\\/\\/down.beiquanren.com\\/chat\\/20200428\\/9005120_1588040832271_738-73-130.jpg\",  \"vedioSize\" : \"10000\"}";
        }else if (i<16) {
            chatModel.messageType = @"34";
            chatModel.content = @"{  \"emojiName\" : \"拜拜\",  \"EmojiID\" : 1160,  \"MessageBubbleName\" : \"\",  \"msgString\" : \"https:\\/\\/down.beiquanren.com\\/biaoqing\\/20190309\\/100035beiquan\\/155211176180803.gif\",  \"msgType\" : \"emoji\"}";
        }else if (i==16) {
            chatModel.messageType = @"89";
            chatModel.content = @"{\"endMsgString\":\"加入了群聊\",\"msgString\":\"你邀请\",\"nickName\":\"001\",\"total\":4,\"userId\":\"9008242\",\"userList\":[{\"headUrl\":\"https://down.beiquanren.com/head/ty1.png?x-oss-process=style/head_132\",\"nickName\":\"好友-体验-01\",\"userId\":\"8000800\"},{\"headUrl\":\"https://down.beiquanren.com/head/ab8c46cb6307bbadbe2efaafb3331c4a.jpg?_t=1582775183945&x-oss-process=style/head_132\",\"nickName\":\"9005121\",\"userId\":\"9005120\"},{\"headUrl\":\"https://down.beiquanren.com/head/4d961704df12deeed3b7162d6e30381f.jpg?_t=1585735281561&x-oss-process=style/head_132\",\"nickName\":\"储翔\",\"userId\":\"9008240\"},{\"headUrl\":\"https://down.beiquanren.com/head/ty1.png?x-oss-process=style/head_132\",\"nickName\":\"好友-体验-01\",\"userId\":\"8000800\"},{\"headUrl\":\"https://down.beiquanren.com/head/ty1.png?x-oss-process=style/head_132\",\"nickName\":\"好友-体验-01\",\"userId\":\"8000800\"},{\"headUrl\":\"https://down.beiquanren.com/head/4d961704df12deeed3b7162d6e30381f.jpg?_t=1585735281561&x-oss-process=style/head_132\",\"nickName\":\"储翔\",\"userId\":\"9008240\"}]}";
        }else if (i==17) {
            chatModel.messageType = @"84";
            chatModel.content = @"{\"endMsgString\":\"\",\"isCreate\":1,\"msgString\":\"邀请你加入了群聊，群聊参与人还有：\",\"nickName\":\"Lyon\",\"total\":3,\"userId\":\"9005120\",\"userList\":[{\"headUrl\":\"https://down.beiquanren.com/head/4d961704df12deeed3b7162d6e30381f.jpg?_t=1585735281561&x-oss-process=style/head_132\",\"nickName\":\"储翔\",\"userId\":\"9008240\"},{\"headUrl\":\"https://down.beiquanren.com/head/9559c8403a3be710c3fe96a3f527ee63.jpg?_t=1586913129148&x-oss-process=style/head_132\",\"nickName\":\"001\",\"userId\":\"9008242\"}]}";
        }else if (i==18) {
            chatModel.messageType = @"34";
            chatModel.content = @"{\"groupName\":\"\",\"creatorUserId\":9008242,\"groupHeadUrl\":\"https://down.beiquanren.com/head/5454c6be9a864083b86a4287430b680c.png?_t=1588058170738&x-oss-process=style/head_132\",\"msgType\":\"\",\"msgString\":\"@所有人 \\n更改了群公告\",\"MessageBubbleName\":\"\",\"creatorHeadUrl\":\"https://down.beiquanren.com/head/9559c8403a3be710c3fe96a3f527ee63.jpg?_t=1586913129148&x-oss-process=style/head_132\",\"aiteId\":[1],\"updateTime\":\"2020-04-28 17:45:54\",\"content\":\"2222\",\"creatorNickName\":\"001\"}";
        }else if (i==19) {
            chatModel.messageType = @"74";
            chatModel.content = @"{\"msgString\":\"本群已开启成员邀请确认\"}";
        }else if (i==20) {
            chatModel.messageType = @"75";
            chatModel.content = @"{\"msgString\":\"本群已关闭成员邀请确认\"}";
        }else if (i==21) {
            chatModel.messageType = @"78";
            chatModel.content = @"{\"msgString\":\"本群已开启成员保护模式\"}";
        }else if (i==22) {
            chatModel.messageType = @"79";
            chatModel.content = @"{\"msgString\":\"本群已关闭成员保护模式\"}";
        }else if (i==23) {
            chatModel.messageType = @"80";
            chatModel.content = @"{\"msgString\":\"本群已显示红包金额\"}";
        }else if (i==24) {
            chatModel.messageType = @"81";
            chatModel.content = @"{\"msgString\":\"本群已隐藏红包金额\"}";
        }else{
            chatModel.messageType = @"2";
            chatModel.content = @"{ \"MessageBubbleName\" : \"\",  \"msgString\" : \"长文字信息长文字信息长文字信息长文字信息长文字信息长文字信息长文字信息长文字信息长文字信息长文字信息长文字信息长文字信息长文字信息长文字信息长文字信息长文字信息\"}";
        }
        chatModel.fromType = @"2";
        chatModel.fromName = @"hh007";
        chatModel.imageIconUrl = @"https://down.beiquanren.com/head/9559c8403a3be710c3fe96a3f527ee63.jpg?_t=1586913129148&x-oss-process=style/head_132";
        chatModel.sendTime = @"1587873039045";
        HHChatFrameModel * frameModel = Init(HHChatFrameModel);
        frameModel.chatModel = chatModel;
        frameModel.userNameBlock = ^(NSInteger userId) {
            AkrLog(@"userId---%@",@(userId));
        };
        [tempDataArray addObject:frameModel];
    }
    NSInteger row = tempDataArray.count;
    [tempDataArray addObjectsFromArray:self.chatArray];
    self.chatArray = tempDataArray;
    [self.chatTableView.mj_header endRefreshing];
    [self.chatTableView reloadData];
    if (self.chatArray.count>pageSize) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        [self.chatTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }else if (self.chatArray.count!=0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.chatArray.count-1 inSection:0];
        [self.chatTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}
#pragma mark --- 解决使用IQKeyboardManager导航栏偏移问题
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    self.inputFunctionView.isLoseEfficacy = NO;
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    self.inputFunctionView.isLoseEfficacy = YES;
}
#pragma mark --- 停止播放语音
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:StopPlayVoiceNotification object:nil];
}
#pragma mark --- 监听inputFunctionView Frame改变 同时改变tableview
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"frame"]) {
        AkrLog(@"self.inputFunctionView.y = %f", self.inputFunctionView.y);
        AkrLog(@"-------%@",@(SCREEN_HEIGHT-SafeAreaBottomHeight-self.inputFunctionView.height));
        if (self.inputFunctionView.y+self.inputFunctionView.height<SCREEN_HEIGHT-SafeAreaBottomHeight) {
            //键盘弹出
            self.hideKeyBoardControl.hidden = NO;
        }else{
            //键盘收起
            self.hideKeyBoardControl.hidden = YES;
        }
        if (self.chatArray.count>0) {
            self.chatTableView.height = self.inputFunctionView.y-kNavBarHeighAndStatusBarHeight;
            [self.chatTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:(self.chatArray.count-1) inSection:0] atScrollPosition:(UITableViewScrollPositionBottom) animated:YES];
        }
    }
}
-(void)rightBarButtonItemAction:(id)sender
{
    UIButton * rightBtn = (UIButton *)sender;
    if (rightBtn.tag==1000) {
        //更多
//        AkrLog(@"更多");
        [self.chatTableView setEditing:!self.chatTableView.isEditing animated:YES];
        ////        [self showEitingView:YES];
        [self.chatTableView reloadData];
        self.chatTableView.allowsSelection = !self.chatTableView.isEditing;
        
    }
}

#pragma mark - 协议 HMJInputFunctionViewDelegate
/**
*  录音完成数据回调
*/
-(void)didFinishRecoingVoiceAction:(NSDictionary *)voiceDic
{
    AkrLog(@"voiceDic---%@",voiceDic);
    if ([self sendFriendVerification]) {
        //构建FrameModel
        HHChatFrameModel * frameModel = [[HHChatMessageManager shareManager] creatChatFrameModelWithMessageType:TOMessageTypeVoice content:[voiceDic mj_JSONString] FromType:@"1" DestID:@"" PreviousModel:self.chatArray.lastObject];
        [self insertTableViewRowsWithModel:frameModel];
    }
}
/**
*  文本、表情数据回调
*/
-(void)didSendMesage:(NSString *)message
{
    AkrLog(@"message---%@",message);
    //判断是否是好友
    if ([self sendFriendVerification]) {
        //构建FrameModel
        HHChatFrameModel * frameModel = [[HHChatMessageManager shareManager] creatChatFrameModelWithMessageType:TOMessageTypeBQMM content:message FromType:@"1" DestID:@"" PreviousModel:self.chatArray.lastObject];
        [self insertTableViewRowsWithModel:frameModel];
    }
}
/// 图片数据回调
-(void)didSendImage:(NSArray *)imageArray
{
    if ([self sendFriendVerification]) {
        for(int i=0;i<imageArray.count;i++){
            //构建FrameModel
            HHChatFrameModel * frameModel = [[HHChatMessageManager shareManager] creatChatFrameModelWithMessageType:TOMessageTypePhoto content:imageArray[i] FromType:@"1" DestID:@"" PreviousModel:self.chatArray.lastObject];
            [self insertTableViewRowsWithModel:frameModel];
        }
    }
}
/// 视频数据回调
-(void)didSendVideo:(NSURL *)videoUrl
{
    if ([self sendFriendVerification]) {
        HHChatFrameModel * frameModel = [[HHChatMessageManager shareManager] creatChatFrameModelWithMessageType:TOMessageTypeVideo content:videoUrl FromType:@"1" DestID:@"" PreviousModel:self.chatArray.lastObject];
        [self insertTableViewRowsWithModel:frameModel];
    }
}
#pragma mark --- 插入数据，刷新表
-(void)insertTableViewRowsWithModel:(HHChatFrameModel *)frameModel
{
    [self.chatArray addObject:frameModel];
    [CATransaction setDisableActions:YES];
    [self.chatTableView beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.chatArray.count - 1 inSection:0];
    [self.chatTableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.chatTableView endUpdates];
    [CATransaction setDisableActions:NO];
    double delayInSeconds = 0.05;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:self.chatArray.count - 1 inSection:0];
        [self.chatTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    });
}
#pragma mark --- 好友验证
- (BOOL)sendFriendVerification{
//    HMJContactsModel *model = [[HMJFMDBContactsDataModel sharedInstance] queryDataFromDataBaseWithContactId:[NSString stringWithFormat:@"%ld",(long)self.destId]].firstObject;
//    if (([model.isFriend isEqualToString:@"0"]) && self.fromType==1) {
//        NSMutableDictionary *messageDic = [NSMutableDictionary dictionary];
//        NSDictionary *content = @{@"msgString": @"你还不是对方好友", @"MessageBubbleName": @""};
//        [messageDic setObject:[content JSONString] forKey:@"content"];
//        [messageDic setObject:@"66" forKey:@"messageType"];
//        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//        [dic setObject:[NSString stringWithFormat:@"%@",self.fromID] forKey:@"fromId"];
//        [dic setObject:[NSString stringWithFormat:@"%ld",self.destId] forKey:@"destId"];
//        [dic setObject:[JSON stringWithObject:messageDic] forKey:@"WithdrawMessage"];
//        [dic setObject:[NSString createUUID] forKey:@"msgId"];
//        [self creatframeModelModelWithContent:[JSON stringWithObject:dic] message:TOMessageFromOther messageType:TOMessageTypeSendMessageRefused];
//        return NO;
//    }
    return YES;
}

#pragma mark --- UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.chatArray.count;
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    HHChatCell * cell = [tableView dequeueReusableCellWithIdentifier:@"HHChatCell" forIndexPath:indexPath];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HHChatFrameModel * frameModel = self.chatArray[indexPath.row];
    return frameModel.cellHeight;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    HHChatCell * chatCell = (HHChatCell *)cell;
    WeakSelf(self)
    chatCell.contentClickBlock = ^{
       
    };
    chatCell.selectionStyle = tableView.isEditing?UITableViewCellSelectionStyleDefault:UITableViewCellSelectionStyleNone;
    chatCell.isEdit = tableView.isEditing;
    chatCell.frameModel = self.chatArray[indexPath.row];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.isEditing) {
        return;
    }
//    //滑动时隐藏长按手势弹出的UIMenuController
//    [[NSNotificationCenter defaultCenter] postNotificationName:UIMenuControllerHiddenNotification object:nil];
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (self.selectIndexArray.count > 0) {
//        [self.selectModelArray removeObject:self.messagesArray[indexPath.row]];
//        [self.selectIndexArray removeObject:indexPath];
//    }
}
//多选
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    HHChatFrameModel * frameModel = self.chatArray[indexPath.row];
    if (frameModel.chatModel.messageType.integerValue == TOMessageTypePeopleJoinGroup || frameModel.chatModel.messageType.integerValue  == TOMessageTypePeopleExitGroup || frameModel.chatModel.messageType.integerValue  == TOMessageTypeReceiveRedPacket|| frameModel.chatModel.messageType.integerValue  == TOMessageTypeTransferReturn || frameModel.chatModel.messageType.integerValue  == TOMessageTypeRedPacketReturn || frameModel.chatModel.messageType.integerValue  == TOMessageTypeRedPacketReceiveTime || frameModel.chatModel.messageType.integerValue  == TOMessageTypeSystem || frameModel.chatModel.messageType.integerValue  == TOMessageTypeWithdraw || frameModel.chatModel.messageType.integerValue  == TOMessageTypeRead|| frameModel.chatModel.messageType.integerValue  == TOMessageTypeAt || frameModel.chatModel.chatFrameType == ChatFrameTypeSystemMessages ) {
        return UITableViewCellEditingStyleNone;
    }
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}

#pragma mark --- 收起键盘
-(void)hideKeyBoard
{
    [self.inputFunctionView hideKeyBoard];
}
#pragma mark --- 懒加载
-(UIImageView *)chatBackgroundIV
{
    if (!_chatBackgroundIV) {
        _chatBackgroundIV = Init(UIImageView);
        _chatBackgroundIV.frame = self.view.frame;
        _chatBackgroundIV.contentMode = UIViewContentModeScaleAspectFill;
        _chatBackgroundIV.clipsToBounds = YES;
        _chatBackgroundIV.hidden = YES;
//        _chatBackgroundIV.image = ImageInit(@"bgImage_02");
    }
    return _chatBackgroundIV;
}
-(UITableView *)chatTableView
{
    if (!_chatTableView) {
        _chatTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNavBarHeighAndStatusBarHeight, SCREEN_WIDTH, SCREEN_HEIGHT-kNavBarHeighAndStatusBarHeight-SafeAreaBottomHeight-InputFunctionViewHeight) style:UITableViewStylePlain];
        _chatTableView.delegate = self;
        _chatTableView.dataSource = self;
        _chatTableView.backgroundColor = ColorManage.nav_color;
        _chatTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_chatTableView registerClass:[HHChatCell class] forCellReuseIdentifier:@"HHChatCell"];
        _chatTableView.tableFooterView = [UIView new];
        _chatTableView.showsVerticalScrollIndicator = NO;
        _chatTableView.mj_header = [HHChatRefreshHeader headerWithRefreshingBlock:^{
            [self loadData];
        }];
        [self.view addSubview:_chatTableView];
    }
    return _chatTableView;
}
-(UIControl *)hideKeyBoardControl
{
    if (!_hideKeyBoardControl) {
        _hideKeyBoardControl = Init(UIControl);
        [_hideKeyBoardControl addTarget:self action:@selector(hideKeyBoard) forControlEvents:UIControlEventTouchDown];
        _hideKeyBoardControl.hidden = YES;
        [self.view addSubview:_hideKeyBoardControl];
        [self.view bringSubviewToFront:_hideKeyBoardControl];
        [_hideKeyBoardControl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_chatTableView);
        }];
    }
    return _hideKeyBoardControl;
}
-(HMJInputFunctionView *)inputFunctionView
{
    if (!_inputFunctionView) {
        _inputFunctionView = [[HMJInputFunctionView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-SafeAreaBottomHeight-InputFunctionViewHeight, SCREEN_WIDTH, InputFunctionViewHeight)];
        _inputFunctionView.delegate = self;
        [self.view addSubview:_inputFunctionView];
        if (SafeAreaBottomHeight!=0) {
            UIView *bottomSafeView = [[UIView alloc] init];
            bottomSafeView.backgroundColor = ColorManage.inputBar_color;
            bottomSafeView.frame = CGRectMake(0, CGRectGetMaxY(_inputFunctionView.frame), SCREEN_WIDTH, SafeAreaBottomHeight);
            [self.view addSubview:bottomSafeView];
        }
    }
    return _inputFunctionView;
}
-(NSMutableArray *)chatArray
{
    if (!_chatArray) {
        _chatArray = [NSMutableArray new];
    }
    return _chatArray;
}

@end
