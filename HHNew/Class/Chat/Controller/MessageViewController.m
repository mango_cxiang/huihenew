//
//  MessageViewController.m
//  HHNew
//
//  Created by 储翔 on 2020/4/20.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "MessageViewController.h"
#import "HomeMessageCell.h"
#import "HHSingleChatViewController.h"
#import "HHGroupChatViewController.h"

#define RightItemBeginTag 100
@interface MessageViewController ()<UITableViewDelegate,UITableViewDataSource,YBPopupMenuDelegate>

@property(nonatomic,strong)UITableView * messageListTableView;
@property(nonatomic,strong)NSMutableArray * messageListArray;
@property (nonatomic, strong)YBPopupMenu * addPopupMenu;

@end

@implementation MessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initNavBar];
    [self.messageListTableView reloadData];
}

#pragma mark --- 导航处理
-(void)initNavBar
{
    self.title = @"聊天";
    [self setLeftBarButtonItem:@"合作"];
    [self setRightBarButtonItemWithImageArray:@[@"tianjia",@"tongxunlu"] BeginTag:RightItemBeginTag];
    
    UIView * searchView = [[UIView alloc]initWithFrame:CGRectMake(0, kNavBarHeighAndStatusBarHeight, SCREEN_WIDTH, 50)];
    searchView.backgroundColor = ColorManage.nav_color;
    UIButton * searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.frame = CGRectMake(15, 8, SCREEN_WIDTH-30, 34);
    searchBtn.layer.cornerRadius = 5;
    searchBtn.layer.masksToBounds = YES;
    [searchBtn setBackgroundColor:ColorManage.background_color];
    [searchBtn addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    [searchBtn setTitle:@"🔍 搜索" forState:UIControlStateNormal];
    [searchBtn setTitleColor:RGB_Color(153, 153, 153) forState:UIControlStateNormal];
    searchBtn.titleLabel.font = Font(14);
    [searchView addSubview:searchBtn];
    [self.view addSubview:searchView];
    
}
-(void)leftBarButtonItemAction:(id)sender
{
    //合作
    NSLog(@"合作");
}
-(void)rightBarButtonItemAction:(id)sender
{
    UIButton * rightBtn = (UIButton *)sender;
    if (rightBtn.tag==RightItemBeginTag) {
        //加号
        NSLog(@"添加");
        [YBPopupMenu showRelyOnView:rightBtn titles:@[@"添加好友",@"发起群聊",@"扫一扫"] icons:@[@"tianjiahaoyou0531line",@"saqunliao0531",@"saoma0531"] menuWidth:150 otherSettings:^(YBPopupMenu *popupMenu) {
            popupMenu.showMaskView = NO;
            popupMenu.backColor = RGB_Color(76, 76, 76);
            popupMenu.itemHeight = 55;
            popupMenu.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            popupMenu.fontSize = 17;
            popupMenu.textColor = RGB_Color(255, 255, 255);
            popupMenu.arrowWidth = 10;
            popupMenu.arrowHeight = 5;
            popupMenu.offset = 10;
            popupMenu.delegate = self;
        }];
    }else{
        //好友通讯录
        NSLog(@"通讯录");
    }
}

-(void)searchAction
{
    //搜索
    NSLog(@"搜索");
}

#pragma mark --- YBPopupMenuDelegate
-(void)ybPopupMenu:(YBPopupMenu *)ybPopupMenu didSelectedAtIndex:(NSInteger)index
{
    switch (index) {
        case 0:
        {
            //添加好友
        }
            break;
        case 1:
        {
            //发起群聊
        }
            break;
        case 2:
        {
            //扫一扫
        }
            break;
        default:
            break;
    }
}
#pragma mark --- UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messageListArray.count;
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    HomeMessageCell * chatCell = [tableView dequeueReusableCellWithIdentifier:@"HomeMessageCell" forIndexPath:indexPath];
    chatCell.messageListModel = self.messageListArray[indexPath.row];
    return chatCell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    HHSingleChatViewController * vc = Init(HHSingleChatViewController);
//    HHGroupChatViewController * vc = Init(HHGroupChatViewController);
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark --- 懒加载
-(UITableView *)messageListTableView
{
    if (!_messageListTableView) {
        _messageListTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNavBarHeighAndStatusBarHeight+50, SCREEN_WIDTH, SCREEN_HEIGHT-kNavBarHeighAndStatusBarHeight-kTabBarHeight-50) style:UITableViewStylePlain];
        _messageListTableView.delegate = self;
        _messageListTableView.dataSource = self;
        _messageListTableView.rowHeight = 70;
        _messageListTableView.backgroundColor = ColorManage.background_color;
        [_messageListTableView registerClass:[HomeMessageCell class] forCellReuseIdentifier:@"HomeMessageCell"];
        _messageListTableView.tableFooterView = [UIView new];
        _messageListTableView.showsVerticalScrollIndicator = NO;
        [self.view addSubview:_messageListTableView];
    }
    return _messageListTableView;
}
-(NSMutableArray *)messageListArray
{
    if (!_messageListArray) {
        _messageListArray = [NSMutableArray new];
        for (int i = 0; i<20; i++) {
//            NSString * str = @"测试";
            HHMessageListModel * listModel = Init(HHMessageListModel);
            if (i==0) {
                listModel.sessionType = [NSNumber numberWithInt:10];
            }else if (i==1) {
                listModel.sessionType = [NSNumber numberWithInt:11];
            }else if (i==2) {
                listModel.sessionType = [NSNumber numberWithInt:12];
            }else if (i==3) {
                listModel.sessionType = [NSNumber numberWithInt:13];
            }else if (i==4) {
                listModel.sessionType = [NSNumber numberWithInt:101];
            }else if (i==5) {
                listModel.sessionType = [NSNumber numberWithInt:201];
            }else if (i==6) {
                listModel.sessionType = [NSNumber numberWithInt:301];
            }else if (i==7) {
                listModel.sessionType = [NSNumber numberWithInt:401];
            }else if (i==8) {
                listModel.sessionType = [NSNumber numberWithInt:500];
            }else if (i==9) {
                listModel.sessionType = [NSNumber numberWithInt:501];
            }else if (i==10) {
                listModel.sessionType = [NSNumber numberWithInt:502];
            }else if (i==11) {
                listModel.sessionType = [NSNumber numberWithInt:503];
            }
            listModel.headImg = @"https://down.beiquanren.com/head/ee07275bcfa44929961a6f65b1d1346f.png?_t=1588924611451&x-oss-process=style/head_132";
            listModel.name = @"测试测试";
            [_messageListArray addObject:listModel];
        }
    }
    return _messageListArray;
}

@end
