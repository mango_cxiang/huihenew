//
//  HMJVideoNSURLCache.h
//  huihe
//
//  Created by changle on 2017/2/4.
//  Copyright © 2017年 Six. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, VideoCacheType) {
    VideoCacheTypeChat, //单聊
    VideoCacheTypeFriendsCircle, //圈子
    VideoCacheTypeShortVideo //短视频
};
@interface HMJVideoNSURLCache : NSObject
+ (instancetype)sharedInstance;
//下载链接 选择存放文件夹
-(void)download:(NSString *)urlStr videoCacheType:(VideoCacheType)type;
@end
