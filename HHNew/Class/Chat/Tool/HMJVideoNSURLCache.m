//
//  HMJVideoNSURLCache.m
//  huihe
//
//  Created by changle on 2017/2/4.
//  Copyright © 2017年 Six. All rights reserved.
//

#import "HMJVideoNSURLCache.h"
#import "HMJFilesManager.h"
@interface HMJVideoNSURLCache ()<NSURLSessionDownloadDelegate>
/** 下载任务 */
@property (nonatomic,strong)NSURLSessionDownloadTask *downloadTask;
/** 下载的数据信息 */
@property (nonatomic,strong)NSData *resumeData;
/** 下载的会话 */
@property (nonatomic,strong)NSURLSession *URLSession;

@property (nonatomic,assign)VideoCacheType videoType;
@end

@implementation HMJVideoNSURLCache

// 单例
+ (instancetype)sharedInstance
{
    static HMJVideoNSURLCache * _videoCache = nil;
    static dispatch_once_t onceT;
    dispatch_once(&onceT, ^{
        _videoCache = [[HMJVideoNSURLCache alloc] init];
    });
    [[HMJFilesManager sharedInstance] creatFolder:@"ChatVideo"];
    [[HMJFilesManager sharedInstance] creatFolder:@"FriendsCircleVideo"];
    return _videoCache;
}

#pragma mark - 创建会话的方法
-(void)download:(NSString *)urlStr videoCacheType:(VideoCacheType)type
{
    // 创建会话
    self.URLSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration
                                                              defaultSessionConfiguration] delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    // 确定URL
    NSURL* url = [NSURL URLWithString:urlStr];
    // 通过会话在确定的URL上创建下载任务
    self.downloadTask = [self.URLSession downloadTaskWithURL:url];
    [self startDownLoad];
    
    self.videoType = type;
    
}
#pragma mark - 按钮监听的点击事件方法
// 开始下载
- (void)startDownLoad
{
    // 启动任务
    [self.downloadTask resume];
}

- (void)cancleDownLoad
{
// 取消下载
    [self.downloadTask suspend];//suspend暂停下载|可恢复的
    //cancelByProducingResumeData取消下载，同时可以获取已经下载的数据相关信息
    [self.downloadTask cancelByProducingResumeData:^(NSData * _Nullable resumeData) {
        self.resumeData = resumeData;
    }];
}

// 继续下载
//NSURLSessionDownloadTask* downloadTask = [self.URLSession downloadTaskWithResumeData:self.resumeData];
//[downloadTask resume];
//self.downloadTask = downloadTask;

#pragma mark - NSURLSessionDataDelegate Function
// 下载了数据的过程中会调用的代理方法
-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten
totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    NSLog(@"%lf",1.0 * totalBytesWritten / totalBytesExpectedToWrite);
//    self.progress.progressValue = 1.0 * totalBytesWritten / totalBytesExpectedToWrite;
}

// 重新恢复下载的代理方法
-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes
{
    
}

// 写入数据到本地的时候会调用的方法
-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location{
    NSString* fullPath;
    NSString * folderPath;
    if (self.videoType == VideoCacheTypeChat) {
        folderPath = @"ChatVideo/";
    }else  if (self.videoType == VideoCacheTypeShortVideo) {
        folderPath = @"ShortVideo/";
    }
    else
    {
        folderPath = @"FriendsCircleVideo/";
    }
    fullPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]
     stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@",folderPath,downloadTask.response.suggestedFilename]];
    
    [[NSFileManager defaultManager] moveItemAtURL:location
                                            toURL:[NSURL fileURLWithPath:fullPath]
                                            error:nil];
    NSLog(@"%@",fullPath);
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DELETEFRIEND_VIDEOSUCCESS object:nil];
}

// 请求完成，错误调用的代理方法
-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    NSLog(@"%@",error);
}

#pragma mark - 懒加载
-(NSData *)resumeData{
    if (!_resumeData) {
        _resumeData = [NSData data];
    }
    return _resumeData;
}

@end
