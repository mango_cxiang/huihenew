//
//  HHChatMessageManager.h
//  HHNew
//
//  Created by 张 on 2020/4/30.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HHMessageListModel;
@class HHChatFrameModel;
NS_ASSUME_NONNULL_BEGIN

@interface HHChatMessageManager : NSObject

+ (HHChatMessageManager *)shareManager;

/// 创建消息Model，构建完直接发送消息
/// @param messageType messageType description
/// @param content content description
/// @param fromType fromType description
/// @param destID destID description
/// @param previousModel previousModel description
-(HHChatFrameModel *)creatChatFrameModelWithMessageType:(TOMessageType)messageType content:(id)content FromType:(NSString *)fromType DestID:(NSString *)destID PreviousModel:(HHChatFrameModel * _Nullable)previousModel;

@end

NS_ASSUME_NONNULL_END
