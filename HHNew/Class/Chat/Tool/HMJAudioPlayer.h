//
//  HMJAudioPlayer.h
//  huihe
//
//  Created by Six on 16/3/18.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@protocol HMJAudioPlayerDelegate <NSObject>

@optional

- (void)audioPlayerWillStartPlayVoice;
- (void)audioPlayerDidStartPlayVoice;
- (void)audioPlayerDidFinishPlayVoice;

@end

/**
 *  播放器
 */
@interface HMJAudioPlayer : NSObject<AVAudioPlayerDelegate>

@property (nonatomic , strong) AVAudioPlayer * player;
// 代理
@property (nonatomic , weak) id <HMJAudioPlayerDelegate> delegate;

@property (nonatomic, assign) CGFloat currentValue;

@property (nonatomic,copy) void(^Clickblock)(CGFloat currentValue);
// 单例
+ (HMJAudioPlayer *)sharedInstance;

- (void)playWithVoiceUrl:(NSString *)voiceUrl;
- (void)playWithVoiceData:(NSData *)voiceData;
- (void)playAroundMyselfVoiceVoiceData:(NSData *)voiceData;
- (void)stopPlayVoice;
- (void)setupPlayVoice;

@end
