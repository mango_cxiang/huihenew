//
//  MoviePlayerViewController.h
//  Player
//
//  Created by dllo on 15/11/7.
//  Copyright © 2015年 zhaoqingwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface MoviePlayerViewController : BaseViewController
@property (nonatomic,strong) NSString *url;
@property (nonatomic,assign) BOOL isLocationVideo;
@property (nonatomic,strong) NSURL * locationUrl;

@end
