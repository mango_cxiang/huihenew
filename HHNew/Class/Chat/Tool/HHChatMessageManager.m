//
//  HHChatMessageManager.m
//  HHNew
//
//  Created by 张 on 2020/4/30.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHChatMessageManager.h"
#import "HHChatFrameModel.h"
#import "VoiceConverter.h"
#import "HMJVideoNSURLCache.h"


@implementation HHChatMessageManager

+ (HHChatMessageManager *)shareManager
{
    static HHChatMessageManager * _chatMessageManager = nil;
    static dispatch_once_t onceT;
    dispatch_once(&onceT, ^{
        _chatMessageManager = Init(HHChatMessageManager);
    });
    return _chatMessageManager;
}

/// 创建消息Model，构建完直接发送消息
/// @param messageType messageType description
/// @param content content description
/// @param fromType fromType description
/// @param destID destID description
/// @param previousModel previousModel description
-(HHChatFrameModel *)creatChatFrameModelWithMessageType:(TOMessageType)messageType content:(id)content FromType:(NSString *)fromType DestID:(NSString *)destID PreviousModel:(HHChatFrameModel * _Nullable)previousModel
{
    HHChatModel * chatModel = Init(HHChatModel);
    chatModel.fromId        = [NSString acquireUserId];
    chatModel.messageType   = [NSString stringWithFormat:@"%@",@(messageType)];
    chatModel.isHide        = YES;
    chatModel.content       = content;
    chatModel.fromType      = fromType;
    chatModel.destId        = destID;
    chatModel.sendTime      = [NSString stringWithFormat:@"%lld",(long long)([[NSDate date] timeIntervalSince1970] * 1000)];
    NSString * msgId        = [[NSString stringWithFormat:@"%@-%@-%@-%@",chatModel.sendTime, [NSString acquireUserId], destID, [NSString createUUID]] MD5ForUpper32Bate];
    chatModel.msgId         = [NSString stringWithFormat:@"200-%@", msgId];
    chatModel.version       = @"-1";
    chatModel.status        = @"正在发送";
    chatModel.sessionId     = [NSString stringWithFormat:@"%@%@%@",fromType,SESSIONID_MIDDLE,destID];
    if (!previousModel) {
        chatModel.hideTime  = NO;
    }else{
        NSTimeInterval seconds = ([chatModel.sendTime longLongValue] - [previousModel.chatModel.sendTime longLongValue])/1000;
        chatModel.hideTime  = ((fabs(seconds)/60) < 1);
    }
    chatModel.destName = @"";
    //获取当前用户头像、昵称
//    chatModel.imageIconUrl = [HMJAccountModel myself].headUrl;
//    NSString *nameStr = [HMJAccountModel myself].nickName;
    if ([fromType isEqualToString:@"1"]) {
        //单聊
//        chatModel.fromName = nameStr;
    }else{
        //群聊
//        HMJContactsModel *contactModel = (HMJContactsModel *)[[HMJFMDBGroupDataModel sharedInstance] queryDataFromGroupMemberDataBaseWithGroupId:message.destId uid:message.fromId].firstObject;
//        message.fromName = (contactModel.markName.length == 0 ? contactModel.nickName:contactModel.markName);
    }
    if (messageType == TOMessageTypePhoto) {
        if ([content isKindOfClass:[UIImage class]]) {
            chatModel.image = content;
//            WeakSelf(self)
//            [HMJBusinessManager OSSuploadImages:@[content] sortId:@"" fileName:BQ_CHAT_PATH isAsync:YES  imageModel:imageModel complete:^(NSArray<NSString *> *names) {
//                NSString *arrayStr = [names componentsJoinedByString:@","];
//                if ([message.fromType isEqualToString:@"2"]) {
//                    message.content = [JSON stringWithObject:@{@"ImageUrl":arrayStr, @"groupName":[[HMJFMDBGroupDataModel sharedInstance] queryDataNameBasegroupId:message.destId], @"groupHeadUrl":message.imageIconUrl}];
//                }else {
//                    message.content = arrayStr;
//                }
//                //[param setValue:arrayStr forKey:@"url"];
//
//                [[HMJFMDBStorageDataModel sharedInstance] updataContentToDataBase:message.destId content:message.content msgId:message.msgId];
//                message.image = content;
//                [weakSelf sendMessageWithMessage:message];
//
//            } fail:^{
//                DLog(@"");
//            }];
        }
    }else if (messageType == TOMessageTypeVideo) {
        if ([content isKindOfClass:[NSURL class]]) {
            NSURL * videoUrl = (NSURL *)content;
            AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoUrl options:nil];
//            UIImage * image = [self getVideoPreViewImageWithAVURLAsset:asset];
            //图片上传到阿里云，传完进行视频上传
//            WEAKSELF
//            [HMJBusinessManager OSSuploadImages:@[img] sortId:@"" fileName:BQ_CHAT_PATH isAsync:YES imageModel:nil complete:^(NSArray<NSString *> *names) {
//                NSString *arrayStr = [names componentsJoinedByString:@","];
//                NSString *urlstr = arrayStr;
//                NSMutableDictionary *dict = [chatModel.content mj_JSONObject];
//                [dict setObject:urlstr forKey:@"getVedioBitmapUrl"];
//                messageFrame.message = tempMessage;
//
//                if([videoUrl.absoluteString hasSuffix:@"mov"]){
//                    //mov转mp4
//                    [weakSelf mov2mp4:videoUrl preImageUrl:urlstr preMessage:previousMessage messageFrame:messageFrame];
//                }else{
//
//                }
//            } fail:^{
//
//            }];
        }
        //下载视频到本地
//        NSDictionary * jsonDic = [content mj_JSONObject];
//        [[HMJVideoNSURLCache sharedInstance] download:[NSString stringWithFormat:@"%@",[jsonDic valueForKey:@"url"]] videoCacheType:VideoCacheTypeChat];
    }else if (messageType == TOMessageTypeVoice) {
        NSDictionary *contentDic = [content mj_JSONObject];
        chatModel.durantion = contentDic[@"time"];
        chatModel.seconds = [contentDic[@"time"] integerValue];
        NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
        NSString *filePath = [contentDic objectForKey:@"url"];
        NSURL *urlpath = [NSURL fileURLWithPath:filePath];
        NSData *amrData = [VoiceConverter EncodeWavToAmr:filePath sampleRateType:Sample_Rate_16000];
        [array addObject:amrData];
        chatModel.voiceData = [NSData dataWithContentsOfURL:urlpath];
        chatModel.audioLocationPath = [urlpath path];
//        [HMJBusinessManager OSSuploadVoices:array fileName:BQ_CHAT_PATH voiceDurantion:message.durantion isAsync:YES complete:^(NSArray<NSString *> *names) {
//            //NSMutableDictionary *param = [NSMutableDictionary dictionary];
//            NSString *arrayStr = [names componentsJoinedByString:@","];
//            //[param setValue:arrayStr forKey:@"url"];
//            NSMutableDictionary *messageDict = [NSMutableDictionary dictionary];
//            NSString *urlstr = arrayStr;
//            [messageDict setObject:urlstr forKey:@"url"];
//            [messageDict setObject:message.durantion forKey:@"time"];
//            [messageDict setObject:[urlpath path] forKey:@"audioLocationPath"];
//            [messageDict setObject:[content objectForKey:@"str"] forKey:@"str"];
//            message.content = [messageDict JSONString];
//            [[HMJFMDBStorageDataModel sharedInstance] updataContentToDataBase:message.destId content:message.content msgId:message.msgId];
//            [self sendMessageWithMessage:message];
//
//        } fail:^{
//
//        }];
        
    }else if (messageType == TOMessageTypeRedPacket || messageType == TOMessageTypeReceiveRedPacket) {
        //通过接口发送消息，只存到到数据库,需要系统给发送消息
//        [[HMJFMDBMessageDataModel sharedInstance] upDataToDataBaseWithmessageModel:message];
    }else{
        //通过Socket发送消息
        [self sendChatMessageWithChatModel:chatModel];
    }
    HHChatFrameModel * frameModel = Init(HHChatFrameModel);
    frameModel.chatModel = chatModel;
    return frameModel;
}
#pragma mark --- 根据URL获取视频占位图
- (UIImage*)getVideoPreViewImageWithAVURLAsset:(AVURLAsset *)asset
{
    AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    gen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *img = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    return img;
}
#pragma mark --- mov格式视频转成MP4格式
-(void)movConvertToMP4:(NSURL *)path
{
    AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:path options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]initWithAsset:avAsset presetName:AVAssetExportPresetMediumQuality];
    NSString *exportPath = [NSString stringWithFormat:@"%@/%@.mp4",[NSHomeDirectory() stringByAppendingString:@"/tmp"],@"1"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtURL:[NSURL fileURLWithPath:exportPath] error:NULL];
    exportSession.outputURL = [NSURL fileURLWithPath:exportPath];
    AkrLog(@"exportPath:%@", exportPath);
    exportSession.outputFileType = AVFileTypeMPEG4;
    WEAKSELF
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        if ([exportSession status] == AVAssetExportSessionStatusCompleted) {
            //上传视频
            
            
        }else{
            [[HMJShowLoding sharedInstance]showText:@"视频上传失败！"];
        }
    }];
}

#pragma mark --- 通过Socket发送消息
-(void)sendChatMessageWithChatModel:(HHChatModel *)chatModel
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[NSString acquireUserId]      forKey:@"fromId"];
    [params setObject:chatModel.destId              forKey:@"destId"];
    [params setObject:@"2"                          forKey:@"devType"];
    [params setObject:chatModel.fromType            forKey:@"fromType"];
    [params setObject:chatModel.sendTime            forKey:@"sendTime"];
    [params setObject:chatModel.messageType         forKey:@"messageType"];
    /******** 处理content作为destName的载体 *********/
    id content = [chatModel.content mj_JSONObject];
    if ([content isKindOfClass:[NSDictionary class]]&&[content valueForKey:@"destName"]) {
        [content removeObjectForKey:@"destName"];
    }
    [params setObject:[content mj_JSONString]       forKey:@"content"];
    [params setObject:chatModel.msgId               forKey:@"msgId"];
    [params setObject:SafeString(chatModel.imageIconUrl)  forKey:@"imageIconUrl"];
    [params setObject:SafeString(chatModel.fromName)  forKey:@"fromName"];
    if (chatModel.destName.length > 0) {
        [params setObject:chatModel.destName        forKey:@"destName"];
    }
    //给自己发消息
    if((chatModel.fromType.integerValue == 1 && [chatModel.destId isEqualToString:[NSString acquireUserId]])||(chatModel.fromType.integerValue == 2 && (chatModel.messageType.integerValue == TOMessageTypeNearbyQuit || chatModel.messageType.integerValue == TOMessageTypeNearbyJoin))){
//        [self sendMessageToMyself:message params:params];
    }else{
        //给他人发消息
        if (chatModel.fromType.integerValue == 2) {
            //群聊判断是否为禁言
//            if([[HMJFMDBGroupDataModel sharedInstance] checkUserGroupMemberGagStatus:message.destId contactId:[NSString acquireUserId]]){
//                NSLog(@"已被禁言，消息未发出");
//                return;
//            }
        }
//        if (chatModel.messageType.integerValue == TOMessageTypeBQMM) {
//            NSDictionary *dic = [chatModel.content mj_JSONObject];
//            NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithDictionary:dic];
//            [mutableDic setObject:[HMJGlobals getMessageBubbleImageName] forKey:@"MessageBubbleName"];
//            message.content = [JSON stringWithObject:mutableDic];
//            [params setObject:message.content forKey:@"content"];
//        }
        //发送消息
//        [[HMJSocketManager sharedInstance] sendChatMessageWithMessage:params];
        if (chatModel.messageType.integerValue == TOMessageTypeVoice) {
            if (chatModel.durantion) {
                [params setObject:chatModel.durantion forKey:@"durantion"];
            }
            if (chatModel.audioLocationPath) {
                [params setObject:chatModel.audioLocationPath forKey:@"audioLocationPath"];
            }
        }
        if (chatModel.messageType.integerValue != TOMessageTypeRead && chatModel.messageType.integerValue != TOMessageTypeMultiVoiceRequest && chatModel.messageType.integerValue != TOMessageTypeSomeBodyJoinOrExitMultiVideoRequest) {//33已读 //163多人音视频邀请 //168有人加入或者退出多人音视频
            [params setObject:[NSString stringWithFormat:@"%@%@%@",chatModel.fromType,SESSIONID_MIDDLE,chatModel.destId] forKey:@"sessionId"];
            //存到数据库
//            [[HMJFMDBMessageDataModel sharedInstance] upDataToDataBaseWithmessageModel:message];
        }
    }
}

@end
