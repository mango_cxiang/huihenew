//
//  HMJFilesManager.m
//  huihe
//
//  Created by changle on 2017/2/6.
//  Copyright © 2017年 Six. All rights reserved.
//

#import "HMJFilesManager.h"

@interface HMJFilesManager ()
@property (nonatomic,assign) BOOL delSuccess;
@end

@implementation HMJFilesManager

// 单例
+ (instancetype)sharedInstance
{
    static HMJFilesManager * _manager = nil;
    static dispatch_once_t onceT;
    dispatch_once(&onceT, ^{
        _manager = [[HMJFilesManager alloc] init];
        //得到沙盒文件夹 下的所有文件
    });
    return _manager;
}


- (void)creatFolder:(NSString *)folder
{
    //  在Documents目录下创建一个名为folder的文件夹
    NSString *path = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:folder];

    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir = FALSE;
    BOOL isDirExist = [fileManager fileExistsAtPath:path isDirectory:&isDir];
    if(!(isDirExist && isDir))
    {
        BOOL bCreateDir = [fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
        if(!bCreateDir){
            NSLog(@"创建文件夹失败！");
        }
        NSLog(@"创建文件夹成功，文件路径%@",path);
    }
}

- (void)createFile:(NSString*)file
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:file])
    {
        [fileManager createFileAtPath:file contents:nil attributes:[NSDictionary dictionaryWithObject:NSFileProtectionNone forKey:NSFileProtectionKey]];
    }
}




- (void)releaseCache
{
    WEAKSELF
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        [weakSelf deleteChatVideoFile:@"ChatVideo"];
        [weakSelf deleteChatVideoFile:@"FriendsCircleVideo"];
    });
}

//删除机制 如果聊天视频文件夹大小超过设定的大小20M 则根据创建时间 删除超过当前时间7天的旧视频数据 如果都没有超时删除最早的一个视频文件
- (void)deleteChatVideoFile:(NSString *)folderPath
{
    NSString * documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath =[documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",folderPath]];
    
    self.delSuccess = NO;
    
    if([self folderSizeAtPath:filePath] > 20)
    {
        NSMutableArray * fileList = [NSMutableArray arrayWithArray:[self sortFiles:[[NSFileManager defaultManager] contentsOfDirectoryAtPath:filePath error:nil]]];
        NSError *error = nil;
        for (NSString *file in fileList) {
            
            NSString *path =[filePath stringByAppendingPathComponent:file];
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:&error];
            NSString *creationDate = [fileAttributes objectForKey:NSFileCreationDate];
            if (creationDate) {
                if ([self timeout:creationDate])
                {
                    [self delFilePath:path];
                    
                    //在此判断是否小于20 如果还是大于继续删
                    [self deleteChatVideoFile:folderPath];
                }
            }
        }
        
        if (!self.delSuccess)
        {
            NSString * file = fileList.firstObject;
            
            NSString *path =[filePath stringByAppendingPathComponent:file];
            
            [self delFilePath:path];
            
            //在此判断是否小于20 如果还是大于继续删
            [self deleteChatVideoFile:folderPath];
        }
    }
    else
    {
        self.delSuccess = YES;
    }
}

//- (NSString *)downLoadStr {
//    [self creatFolder:@"FileDownLoad"];
//}

- (void)delFilePath:(NSString *)path
{
    if([[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        NSError *error = nil;

        if ([[NSFileManager defaultManager] removeItemAtPath:path error:&error])
        {
            self.delSuccess = YES;
            NSLog(@"删除文件成功 路径：%@",path);
        }
        else
        {
            NSLog(@"删除文件失败 路径：%@",path);
        }
    }
}


//遍历文件夹获得文件夹大小，返回多少M
- (float )folderSizeAtPath:(NSString*)folderPath{
    
    NSFileManager* manager = [NSFileManager defaultManager];
    
    if (![manager fileExistsAtPath:folderPath]) return 0;
    
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
    
    NSString* fileName;
    
    long long folderSize = 0;
    
    while ((fileName = [childFilesEnumerator nextObject]) != nil){
        
        NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        
        folderSize += [self fileSizeAtPath:fileAbsolutePath];
        
    }
    
    return folderSize/(1024.0*1024.0);
}

//单个文件的大小
- (long long)fileSizeAtPath:(NSString*) filePath{
    
    NSFileManager* manager = [NSFileManager defaultManager];
    
    if ([manager fileExistsAtPath:filePath]){
        
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize];
    }
    return 0;
}

//是否 时间差大于七天
- (BOOL)timeout:(NSString *)creatTime
{
    NSLog(@"File creationDate: %@\n", creatTime);
    
    NSArray * dateArray = [[NSString stringWithFormat:@"%@",creatTime] componentsSeparatedByString:@" "];
    
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    
    //时差
    [dateformatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    [dateformatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate * oldDate = [dateformatter dateFromString:[NSString stringWithFormat:@"%@ %@",dateArray[0],dateArray[1]]];
    
    NSString * oldTime = [NSString stringWithFormat:@"%f",[oldDate timeIntervalSince1970]];
    
    //    计算现在距1970年多少秒
    NSDate *datenow = [NSDate date];
    //当前时间转时间戳
    NSString *currentTime = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]];
    
    double timeDiffence = [currentTime longLongValue] - [oldTime longLongValue];
    
    NSLog(@"%f",timeDiffence);
    
    if(timeDiffence > 60*60*24*7)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

//将文件进行时间从早到晚排序
- (NSArray *)sortFiles:(NSArray *)files
{
    NSArray *sortFiles = [files sortedArrayUsingComparator:^(NSString * firstPath, NSString* secondPath) {
        NSString * firstData = [firstPath componentsSeparatedByString:@"_"].firstObject;
        NSString * secondData = [secondPath componentsSeparatedByString:@"_"].firstObject;//获取前一个文件修改时间
        if (firstData.longLongValue > secondData.longLongValue)
        {
            return (NSComparisonResult)NSOrderedDescending;
        }
        else if (firstData.longLongValue < secondData.longLongValue)
        {
            return (NSComparisonResult)NSOrderedAscending;
        }
        else
        {
            return (NSComparisonResult)NSOrderedSame;
        }
    }];
    return sortFiles;
}
@end
