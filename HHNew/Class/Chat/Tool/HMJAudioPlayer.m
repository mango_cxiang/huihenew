//
//  HMJAudioPlayer.m
//  huihe
//
//  Created by Six on 16/3/18.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJAudioPlayer.h"

@interface HMJAudioPlayer()
@property (nonatomic,strong) NSTimer *timer;
@end

@implementation HMJAudioPlayer

// 单例
+ (HMJAudioPlayer *)sharedInstance
{
    static HMJAudioPlayer * _audioPlayer = nil;
    static dispatch_once_t onceT;
    dispatch_once(&onceT, ^{
        _audioPlayer = [[HMJAudioPlayer alloc] init];
    });
    return _audioPlayer;
}

- (void)playWithVoiceUrl:(NSString *)voiceUrl
{
    dispatch_async(dispatch_queue_create("playWithVoiceUrl", NULL), ^{
        if (self.delegate && [self.delegate respondsToSelector:@selector(audioPlayerWillStartPlayVoice)])
        {
            [self.delegate audioPlayerWillStartPlayVoice];
        }
        NSData * voiceData = [NSData dataWithContentsOfURL:[NSURL URLWithString:voiceUrl]];
        NSLog(@"voiceData_%@",voiceData);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self playVoiceWithVoiceData:voiceData];
        });
    });
}

- (void)playWithVoiceData:(NSData *)voiceData
{
    [self setupPlayVoice];
    [self playVoiceWithVoiceData:voiceData];
}

- (void)setupPlayVoice
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActiveNotification:) name:UIApplicationWillResignActiveNotification object:[UIApplication sharedApplication]];
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionModeMeasurement error: nil];
}

- (void)playAroundMyselfVoiceVoiceData:(NSData *)voiceData{
    [self setupAroundMyselfPlayVoice];
    [self playVoiceWithVoiceData:voiceData];
}

- (void)setupAroundMyselfPlayVoice{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActiveNotification:) name:UIApplicationWillResignActiveNotification object:[UIApplication sharedApplication]];
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: nil];
}

- (void)playVoiceWithVoiceData:(NSData *)voiceData
{
    if (_player)
    {
        [_player stop];
        _player.delegate = nil;
        _player = nil;
    } 
    NSError * error;
    _player = [[AVAudioPlayer alloc]initWithData:voiceData error:&error];
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    [audioSession setActive:YES error:nil];
//    AVAudioFormat* format = _player.format;
//    NSLog(@"采样频率==%lf",format.sampleRate);////默认为:25600,所以也是按照这个频率来计算的
    _player.volume = 1.0;
    if (!_player)
    {
        AkrLog(@"AVAudioPlayer 错误信息: %@" , error.description);
    }
    _player.delegate = self;
    [_player play];
    if (_timer == nil) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateProgress) userInfo:nil repeats:YES];
    }
   // _currentValue = _player.currentTime/_player.duration;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(audioPlayerDidStartPlayVoice)])
    {
        [self.delegate audioPlayerDidStartPlayVoice];
    }
}

-(void)updateProgress{
    //进度条显示播放进度
    if(_player!=nil){
        if (self.Clickblock) {
            self.Clickblock(_player.currentTime/_player.duration);
        }
    }
}

- (void)stopPlayVoice
{
    if (_player && _player.isPlaying)
    {
        [_player stop];
    }
    if(_timer!=nil){
        [_timer invalidate];
        _timer = nil;
    }
}

#pragma mark - 通知中心
- (void)applicationWillResignActiveNotification:(NSNotification *)notification
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(audioPlayerDidFinishPlayVoice)])
    {
        [self.delegate audioPlayerDidFinishPlayVoice];
    }
}

#pragma mark - 协议 AVAudioPlayerDelegate
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(audioPlayerDidFinishPlayVoice)])
    {
        [self.delegate audioPlayerDidFinishPlayVoice];
    }
}

@end
