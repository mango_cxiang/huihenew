//
//  HMJFilesManager.h
//  huihe
//
//  Created by changle on 2017/2/6.
//  Copyright © 2017年 Six. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DownLoadFilePath @"DownLoadFilePath"

@interface HMJFilesManager : NSObject
+ (instancetype)sharedInstance;
- (void)creatFolder:(NSString *)folder;
- (void)createFile:(NSString*)file;
- (void)releaseCache;
//- (NSString *)downLoadStr;
@end
