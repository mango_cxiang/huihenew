//
//  HHImageTypeModel.h
//  HHNew
//
//  Created by 张 on 2020/5/6.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, ImageType) {
    ImageTypeGif = 0,
    ImageTypeJpg = 1,
    ImageTypePng = 2
};

@interface HHImageTypeModel : NSObject

@property (nonatomic, assign) ImageType imageType;
@property (nonatomic, strong) UIImage   *image;
@property (nonatomic, strong) NSData    *imageData;
@property (nonatomic, copy  ) NSString  *imageTypeString;
@property (nonatomic, assign) BOOL      isGif;

@end

NS_ASSUME_NONNULL_END
