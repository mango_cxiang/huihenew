//
//  HHChatModel.h
//  HHNew
//
//  Created by 张 on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HHChatModel : NSObject

@property (nonatomic , copy) NSString * fromType;//fromType1：人对人消息，2：群消息，3：系统通知
@property (nonatomic , copy) NSString * destId;//destId
@property (nonatomic , copy) NSString * fromId;
@property (nonatomic , copy) NSString * version;//消息版本号version
@property (nonatomic , copy) NSString * msgId;//发消息方产生的uuid
@property (nonatomic , copy) NSString * messageType;//messageType
@property (nonatomic , copy) NSString * content;//msgContent
//content转化，只转化一次，避免多次转化造成消耗
@property (nonatomic , copy) NSMutableAttributedString * attributedContent;
@property (nonatomic , copy) NSString * durantion;
@property (nonatomic , copy) NSString * status;
@property (nonatomic , copy) NSString * sendTime;
//转化后的时间字符串,初始化时转化，避免多次转化造成消耗
@property (nonatomic , copy) NSString * timeString;
@property (nonatomic , copy) NSString * devType;
@property (nonatomic , copy) NSString * translateContent;
@property (nonatomic , copy) NSString * audioLocationPath;
@property (nonatomic , copy) NSString * isRead;
@property (nonatomic , copy) NSString * sessionId;
@property (nonatomic , copy) NSString * isFired ; //是否焚毁
@property (nonatomic , copy) NSString * isUnpack ; //是否拆包
@property (nonatomic , copy) NSString *voiceconren;

@property (nonatomic, copy) NSString *destName; // 群主撤销消息对象

@property (nonatomic, copy) NSString *isActure; // 群邀请消息是否已确认

/// 消息来源名字
@property (nonatomic, copy) NSString *fromName;
/// 消息来源头像
@property (nonatomic, copy) NSString *imageIconUrl;

@property (nonatomic , copy) NSString *voiceText;
@property (nonatomic , copy) NSString *voiceStatus;
@property (nonatomic , copy) NSString *inviteGroupStatus;
    
/*
 * messageType
 * 1、客户端确认收到ready回应，并且处理完好友列表时，发送给服务器
 * 2、文本及表情消息
 * 3、图片消息
 * 4、文件
 * 5、好友上线，这时fromId为上线人的Id，destId为自己
 * 6、好友离线，这时fromId为离线人的Id，destId为自己
 * 7、解除好友，这时fromId为主动解除人的Id，destId为自己
 * 8、有人加入了群，这时fromId为加入的人，destId为群Id
 * 9、有人退出了群，这时fromId为退出人，destId为群Id
 * 10、被邀请加入群，这时fromId为被邀请人，destId为群id--->类同8同。
 * 11、有人申请加好友，这时fromId为请求人
 * 12、申请加好友的请求被通过，这时fromId为对方id,  destId为自己
 * 13、有人申请加入群，这时fromId为申请人，destId为群id
 * 14、申请加入群的请求被通过，这时fromId为审核人id，destId为群id
 * 15、账户从其它地方登陆，踢下线
 */
//是否转菊花
@property (nonatomic , assign) BOOL isHide;
//消息是否已读
@property (nonatomic , assign) NSNumber * read;
//是否隐藏时间
@property (nonatomic , assign) BOOL hideTime;


//照片
@property (nonatomic , strong) UIImage * image;
//录音数据
@property (nonatomic , strong) NSData * voiceData;
//录音秒数值
@property (nonatomic , assign) NSInteger seconds;
//是显示翻译
@property (nonatomic , assign) BOOL isTranslateContentShow;
//是否显示wave
@property (nonatomic , assign) BOOL iswaveShow;

#pragma mark ---以下是 新增的活动 合作 赞助 票务 属性
//高度
@property (nonatomic , assign) CGFloat cellHeight;
//接受赞助的状态
@property (nonatomic , copy) NSString *acceptType;
#pragma mark ---以上是 新增的活动 合作 赞助 票务 属性

#pragma mark --- 添加视图width
@property(nonatomic,assign)CGFloat viewWidth;

@property(nonatomic,assign)NSInteger chatFrameType;

//拼接的群通知消息
@property (nonatomic , copy) NSMutableAttributedString * jointGroupTip;
@end

NS_ASSUME_NONNULL_END
