//
//  HHSendRedPacketViewController.m
//  HHNew
//
//  Created by 张 on 2020/5/6.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHSendRedPacketViewController.h"

@interface HHSendRedPacketViewController ()<UITextFieldDelegate>

@property (nonatomic, strong) UITextField * totalMoneyTF;
@property (nonatomic, strong) UITextField * contentTF;
@property (nonatomic, strong) UILabel * moneyLabel;
@property (nonatomic, strong) UIButton * insertMoneyButton;
//群聊发红包
@property (nonatomic, strong) UILabel * pinLB;
@property (nonatomic, strong) UILabel * totalMoneyLB;
@property (nonatomic, strong) UILabel * currentTypeLB;
@property (nonatomic, strong) UIButton * otherTypeBtn;
@property (nonatomic, strong) UITextField * numberTF;
@property (nonatomic, strong) UILabel * receiveLB;
@property (nonatomic, strong) UILabel * redPacketTipLB;
//红包类型  1拼手气  2普通红包
@property(nonatomic,assign)NSInteger redPacketType;

@end

@implementation HHSendRedPacketViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"发红包";
    self.view.backgroundColor = ColorManage.nav_color;
    self.isGroup = YES;
    if (!self.isGroup) {
        UILabel * totalMoneyLB = Init(UILabel);
        totalMoneyLB.text = @"    总金额    ";
        totalMoneyLB.textColor = ColorManage.text_color;
        totalMoneyLB.font = SystemFont(16);
        UILabel * unitLB = Init(UILabel);
        unitLB.text = @"  元  ";
        unitLB.textColor = ColorManage.text_color;
        unitLB.font = SystemFont(16);
        _totalMoneyTF = Init(UITextField);
        _totalMoneyTF.textColor = ColorManage.text_color;
        _totalMoneyTF.font = SystemFont(16);
        _totalMoneyTF.placeholder = @"0.00";
        _totalMoneyTF.backgroundColor = ColorManage.darkBackgroundColor;
        _totalMoneyTF.leftView = totalMoneyLB;
        _totalMoneyTF.leftViewMode = UITextFieldViewModeAlways;
        _totalMoneyTF.rightView = unitLB;
        _totalMoneyTF.rightViewMode = UITextFieldViewModeAlways;
        _totalMoneyTF.textAlignment = NSTextAlignmentRight;
        _totalMoneyTF.delegate = self;
        _totalMoneyTF.keyboardType = UIKeyboardTypeDecimalPad;
        [_totalMoneyTF addTarget:self action:@selector(textFieldTextShouldChanged:) forControlEvents:UIControlEventEditingChanged];
        [_totalMoneyTF viewWithRadis:5];
        [self.view addSubview:_totalMoneyTF];
        [_totalMoneyTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(kNavBarHeighAndStatusBarHeight+30);
            make.left.offset(25);
            make.width.offset(SCREEN_WIDTH-50);
            make.height.offset(55);
        }];
    }else{
        self.redPacketType = 1;
        
        _redPacketTipLB = Init(UILabel);
        _redPacketTipLB.frame = CGRectMake(0, 0, SCREEN_WIDTH, 30);
        _redPacketTipLB.backgroundColor = RGB(217, 105, 68);
        _redPacketTipLB.text = @"单个红包不可低于0.01元";
        _redPacketTipLB.font = SystemFont(14);
        _redPacketTipLB.textColor = UIColor.whiteColor;
        _redPacketTipLB.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:_redPacketTipLB];
        
        UIView * tfLeftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 120, 55)];
        _pinLB = Init(UILabel);
        _pinLB.frame = CGRectMake(10, 17.5, 20, 20);
        _pinLB.text = @"拼";
        _pinLB.textColor = UIColor.whiteColor;
        _pinLB.backgroundColor = RGB(197, 152, 76);
        _pinLB.font = SystemFont(12);
        _pinLB.textAlignment = NSTextAlignmentCenter;
        [_pinLB viewWithRadis:5];
        [tfLeftView addSubview:_pinLB];
        _totalMoneyLB = Init(UILabel);
        _totalMoneyLB.frame = CGRectMake(35, 0, 85, 55);
        _totalMoneyLB.text = @"总金额";
        _totalMoneyLB.textColor = ColorManage.text_color;
        _totalMoneyLB.font = SystemFont(16);
        [tfLeftView addSubview:_totalMoneyLB];
        UILabel * unitLB = Init(UILabel);
        unitLB.text = @"  元  ";
        unitLB.textColor = ColorManage.text_color;
        unitLB.font = SystemFont(16);
        _totalMoneyTF = Init(UITextField);
        _totalMoneyTF.textColor = ColorManage.text_color;
        _totalMoneyTF.font = SystemFont(16);
        _totalMoneyTF.placeholder = @"0.00";
        _totalMoneyTF.backgroundColor = ColorManage.darkBackgroundColor;
        _totalMoneyTF.leftView = tfLeftView;
        _totalMoneyTF.leftViewMode = UITextFieldViewModeAlways;
        _totalMoneyTF.rightView = unitLB;
        _totalMoneyTF.rightViewMode = UITextFieldViewModeAlways;
        _totalMoneyTF.textAlignment = NSTextAlignmentRight;
        _totalMoneyTF.delegate = self;
        _totalMoneyTF.keyboardType = UIKeyboardTypeDecimalPad;
        [_totalMoneyTF addTarget:self action:@selector(textFieldTextShouldChanged:) forControlEvents:UIControlEventEditingChanged];
        [_totalMoneyTF viewWithRadis:5];
        [self.view addSubview:_totalMoneyTF];
        [_totalMoneyTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(kNavBarHeighAndStatusBarHeight+30);
            make.left.offset(25);
            make.width.offset(SCREEN_WIDTH-50);
            make.height.offset(55);
        }];
        _currentTypeLB = Init(UILabel);
        _currentTypeLB.text = @"当前为拼手气红包，";
        _currentTypeLB.textColor = ColorManage.text_color;
        _currentTypeLB.font = SystemFont(12);
        [self.view addSubview:_currentTypeLB];
        [_currentTypeLB mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.totalMoneyTF.mas_bottom);
            make.left.offset(35);
            make.height.offset(30);
        }];
        _otherTypeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _otherTypeBtn.titleLabel.font = SystemFont(12);
        [_otherTypeBtn setTitleColor:RGB(200, 133, 0) forState:UIControlStateNormal];
        [_otherTypeBtn setTitle:@"改为普通红包" forState:UIControlStateNormal];
        [_otherTypeBtn addTarget:self action:@selector(changeRedPacketType) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_otherTypeBtn];
        [_otherTypeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.totalMoneyTF.mas_bottom);
            make.left.equalTo(self.currentTypeLB.mas_right);
            make.height.offset(30);
        }];
        UILabel * redPacketNumLB = Init(UILabel);
        redPacketNumLB.text = @"    红包个数    ";
        redPacketNumLB.textColor = ColorManage.text_color;
        redPacketNumLB.font = SystemFont(16);
        UILabel * gLB = Init(UILabel);
        gLB.text = @"  个  ";
        gLB.textColor = ColorManage.text_color;
        gLB.font = SystemFont(16);
        _numberTF = Init(UITextField);
        _numberTF.textColor = ColorManage.text_color;
        _numberTF.font = SystemFont(16);
        _numberTF.placeholder = @"填写个数";
        _numberTF.backgroundColor = ColorManage.darkBackgroundColor;
        _numberTF.leftView = redPacketNumLB;
        _numberTF.leftViewMode = UITextFieldViewModeAlways;
        _numberTF.rightView = gLB;
        _numberTF.rightViewMode = UITextFieldViewModeAlways;
        _numberTF.textAlignment = NSTextAlignmentRight;
        _numberTF.keyboardType = UIKeyboardTypeNumberPad;
        [_numberTF viewWithRadis:5];
        [self.view addSubview:_numberTF];
        [_numberTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.otherTypeBtn.mas_bottom);
            make.left.offset(25);
            make.width.offset(SCREEN_WIDTH-50);
            make.height.offset(55);
        }];
        UIView * receiveView = Init(UIView);
        receiveView.backgroundColor = ColorManage.darkBackgroundColor;
        [receiveView viewWithRadis:5];
        [self.view addSubview:receiveView];
        [receiveView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.numberTF.mas_bottom).offset(15);
            make.left.offset(25);
            make.width.offset(SCREEN_WIDTH-50);
            make.height.offset(55);
        }];
        UILabel * receiveTipLB = Init(UILabel);
        receiveTipLB.text = @"    谁可以领    ";
        receiveTipLB.textColor = ColorManage.text_color;
        receiveTipLB.font = SystemFont(16);
        [receiveView addSubview:receiveTipLB];
        [receiveTipLB mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.left.offset(0);
        }];
        UIImageView * arrowIV = Init(UIImageView);
        arrowIV.image = ImageInit(@"next");
        [receiveView addSubview:arrowIV];
        [arrowIV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.offset(0);
            make.right.offset(-10);
        }];
        _receiveLB = Init(UILabel);
        _receiveLB.text = @"所有人";
        _receiveLB.textColor = ColorManage.text_color;
        _receiveLB.font = SystemFont(16);
        [receiveView addSubview:_receiveLB];
        [_receiveLB mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.offset(0);
            make.right.equalTo(arrowIV.mas_left).offset(-10);
            make.left.equalTo(receiveTipLB.mas_right);
            make.height.offset(55);
        }];
    }
    _contentTF = Init(UITextField);
    _contentTF.textColor = ColorManage.text_color;
    _contentTF.font = SystemFont(16);
    _contentTF.placeholder = @"恭喜发财，大吉大利";
    _contentTF.backgroundColor = ColorManage.darkBackgroundColor;
    _contentTF.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 55)];
    _contentTF.leftViewMode = UITextFieldViewModeAlways;
    _contentTF.rightView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 55)];
    _contentTF.rightViewMode = UITextFieldViewModeAlways;
    [_contentTF viewWithRadis:5];
    [self.view addSubview:_contentTF];
    [_contentTF mas_makeConstraints:^(MASConstraintMaker *make) {
        if (self.isGroup) {
            make.top.equalTo(self.receiveLB.mas_bottom).offset(15);
        }else{
            make.top.equalTo(self.totalMoneyTF.mas_bottom).offset(15);
        }
        make.left.offset(25);
        make.width.offset(SCREEN_WIDTH-50);
        make.height.offset(55);
    }];
    
    _moneyLabel = [[UILabel alloc] init];
    _moneyLabel.text = @"¥0.00";
    _moneyLabel.font = BoldFont(50);
    _moneyLabel.textColor = ColorManage.text_color;
    _moneyLabel.alpha = 0.6;
    [self.view addSubview:_moneyLabel];
    [_moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.top.equalTo(self.contentTF.mas_bottom).offset(self.isGroup?50:150);
    }];
    
    _insertMoneyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _insertMoneyButton.titleLabel.font = SystemFont(16);
    [_insertMoneyButton setTitle:@"塞钱进红包" forState:UIControlStateNormal];
    [_insertMoneyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_insertMoneyButton addTarget:self action:@selector(insertButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [_insertMoneyButton viewWithRadis:5];
    _insertMoneyButton.backgroundColor = RGB(217, 105, 68);
    _insertMoneyButton.alpha = 0.6;
    _insertMoneyButton.enabled = NO;
    [self.view addSubview:_insertMoneyButton];
    [_insertMoneyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.moneyLabel.mas_bottom).offset(20);
        make.size.mas_equalTo(CGSizeMake(184, 48));
    }];
    
    UILabel *tipLabel = Init(UILabel);
    tipLabel.text = @"未领取的红包，将于24小时后发起退款";
    tipLabel.textColor = RGB(102, 102, 102);
    tipLabel.font = SystemFont(16);
    tipLabel.textAlignment = NSTextAlignmentCenter;
    [tipLabel setAdjustsFontSizeToFitWidth:YES];
    [self.view addSubview:tipLabel];
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.bottom.offset(-SafeAreaBottomHeight-30);
        make.width.offset(SCREEN_WIDTH-50);
    }];
    
}
#pragma mark --- 改变红包类型
-(void)changeRedPacketType
{
    self.redPacketType = self.redPacketType==1?2:1;
    if (self.redPacketType==2) {
        [_otherTypeBtn setTitle:@"改为拼手气红包" forState:UIControlStateNormal];
        _currentTypeLB.text = @"当前为普通红包，";
        _pinLB.hidden = YES;
        _totalMoneyLB.text = @"单个金额";
        _totalMoneyLB.x = 10;
    }else{
        [_otherTypeBtn setTitle:@"改为普通红包" forState:UIControlStateNormal];
        _currentTypeLB.text = @"当前为拼手气红包，";
        _pinLB.hidden = NO;
        _totalMoneyLB.text = @"总金额";
        _totalMoneyLB.x = 35;
    }
//    WEAKSELF
//    [UIView animateWithDuration:0.5 animations:^{
//        weakSelf.redPacketTipLB.y = self.redPacketType==2?kNavBarHeighAndStatusBarHeight:0;
//    }];
}
#pragma mark --- UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if([textField isEqual: _totalMoneyTF]){
        //    限制只能输入数字
        BOOL isHaveDian = YES;
        if ([string isEqualToString:@" "]) {
            return NO;
        }
        if ([textField.text rangeOfString:@"."].location == NSNotFound) {
            isHaveDian = NO;
        }
        if ([string length] > 0) {
            unichar single = [string characterAtIndex:0];//当前输入的字符
            if ((single >= '0' && single <= '9') || single == '.') {//数据格式正确
                if([textField.text length] == 0){
                    if(single == '.') {
                        [textField.text stringByReplacingCharactersInRange:range withString:@""];
                        return NO;
                    }
                }
                //输入的字符是否是小数点
                if (single == '.') {
                    if(!isHaveDian)//text中还没有小数点
                    {
                        isHaveDian = YES;
                        return YES;
                        
                    }else{
                        //[self showText:(@"数据格式有误")];
                        [textField.text stringByReplacingCharactersInRange:range withString:@""];
                        return NO;
                    }
                }else{
                    if (isHaveDian) {//存在小数点
                        //判断小数点的位数
                        NSString *textStr = textField.text;
                        NSRange lastRange = NSMakeRange(textStr.length-1, 1);
                        NSRange ran = [textStr rangeOfString:@"."];
                        if(lastRange.location - ran.location>1){
                            return NO;
                        }
                        
                        if ((range.location - ran.location <= 2)&&(range.location - ran.location >= 1)) {
                            return YES;
                        }else{
                            //[self showText:(@"最多两位小数")];
                            return NO;
                        }
                    }else{
                        CGFloat length = textField.text.length;
                        if (length > 4) {
                           // _insertMoneyButton.userInteractionEnabled = NO;
                            return NO;
                        }else{
                           // _insertMoneyButton.userInteractionEnabled = YES;
                            return YES;
                        }
                        return YES;
                    }
                }
            }else{//输入的数据格式不正确
                [textField.text stringByReplacingCharactersInRange:range withString:@""];
                return NO;
            }
        }
        else{
            return YES;
        }
    }
    return YES;
}
- (void)textFieldTextShouldChanged:(UITextField *)textField {
    if (textField == self.totalMoneyTF) {
        if (textField.text.length > 0 && textField.text.floatValue!=0) {
            self.moneyLabel.text = [NSString stringWithFormat:@"¥%.2f", textField.text.floatValue];
            _insertMoneyButton.alpha = 1;
            _moneyLabel.alpha = 1;
            _insertMoneyButton.enabled = YES;
        }else{
            self.moneyLabel.text = @"¥0.00";
            _insertMoneyButton.alpha = 0.6;
            _moneyLabel.alpha = 0.6;
            _insertMoneyButton.enabled = NO;
        }
    }
}
#pragma mark --- 塞钱进红包
-(void)insertButtonClicked
{
    [self.view endEditing:YES];
    NSString *money = self.totalMoneyTF.text;
//    NSString *contentStr = [self.contentTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (money.length == 0) {
        ShowToast(@"请输入金额")
        return;
    }
    if (money.floatValue <= 0.0 || money.floatValue > 50000.0) {
        ShowToast(@"红包金额在0~50000元之间")
        return;
    }
    // 获取零钱再拉起支付弹框
    [self getBalance];
}
#pragma mark --- 获取零钱
-(void)getBalance
{
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
