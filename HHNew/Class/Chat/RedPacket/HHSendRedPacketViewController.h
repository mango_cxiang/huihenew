//
//  HHSendRedPacketViewController.h
//  HHNew
//
//  Created by 张 on 2020/5/6.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^SendRedPacketBlock)(NSString * content);
@interface HHSendRedPacketViewController : BaseViewController

@property(nonatomic,assign)BOOL isGroup;
@property(nonatomic,copy)NSString * destID;
@property(nonatomic,copy)SendRedPacketBlock sendRedPacketBlock;

@end

NS_ASSUME_NONNULL_END
