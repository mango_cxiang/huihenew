//
//  HHAddFriendsSearchResultTableView.m
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHAddFriendsSearchResultTableView.h"
#import "HHAddFriendsSearchResultTableViewCell.h"

@interface HHAddFriendsSearchResultTableView ()<UITableViewDelegate,UITableViewDataSource>

@end
@implementation HHAddFriendsSearchResultTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.delegate = self;
        self.dataSource = self;
        self.backgroundColor = ColorManage.grayBackgroundColor;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
    }
    return self;
}

- (void)setdataArry:(NSMutableArray *)dataArry{
    _dataArry = dataArry;
    [self reloadData];
}

#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
    return self.dataArry.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellName = @"HHAddFriendsSearchResultTableViewCell";
    HHAddFriendsSearchResultTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if(!cell) {
           cell = LoadCell(@"HHAddFriendsSearchResultTableViewCell");
       }
    if (indexPath.row == 0) {
        cell.dataDict = @{@"icon" : @"qunliao" , @"title" :@"英雄联盟",  @"desc" :@"123456789"};
    } else if (indexPath.row == 1) {
        cell.dataDict = @{@"icon" : @"qunliao" , @"title" :@"王者荣耀", @"desc" :@"987654321"};
    }
    cell.backgroundColor = ColorManage.darkBackgroundColor;
    cell.selectedBackgroundView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.clickAddFriendsSearchResultTableViewCell) {
        self.clickAddFriendsSearchResultTableViewCell(indexPath);
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] init];
    header.backgroundColor = ColorManage.grayBackgroundColor;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 200, 30)];
    label.text = @"手机联系人";
    label.textColor = ColorManage.grayTextColor_Deault;
    label.font = kRegularFont(14);
    [header addSubview:label];
    return header;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


@end
