//
//  HHAddGroupView.m
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHAddGroupTopView.h"
@interface HHAddGroupTopView ()<UITextFieldDelegate>

@end

@implementation HHAddGroupTopView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        //        [self loadView];
        self =  [[[NSBundle mainBundle]loadNibNamed:@"HHAddGroupTopView" owner:self options:nil] lastObject];
        self.frame = frame;
        self.groupNameTextField.delegate = self;
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    self.setNameView.backgroundColor = ColorManage.darkBackgroundColor;
    self.addMembersView.backgroundColor = ColorManage.darkBackgroundColor;
    
    self.groupNameTextField.borderStyle = UITextBorderStyleNone;
    self.groupNameTextField.tintColor = RGB_GreenColor;
    self.groupNameTextField.backgroundColor = ColorManage.darkBackgroundColor;
    self.groupNameTextField.placeholder = @"输入分组名称";
    self.groupNameTextField.borderStyle = UITextBorderStyleNone;
    self.groupNameTextField.clearButtonMode = UITextFieldViewModeAlways;
    self.groupNameTextField.returnKeyType = UIReturnKeyDone;
    self.groupNameTextField.font = kRegularFont(16);
    
    self.groupNameLabel.font = kRegularFont(14);
    self.groupNameLabel.textColor = ColorManage.text_color;
    self.groupNameLabel.backgroundColor =ColorManage.grayBackgroundColor;
    
    self.groupMembersLabel.font = kRegularFont(14);
    self.groupMembersLabel.textColor = ColorManage.text_color;
    self.groupMembersLabel.backgroundColor = ColorManage.grayBackgroundColor;
    self.addGroupMembersBtn.titleLabel.font = kRegularFont(16);
    self.addGroupMembersBtn.titleLabel.textColor = ColorManage.text_color;
    
    [self.addGroupMembersBtn setTitleColor:ColorManage.text_color forState:UIControlStateNormal];
}

- (void)setMembersCount:(NSInteger)membersCount{
    self.groupMembersLabel.text = [NSString stringWithFormat:@"   分组成员（%ld）",(long)membersCount];
}



- (void)layoutSubviews{
    [super layoutSubviews];
//    gl.frame = self.bounds;
//    [self.bgView.layer insertSublayer:gl atIndex:0];
    
}

- (IBAction)addGroupMembers:(id)sender {
    if (self.addGroupMembers) {
        self.addGroupMembers();
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
@end
