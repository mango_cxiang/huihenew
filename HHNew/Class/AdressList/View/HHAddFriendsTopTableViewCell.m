//
//  HHAddFriendsTopTableViewCell.m
//  HHNew
//
//  Created by Liubin on 2020/4/23.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHAddFriendsTopTableViewCell.h"

@implementation HHAddFriendsTopTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = ColorManage.grayBackgroundColor;
    self.chatIDLabel.font = kRegularFont(14);
    self.chatIDLabel.textColor = ColorManage.text_color;
}

- (void)setDataDict:(NSDictionary *)dataDict
{
    _dataDict = dataDict;
//    NSString *headUrl = dataDict[@"icon"];
    [self.qrCodeBtn setImage:[UIImage imageNamed:@"qrcode"] forState:UIControlStateNormal];
    self.chatIDLabel.text = [NSString stringWithFormat:@"我的会合号：%@", dataDict[@"chatID"]];
}


- (IBAction)clickQrCodeBtn:(id)sender {
    if (self.showQrCode) {
        self.showQrCode();
    }
}
@end
