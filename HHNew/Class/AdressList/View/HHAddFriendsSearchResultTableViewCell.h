//
//  HHAddFriendsSearchResultTableViewCell.h
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HHContactsModel;
NS_ASSUME_NONNULL_BEGIN

@interface HHAddFriendsSearchResultTableViewCell : UITableViewCell
@property (nonatomic , strong) NSDictionary * dataDict;

@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIView *lineView;

@property (strong, nonatomic) HHContactsModel *contactsModel;

- (void)updateTitleLabelWithKeywordMode:(NSString *)keywordMode hintStr:(NSString *)hintStr;

@end

NS_ASSUME_NONNULL_END
