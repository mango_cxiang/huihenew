//
//  HMJGroupMembersTableViewCell.m
//  huihe
//
//  Created by mysun on 16/3/8.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "HMJGroupMembersTableViewCell.h"
#import "UIView+Extension.h"

@implementation HMJGroupMembersTableViewCell

- (void)awakeFromNib{
    [super awakeFromNib];
//    self.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    //[self.hearImage radiusWithAngle:5];
    self.hearImage.clipsToBounds = YES;
    self.hearImage.contentMode = UIViewContentModeScaleAspectFill;
    [self.hearImage setContentScaleFactor:[[UIScreen mainScreen] scale]];
    self.hearImage.layer.cornerRadius = 8;
    self.nameLable.font = SystemFont(16);
    self.redDot.clipsToBounds = YES;
    self.redDot.layer.cornerRadius = 5;
    self.redDot.hidden = YES;
    
    self.rightLabel.hidden = YES;
    self.rightLabel.textColor = ColorManage.grayTextColor_Deault;
    self.rightLabel.font = kRegularFont(14);
    self.lineView.backgroundColor = ColorManage.lineViewColor;
}



- (void)setDataDict:(NSDictionary *)dataDict
{
    _dataDict = dataDict;
    NSString *headUrl = dataDict[@"icon"];
    if([headUrl hasPrefix:@"http"]){
        [self.hearImage sd_setImageWithURL:[NSURL URLWithString:headUrl] placeholderImage:nil options:SDWebImageRefreshCached];
    }else{
      self.hearImage.image = [UIImage imageNamed:headUrl];
    }
    self.nameLable.text = dataDict[@"name"];
    if([self.nameLable.text isEqualToString:@"新的朋友"]){
//        if(![[[AppSetting sharedAppSetting] getNewFriendDot] isEqualToString:@"1"]){
//            self.redDot.hidden = YES;
//        }else{
//            self.redDot.hidden = NO;
//        }
    }
}

- (void)setContacts:(HHContactsModel *)contacts{
    _contacts = contacts;
    NSString *headUrl = contacts.headUrl;
       if([headUrl hasPrefix:@"http"]){
           [self.hearImage sd_setImageWithURL:[NSURL URLWithString:headUrl] placeholderImage:nil options:SDWebImageRefreshCached];
       }else{
         self.hearImage.image = [UIImage imageNamed:headUrl];
       }
    self.nameLable.text = contacts.markName?contacts.markName:contacts.nickName;
}

- (void)updateWithKeywordMode:(NSString *)keywordMode hintStr:(NSString *)hintStr{
    if (keywordMode) {
        NSString *string = keywordMode;
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.f],NSForegroundColorAttributeName:ColorManage.text_color}];
        
        if (hintStr) {
            NSRange range = [string.lowercaseString rangeOfString:hintStr.lowercaseString];
            if (range.location != NSNotFound) {
                UIColor *tintColor =  RGB_GreenColor;
                [attString setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.f],NSForegroundColorAttributeName:tintColor} range:range];
            }
        }
        self.nameLable.attributedText = attString;
    }
}
@end
