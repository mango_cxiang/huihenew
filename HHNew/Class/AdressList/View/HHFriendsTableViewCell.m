//
//  HHAddNewFriendsTableViewCell.m
//  HHNew
//
//  Created by Liubin on 2020/4/23.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHFriendsTableViewCell.h"

@implementation HHFriendsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = ColorManage.darkBackgroundColor;
    self.titleLabel.font = kRegularFont(17);
    self.titleLabel.textColor = ColorManage.text_color;
    self.descLabel.font = kRegularFont(14);
    self.descLabel.textColor = RGB_GrayColor_Text;
    self.iconImgView.layer.cornerRadius = 5;
    self.iconImgView.layer.masksToBounds = YES;
    self.lineView.backgroundColor = ColorManage.lineViewColor;
}

- (void)setDataDict:(NSDictionary *)dataDict
{
    _dataDict = dataDict;
    NSString *headUrl = dataDict[@"icon"];
    if([headUrl hasPrefix:@"http"]){
    [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:headUrl] placeholderImage:nil options:SDWebImageRefreshCached];
    }else{
      self.iconImgView.image = [UIImage imageNamed:headUrl];
    }
    self.titleLabel.text = dataDict[@"title"];
    self.descLabel.text = dataDict[@"desc"];
}

- (void)setContactsModel:(HHContactsModel *)contactsModel{
    _contactsModel = contactsModel;
//    NSString *headUrl = contactsModel.headUrl;
    [self.iconImgView xxd_setHeadImageView:self.iconImgView imgUrl:contactsModel.headUrl];
//    if([headUrl hasPrefix:@"http"]){
//    [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:headUrl] placeholderImage:nil options:SDWebImageRefreshCached];
//    }else{
//        self.iconImgView.image = [UIImage imageNamed:headUrl];
//    }
    self.titleLabel.text = contactsModel.markName.length >0 ?contactsModel.markName:contactsModel.nickName;
    self.descLabel.text = [NSString stringWithFormat:@"%@ 退群",contactsModel.updatetime];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
