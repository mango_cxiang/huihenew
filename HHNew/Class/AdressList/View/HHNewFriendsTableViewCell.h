//
//  HHNewFriendsTableViewCell.h
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^agreeNewFriends)(NSString *userID);
@interface HHNewFriendsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UILabel *remindLabel;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;
- (IBAction)clickAgreeBtn:(id)sender;

@property (nonatomic , strong) NSDictionary * dataDict;
@property (nonatomic , copy) agreeNewFriends  agreeNewFriends;

@end

NS_ASSUME_NONNULL_END
