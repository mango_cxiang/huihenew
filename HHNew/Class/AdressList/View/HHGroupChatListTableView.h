//
//  HHGroupChatListTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^clickHHGroupChatListTableView)(NSIndexPath *indexPath);

@interface HHGroupChatListTableView : UITableView

@property (nonatomic, copy) clickHHGroupChatListTableView clickHHGroupChatListTableView;
@property (strong, nonatomic) NSMutableArray *dataArry;

@end

NS_ASSUME_NONNULL_END
