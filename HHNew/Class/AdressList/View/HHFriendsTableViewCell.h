//
//  HHAddNewFriendsTableViewCell.h
//  HHNew
//
//  Created by Liubin on 2020/4/23.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HHContactsModel;
NS_ASSUME_NONNULL_BEGIN

@interface HHFriendsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIImageView *nextImgaeView;

//数据
@property (nonatomic , strong) NSDictionary * dataDict;

@property (nonatomic , strong) HHContactsModel * contactsModel;

@end

NS_ASSUME_NONNULL_END
