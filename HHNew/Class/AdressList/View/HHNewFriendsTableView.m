//
//  HHNewFriendsTableView.m
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHNewFriendsTableView.h"
#import "HHNewFriendsTableViewCell.h"

@interface HHNewFriendsTableView ()<UITableViewDelegate,UITableViewDataSource>

@end
@implementation HHNewFriendsTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.delegate = self;
        self.dataSource = self;
        self.backgroundColor = ColorManage.grayBackgroundColor;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
    }
    return self;
}

- (void)setdataArry:(NSMutableArray *)dataArry{
    _dataArry = dataArry;
    [self reloadData];
}

#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellName = @"HHNewFriendsTableViewCell";
    HHNewFriendsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if(!cell) {
           cell = LoadCell(@"HHNewFriendsTableViewCell");
       }
    if(indexPath.section == 0){
        if (indexPath.row == 0) {
            cell.dataDict = @{@"icon" : @"qunliao" , @"name" :@"吃鸡战场",  @"desc" :@"11111",@"status":@"1"};
        } else if (indexPath.row == 1) {
            cell.dataDict = @{@"icon" : @"qunliao" , @"name" :@"LOL", @"desc" :@"0000000",@"status":@"0"};
        }
    }
    else{
        if (indexPath.row == 0) {
            cell.dataDict = @{@"icon" : @"qunliao" , @"name" :@"英雄联盟",  @"desc" :@"123456789",@"status":@"1"};
        } else if (indexPath.row == 1) {
            cell.dataDict = @{@"icon" : @"qunliao" , @"name" :@"王者荣耀", @"desc" :@"987654321",@"status":@"2"};
        }
        else if (indexPath.row == 2) {
            cell.dataDict = @{@"icon" : @"qunliao" , @"name" :@"王者荣耀", @"desc" :@"987654321",@"status":@"1"};
        }
    }
    WeakSelf(self)
    cell.agreeNewFriends = ^(NSString * _Nonnull userID) {
        StrongSelf(weakself)
        if (strongweakself.agressNewFriends) {
            strongweakself.agressNewFriends(userID);
        }
    };
    cell.backgroundColor = ColorManage.darkBackgroundColor;
    cell.selectedBackgroundView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.clickNewFriendsTableViewCell) {
        self.clickNewFriendsTableViewCell(indexPath);
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.newFriendsTableViewDidScroll) {
        self.newFriendsTableViewDidScroll(scrollView);
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    } else {
        return 3;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"删除";
}

//TODO:删除成员
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
   
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if(section==0){
        return [self createHeaderView:@"最近三天"];
    }
    return [self createHeaderView:@"三天前"];
}

- (UIView *)createHeaderView:(NSString *)title{
    UIView *header = [[UIView alloc] init];
    header.backgroundColor = ColorManage.grayBackgroundColor;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 200, 30)];
    label.text = title;
    label.textColor = ColorManage.grayTextColor_Deault;
    label.font = [UIFont systemFontOfSize:14];
    [header addSubview:label];
    return header;
}
@end
