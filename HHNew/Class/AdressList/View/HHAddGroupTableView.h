//
//  HHAddGroupTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^clickAddGroupMemberTableViewCell)(NSIndexPath *indexPath);

@interface HHAddGroupTableView : UITableView
@property (strong, nonatomic) NSMutableArray *dataListArry;
@property (copy, nonatomic) clickAddGroupMemberTableViewCell clickAddGroupMemberTableViewCell;

@end

NS_ASSUME_NONNULL_END
