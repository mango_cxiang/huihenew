//
//  HHAddNewFriendsTableView.m
//  HHNew
//
//  Created by Liubin on 2020/4/23.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHAddNewFriendsTableView.h"
#import "HHFriendsTableViewCell.h"
#import "HHAddFiendsModel.h"
#import "HHAddFriendsTopTableViewCell.h"
@interface HHAddNewFriendsTableView ()<UITableViewDelegate,UITableViewDataSource>

@end
@implementation HHAddNewFriendsTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.delegate = self;
        self.dataSource = self;
//        self.backgroundColor = ColorManage.grayBackgroundColor;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
    }
    return self;
}

- (void)setdataArry:(NSMutableArray *)dataArry{
    _dataArry = dataArry;
    [self reloadData];
}

#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 68;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellName = @"";
    if (indexPath.row == 0) {
        cellName = @"HHAddFriendsTopTableViewCell";
    }
    else {
        cellName = @"HHFriendsTableViewCell";
    }
   
    if ([cellName isEqualToString:HHAddFriendsTopTableViewCell.className]) {
        HHAddFriendsTopTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
        if(!cell) {
            cell = LoadCell(@"HHAddFriendsTopTableViewCell");
        }
        cell.dataDict = @{@"icon" : @"new_friends" , @"chatID" :@"ma1992"};
        //展示二维码
        cell.showQrCode = ^{
           
        };
        cell.backgroundColor = ColorManage.darkBackgroundColor;
        return cell;
    }
    HHFriendsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if(!cell) {
           cell = LoadCell(@"HHFriendsTableViewCell");
       }
    if (indexPath.row == 1) {
        cell.dataDict = @{@"icon" : @"saoyisao" , @"title" :@"扫一扫",  @"desc" :@"扫描二维码名片"};
    } else if (indexPath.row == 2) {
        cell.dataDict = @{@"icon" : @"phone" , @"title" :@"手机联系人", @"desc" :@"添加通讯录中的联系人"};
    }
    cell.selectedBackgroundView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        return;
    }
    if (self.clickAddNewFriendsTableView) {
        self.clickAddNewFriendsTableView(indexPath);
    }
}


@end
