//
//  HHAddressTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/22.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^clickAddressTableView)(NSIndexPath *indexPath);
typedef void(^addressTableViewWillBeginDragging)(UIScrollView *scrollView);
typedef void(^addressTableViewDidEndDragging)(void);
typedef void(^addressTableViewDidScroll)(UIScrollView *scrollView);

@interface HHAddressTableView : UITableView

@property (nonatomic, copy) clickAddressTableView clickAddressTableView;
@property (nonatomic, copy) addressTableViewWillBeginDragging addressTableViewWillBeginDragging;
@property (nonatomic, copy) addressTableViewDidEndDragging addressTableViewDidEndDragging;
@property (nonatomic, copy) addressTableViewDidScroll addressTableViewDidScroll;

@property (strong, nonatomic) NSMutableArray *dataListArry;
@property (nonatomic,strong) UILabel * footerView;

@end

NS_ASSUME_NONNULL_END
