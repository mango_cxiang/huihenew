//
//  HHAddressSearchResultTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/23.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^clickAddressSearchResultTableView)(NSIndexPath *indexPath);

@interface HHAddressSearchResultTableView : UITableView

@property (nonatomic, copy) clickAddressSearchResultTableView clickAddressSearchResultTableView;
@property (strong, nonatomic) NSMutableArray *dataListArry;
@property (copy, nonatomic) NSString *inputWords;

@end

NS_ASSUME_NONNULL_END
