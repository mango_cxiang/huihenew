//
//  HHAddFriendsSearchResultTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^clickAddFriendsSearchResultTableViewCell)(NSIndexPath *indexPath);

@interface HHAddFriendsSearchResultTableView : UITableView
@property (nonatomic, copy) clickAddFriendsSearchResultTableViewCell clickAddFriendsSearchResultTableViewCell;
@property (strong, nonatomic) NSMutableArray *dataArry;
@end

NS_ASSUME_NONNULL_END
