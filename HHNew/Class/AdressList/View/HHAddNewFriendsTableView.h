//
//  HHAddNewFriendsTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/23.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^clickAddNewFriendsTableView)(NSIndexPath *indexPath);
typedef void(^showMyQrCode)(void);
@interface HHAddNewFriendsTableView : UITableView
@property (nonatomic, copy) showMyQrCode showMyQrCode;

@property (nonatomic, copy) clickAddNewFriendsTableView clickAddNewFriendsTableView;
@property (strong, nonatomic) NSMutableArray *dataArry;


@end

NS_ASSUME_NONNULL_END
