//
//  HHNewFriendsTableViewCell.m
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHNewFriendsTableViewCell.h"

@implementation HHNewFriendsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = ColorManage.background_color;
    self.headImgView.layer.masksToBounds = YES;
    self.headImgView.layer.cornerRadius = 5;
    self.agreeBtn.layer.masksToBounds = YES;
    self.agreeBtn.layer.cornerRadius = 5;
    self.nameLabel.font = kRegularFont(17);
    self.nameLabel.textColor = ColorManage.text_color;
    self.descLabel.font = kRegularFont(14);
    self.descLabel.textColor = ColorManage.grayTextColor_Deault;
    self.remindLabel.font = kRegularFont(14);
    self.remindLabel.textColor = ColorManage.grayTextColor_Deault;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setDataDict:(NSDictionary *)dataDict{
     _dataDict = dataDict;
    NSString *headUrl = dataDict[@"icon"];
    if([headUrl hasPrefix:@"http"]){
    [self.headImgView sd_setImageWithURL:[NSURL URLWithString:headUrl] placeholderImage:nil options:SDWebImageRefreshCached];
    }else{
      self.headImgView.image = [UIImage imageNamed:headUrl];
    }
    self.nameLabel.text = [NSString stringWithFormat:@"%@", dataDict[@"name"]];
    self.descLabel.text = [NSString stringWithFormat:@"%@", dataDict[@"desc"]];
    NSString * str = dataDict[@"status"];
    if ([str isEqualToString:@"1"]) {
        self.remindLabel.text = @"已添加";
    }
    if ([str isEqualToString:@"2"]) {
        self.remindLabel.text = @"已过期";
    }
    if ([str isEqualToString:@"0"]) {
        self.remindLabel.hidden = YES;
    }
    else{
        self.agreeBtn.hidden = YES;
    }
}

- (IBAction)clickAgreeBtn:(id)sender {
    if (self.agreeNewFriends) {
        self.agreeNewFriends(@"");
    }
}
@end
