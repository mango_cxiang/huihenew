//
//  HHGroupChatListTableView.m
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupChatListTableView.h"
#import "HMJGroupMembersTableViewCell.h"
@interface HHGroupChatListTableView ()<UITableViewDelegate,UITableViewDataSource>

@end
@implementation HHGroupChatListTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.delegate = self;
        self.dataSource = self;
        self.backgroundColor = ColorManage.grayBackgroundColor;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
    }
    return self;
}

- (void)setdataArry:(NSMutableArray *)dataArry{
    _dataArry = dataArry;
    [self reloadData];
}

#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
    return self.dataArry.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellName = @"HMJGroupMembersTableViewCell";
    HMJGroupMembersTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if(!cell) {
           cell = LoadCell(@"HMJGroupMembersTableViewCell");
       }
     if (indexPath.row == 0) {
        cell.dataDict = @{@"icon" : @"new_friends" , @"name" :@"美食群"};
    } else if (indexPath.row == 1) {
        cell.dataDict = @{@"icon" : @"qunliao" , @"name" :@"无聊透顶"};
    }
    cell.backgroundColor = ColorManage.darkBackgroundColor;
    cell.selectedBackgroundView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        return;
    }
    if (self.clickHHGroupChatListTableView) {
        self.clickHHGroupChatListTableView(indexPath);
    }
}
@end
