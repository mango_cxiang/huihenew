//
//  HHNewFriendsTableView.h
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^clickNewFriendsTableViewCell)(NSIndexPath *indexPath);
typedef void(^agressNewFriends)(NSString *userID);

typedef void(^newFriendsTableViewDidScroll)(UIScrollView *scrollView);


@interface HHNewFriendsTableView : UITableView
@property (nonatomic, copy) clickNewFriendsTableViewCell clickNewFriendsTableViewCell;
@property (nonatomic, copy) newFriendsTableViewDidScroll newFriendsTableViewDidScroll;
@property (nonatomic, copy) agressNewFriends agressNewFriends;

@property (strong, nonatomic) NSMutableArray *dataArry;
@end

NS_ASSUME_NONNULL_END
