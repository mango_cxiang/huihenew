//
//  HHAddFriendsSearchResultTableViewCell.m
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHAddFriendsSearchResultTableViewCell.h"
#import "HHContactsModel.h"

@implementation HHAddFriendsSearchResultTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.iconImgView.layer.masksToBounds = YES;
    self.iconImgView.layer.cornerRadius = 5;
    self.lineView.backgroundColor = ColorManage.lineViewColor;
}

- (void)setDataDict:(NSDictionary *)dataDict
{
    _dataDict = dataDict;
    NSString *headUrl = dataDict[@"icon"];
    if([headUrl hasPrefix:@"http"]){
    [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:headUrl] placeholderImage:nil options:SDWebImageRefreshCached];
    }else{
      self.iconImgView.image = [UIImage imageNamed:headUrl];
    }
    self.titleLabel.text = dataDict[@"title"];
    
    NSString *str = [NSString stringWithFormat:@"手机号：%@",dataDict[@"desc"]];
    self.descLabel.attributedText = [self updateAttributedText:str prefixStr:@"手机号："];
}

- (void)setContactsModel:(HHContactsModel *)contactsModel{
    _contactsModel = contactsModel;
    NSString *headUrl = _contactsModel.headUrl;
    if([headUrl hasPrefix:@"http"]){
    [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:headUrl] placeholderImage:nil options:SDWebImageRefreshCached];
    }else{
      self.iconImgView.image = [UIImage imageNamed:headUrl];
    }
//    self.titleLabel.text = _contactsModel.markName.length>0?_contactsModel.markName:_contactsModel.nickName;
    NSString *str = [NSString stringWithFormat:@"昵称：%@",_contactsModel.nickName];
    self.descLabel.attributedText = [self updateAttributedText:str prefixStr:@"昵称："];
}


- (NSMutableAttributedString *)updateAttributedText:(NSString *)contentStr prefixStr:(NSString *)prefixStr{
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:contentStr attributes: @{NSFontAttributeName: kRegularFont(14),NSForegroundColorAttributeName: RGB_GrayColor_Text}];
    [string addAttributes:@{NSFontAttributeName:kRegularFont(14) , NSForegroundColorAttributeName: RGB_GreenColor_Text} range:NSMakeRange(prefixStr.length, contentStr.length-prefixStr.length)];
    return string;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateTitleLabelWithKeywordMode:(NSString *)keywordMode hintStr:(NSString *)hintStr{
    if (keywordMode) {
        NSString *string = keywordMode;
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.f],NSForegroundColorAttributeName:ColorManage.text_color}];
        
        if (hintStr) {
            NSRange range = [string.lowercaseString rangeOfString:hintStr.lowercaseString];
            if (range.location != NSNotFound) {
                UIColor *tintColor =  RGB_GreenColor;
                [attString setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.f],NSForegroundColorAttributeName:tintColor} range:range];
            }
        }
        self.titleLabel.attributedText = attString;
    }
}
@end
