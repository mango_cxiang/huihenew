//
//  HHAddFriendsSearchResultTopView.m
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHAddFriendsSearchResultTopView.h"

@implementation HHAddFriendsSearchResultTopView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        //        [self loadView];
        self =  [[[NSBundle mainBundle]loadNibNamed:@"HHAddFriendsSearchResultTopView" owner:self options:nil] lastObject];
        self.frame = frame;
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toSearch)]];
    }
    return self;
}

- (void)setSearchWords:(NSString *)searchWords{
    _searchWords = searchWords;
    self.phoneNumberLabel.text = searchWords;
    self.iconImgView.layer.masksToBounds = YES;
    self.iconImgView.layer.cornerRadius = 5;
}

- (void)toSearch{
    if (self.toSearchNewFriends) {
        self.toSearchNewFriends(_searchWords);
    }
}

- (void)awakeFromNib{
    [super awakeFromNib];
//    gl.frame = self.bounds;
//    [self.bgView.layer insertSublayer:gl atIndex:0];
    self.searchTextLabel.font = kMediumFont(17);
    self.searchTextLabel.textColor = ColorManage.text_color;
    self.phoneNumberLabel.textColor = ColorManage.greenTextColor;

}

@end
