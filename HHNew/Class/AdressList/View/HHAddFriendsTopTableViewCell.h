//
//  HHAddFriendsTopTableViewCell.h
//  HHNew
//
//  Created by Liubin on 2020/4/23.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^showQrCode)(void);
@interface HHAddFriendsTopTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *chatIDLabel;
@property (weak, nonatomic) IBOutlet UIButton *qrCodeBtn;
- (IBAction)clickQrCodeBtn:(id)sender;
//数据
@property (nonatomic , strong) NSDictionary * dataDict;
@property (nonatomic , copy) showQrCode showQrCode;

@end

NS_ASSUME_NONNULL_END
