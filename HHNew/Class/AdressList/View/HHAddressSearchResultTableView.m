//
//  HHAddressSearchResultTableView.m
//  HHNew
//
//  Created by Liubin on 2020/4/23.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHAddressSearchResultTableView.h"
#import "HHContactsModel.h"
#import "HMJGroupMembersTableViewCell.h"

@interface HHAddressSearchResultTableView ()<UITableViewDelegate,UITableViewDataSource>

@end
@implementation HHAddressSearchResultTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.delegate = self;
        self.dataSource = self;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
    }
    return self;
}

- (void)setDataListArry:(NSMutableArray *)dataListArry{
    _dataListArry = dataListArry;
    [self reloadData];
}

#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataListArry.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellName = @"HHAdressSearchResultGroupMembersTableViewCell";
    HMJGroupMembersTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if(!cell) {
        cell = LoadCell(@"HMJGroupMembersTableViewCell");
    }
    cell.indexPath = indexPath;
    HHContactsModel *model = self.dataListArry[indexPath.row];
    cell.dataDict = @{@"icon" : @"group" , @"name":model.nickName};
    
    [cell updateWithKeywordMode:model.nickName hintStr:self.inputWords];
    cell.backgroundColor = ColorManage.darkBackgroundColor;
    cell.selectedBackgroundView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.clickAddressSearchResultTableView) {
        self.clickAddressSearchResultTableView(indexPath);
    }
}

@end
