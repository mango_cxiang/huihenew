//
//  HHAddGroupView.h
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^addGroupMembers)(void);

@interface HHAddGroupTopView : UIView
@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupMembersLabel;
@property (weak, nonatomic) IBOutlet UITextField *groupNameTextField;
- (IBAction)addGroupMembers:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addGroupMembersBtn;
@property (weak, nonatomic) IBOutlet UIView *setNameView;

@property (nonatomic, assign) NSInteger membersCount;
@property (weak, nonatomic) IBOutlet UIView *addMembersView;

@property (nonatomic, copy) addGroupMembers addGroupMembers;

@end

NS_ASSUME_NONNULL_END
