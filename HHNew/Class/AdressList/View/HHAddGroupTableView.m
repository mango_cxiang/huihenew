//
//  HHAddGroupTableView.m
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHAddGroupTableView.h"
#import "DataHelper.h"
#import "HMJGroupMembersTableViewCell.h"
#import "HHContactsModel.h"
@interface HHAddGroupTableView ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) NSMutableArray *indexPathArray;

@end

@implementation HHAddGroupTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.delegate = self;
        self.dataSource = self;
        self.backgroundColor = ColorManage.grayBackgroundColor;
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 30, SCREEN_WIDTH,20)];
        label.textColor = ColorManage.grayTextColor_Deault;
        self.tableFooterView = label;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
    }
    return self;
}

- (void)setDataListArry:(NSMutableArray *)dataListArry{
    _dataListArry = [DataHelper getContactListDataBy:dataListArry];
    _indexPathArray = [DataHelper getContactListSectionBy:_dataListArry];
    [self reloadData];
}

#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.indexPathArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   
    return [self.dataListArry[section] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellName = @"HHAdressGroupMembersTableViewCell";
    HMJGroupMembersTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if(!cell) {
        cell = LoadCell(@"HMJGroupMembersTableViewCell");
    }
    cell.indexPath = indexPath;
    
    HHContactsModel *model = self.dataListArry[indexPath.section][indexPath.row];
    cell.dataDict = @{@"icon" : @"group" , @"name":model.nickName};
    cell.selectedBackgroundView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];
    cell.backgroundColor = ColorManage.darkBackgroundColor;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] init];
    header.backgroundColor = ColorManage.grayBackgroundColor;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 200, 30)];
    label.text = [_indexPathArray objectAtIndex:section];
    label.textColor = ColorManage.grayTextColor_Deault;
    label.font = [UIFont systemFontOfSize:14];
    [header addSubview:label];
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.clickAddGroupMemberTableViewCell) {
        self.clickAddGroupMemberTableViewCell(indexPath);
    }
}

@end
