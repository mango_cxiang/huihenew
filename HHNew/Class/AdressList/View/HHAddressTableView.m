//
//  HHAddressTableView.m
//  HHNew
//
//  Created by Liubin on 2020/4/22.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHAddressTableView.h"
#import "HMJGroupMembersTableViewCell.h"
#import "HHContactsModel.h"
#import "DataHelper.h"
#import "UILabel+HHTextSpace.h"
#import "UIImage+Ext.h"


@interface HHAddressTableView ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, assign) BOOL translucent;

@property (strong, nonatomic) NSMutableArray *indexPathArray;
@end
@implementation HHAddressTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    if (self == [super initWithFrame:frame style:style]) {
        self.frame = frame;
        self.delegate = self;
        self.dataSource = self;
        self.translucent = YES;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return self;
}

- (void)setDataListArry:(NSMutableArray *)dataListArry{
    _dataListArry = [DataHelper getContactListDataBy:dataListArry];
    _indexPathArray = [DataHelper getContactListSectionBy:_dataListArry];
    [self createUI:dataListArry.count];
    [self reloadData];
}

- (void)createUI:(NSInteger)count{
    self.footerView.frame = CGRectMake(0, 20, SCREEN_WIDTH,80);
    self.footerView.font = SystemFont(14);
    [self.footerView setTextAlignment:NSTextAlignmentCenter];
    self.footerView.textColor = ColorManage.grayTextColor_Deault;
    self.tableFooterView = self.footerView;
    self.footerView.text = [NSString stringWithFormat:@"%zi位联系人",count];
}

#pragma mark - 协议 UITableViewDataSource 和 UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.indexPathArray.count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 5;
    } else {
        return [self.dataListArry[section-1] count];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellName = @"HHAdressGroupMembersTableViewCell";
    HMJGroupMembersTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if(!cell) {
        cell = LoadCell(@"HMJGroupMembersTableViewCell");
    }
    cell.indexPath = indexPath;
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.dataDict = @{@"icon" : @"new_friends" , @"name" :@"新的朋友"};
        } else if (indexPath.row == 1) {
            cell.dataDict = @{@"icon" : @"qunliao" , @"name" :@"群聊"};
        }else if (indexPath.row==2){
            cell.dataDict = @{@"icon" : @"openGroup" , @"name" :@"公开群聊"};
        }else if (indexPath.row == 3) {
            cell.dataDict = @{@"icon" : @"group" , @"name" :@"分组"};
        } else if (indexPath.row == 4){
//            NSString *headStr = [HMJAccountModel myself].headUrl;
            cell.dataDict = @{@"icon" : @"new_friends" , @"name" :@"我" };
        }
    } else {
        HHContactsModel *model = self.dataListArry[indexPath.section-1][indexPath.row];
        cell.dataDict = @{@"icon" : @"group" , @"name":model.nickName};
    }
    cell.backgroundColor = ColorManage.darkBackgroundColor;
    cell.selectedBackgroundView = [[HHDefaultTools sharedInstance] cellSelectedHighlightBgView];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if(section==0){
        return [UIView new];
    }
    UIView *header = [[UIView alloc] init];
    header.backgroundColor = ColorManage.grayBackgroundColor;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 200, 30)];
    label.text = [_indexPathArray objectAtIndex:section-1];
    label.textColor = ColorManage.grayTextColor_Deault;
    label.font = [UIFont systemFontOfSize:14];
    [header addSubview:label];
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0.1;
    }
    return 30;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.clickAddressTableView) {
        self.clickAddressTableView(indexPath);
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (self.addressTableViewWillBeginDragging) {
        self.addressTableViewWillBeginDragging(scrollView);
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (self.addressTableViewDidEndDragging) {
        self.addressTableViewDidEndDragging();
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.addressTableViewDidScroll) {
        self.addressTableViewDidScroll(scrollView);
    }

    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY < 0) {
        for (UIView *view in self.subviews) {
            NSLog(@"子视图：%@",view);
//            view.backgroundColor = ColorManage.darkBackgroundColor;
        }
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = ColorManage.grayBackgroundColor;
    return view;
}




- (NSMutableArray *)indexPathArray{
    if (!_indexPathArray) {
        _indexPathArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _indexPathArray;
}

- (UILabel *)footerView {
    if (!_footerView) {
        _footerView = [[UILabel alloc] init];
    }
    return _footerView;
}


@end
