//
//  HHAddFriendsSearchResultTopView.h
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^toSearchNewFriends)(NSString *inputWords);
@interface HHAddFriendsSearchResultTopView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *searchTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;

@property (copy, nonatomic) NSString *searchWords;

@property (copy, nonatomic) toSearchNewFriends toSearchNewFriends;

@end

NS_ASSUME_NONNULL_END
