//
//  HMJGroupMembersTableViewCell.h
//  huihe
//
//  Created by mysun on 16/3/8.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "HMJCompaniesModel.h"
#import "HHContactsModel.h"
//#import "HMJGroupModel.h"

//@protocol HMJGroupMembersTableViewCellDelegate <NSObject>
//- (void)groupMembersTableViewCellAddFriendClicked:(HMJContactsModel *)model;
//@end
//群成员cell
@interface HMJGroupMembersTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

//头像
@property (weak, nonatomic) IBOutlet UIImageView *hearImage;
//名字
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UIView *redDot;
@property (weak, nonatomic) IBOutlet UIView *lineView;

//数据
@property (nonatomic , strong) NSDictionary * dataDict;

////公司_模型
//@property (nonatomic , strong) HMJCompaniesModel * company;
////联系人_模型
@property (nonatomic , strong) HHContactsModel * contacts;
////群_模型
//@property (nonatomic , strong) HMJGroupModel * group;

@property (nonatomic, strong) NSIndexPath *indexPath;

//@property (nonatomic, weak) id<HMJGroupMembersTableViewCellDelegate> delegate;

- (void)updateWithKeywordMode:(NSString *)keywordMode hintStr:(NSString *)hintStr;


@end
