//
//  HHAddFiendsModel.h
//  HHNew
//
//  Created by Liubin on 2020/4/23.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HHAddFiendsModel : NSObject
@property (nonatomic, copy) UIImage *icon; // 图片
@property (nonatomic, copy) NSString *title;     // 标题
@property (nonatomic, copy) NSString *descText;     //简述



@end

NS_ASSUME_NONNULL_END
