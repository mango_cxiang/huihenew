//
//  HHFriendsGroupViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupViewController.h"
#import "HHAddGroupViewController.h"

@interface HHGroupViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataArray;

@end

@implementation HHGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createUI];
}

-(void)createUI{
    [self.view addSubview:self.tableView];
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(20, 0, headerView.width - 20, headerView.height);
    [button setTitle:@"添加分组" forState:UIControlStateNormal];
    [button setTitleColor:ColorManage.loginTextColor forState:UIControlStateNormal];
    button.titleLabel.font = SystemFont(15);
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button addTarget:self action:@selector(clickAddTagButton) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:button];
    self.tableView.tableHeaderView = headerView;
}
//添加分组
- (void)clickAddTagButton{
   [self pushHHAddGroupViewController:@"0"];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%@ (%d)",@"英雄联盟小队",223];
    cell.detailTextLabel.font = kRegularFont(17);
    cell.textLabel.textColor = ColorManage.text_color;

    cell.detailTextLabel.font = kRegularFont(14);
    cell.detailTextLabel.textColor = ColorManage.grayTextColor_Deault;
    cell.detailTextLabel.text = @"蛮王、剑圣、菊花信、伊泽瑞尔";
    
    cell.backgroundColor = ColorManage.darkBackgroundColor;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 72;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self pushHHAddGroupViewController:@"1"];

}
- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    //设置删除按钮
    WeakSelf(self);
    UITableViewRowAction * deleteRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
    }];
    deleteRowAction.backgroundColor = UIColorFromRGB(0xfa3c3b);
    return @[deleteRowAction];
}


- (void)pushHHAddGroupViewController:(NSString *)groupID{
    HHAddGroupViewController *hvc = Init(HHAddGroupViewController);
    hvc.groupID = groupID;
    [self.navigationController pushViewController:hvc animated:YES];
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return _tableView;
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArray;
}

@end
