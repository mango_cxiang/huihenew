//
//  HHAddressListViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/22.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHAddressListViewController.h"
#import "HHAddressSearchResultViewController.h"
#import "SCIndexViewConfiguration.h"
#import "SCIndexView.h"
#import "HHAddressTableView.h"
#import "HHContactsModel.h"
#import "DataHelper.h"
#import "HHSearchController.h"
#import "HHAddNewFiendsViewController.h"
#import "HHGroupChatListViewController.h"
#import "HHNewFriendsViewController.h"
#import "HHGroupViewController.h"
#import "HHFriendsDetailsViewController.h"

@interface HHAddressListViewController ()<UISearchControllerDelegate,SCIndexViewDelegate>
@property (strong, nonatomic) HHSearchController *searchController;
@property (strong, nonatomic) HHAddressSearchResultViewController *searchResultVC;
@property (nonatomic, strong) HHAddressTableView *tableView;
@property (nonatomic, assign) BOOL translucent;
@property (nonatomic,strong) UIView * footerView;
@property (nonatomic, strong) SCIndexView *indexView;
@property (nonatomic, assign) SCIndexViewStyle indexViewStyle;
/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage *searchGrayImage;
/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage *searchWhiteImage;

@property (nonatomic, strong) UIView *tableHeadView;

@end

@implementation HHAddressListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"通讯录";
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    self.translucent = YES;
    self.definesPresentationContext = YES;
    if (@available(iOS 11.0, *)) {
           self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
       } else {
           self.automaticallyAdjustsScrollViewInsets = NO;
       }
    [self createUI];
    [self loadData];
    [self handleBlock];
}

- (void)createUI{
    UIButton * rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    [rightBtn setImage:[UIImage imageNamed:@"add_new_friends"] forState:UIControlStateNormal];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [rightBtn addTarget:self action:@selector(rightBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self setupRightBarButtonItem:rightBtn];
        
    self.searchController.searchResultsUpdater = self.searchResultVC;
    self.searchResultVC.searchBar = self.searchController.searchBar;
    self.searchResultVC.nav = self.navigationController;

    [self.view addSubview:self.tableView];
    self.tableView.tableHeaderView = self.tableHeadView;
    [self.tableHeadView addSubview:self.searchController.searchBar];
    [self.view addSubview:self.indexView];
}

- (void)rightBtnAction{
    HHAddNewFiendsViewController *avc = [[HHAddNewFiendsViewController alloc] init];
    [self.navigationController pushViewController:avc animated:YES];
}

- (void)loadData{
    //假数据：产生100个数字+三个随机字母
    NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:0];
    for (NSInteger i =0; i<50; i++) {
        HHContactsModel *model = [[HHContactsModel alloc] init];
        model.nickName = [NSString stringWithFormat:@"%@%ld",[[HHDefaultTools sharedInstance] shuffledAlphabet], (long)i];
        [tempArray addObject:model];
    }
    self.tableView.dataListArry = tempArray;
    self.searchResultVC.dataListArry = self.tableView.dataListArry;
    NSMutableArray* indexPathArray = [DataHelper getContactListSectionBy:self.tableView.dataListArry];

    // 配置索引数据
    if ([indexPathArray count]>0) {
        NSMutableArray *indexViewDataSource = [NSMutableArray array];
        [indexViewDataSource addObject:@"☆"];
        for (NSString *str in indexPathArray) {
            [indexViewDataSource addObject:str];
        }
        self.indexView.dataSource = indexViewDataSource.copy;
    }
}

- (void)handleBlock{
    WeakSelf(self);
    //点击通讯录cell，区分 section 和 row 处理
    self.tableView.clickAddressTableView = ^(NSIndexPath * _Nonnull indexPath) {
        if (indexPath.section == 0) {
            switch (indexPath.row) {
                case 0://新的朋友
                {
                    HHNewFriendsViewController *hvc = Init(HHNewFriendsViewController);
                    [weakself.navigationController pushViewController:hvc animated:YES];
                }
                    break;
                case 1://群聊
                {
                    HHGroupChatListViewController *hvc = Init(HHGroupChatListViewController);
                    [weakself.navigationController pushViewController:hvc animated:YES];
                }
                    break;
                case 2://公开群聊
                {
                    
                }
                    break;
                case 3://分组
                {
                    HHGroupViewController *hvc = Init(HHGroupViewController);
                    [weakself.navigationController pushViewController:hvc animated:YES];
                }
                    break;
                case 4://我
                {
                    
                }
                    break;
                default:
                    break;
            }
        }
        else{
            HHFriendsDetailsViewController *dvc = Init(HHFriendsDetailsViewController);
            [weakself.navigationController pushViewController:dvc animated:YES];
        }
    };
}


#pragma mark - SCIndexViewDelegate
- (void)indexView:(SCIndexView *)indexView didSelectAtSection:(NSUInteger)section{
}


- (void)willPresentSearchController:(UISearchController *)searchController{
    // 4.设置搜索框图标的偏移
    [self.searchController.searchBar setPositionAdjustment:UIOffsetMake(0, 0) forSearchBarIcon:UISearchBarIconSearch];
}

- (void)willDismissSearchController:(UISearchController *)searchController{
    [self.searchResultVC clearResultData];
    // 4.设置搜索框图标的偏移
    CGFloat offsetX = (SCREEN_WIDTH -60 - 32) / 2;
    [_searchController.searchBar setPositionAdjustment:UIOffsetMake(offsetX, 0) forSearchBarIcon:UISearchBarIconSearch];
}

- (void)didDismissSearchController:(UISearchController *)searchController{
}

- (HHSearchController *)searchController {
    if (!_searchController) {
        _searchController = [[HHSearchController alloc]initWithSearchResultsController:self.searchResultVC];
        _searchController.delegate = self;
       _searchController.searchBar.backgroundImage = [HHDefaultTools sharedInstance].searchBarGrayImage;
        [_searchController.searchBar setSearchFieldBackgroundImage:[HHDefaultTools sharedInstance].searchBarTextFieldImage forState:UIControlStateNormal];
        _searchController.hidesNavigationBarDuringPresentation = YES;
        _searchController.dimsBackgroundDuringPresentation = YES;
        _searchController.searchBar.placeholder =  @"搜索";
//        // 4.设置搜索框图标的偏移
        CGFloat offsetX = (SCREEN_WIDTH -60 - 32) / 2;
        [_searchController.searchBar setPositionAdjustment:UIOffsetMake(offsetX, 0) forSearchBarIcon:UISearchBarIconSearch];
    }
    return _searchController;
}


- (HHAddressTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHAddressTableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (HHAddressSearchResultViewController *)searchResultVC{
    if (!_searchResultVC) {
        _searchResultVC = [[HHAddressSearchResultViewController alloc]init];
    }
    return _searchResultVC;
}

- (SCIndexView *)indexView{
    if (!_indexView) {
        _indexView = [[SCIndexView alloc] initWithTableView:self.tableView configuration:[SCIndexViewConfiguration configurationWithIndexViewStyle:self.indexViewStyle]];
        _indexView.translucentForTableViewInNavigationBar = self.translucent;
        _indexView.delegate = self;
    }
    return _indexView;
}

- (UIView *)tableHeadView{
    if (!_tableHeadView) {
        _tableHeadView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,HHSearchControllerSearchBarHeight)];
        _tableHeadView.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return _tableHeadView;
}



@end
