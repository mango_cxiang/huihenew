//
//  HHAddNewFiendsViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/23.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHAddNewFiendsViewController.h"
#import "HHNewFiendsSearchResultViewController.h"
#import "HHAddFiendsModel.h"
#import "HHAddNewFriendsTableView.h"
@interface HHAddNewFiendsViewController ()<UISearchControllerDelegate>
@property (strong, nonatomic) HHSearchController *searchController;
@property (strong, nonatomic) HHNewFiendsSearchResultViewController *searchResultVC;
@property (strong, nonatomic) HHAddNewFriendsTableView *tableView;
@property (strong, nonatomic) UIView *tableHeadView;

/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage *searchGrayImage;
/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage *searchWhiteImage;
@end

@implementation HHAddNewFiendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    self.title = @"添加联系人";
    // Do any additional setup after loading the view.
    self.definesPresentationContext = YES;
    self.searchController.searchResultsUpdater = self.searchResultVC;
    self.searchResultVC.searchBar = self.searchController.searchBar;
    self.searchResultVC.nav = self.navigationController;

    [self createUI];
    [self handleBlock];
}

- (void)createUI{
    [self.view addSubview:self.tableView];
    self.tableView.tableHeaderView = self.tableHeadView;
    [self.tableHeadView addSubview:self.searchController.searchBar];
}

- (void)handleBlock{
    //点击 cell
    self.tableView.clickAddNewFriendsTableView = ^(NSIndexPath * _Nonnull indexPath) {
        
    };
}

#pragma mark - UISearchControllerDelegate
- (void)willPresentSearchController:(UISearchController *)searchController{
    // 4.设置搜索框图标的偏移
    [_searchController.searchBar setPositionAdjustment:UIOffsetMake(0, 0) forSearchBarIcon:UISearchBarIconSearch];
}

- (void)willDismissSearchController:(UISearchController *)searchController{
    [self.searchResultVC clearResultData];
    // 4.设置搜索框图标的偏移
    CGFloat offsetX = (SCREEN_WIDTH -130 - 32) / 2;
    [_searchController.searchBar setPositionAdjustment:UIOffsetMake(offsetX, 0) forSearchBarIcon:UISearchBarIconSearch];
}

- (void)didDismissSearchController:(UISearchController *)searchController{
}

- (HHSearchController *)searchController {
    if (!_searchController) {
        _searchController = [[HHSearchController alloc]initWithSearchResultsController:self.searchResultVC];
        _searchController.delegate = self;
        _searchController.searchBar.backgroundImage = [HHDefaultTools sharedInstance].searchBarGrayImage;
        [_searchController.searchBar setSearchFieldBackgroundImage:[HHDefaultTools sharedInstance].searchBarTextFieldImage forState:UIControlStateNormal];
        _searchController.searchBar.placeholder =  @"会合号/手机号";
//        // 4.设置搜索框图标的偏移
        CGFloat offsetX = (SCREEN_WIDTH -130 - 32) / 2;
        [_searchController.searchBar setPositionAdjustment:UIOffsetMake(offsetX, 0) forSearchBarIcon:UISearchBarIconSearch];
    }
    return _searchController;
}


- (HHAddNewFriendsTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHAddNewFriendsTableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ColorManage.darkBackgroundColor;
    }
    return _tableView;
}

- (HHNewFiendsSearchResultViewController *)searchResultVC{
    if (!_searchResultVC) {
        _searchResultVC = [[HHNewFiendsSearchResultViewController alloc]init];
        _searchResultVC.view.backgroundColor = ColorManage.darkBackgroundColor;
    }
    return _searchResultVC;
}

- (UIView *)tableHeadView{
    if (!_tableHeadView) {
        _tableHeadView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,HHSearchControllerSearchBarHeight)];
        _tableHeadView.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return _tableHeadView;
}

@end
