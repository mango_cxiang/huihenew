//
//  HHAddressSearchResultViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/22.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHAddressSearchResultViewController.h"
#import "HHContactsModel.h"
#import "NSString+Capitallize.h"
#import "HHAddressSearchResultTableView.h"
#import "HHFriendsDetailsViewController.h"
@interface HHAddressSearchResultViewController ()
@property (nonatomic, strong) HHAddressSearchResultTableView *tableView;
@property (nonatomic,strong)NSMutableArray * searchResultArray;

@end

@implementation HHAddressSearchResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    self.definesPresentationContext = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.view addSubview:self.tableView];
    [self handleBlock];
}

- (void)handleBlock{
    WeakSelf(self)
    //点击搜索结果 cell
    self.tableView.clickAddressSearchResultTableView = ^(NSIndexPath * _Nonnull indexPath) {
        HHFriendsDetailsViewController *dvc = Init(HHFriendsDetailsViewController);
        [weakself.nav pushViewController:dvc animated:YES];
    };
}

//清楚数据
- (void)clearResultData{
    self.tableView.inputWords = @"";
    [self.searchResultArray removeAllObjects];
    [self.tableView.dataListArry removeAllObjects];
    [self.tableView reloadData];
}

#pragma mark - UISearchResultsUpdating
//每输入一个字符都会执行一次
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    searchController.searchResultsController.view.hidden = NO;
    if (searchController.searchBar.searchTextField.text.length <= 0) {
        return;
    }
    NSString *inputWord = [searchController.searchBar.searchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];;
    if (!inputWord.length){
        searchController.searchBar.searchTextField.text = @"";
        return;
    }
    //取消执行 selector
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(automaticCompletionWithKeyword:) object:inputWord];
    //获取是否有高亮
    UITextRange *selectedRange = [searchController.searchBar.searchTextField markedTextRange];
    if(!selectedRange){
        [self automaticCompletionWithKeyword:inputWord];
    }
}

#pragma mark - 自动补全
- (void)automaticCompletionWithKeyword:(NSString *)inputWord {
    [self.searchResultArray removeAllObjects];
    NSMutableArray *tempResults = [NSMutableArray array];
    NSMutableArray *contactsSource = [NSMutableArray arrayWithArray:self.dataListArry];
    NSUInteger searchOptions = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch;

    for (NSArray * array in contactsSource) {
          for (HHContactsModel * model in array) {
              [tempResults addObject:model];
          }
      }
    for (int i = 0; i < tempResults.count; i++) {
        NSString *storeString = [[(HHContactsModel *)tempResults[i] nickName] pinyinOfName];
        NSString *wordString = [inputWord pinyinOfName];
        NSRange storeRange = NSMakeRange(0, storeString.length);
        if (storeString.length < storeRange.length) {
               continue;
           }
           NSRange foundRange = [storeString rangeOfString:wordString options:searchOptions range:storeRange];
           if (foundRange.length) {
               [self.searchResultArray addObject:tempResults[i]];
           }
       }
    //刷新 tableView
    self.tableView.inputWords = inputWord;
    self.tableView.dataListArry = self.searchResultArray;
    [self.tableView reloadData];
}


- (HHAddressSearchResultTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHAddressSearchResultTableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-SafeAreaTopHeight-SafeAreaBottomHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    }
    return _tableView;
}


- (NSMutableArray *)searchResultArray{
    if (!_searchResultArray) {
        _searchResultArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _searchResultArray;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.searchBar.searchTextField resignFirstResponder];
}

@end
