//
//  HHAddGroupViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHAddGroupViewController.h"
#import "HHAddGroupTableView.h"
#import "HHAddGroupTopView.h"
#import "HHContactsModel.h"
#import "HHFriendsDetailsViewController.h"
#import "HHSelectMembersListViewController.h"
@interface HHAddGroupViewController ()
@property (nonatomic, strong) HHAddGroupTableView *tableView;
@property (nonatomic, strong) HHAddGroupTopView *addGroupView;
@property (nonatomic, strong) NSMutableArray *membersArray;

@end

@implementation HHAddGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"添加分组";
    self.view.backgroundColor = ColorManage.nav_color;
    
    self.definesPresentationContext = YES;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
       
    UIButton * rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [rightBtn setTitleColor:ColorManage.text_color forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightBtnAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightButton;
            
    [self.view addSubview:self.tableView];
//    [self loadData];
    [self createUI];
    [self handleBlock];
}

- (void)rightBtnAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setGroupID:(NSString *)groupID{
    _groupID = groupID;
    if ([groupID integerValue] <= 0) {
        return;
    }
    [self loadData];
}

- (void)loadData{
    //假数据：产生100个数字+三个随机字母
    NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:0];
    for (NSInteger i =0; i<10; i++) {
        HHContactsModel *model = [[HHContactsModel alloc] init];
        model.nickName = [NSString stringWithFormat:@"%@%ld",[[HHDefaultTools sharedInstance] shuffledAlphabet], (long)i];
        [tempArray addObject:model];
    }
    self.membersArray = tempArray;
    self.tableView.dataListArry = tempArray;
    self.addGroupView.membersCount = tempArray.count;
    self.addGroupView.groupNameTextField.text = @"英雄联盟";
}

- (void)createUI{
    [self.view addSubview:self.tableView];
    self.tableView.tableHeaderView = self.addGroupView;
}

- (void)handleBlock{
    WeakSelf(self);
    self.tableView.clickAddGroupMemberTableViewCell = ^(NSIndexPath * _Nonnull indexPath) {
        HHFriendsDetailsViewController *dvc = Init(HHFriendsDetailsViewController);
        [weakself.navigationController pushViewController:dvc animated:YES];
    };
    //添加成员
    self.addGroupView.addGroupMembers = ^{
        HHSelectMembersListViewController * creatGroupViewController = [[HHSelectMembersListViewController alloc]init];
        creatGroupViewController.addMembers = YES;
        creatGroupViewController.finishSelectNewGroupMembers = ^(NSMutableArray * _Nonnull array,BOOL add) {
            if (!add) {
                return ;
            }
            [weakself.membersArray addObjectsFromArray:array];
            weakself.tableView.dataListArry = weakself.membersArray;
        };
        [weakself.navigationController pushViewController:creatGroupViewController animated:YES];
    };
}

- (HHAddGroupTopView *)addGroupView{
    if (!_addGroupView) {
        _addGroupView = [[HHAddGroupTopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 170)];
    }
    return _addGroupView;
}

- (HHAddGroupTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHAddGroupTableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    }
    return _tableView;
}

- (NSMutableArray *)membersArray{
    if (!_membersArray) {
        _membersArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _membersArray;
}



@end
