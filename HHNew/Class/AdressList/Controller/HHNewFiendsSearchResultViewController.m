//
//  HHNewFiendsSearchResultViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/23.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHNewFiendsSearchResultViewController.h"
#import "HHAddFriendsSearchResultTableView.h"
#import "HHAddFriendsSearchResultTopView.h"
#import "HHFriendsDetailsViewController.h"

@interface HHNewFiendsSearchResultViewController ()
@property (nonatomic, strong) HHAddFriendsSearchResultTableView *tableView;
@property (nonatomic, strong) HHAddFriendsSearchResultTopView *topView;
@property (nonatomic,strong)NSMutableArray * searchResultArray;
@end

@implementation HHNewFiendsSearchResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    self.definesPresentationContext = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self createUI];
    [self handleBlock];
}

- (void)createUI{
    self.tableView.hidden = YES;
    self.tableView.tableHeaderView = self.topView;
    [self.view addSubview:self.tableView];
}

- (void)handleBlock{
    WeakSelf(self)
    self.topView.toSearchNewFriends = ^(NSString * _Nonnull inputWords) {
        
    };
    self.tableView.clickAddFriendsSearchResultTableViewCell = ^(NSIndexPath * _Nonnull indexPath) {
        HHFriendsDetailsViewController *dvc = Init(HHFriendsDetailsViewController);
        [weakself.nav pushViewController:dvc animated:YES];
    };
}

#pragma mark - UISearchResultsUpdating
//每输入一个字符都会执行一次
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    searchController.searchResultsController.view.hidden = NO;
    if (searchController.searchBar.searchTextField.text.length <= 0) {
        [self clearResultData];
        return;
    }
    self.tableView.hidden = NO;
    NSString *inputWord = [searchController.searchBar.searchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];;
    if (!inputWord.length){
        searchController.searchBar.searchTextField.text = @"";
        return;
    }
    //取消执行 selector
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(automaticCompletionWithKeyword:) object:inputWord];
    //获取是否有高亮
    UITextRange *selectedRange = [searchController.searchBar.searchTextField markedTextRange];
    if(!selectedRange){
        [self automaticCompletionWithKeyword:inputWord];
    }
}

//根据搜索内容，刷新数据
- (void)automaticCompletionWithKeyword:(NSString *)inputWord{
    self.topView.searchWords = inputWord;
}

//清楚数据
- (void)clearResultData{
    self.topView.searchWords = @"";
    self.tableView.hidden = YES;
    [self.dataListArry removeAllObjects];
    [self.tableView reloadData];
}

- (HHAddFriendsSearchResultTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHAddFriendsSearchResultTableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-SafeAreaTopHeight-SafeAreaBottomHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    }
    return _tableView;
}

- (HHAddFriendsSearchResultTopView *)topView{
    if (!_topView) {
        _topView = [[HHAddFriendsSearchResultTopView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 70)];
        _topView.backgroundColor = ColorManage.darkBackgroundColor;
    }
    return _topView;
}

- (NSMutableArray *)searchResultArray{
    if (!_searchResultArray) {
        _searchResultArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _searchResultArray;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.searchBar.searchTextField resignFirstResponder];
}

@end
