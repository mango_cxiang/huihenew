//
//  HHNewFriendsViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHNewFriendsViewController.h"
#import "HHSearchController.h"
#import "HHNewFriendsTableView.h"
#import "HHNewFiendsSearchResultViewController.h"
#import "HHFriendsDetailsViewController.h"

@interface HHNewFriendsViewController ()<UISearchControllerDelegate>
/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage *searchGrayImage;
/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage *searchWhiteImage;
@property (nonatomic, strong) UIButton *scanBtn;
@property (nonatomic, strong) HHNewFriendsTableView *tableView;

@property (strong, nonatomic) HHSearchController *searchController;
@property (strong, nonatomic) HHNewFiendsSearchResultViewController *searchResultVC;
@property (strong, nonatomic) UIView *tableHeadView;

@end

@implementation HHNewFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"新的朋友";
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    self.definesPresentationContext = YES;
    [self.view addSubview:self.tableView];
            
    self.tableView.tableHeaderView = self.tableHeadView;
    [self.tableHeadView addSubview:self.searchController.searchBar];

    self.scanBtn.frame = CGRectMake(SCREEN_WIDTH - 40, 10, 20, 18);
    self.scanBtn.centerY = self.tableHeadView.centerY;
    [self.tableHeadView addSubview:self.scanBtn];
    
    self.searchController.searchResultsUpdater = self.searchResultVC;
    self.searchResultVC.searchBar = self.searchController.searchBar;
    self.searchResultVC.nav = self.navigationController;
    [self handleBlock];
}

- (void)handleBlock{
    WeakSelf(self)
    //点击cell
    self.tableView.clickNewFriendsTableViewCell = ^(NSIndexPath * _Nonnull indexPath) {
        HHFriendsDetailsViewController *dvc = Init(HHFriendsDetailsViewController);
        [weakself.navigationController pushViewController:dvc animated:YES];
    };
    //点击同意好友
    self.tableView.agressNewFriends = ^(NSString * _Nonnull userID) {
        
    };
}

//TODO:扫描二维码
- (void)scanQrCode{
    
}

- (void)willPresentSearchController:(UISearchController *)searchController{
//    self.scanBtn.hidden = YES;
}

- (void)willDismissSearchController:(UISearchController *)searchController{
//    self.scanBtn.hidden = NO;
    [self.searchResultVC clearResultData];
}

- (void)didDismissSearchController:(UISearchController *)searchController{
    [self.tableHeadView bringSubviewToFront:self.scanBtn];
}

- (HHNewFriendsTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHNewFriendsTableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (HHSearchController *)searchController {
    if (!_searchController) {
        _searchController = [[HHSearchController alloc]initWithSearchResultsController:self.searchResultVC];
        _searchController.delegate = self;
        _searchController.searchBar.backgroundImage = [HHDefaultTools sharedInstance].searchBarGrayImage;
        [_searchController.searchBar setSearchFieldBackgroundImage:[HHDefaultTools sharedInstance].searchBarTextFieldImage forState:UIControlStateNormal];
        _searchController.hidesNavigationBarDuringPresentation = YES;
        _searchController.searchBar.placeholder =  @"会合号/手机号";
    }
    return _searchController;
}

- (UIButton *)scanBtn{
    if (!_scanBtn) {
        _scanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_scanBtn setImage:[UIImage imageNamed:@"scan_gray"] forState:UIControlStateNormal];
        [_scanBtn addTarget:self action:@selector(scanQrCode) forControlEvents:UIControlEventTouchUpInside];
    }
    return _scanBtn;
}

- (HHNewFiendsSearchResultViewController *)searchResultVC{
    if (!_searchResultVC) {
        _searchResultVC = [[HHNewFiendsSearchResultViewController alloc]init];
        _searchResultVC.view.backgroundColor = ColorManage.darkBackgroundColor;
    }
    return _searchResultVC;
}

- (UIView *)tableHeadView{
    if (!_tableHeadView) {
        _tableHeadView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,HHSearchControllerSearchBarHeight)];
        _tableHeadView.backgroundColor = ColorManage.grayBackgroundColor;
    }
    return _tableHeadView;
}


@end
