//
//  HHGroupChatListViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/24.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHGroupChatListViewController.h"
#import "HHGroupChatListTableView.h"
@interface HHGroupChatListViewController ()
@property (strong, nonatomic) HHGroupChatListTableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (nonatomic,strong) UIView * footerView;

@end

@implementation HHGroupChatListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"群聊";
    [self.view addSubview:self.tableView];
    [self createUI];
}


- (void)createUI{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH,20)];
    label.font = SystemFont(14);
    [label setTextAlignment:NSTextAlignmentCenter];
    label.text = [NSString stringWithFormat:@"%d个群聊",2];
    label.textColor = ColorManage.grayTextColor_Deault;
    self.footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH,80);
    [self.footerView addSubview:label];
    self.tableView.tableFooterView = self.footerView;
}

- (HHGroupChatListTableView *)tableView{
    if (!_tableView) {
        _tableView = [[HHGroupChatListTableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, SCREEN_HEIGHT -SafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (UIView *)footerView{
    if (!_footerView) {
        _footerView = [UIView new];
    }
    return _footerView;
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArray;
}
@end
