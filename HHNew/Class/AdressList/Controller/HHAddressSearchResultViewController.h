//
//  HHAddressSearchResultViewController.h
//  HHNew
//
//  Created by Liubin on 2020/4/22.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HHAddressSearchResultViewController : BaseViewController<UISearchResultsUpdating>

@property (strong, nonatomic) UINavigationController *nav;
@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) NSMutableArray *dataListArry;

- (void)clearResultData;

@end

NS_ASSUME_NONNULL_END
