//
//  HHNewFiendsSearchResultViewController.h
//  HHNew
//
//  Created by Liubin on 2020/4/23.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HHNewFiendsSearchResultViewController : BaseViewController<UISearchResultsUpdating>
@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) NSMutableArray *dataListArry;
@property (strong, nonatomic) UINavigationController *nav;

- (void)clearResultData;

@end

NS_ASSUME_NONNULL_END
