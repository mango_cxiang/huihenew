//
//  HHFriendsDetailsHeaderView.h
//  HHNew
//
//  Created by Liubin on 2020/4/26.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HHContactsModel;
NS_ASSUME_NONNULL_BEGIN
typedef void(^friendsDetailsClickSendMsgBtn)(void);
typedef void(^friendsDetailsClickGroupDetailsBtn)(void);
typedef void(^friendsDetailsClickChangeRemarkBtn)(void);

@interface HHFriendsDetailsHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *hhIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIButton *changeRemarkBtn;
- (IBAction)clickChangeRemarkBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *qmTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *qmContentLabel;


@property (weak, nonatomic) IBOutlet UIView *lineView;

@property (nonatomic , copy) friendsDetailsClickSendMsgBtn friendsDetailsClickSendMsgBtn;
@property (nonatomic , copy) friendsDetailsClickGroupDetailsBtn friendsDetailsClickGroupDetailsBtn;
@property (nonatomic , copy) friendsDetailsClickChangeRemarkBtn friendsDetailsClickChangeRemarkBtn;
@property (weak, nonatomic) IBOutlet UIView *headBgView;
@property (weak, nonatomic) IBOutlet UIView *qmBgView;


@property (nonatomic , strong) HHContactsModel *friendsModel;

@end

NS_ASSUME_NONNULL_END
