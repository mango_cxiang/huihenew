//
//  HHFriendsGroupDetailsView.m
//  HHNew
//
//  Created by Liubin on 2020/5/7.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHFriendsGroupDetailsView.h"

@implementation HHFriendsGroupDetailsView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        //        [self loadView];
        self =  [[[NSBundle mainBundle]loadNibNamed:@"HHFriendsGroupDetailsView" owner:self options:nil] lastObject];
        self.frame = frame;
    }
    return self;
}

- (IBAction)clickGroupDetailsBtn:(id)sender {
    if (self.friendsDetailsClickGroupDetailsBtn) {
        self.friendsDetailsClickGroupDetailsBtn();
    }
}


- (void)awakeFromNib{
    [super awakeFromNib];
    self.groupBgView.backgroundColor = ColorManage.darkBackgroundColor;
    self.groupTitleLabel.textColor = ColorManage.text_color;
    self.groupTitleLabel.font = kRegularFont(17);
    self.groupNameLabel.textColor = ColorManage.grayTextColor_Deault;
    self.groupNameLabel.font = kRegularFont(16);
    
}
@end
