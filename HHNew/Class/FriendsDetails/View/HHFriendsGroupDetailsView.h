//
//  HHFriendsGroupDetailsView.h
//  HHNew
//
//  Created by Liubin on 2020/5/7.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^friendsDetailsClickGroupDetailsBtn)(void);


@interface HHFriendsGroupDetailsView : UIView
@property (weak, nonatomic) IBOutlet UILabel *groupTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *groupDetailsBtn;
- (IBAction)clickGroupDetailsBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *groupBgView;

@property (nonatomic , copy) friendsDetailsClickGroupDetailsBtn friendsDetailsClickGroupDetailsBtn;

@end

NS_ASSUME_NONNULL_END
