//
//  HHFriendsSendMsgDetailsView.h
//  HHNew
//
//  Created by Liubin on 2020/5/7.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^friendsDetailsClickSendBtn)(void);

@interface HHFriendsSendMsgDetailsView : UIView
@property (weak, nonatomic) IBOutlet UIButton *sendMsgBtn;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (copy, nonatomic)  NSString *friendsStatus;
- (IBAction)clickSendBtn:(id)sender;
@property (nonatomic , copy) friendsDetailsClickSendBtn friendsDetailsClickSendBtn;

@end

NS_ASSUME_NONNULL_END
