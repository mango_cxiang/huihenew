//
//  HHFriendsDetailsHeaderView.m
//  HHNew
//
//  Created by Liubin on 2020/4/26.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHFriendsDetailsHeaderView.h"
#import "HHFriendsGroupDetailsView.h"
#import "HHFriendsSendMsgDetailsView.h"

@interface HHFriendsDetailsHeaderView ()<UIGestureRecognizerDelegate>
@property (nonatomic , strong) HHFriendsGroupDetailsView *groupDetailsView;
@property (nonatomic , strong) HHFriendsSendMsgDetailsView *sendMsgView;
@end

@implementation HHFriendsDetailsHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        //        [self loadView];
        self =  [[[NSBundle mainBundle]loadNibNamed:@"HHFriendsDetailsHeaderView" owner:self options:nil] lastObject];
        self.frame = frame;
        self.backgroundColor = ColorManage.grayBackgroundColor;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHeadImgView)];
        [self.headImgView addGestureRecognizer:tap];
        [self handleBlock];
    }
    return self;
}

- (void)setFriendsModel:(HHContactsModel *)friendsModel{
    _friendsModel = friendsModel;
    
    [self.headImgView xxd_setHeadImageView:self.headImgView imgUrl:_friendsModel.headUrl];
    self.titleNameLabel.text = _friendsModel.markName.length >0 ?_friendsModel.markName:_friendsModel.nickName;
    self.nickNameLabel.text = _friendsModel.nickName;
    self.hhIDLabel.text = _friendsModel.contactId;
    self.addressLabel.text = [NSString stringWithFormat:@"地区：%@ %@",_friendsModel.province,_friendsModel.city];
    self.qmContentLabel.text = _friendsModel.sign;
    self.groupDetailsView.groupNameLabel.text = _friendsModel.groupName;
    self.sendMsgView.friendsStatus = _friendsModel.status;
    [self creatUI];
}

- (void)creatUI{
    if([_friendsModel.status intValue] > 0){
        [self addSubview:self.groupDetailsView];
        [self.groupDetailsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.qmBgView.mas_bottom).offset(10);
            make.left.right.mas_equalTo(self);
            make.height.mas_equalTo(55);
        }];
    }
    [self addSubview:self.sendMsgView];
    [self.sendMsgView mas_makeConstraints:^(MASConstraintMaker *make) {
        if([_friendsModel.status intValue] > 0){
            make.top.mas_equalTo(self.groupDetailsView.mas_bottom).offset(10);
        }
        else{
            make.top.mas_equalTo(self.qmBgView.mas_bottom).offset(10);
        }
        make.left.right.mas_equalTo(self);
        make.height.mas_equalTo(55);
    }];
}

- (void)handleBlock{
    WeakSelf(self);
    self.groupDetailsView.friendsDetailsClickGroupDetailsBtn = ^{
        StrongSelf(weakself)
        if (strongweakself.friendsDetailsClickGroupDetailsBtn) {
            strongweakself.friendsDetailsClickGroupDetailsBtn();
        }
    };
    self.sendMsgView.friendsDetailsClickSendBtn = ^{
        StrongSelf(weakself)
        if (strongweakself.friendsDetailsClickSendMsgBtn) {
            strongweakself.friendsDetailsClickSendMsgBtn();
        }
    };
}

- (void)tapHeadImgView{
    NSMutableArray *datas = [NSMutableArray arrayWithCapacity:0];
    YBIBImageData *data = [YBIBImageData new];
    data.imageURL = [NSURL URLWithString:_friendsModel.headUrl];
    [datas addObject:data];
     
    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = datas;
    [browser show];
}


- (void)awakeFromNib{
    [super awakeFromNib];
    self.headBgView.backgroundColor = ColorManage.darkBackgroundColor;
    self.qmBgView.backgroundColor = ColorManage.darkBackgroundColor;
    
    self.headImgView.layer.masksToBounds = YES;
    self.headImgView.layer.cornerRadius = 5;
    
    [self.changeRemarkBtn setTitleColor:ColorManage.loginTextColor forState:UIControlStateNormal];
    self.changeRemarkBtn.titleLabel.font = kRegularFont(14);
    
    self.titleNameLabel.textColor = ColorManage.text_color;
    self.titleNameLabel.font = kSemiboldsFont(20);

    self.nickNameLabel.textColor = ColorManage.grayTextColor_Deault;
    self.nickNameLabel.font = kRegularFont(12);
    self.hhIDLabel.textColor = ColorManage.grayTextColor_Deault;
    self.hhIDLabel.font = kRegularFont(12);
    self.addressLabel.textColor = ColorManage.grayTextColor_Deault;
    self.addressLabel.font = kRegularFont(12);
    
    self.qmTitleLabel.textColor = ColorManage.text_color;
    self.qmTitleLabel.font = kRegularFont(17);
    self.qmContentLabel.textColor = ColorManage.grayTextColor_Deault;
    self.qmContentLabel.font = kRegularFont(16);
    
    [self.changeRemarkBtn setTitleColor:ColorManage.loginTextColor forState:UIControlStateNormal];
    self.changeRemarkBtn.titleLabel.font = kRegularFont(14);
    self.lineView.backgroundColor = ColorManage.lineViewColor;
}


//修改备注名称
- (IBAction)clickChangeRemarkBtn:(id)sender {
    if (self.friendsDetailsClickChangeRemarkBtn) {
        self.friendsDetailsClickChangeRemarkBtn();
    }
}

- (HHFriendsGroupDetailsView *)groupDetailsView{
    if (!_groupDetailsView) {
        _groupDetailsView = [[HHFriendsGroupDetailsView alloc] init];
    }
    return _groupDetailsView;
}

- (HHFriendsSendMsgDetailsView *)sendMsgView{
    if (!_sendMsgView) {
        _sendMsgView = [[HHFriendsSendMsgDetailsView alloc] init];
    }
    return _sendMsgView;
}

@end
