//
//  HHFriendsSendMsgDetailsView.m
//  HHNew
//
//  Created by Liubin on 2020/5/7.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHFriendsSendMsgDetailsView.h"

@implementation HHFriendsSendMsgDetailsView
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        //        [self loadView];
        self =  [[[NSBundle mainBundle]loadNibNamed:@"HHFriendsSendMsgDetailsView" owner:self options:nil] lastObject];
        self.frame = frame;
    }
    return self;
}

- (void)setFriendsStatus:(NSString *)friendsStatus{
    _friendsStatus = friendsStatus;
    //是好友
    if ([friendsStatus integerValue] > 0) {
        [self.sendMsgBtn setImage:[UIImage imageNamed:@"send_msg"] forState:UIControlStateNormal];
        [self.sendMsgBtn setTitle:@" 发送消息" forState:UIControlStateNormal];
    }
    else{
        [self.sendMsgBtn setImage:nil forState:UIControlStateNormal];
        [self.sendMsgBtn setTitle:@"添加到通讯录" forState:UIControlStateNormal];
    }
}



- (void)awakeFromNib{
    [super awakeFromNib];
    self.bgView.backgroundColor = ColorManage.darkBackgroundColor;
        
    [self.sendMsgBtn setTitleColor:ColorManage.loginTextColor forState:UIControlStateNormal];
    self.sendMsgBtn.titleLabel.font = kMediumFont(17);
    
}

- (IBAction)clickSendBtn:(id)sender {
    if (self.friendsDetailsClickSendBtn) {
        self.friendsDetailsClickSendBtn();
    }

}
@end
