//
//  HHChangeFriendsRemarkViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/26.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHChangeFriendsRemarkViewController.h"

@interface HHChangeFriendsRemarkViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) UITextField *textField;

@end

@implementation HHChangeFriendsRemarkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置备注";
    self.view.backgroundColor = ColorManage.addressHeaderViewTextColor;
    [self createUI];
}

- (void)createUI {
    UIButton * rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    [rightBtn setTitle:@"确定" forState:UIControlStateNormal];
    [rightBtn setTitleColor:ColorManage.text_color forState:UIControlStateNormal];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [rightBtn addTarget:self action:@selector(rightBtnAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    UILabel *name = [[UILabel alloc] init];
    name.text = @"备注名";
    name.frame = CGRectMake(15, 10 + SafeAreaTopHeight, SCREEN_WIDTH, 20);
    name.textColor = ColorManage.grayTextColor_Deault;
    name.font = kRegularFont(14);
    [self.view addSubview:name];
    
    UIView *whiteView=[[UIView alloc] initWithFrame:CGRectMake(0, name.bottom + 10, SCREEN_WIDTH, 55)];
    whiteView.backgroundColor = ColorManage.background_color;
    [self.view addSubview:whiteView];
    
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(20, 0, SCREEN_WIDTH-20, 55)];
    _textField.returnKeyType = UIReturnKeyDone;
    _textField.delegate = self;
    _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _textField.tintColor = ColorManage.greenTextColor;
    _textField.font = SystemFont(16);
    _textField.placeholder= @"请输入备注";
    [_textField becomeFirstResponder];
    _textField.text = self.name;
    [whiteView addSubview:_textField];
}

- (void)rightBtnAction{
    [self.navigationController popViewControllerAnimated:YES];

    if (_textField.text.length <= 0 || [_textField.text isEqualToString:self.name]) {
        return;
    }
    if (self.changeFriendsRemarkName) {
        self.changeFriendsRemarkName(_textField.text);
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self rightBtnAction];
    return YES;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.textField resignFirstResponder];
}
@end
