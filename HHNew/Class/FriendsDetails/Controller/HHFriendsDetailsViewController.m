//
//  HHFriendsDetailsViewController.m
//  HHNew
//
//  Created by Liubin on 2020/4/26.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "HHFriendsDetailsViewController.h"
#import "HHFriendsDetailsHeaderView.h"
#import "HHAddGroupViewController.h"
#import "HHChangeFriendsRemarkViewController.h"
#import "HHContactsModel.h"
@interface HHFriendsDetailsViewController ()

@property (nonatomic , strong) HHFriendsDetailsHeaderView * detailsView;

@end

@implementation HHFriendsDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = ColorManage.grayBackgroundColor;
    [self.view addSubview:self.detailsView];
    [self createUI];
    [self loadData];
    [self handleBlock];
}

- (void)loadData{
    HHContactsModel *model = [[HHContactsModel alloc] init];
    model.headUrl = @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1587891788640&di=df206b78bfd8874f4adce7c04fbb1475&imgtype=0&src=http%3A%2F%2Fwx3.sinaimg.cn%2Fmw690%2Fe732ef12ly1gdqzzj4d7cj20m80m8tbm.jpg";
    model.markName = @"哈哈哈";
    model.nickName = @"嘿嘿嘿";
    model.contactId = @"1234567";
    model.province = @"安徽";
    model.city = @"合肥";
    model.sign = @"我就是我，颜色不一样的烟火";
    model.groupName = @"无所不能";
    model.status = @"1";
    self.detailsView.friendsModel = model;
}

- (void)createUI{
    UIButton * rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    [rightBtn setImage:[UIImage imageNamed:@"more"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightBtnAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightButton;
}

- (void)rightBtnAction{
    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:0];
    [arr addObject:@"发送该名片"];
    [arr addObject:@"删除好友"];
    [arr addObject:@"投诉"];

    ZGQActionSheetView *sheetView = [[ZGQActionSheetView alloc] initWithOptions:arr completion:^(NSInteger index) {
        switch (index) {
            case 0:
                {
                    
                }
                break;
            case 1:
                {
                
                }
                break;
            case 2:
                {
                
                }
                break;
            default:
                break;
            }
        } cancel:^{
            
        }];
        
    [sheetView show];
}

- (void)handleBlock{
    WeakSelf(self);
    //修改备注
    self.detailsView.friendsDetailsClickChangeRemarkBtn = ^{
        HHChangeFriendsRemarkViewController *rvc = Init(HHChangeFriendsRemarkViewController);
        rvc.name = @"嘿嘿";
        rvc.changeFriendsRemarkName = ^(NSString * _Nonnull nameStr) {
            weakself.detailsView.titleNameLabel.text = nameStr;
        };
        [weakself.navigationController pushViewController:rvc animated:YES];

    };
    //设置分组
    self.detailsView.friendsDetailsClickGroupDetailsBtn  = ^{
        HHAddGroupViewController *gvc = Init(HHAddGroupViewController);
        gvc.groupID = @"1";
        [weakself.navigationController pushViewController:gvc animated:YES];
    };
    //发消息，或者添加通讯录
    self.detailsView.friendsDetailsClickSendMsgBtn = ^{
        
    };
}


- (HHFriendsDetailsHeaderView *)detailsView{
    if (!_detailsView) {
        _detailsView = [[HHFriendsDetailsHeaderView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, SCREEN_WIDTH, 325)];
    }
    return _detailsView;
}

@end
