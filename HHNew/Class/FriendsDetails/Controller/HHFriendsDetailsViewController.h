//
//  HHFriendsDetailsViewController.h
//  HHNew
//
//  Created by Liubin on 2020/4/26.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@class HHContactsModel;
NS_ASSUME_NONNULL_BEGIN

@interface HHFriendsDetailsViewController : BaseViewController
@property (nonatomic , strong) NSString *groupId;

@end

NS_ASSUME_NONNULL_END
