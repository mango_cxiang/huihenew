//
//  HHChangeFriendsRemarkViewController.h
//  HHNew
//
//  Created by Liubin on 2020/4/26.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^changeFriendsRemarkName)(NSString *nameStr);
@interface HHChangeFriendsRemarkViewController : BaseViewController

@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) changeFriendsRemarkName changeFriendsRemarkName;

@end

NS_ASSUME_NONNULL_END
