//
//  UIButton (UIButtonExt).h
//  EHSYBeta
//
//  Created by 李胜书 on 16/2/19.
//  Copyright © 2016年 EHSY_SanLi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ButtonLayoutType){
    initLayout,
    afterAnimationLayout
};

@interface UIButton (Ext)

- (void)centerImageAndTitle:(float)space;
- (void)centerImageAndTitle;
- (void)centerImageAndTitleWithLayoutType:(ButtonLayoutType)layoutType;

- (void)changeButtonImageAndLabel;
/**
 *  创建一个按钮
 *
 *  @param normalImageName      默认图片名
 *  @param highlightedImageName 高亮时 图片名
 *  @param selectedImageName    选中图片名
 *
 *  @return UIButton
 */
+ (UIButton *)createButtonWithNormalImageName:(NSString *)normalImageName highlightedImageName:(NSString *)highlightedImageName selectedImageName:(NSString *)selectedImageName;
@end
