//
//  UIButton+TimeInterval.h
//  FYJD
//
//  Created by feeyo on 2019/12/20.
//  Copyright © 2019 feeyo. All rights reserved.
//



NS_ASSUME_NONNULL_BEGIN

@interface UIButton (TimeInterval)
/**
 点击时间间隔
 */
@property (nonatomic, assign) NSTimeInterval timeInterval;
@end

NS_ASSUME_NONNULL_END
