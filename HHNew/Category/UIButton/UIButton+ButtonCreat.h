//
//  UIButton+ButtonCreat.h
//  huihe
//
//  Created by changle on 2016/12/13.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (ButtonCreat)

//设置圆角
- (UIButton *)creatButtonWithfillet:(CGFloat)fillet;

//设置圆角 边框粗细和颜色
- (UIButton *)creatButtonWithfillet:(CGFloat)fillet borderColor:(UIColor *)color borderWidth:(CGFloat)borderWidth;

//创建一个带title的普通按钮
- (UIButton *)creatButtonWithTitle:(NSString *)title;

//创建一个按钮 同时设置标题，背景
- (UIButton *)creatButtonWithTitle:(NSString *)title backgroundColor:(UIColor *)bkgColor tintColor:(UIColor *)tintColor titleFont:(UIFont *)titleFont;

//定时器开始倒计时定时
- (void)timeFailBeginFrom:(NSInteger)timeCount;

//上下居中，图片在上，文字在下
- (void)verticalCenterImageAndTitle:(CGFloat)spacing;
- (void)verticalCenterImageAndTitle; //默认6.0

//左右居中，文字在左，图片在右
- (void)horizontalCenterTitleAndImage:(CGFloat)spacing;
- (void)horizontalCenterTitleAndImage; //默认6.0

//左右居中，图片在左，文字在右
- (void)horizontalCenterImageAndTitle:(CGFloat)spacing;
- (void)horizontalCenterImageAndTitle; //默认6.0

//文字居中，图片在左边
- (void)horizontalCenterTitleAndImageLeft:(CGFloat)spacing;
- (void)horizontalCenterTitleAndImageLeft; //默认6.0

//文字居中，图片在右边
- (void)horizontalCenterTitleAndImageRight:(CGFloat)spacing;
- (void)horizontalCenterTitleAndImageRight; //默认6.0
@end
