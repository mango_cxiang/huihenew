//
//  UILabel+HHTextSpace.h
//  HHNew
//
//  Created by Liubin on 2020/4/21.
//  Copyright © 2020 储翔. All rights reserved.
//



#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (HHTextSpace)

- (void)changeSpaceWithLineSpace:(float)lineSpace WordSpace:(float)wordSpace ;
@property (nonatomic, assign) BOOL isTop;
@property (nonatomic, assign) BOOL isBottom;
@end

NS_ASSUME_NONNULL_END
