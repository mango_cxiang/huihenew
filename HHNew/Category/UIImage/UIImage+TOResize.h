//
//  UIImage+TOResize.h
//  huihe
//
//  Created by sam on 17/1/10.
//  Copyright © 2017年 Six. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (TOResize)
//按形状切割图像
- (UIImage*)cutImageWithRadius:(int)radius;
+ (UIImage *)rotateImage:(UIImage *)aImage ;
@end
