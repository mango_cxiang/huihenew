//
//  UITableViewCell+Utility.m
//  KKNovel
//
//  Created by fengzi on 2018/3/27.
//  Copyright © 2018年 Chongzi. All rights reserved.
//

#import "UITableViewCell+Utility.h"

@implementation UITableViewCell (Utility)

+ (UINib *)nib {
    return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:[NSBundle mainBundle]];
}

+ (instancetype)instanceFromNib {
    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] lastObject];
}

+ (NSString *)reuseIdentifier {
    return [self className];
}

+ (NSString *)className {
    return [NSString stringWithUTF8String:object_getClassName(self)];
}

@end
