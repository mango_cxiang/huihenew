//
//  UITableViewCell+Utility.h
//  KKNovel
//
//  Created by fengzi on 2018/3/27.
//  Copyright © 2018年 Chongzi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (Utility)

+ (UINib *)nib;
+ (instancetype)instanceFromNib;
+ (NSString *)reuseIdentifier;
+ (NSString *)className;

@end
