//
//  NSMutableAttributedString+Emoji.m
//  huihe
//
//  Created by Six on 16/3/7.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "NSMutableAttributedString+Emoji.h"
#import "HMJEmojiTextAttachment.h"
#import "HMJEmojiFaceView.h"

@implementation NSMutableAttributedString (Emoji)

+ (instancetype)returnEmojiStrWithText:(NSString *)text
{
    if (!text)
    {
        return nil;
    }
    
    static dispatch_once_t onceT;
    static NSRegularExpression * regularExpression = nil;
    dispatch_once(&onceT, ^{
        NSString * pattern = @"\\[\\:(\\w+)\\]";
        regularExpression = [[NSRegularExpression alloc] initWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:NULL];
    });
    
    NSMutableAttributedString * attributedStr = [[NSMutableAttributedString alloc] initWithString:text];
    NSRange matchingRange = NSMakeRange(0, [attributedStr length]);
    __block NSInteger length = 0;
    [regularExpression enumerateMatchesInString:text options:NSMatchingReportCompletion range:matchingRange usingBlock:^(NSTextCheckingResult * result, NSMatchingFlags flags, BOOL * stop) {
        if (result && result.resultType == NSTextCheckingTypeRegularExpression && !(flags & NSMatchingInternalError))
        {
            NSRange range = result.range;
            if (range.location != NSNotFound)
            {
                HMJEmojiTextAttachment * emojiTextAttachment = [HMJEmojiTextAttachment new];
                emojiTextAttachment.descender = SystemFont(16).descender;
                NSString * faceStr = [attributedStr.string substringWithRange:NSMakeRange(range.location - length, range.length)];
                emojiTextAttachment.faceStr = faceStr;
                NSString * imageName = [[HMJEmojiFaceView localEmojiDict] objectForKey:faceStr];
                emojiTextAttachment.image = [UIImage imageNamed:imageName];
                [attributedStr replaceCharactersInRange:NSMakeRange(range.location - length, range.length) withAttributedString:[NSAttributedString attributedStringWithAttachment:emojiTextAttachment]];
                length += range.length - 1;
            }
        }
    }];
    
    [attributedStr beginEditing];
    NSMutableParagraphStyle * paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    [attributedStr addAttributes:@{NSParagraphStyleAttributeName : paragraphStyle , NSFontAttributeName : SystemFont(16)} range:NSMakeRange(0, [attributedStr length])];
    [attributedStr endEditing];
    return attributedStr;
}
+ (instancetype)returnKeepingStrWithText:(NSString *)text Font:(CGFloat)font{
    
    if (!text)
    {
        return nil;
    }
    static dispatch_once_t onceT;
    static NSRegularExpression * regularExpression = nil;
    dispatch_once(&onceT, ^{
        NSString * pattern = @"\\[\\:(\\w+)\\]";
        regularExpression = [[NSRegularExpression alloc] initWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:NULL];
    });
    
    NSMutableAttributedString * attributedStr = [[NSMutableAttributedString alloc] initWithString:text];
    NSRange matchingRange = NSMakeRange(0, [attributedStr length]);
    __block NSInteger length = 0;
    [regularExpression enumerateMatchesInString:text options:NSMatchingReportCompletion range:matchingRange usingBlock:^(NSTextCheckingResult * result, NSMatchingFlags flags, BOOL * stop) {
        if (result && result.resultType == NSTextCheckingTypeRegularExpression && !(flags & NSMatchingInternalError))
        {
            NSRange range = result.range;
            if (range.location != NSNotFound)
            {
                HMJEmojiTextAttachment * emojiTextAttachment = [HMJEmojiTextAttachment new];
                emojiTextAttachment.descender = SystemFont(font).descender;
                NSString * faceStr = [attributedStr.string substringWithRange:NSMakeRange(range.location - length, range.length)];
                emojiTextAttachment.faceStr = faceStr;
                NSString * imageName = [[HMJEmojiFaceView localEmojiDict] objectForKey:faceStr];
                emojiTextAttachment.image = [UIImage imageNamed:imageName];
                [attributedStr replaceCharactersInRange:NSMakeRange(range.location - length, range.length) withAttributedString:[NSAttributedString attributedStringWithAttachment:emojiTextAttachment]];
                length += range.length - 1;
            }
        }
    }];
    
    [attributedStr beginEditing];
    NSMutableParagraphStyle * paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    [attributedStr addAttributes:@{NSParagraphStyleAttributeName : paragraphStyle , NSFontAttributeName : SystemFont(font)} range:NSMakeRange(0, [attributedStr length])];
    [attributedStr endEditing];
    return attributedStr;
}

- (NSString *)getPlainStr
{
    NSMutableString * mStr = [NSMutableString stringWithString:self.string];
    __block NSUInteger length = 0;
    [self enumerateAttribute:NSAttachmentAttributeName inRange:NSMakeRange(0, self.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        if (value && [value isKindOfClass:[HMJEmojiTextAttachment class]])
        {
            HMJEmojiTextAttachment * emojiTextAttachment = (HMJEmojiTextAttachment *)value;
            [mStr replaceCharactersInRange:NSMakeRange(range.location + length, range.length) withString:emojiTextAttachment.faceStr];
            length += emojiTextAttachment.faceStr.length - 1;
        }
    }];
    return mStr;
}
@end
