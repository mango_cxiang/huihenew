//
//  NSString+Format.h
//  huihe
//
//  Created by Six on 16/1/12.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <Foundation/Foundation.h>

//判断格式是否有误
@interface NSString (Format)

//验证手机
+ (BOOL)verificationIsValidWithPhoneNumberStr:(NSString *)phoneNumber_str;

//验证邮箱
+ (BOOL)verificationIsValidWithEmailStr:(NSString *)email_str;

//验证密码是否为字母和数字结合
+ (BOOL)verificationIsValidWithPWDStr:(NSString *)pwd_str;

//判空
+ (BOOL)verificationIsValidWithStr:(NSString *)str;
//字符值判零
+ (BOOL)verificationIsValidWithZeroStr:(NSString *)zero_Str;

//判断文本信息是不是链接
+ (BOOL)isUrlAddress:(NSString *)url;

//判断文本信息是不是链接
+ (NSArray *)getWebsitesWithString:(NSString *)string;

- (NSString *)iPhoneStandardFormat;

//判断身份证
//+ (BOOL)validateIdentityCard: (NSString *)identityCard;


+ (BOOL)judgeIdentityStringValid:(NSString *)identityString;
@end
