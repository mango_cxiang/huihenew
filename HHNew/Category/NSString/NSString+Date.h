//
//  NSString+Date.h
//  huihe
//
//  Created by Six on 16/4/11.
//  Copyright © 2016年 Six. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Date)

/**
 *  根据 毫秒数 返回 时间字符串
 *
 *  @param time 时间（毫秒数）
 *
 *  @return 时间字符串
 */
+ (NSString *)returnDateStrWithTime:(NSTimeInterval)time;

/**
 *  根据 毫秒数 返回 时间天
 *
 *  @param time 时间（毫秒数）
 *
 *  @return 时间字符串
 */
+ (NSString *)returnDateTimeWithTime:(NSTimeInterval)time;
//当前时间戳

//计算视频总秒数
+ (NSString *)returnVideoTime:(NSString *)totalSeconds;

/**
 *  是否为同一天
 */
+ (BOOL)isSameDay:(NSDate*)date1 date2:(NSDate*)date2;

//将某个时间戳转化成 时间
#pragma mark - 将某个时间戳转化成 时间
+(NSString *)timestampSwitchTime:(NSInteger)timestamp andFormatter:(NSString *)format;

@end
