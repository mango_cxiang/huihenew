//
//  NSString+md5.h
//  HHNew
//
//  Created by 张 on 2020/4/30.
//  Copyright © 2020 储翔. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (md5)

//md5 加密
- (NSString *)md5;
#pragma mark - MD5加密 32位 大写
- (NSString *)MD5ForUpper32Bate;

@end

NS_ASSUME_NONNULL_END
