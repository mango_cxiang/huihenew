//
//  NSString+Date.m
//  huihe
//
//  Created by Six on 16/4/11.
//  Copyright © 2016年 Six. All rights reserved.
//

#import "NSString+Date.h"

@implementation NSString (Date)

/**
 *  根据 毫秒数 返回 时间字符串
 *
 *  @param time 时间（毫秒数）
 *
 *  @return 时间字符串
 */
+ (NSString *)returnDateStrWithTime:(NSTimeInterval)time
{
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:time / 1000];
    NSCalendarUnit unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekdayOrdinal | NSCalendarUnitWeekday | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute;
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents * messageDateComponents = [calendar components:unitFlags fromDate:date];
    NSDateComponents * todayDateComponents = [calendar components:unitFlags fromDate:[NSDate date]];
    
    NSUInteger dayOfYearForMessage = [calendar ordinalityOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitYear forDate:date];
    NSUInteger dayOfYearForToday = [calendar ordinalityOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitYear forDate:[NSDate date]];

    NSString * dateStr = @"";
    if (messageDateComponents.year == todayDateComponents.year && messageDateComponents.month == todayDateComponents.month && messageDateComponents.day == todayDateComponents.day)
    {
        dateStr = [NSString stringWithFormat:@"%02zi:%02zi" , messageDateComponents.hour , messageDateComponents.minute];
    }
    else if (messageDateComponents.year == todayDateComponents.year && dayOfYearForMessage == (dayOfYearForToday - 1))
    {
        dateStr = [NSString stringWithFormat:@"%@ %02zi:%02zi" ,@"昨天", messageDateComponents.hour , messageDateComponents.minute];
    }
    else
    {
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.timeZone = [NSTimeZone localTimeZone];
        dateFormatter.dateFormat = @"YYYY年MM月dd日";
        dateStr = [dateFormatter stringFromDate:date];
        dateStr = [NSString stringWithFormat:@"%@ %02zi:%02zi", dateStr,  messageDateComponents.hour , messageDateComponents.minute ];
    }
    return dateStr;
}

+ (NSString *)returnDateTimeWithTime:(NSTimeInterval)time
{
    if (time > 0) {
        return  [NSString stringWithFormat:@"%.0f",time/24.0/60.0/60.0];
    }
    return @"0";
}

//计算视频总秒数
+ (NSString *)returnVideoTime:(NSString *)totalSeconds
{
    int total = [totalSeconds intValue] /1000;
    int seconds = total % 60;
    int minutes = (total / 60) % 60;
    return [NSString stringWithFormat:@"%02d:%02d",minutes, seconds];
}

#pragma mark - 是否为同一天
/**
 *  是否为同一天
 */
+ (BOOL)isSameDay:(NSDate*)date1 date2:(NSDate*)date2
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

//将某个时间戳转化成 时间
#pragma mark - 将某个时间戳转化成 时间
+(NSString *)timestampSwitchTime:(NSInteger)timestamp andFormatter:(NSString *)format{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setDateFormat:format]; // （@"YYYY-MM-dd hh:mm:ss"）----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
//        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
//        [formatter setTimeZone:timeZone];
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:timestamp];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    
    return confromTimespStr;
}
@end
