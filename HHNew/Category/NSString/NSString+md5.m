//
//  NSString+md5.m
//  HHNew
//
//  Created by 张 on 2020/4/30.
//  Copyright © 2020 储翔. All rights reserved.
//

#import "NSString+md5.h"
#import <CommonCrypto/CommonDigest.h>

//16位加密
#define BitNum 16
@implementation NSString (md5)

//md5 加密
- (NSString *)md5
{
    const char * cStr = [self UTF8String];
    unsigned char result[BitNum];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    NSMutableString * mStr = [NSMutableString stringWithCapacity:BitNum];
    for (NSInteger i = 0; i < BitNum; i++)
    {
        [mStr appendFormat:@"%02X" , result[i]];
    }
    return mStr.lowercaseString;
}

- (NSString *)MD5ForUpper32Bate{
    
    //要进行UTF8的转码
    const char* input = [self UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(input, (CC_LONG)strlen(input), result);
    
    NSMutableString *digest = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (NSInteger i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [digest appendFormat:@"%02X", result[i]];
    }
    
    return digest;
}

@end
